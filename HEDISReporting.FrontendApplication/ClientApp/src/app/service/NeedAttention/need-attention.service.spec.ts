import { TestBed } from '@angular/core/testing';

import { NeedAttentionService } from './need-attention.service';

describe('NeedAttentionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NeedAttentionService = TestBed.get(NeedAttentionService);
    expect(service).toBeTruthy();
  });
});
