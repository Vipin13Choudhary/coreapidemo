import { Injectable } from '@angular/core';
import { HttpClient , HttpErrorResponse } from '@angular/common/http'
import { Router } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs';
import { GLOBAL } from '../../shared/global';
import { NeedAttentionFilter } from '../../model/NeedAttention/NeedAttention';

@Injectable({
  providedIn: 'root'
})
export class NeedAttentionService {
  private messageSource = new BehaviorSubject('default message');
  currentMessage = this.messageSource.asObservable();

  userId: string;
  constructor(private http: HttpClient,private router: Router) {
    this.userId = localStorage.getItem('LoginUserID');

   }
   GetNeedAttention(commonFilter:NeedAttentionFilter):Observable<any>{
    debugger
    const url= GLOBAL.NeedAttentionAtrribution;
    return this.http.post(url,commonFilter)
    .catch(this.handleError);
  }

  changeMessage(message: any) {
    this.messageSource.next(message)
  } 

  UpdateNeedAttention(patientstatus:any):Observable<any>{
    const url= GLOBAL.UpdateNeedAttentionAtrribution;
    return this.http.post(url,patientstatus)
    .catch(this.handleError);
  }

  UpdateCareGaps(patientstatus:any):Observable<any>{
    const url= GLOBAL.UpdateCareGapAtrribution;
    return this.http.post(url,patientstatus)
    .catch(this.handleError);
  }

  UpdateNeedAttentiondata(patientstatus:any):Observable<any>{
    const url= GLOBAL.UpdateNeedAttentionAtrributiondata;
    return this.http.post(url,patientstatus)
    .catch(this.handleError);
  }
  

  private handleError(error: HttpErrorResponse):
  Observable<any> {
    console.error('observable error: ', error);
    return Observable.throw(error);
}
}
