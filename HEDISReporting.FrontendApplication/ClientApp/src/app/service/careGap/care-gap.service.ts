import { Injectable } from '@angular/core';
// import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { HttpClient, HttpHeaders,HttpRequest, HttpErrorResponse } from '@angular/common/http'
import {GLOBAL} from '../../shared/global';
import 'rxjs/Rx';
import { Router } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs/Rx';
import { ResponseType } from '@angular/http';
import { CommonFilters } from 'src/app/model/dashboard/dashboard';

@Injectable({
  providedIn: 'root'
})
export class CareGapService {
  private messageSource = new BehaviorSubject('default message');
  currentMessage = this.messageSource.asObservable();
  userName: string;
  userId: string;
  authToken: string;
  constructor(private http: HttpClient,private router: Router) { 
    this.userId = localStorage.getItem('LoginUserID');
    this.userName=localStorage.getItem('LoginUserName');
    this.authToken=  localStorage.getItem('authenticationtoken');
  }
  GetABACareGap2018(listOperand:CommonFilters):Observable<any>{
    debugger
    const url= GLOBAL.GetABACareGap2018;
    return this.http.post(url, listOperand)
    .catch(this.handleError);
  }
  changeMessage(message: string) {
    this.messageSource.next(message)
  }

  GetImunizationGap(listFilter:CommonFilters):Observable<any> {
    return this.http.post(GLOBAL.GetImunization2018,listFilter)
   .catch(this.handleError);
  }
  private handleError(error: HttpErrorResponse):
      Observable<any> {
        console.error('observable error: ', error);
        return Observable.throw(error);
    }
}
