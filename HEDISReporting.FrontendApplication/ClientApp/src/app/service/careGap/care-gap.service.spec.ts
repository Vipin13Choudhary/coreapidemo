import { TestBed } from '@angular/core/testing';

import { CareGapService } from './care-gap.service';

describe('CareGapService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CareGapService = TestBed.get(CareGapService);
    expect(service).toBeTruthy();
  });
});
