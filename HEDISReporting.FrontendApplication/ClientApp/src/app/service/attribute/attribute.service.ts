import { Injectable } from '@angular/core';
import { HttpClient , HttpErrorResponse } from '@angular/common/http'
import {GLOBAL} from '../../shared/global';
import 'rxjs/Rx';
import { Observable, BehaviorSubject } from 'rxjs/Rx';
import { CommonFilters } from 'src/app/model/dashboard/dashboard';

@Injectable({
  providedIn: 'root'
})
export class AttributeService {
  private messageSource = new BehaviorSubject('default message');
  currentMessage = this.messageSource.asObservable();
  constructor(private http: HttpClient) { }
  UploadExcelFile(formData:any):Observable<any>{
    debugger
    const url= GLOBAL.UploadExcelFile;
    return this.http.post(url, formData)
    .catch(this.handleError);
  }

  changeMessage(message: string) {
    this.messageSource.next(message)
  } 
  organizationPayer():Observable<any>{
    debugger
    const url= GLOBAL.getOrganizationPayer;
    return this.http.get(url)
    .catch(this.handleError);
  }

 GetAttributionPatientList(filters:CommonFilters):Observable<any>{
    const url=GLOBAL.GetAttributionPatientDetails;
    return this.http.post(url,filters);
  }

  getGapPatientList(filters:CommonFilters):Observable<any>{
    const url=GLOBAL.CaregapPatientList;
    return this.http.post(url,filters);
  }

  private handleError(error: HttpErrorResponse):
      Observable<any> {
        console.error('observable error: ', error);
        return Observable.throw(error);
    }
 
}
