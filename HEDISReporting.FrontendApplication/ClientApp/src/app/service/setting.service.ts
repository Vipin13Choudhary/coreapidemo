import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class SettingService {

  constructor() { }


  public getBaseUrl(): string {
    return environment.baseUrl;
  }
  // public getImageUrl(): string {
  //   return environment.imageurl;
  // }
}
