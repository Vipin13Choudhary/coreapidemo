import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import {GLOBAL} from '../../shared/global';
import 'rxjs/Rx';
import { HttpErrorResponse  } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { listOperation } from '../../model/common/common';
import {orgDataSourceModal} from '../../model/organization/orgDataSourceModal';
import {organizationClinic} from '../../model/organization/OrganizationClinic';
import { Organization } from '../../model/Organization';
import { department } from '../../model/organization/orgDepartment';



@Injectable({
  providedIn: 'root'
})
export class OrganizationsService {
  options = new RequestOptions({ headers: new Headers({ 'Content-Type': 'application/json' }) });
  constructor(private http: Http) { }

  getOrganizationList(listOperand:listOperation){
    debugger;
    const url= GLOBAL.getOrganizationList;
    return this.http.get(url, {
        body: listOperand,
        method: 'POST', headers: new Headers({
            'Content-Type': 'application/json'
        })
    })
    .map((response: Response) => {
        const loginResponse = response.json();
        return loginResponse;
    });
  }

  
//Get IP Adress using http://freegeoip.net/json/?callback
getIpAddress() {
  return this.http
        .get('https://jsonip.com/')
        .map(response => response || {})
        .catch(this.handleError);
       
}





private handleError(error: HttpErrorResponse):
      Observable<any> {
        //Log error in the browser console
        console.error('observable error: ', error);
        return Observable.throw(error);
    }

getCountryCodeFromIP(IPAddress){
    return this.http.get(GLOBAL.GetCountryCodeFromIP,{params: {IPAddress: IPAddress}})
    .map((response:Response)=>{
      return response.json();
    });
    }


   getAllDropDown(userId,OrganizationId){
    let obj:any={};
    obj.UserId=userId;
    obj.OrganizationId=OrganizationId;
    const url= GLOBAL.getOrgDropDown;
    return this.http.get(url, {
        body: obj,
        method: 'POST', headers: new Headers({
            'Content-Type': 'application/json'
        })
    })
    .map((response: Response) => {
        const loginResponse = response.json();
        return loginResponse;
    });
  }

  GetAllEntity(TypeId:number,LoginOrganizationId:number){
    
    const url= GLOBAL.GetAllEntity;
    return this.http.get(url, {params: {TypeId: TypeId.toString(),OrganizationId:LoginOrganizationId}})
    .map((response: Response) => {
        const loginResponse = response.json();
        return loginResponse;
    });
  }
  //To save organization Configuration
  saveOrganizationconfiguration(sftModal:orgDataSourceModal)
  {
    return this.http.post(GLOBAL.OrganizationConfigration, sftModal, this.options)
    .map((response:Response)=>{
      return response.json();
    })
              
  }

  saveOrganization(orgModal:Organization)
  {
    return this.http.post(GLOBAL.OrganizationSave, JSON.stringify(orgModal), this.options)
    .map((response:Response)=>{
      return response.json();
    })
              
  }
  saveOrganizationClinic(organizationClinic:organizationClinic)
  {
    return this.http.post(GLOBAL.SaveOrgnizationClinic, JSON.stringify(organizationClinic), this.options)
    .map((response:Response)=>{
      return response.json();
    })
  }

  saveOrganizationDepartment(departmentModal:department){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(GLOBAL.DepartmentSave, JSON.stringify(departmentModal), options)
    .map((response:Response)=>{
      return response.json();
    })
  }
  GetCountriesStates(countryId) {
    return this.http.get(GLOBAL.GetCountriesStates, {params: {countryId: countryId}})
    .map((response: Response) => {
    const result = response.json();
    return result;
    });
  }
  GetOrganizationConfigList(filters:{})
  {
    return this.http.post(GLOBAL.GetOrganizationConfigurationList,  JSON.stringify(filters), this.options)
    .map((response:Response)=>{
      return response.json();
    })
  }
  getDepartmentList(listOperand:listOperation){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(GLOBAL.getDepartmentList, JSON.stringify(listOperand), options)
    .map((response:Response)=>{
      return response.json();
    })
  }
  GetOrganizationsClinicList(filters:{})
  {
    return this.http.post(GLOBAL.GetOrganizationsClinicsList,  JSON.stringify(filters), this.options)
    .map((response:Response)=>{
      return response.json();
    })
  }
  getOrganizationRegionList(filters:{})
  {
    return this.http.post(GLOBAL.GetOrganizationRegionList,  JSON.stringify(filters), this.options)
    .map((response:Response)=>{
      return response.json();
    })
  }
  
  GetDataSourceDropDowns(id:any)
  {
    return this.http.get(GLOBAL.GetDataSourceDropDowns,{params: {configurationTypeId: id}}).map(response=>{
      return response.json();
    })
  }

  saveOrganizationRegion(data){
    return this.http.post(GLOBAL.SaveOrganizationRegion,  JSON.stringify(data), this.options)
    .map((response:Response)=>{
      return response.json();
    })
  }

 deleteSelectedItem(Id,tableName){
   let obj :any={}
   obj.Id=Id;
   obj.TableName=tableName;
   obj.UserId=localStorage.getItem('LoginUserID');

  return this.http.post(GLOBAL.DeleteOrganization,  JSON.stringify(obj), this.options)
  .map((response:Response)=>{
    return response.json();
  })
 }

 getOrganizationDetailById(organizationId){
  return this.http.get(GLOBAL.GetOrganizationDetailById, {params: {organizationId: organizationId}})
    .map((response: Response) => {
      debugger;
    const result = response.json();
    return result;
    });
 }

 editOrganization(orgModal:Organization)
 {
   return this.http.post(GLOBAL.OrganizationSave, JSON.stringify(orgModal), this.options)
   .map((response:Response)=>{
     return response.json();
   })
             
 }
    GetDataSourceFrequencyAndDays() {
        return this.http.get(GLOBAL.GetDataSourceFrequencyAndDays).map(response => {
            return response.json();
        })
    }

    GetOrganizationClinicDetailById(clinicId){
      return this.http.get(GLOBAL.GetOrganizationClinicDetailById, {params: {clinicId: clinicId}})
      .map((response: Response) => {
      const result = response.json();
      return result;
      });
    }

    GetOrganizationDepartmentDetailById(departmentId){
      return this.http.get(GLOBAL.GetOrganizationDepartmentDetailById, {params: {departmentId: departmentId}})
      .map((response: Response) => {
      const result = response.json();
      return result;
      });
    }
    SaveOrganizationConfiguration(organizationDataSource){
      
      return this.http.post(GLOBAL.SaveOrganizationConfiguration,  JSON.stringify(organizationDataSource), this.options)
    .map((response:Response)=>{
      return response.json();
    })

    }

    GetOrganizationsDataSourceDetail(OrganizationId,DataSourceId){
      return this.http.get(GLOBAL.GetOrganizationsDataSourceDetail, {params: {OrganizationId: OrganizationId,DataSourceId:DataSourceId}})
      .map((response: Response) => {
      const result = response.json();
      return result;
      });
    }
}
