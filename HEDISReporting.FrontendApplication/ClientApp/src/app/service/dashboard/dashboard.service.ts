import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { GLOBAL } from '../../shared/global';
import 'rxjs/Rx';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { CommonFilters, GoalDetails } from '../../model/dashboard/dashboard';
import { BehaviorSubject } from 'rxjs';
import { MasterRule, OrganizationGoalModal, MasterGoal } from 'src/app/model/dashboard/MeasureRules';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
debugger;
  userName: string;
  userId: string;
  authToken: string;
  private showFilters = new BehaviorSubject('false');
   currentFilter = this.showFilters.asObservable();
  private clearFilters = new BehaviorSubject('false');
  currentClearFilter = this.clearFilters.asObservable();


  ABAFilters: CommonFilters;
  private shareFilters = new BehaviorSubject(this.ABAFilters);
  currentSharedFilter = this.shareFilters.asObservable();
  constructor(private http: HttpClient, private router: Router) {
    this.userId = localStorage.getItem('LoginUserID');
    this.userName = localStorage.getItem('LoginUserName');
    this.authToken = localStorage.getItem('authenticationtoken');
  }

  //show filter scrren
  changeScreenSetting(value: string) {
    debugger
    this.showFilters.next(value)
  }
  clearFilterSetting(value: string) {
    debugger;
    this.clearFilters.next(value)
  }

  getAllABADetails(filters: CommonFilters): Observable<any> {
    
    const url = GLOBAL.GetAllABADetails;
    return this.http.post(url, filters);
  }
  GetAllMeasurePatientList(filters: CommonFilters): Observable<any> {
    
    const url = GLOBAL.GetAllMeasurePatientList;
    return this.http.post(url, filters);
  }

  getAllDropdowns(): Observable<any> {
    
    const url = GLOBAL.getAllDropdowns;
    return this.http.post(url, this.userId);
  }

  ShowDataWithFilters(filters: any) {
    
    this.shareFilters.next(filters)
  }
  GetMeasureAnalysis(filters: CommonFilters): Observable<any> {
    
    const url = GLOBAL.GetMeasureAnalysis;
    return this.http.post(url, filters);
  }
  GetMeasureDetails(MeasureId: number) :Observable<any> {
    
    let Id: Number;
    Id = MeasureId
    const url = GLOBAL.GetMeasureDetails;
    let MeasureTypeId:string;
    MeasureTypeId=localStorage.getItem('MasterMeaure');
    
    return this.http.get(url,{params:{Id: Id.toString(),MeasureTypeId:MeasureTypeId }});
  }
  
  SetMeasureDetails(measureDetails: MasterRule) :Observable<any> {
    debugger;
    const url = GLOBAL.SetMeasuredetails;
    return this.http.post(url, measureDetails);
  }

  getDefaultGoalsDetails(selectedGoal): Observable<any> {
    let obj: any = {};
      obj.UserId = this.userId;
     // obj.Case = Case;
     obj. OrganizationId=selectedGoal.OrganizationId;

     obj.ProviderId=selectedGoal.ProviderId;
     obj.PayerId=selectedGoal.PayerId;
     obj.ClinicId=selectedGoal.ClinicId;
    // let obj: any = {};
   // obj.UserId = this.userId;
  //  obj.OrganizationId = "0";
    const url = GLOBAL.GetDefaultGoalsDetails;
    //return this.http.get(GLOBAL.GetDefaultGoalsDetails, {params: {UserId: this.userId}});
    return this.http.post(url, obj);
  }
  getGoalsDetails(organizationId:number): Observable<any> {
    
    let obj: any = {};
    obj.UserId = this.userId;
    obj.OrganizationId = organizationId;
    const url = GLOBAL.GetGoalsDetails;
    // return this.http.get(GLOBAL.GetDefaultGoalsDetails, {params: {UserId: this.userId}});
    return this.http.post(url, obj);
  }
  
  SubmitGoalData(orgModal: GoalDetails) {
    const url = GLOBAL.SubmitGoalsDetails;
    return this.http.post(url, orgModal);
  }
  SubmitMasterGoalData(orgModal: MasterGoal) {
    const url = GLOBAL.SubmitMasterGoalsDetails;
    return this.http.post(url, orgModal);
  }
  GetClinicPayerForOrganizaton(SEntityType:number,SEntityId:number,LoginOrganizationId:number) :Observable<any> {
    
    // let Id: Number;
    // Id = OrganizationId
    const url = GLOBAL.GetClinicPayerForOrganizaton;
    // ,{params:{Id: Id.toString()}}
    return this.http.get(url,{params:{Type:SEntityType.toString(),Id:SEntityId.toString(),LoginOrganizationId:LoginOrganizationId.toString()}});
  }
  GetEntityGoal(Id:number)
  {
    const url = GLOBAL.GetEntityGoal;
    return this.http.get(url,{params:{Id: Id.toString()}});
  }
  DeleteMeasure(Id:number)
  {
    const url = GLOBAL.DeleteMeasure;
    return this.http.get(url,{params:{Id: Id.toString()}});
  }
  DeleteAllMeasure(Id:number,Type:number)
  {
    const url = GLOBAL.DeleteAllMeasure;
    return this.http.get(url,{params:{Id: Id.toString(),Type:Type.toString()}});
  }
  GetEntityMeasure(EntityType:number,EntityId:number)
  {
    const url = GLOBAL.GetEntityMeasure;
    return this.http.get(url,{params:{EntityType: EntityType.toString(),EntityId: EntityId.toString()}});
  }
  GetMeasureForOrganizaton(Id: number,type:number) :Observable<any> {
    debugger;
    let id: Number;
    id = Id
    const url = GLOBAL.MasterSelectedMeasure;
    
    return this.http.get(url,{params:{OrganizationId: Id.toString(),Type:type.toString()}});
  }
  GetMeasureGoalsForOrganizaton(Id: number,type:number,search:string) :Observable<any> {
    
    let id: Number;
    id = Id
    const url = GLOBAL.MasterMeasureGoals;
    
    return this.http.get(url,{params:{OrganizationId: Id.toString(),Type:type.toString(),search:search}});
  }
  CopyMersure(centityType: number, centityId: number, UserId: number, entityId: number, entityType: number): Observable<any> {

    // let id: Number;
    // id = Id
    const url = GLOBAL.CopyMersure;

    return this.http.get(url, { params: { CEntityType: centityType.toString(), CEntityId: centityId.toString(), userId: UserId.toString(), EntityId: entityId.toString(), EntityType: entityType.toString() } });
  }

  getMeasuresList(filters: CommonFilters): Observable<any> {
    const url = GLOBAL.GetMeasureList;
    return this.http.post(url, filters);
  }

  
  

  
  getMeasurename(userId: any): Observable<any> {
    debugger;
    const url = GLOBAL.GetMeasureName;
    return this.http.get(url, {params: {userId: userId}});
  }
  SaveDashboardGoalDetails(selectedGoal, Case, Organization): Observable<any> {
    // if(selectedGoal.IsFilter==1){
      let obj: any = {};
      obj.UserId = selectedGoal.UserId;
      obj.Case = Case;
     obj. OrganizationId=selectedGoal.OrganizationId;
     obj.CorporateGoal=selectedGoal.OrganizationId;
     obj.ProviderGoal=selectedGoal.ProviderId;
     obj.PayerGoal=selectedGoal.PayerId;
     obj.ClinicGoal=selectedGoal.ClinicId;
 

      
    //   return this.http.post(url,obj)
    // }else{
    // let obj: any = {};
    // obj.UserId = this.userId;
    // obj.SelectedGoal =selectedGoal;// Number(selectedGoal);
    // obj.Case = Case;
    // obj.OrganizationId = Organization;
    const url = GLOBAL.SaveDashboardGoalDetails;
    
    // return this.http.get(GLOBAL.GetDefaultGoalsDetails, {params: {UserId: this.userId}});
    return this.http.post(url, obj);
    // }
  }

  GetCOLAllDetails(filters: CommonFilters): Observable<any> {
    
    const url = GLOBAL.GetCOLAllDetails;
    return this.http.post(url, filters);
  }
  GetCOLPatientsList(filters: CommonFilters): Observable<any> {
    
    const url = GLOBAL.GetCOLPatientsList;
    return this.http.post(url, filters);
  }


  GetBCSAllDetails(filters:CommonFilters):Observable<any> {
    
    const url=GLOBAL.GetBCSAllDetails;
    return this.http.post(url, filters);
  }
  GetBCSPatientsList(filters:CommonFilters):Observable<any> {
    
    const url=GLOBAL.GetBCSPatientsList;
    return this.http.post(url, filters);
  }

  GetCCSAllDetails(filters:CommonFilters):Observable<any>{
    
    const url=GLOBAL.GetCCSAllDetails;
    return this.http.post(url, filters);
  }
  GetCCSPatientList(filters:CommonFilters):Observable<any> {
     
    const url=GLOBAL.GetCCSPatientList;
    return this.http.post(url, filters);
  }

  GetCBPAllDetails(filters:CommonFilters):Observable<any>{
    const url=GLOBAL.GetCBPAllDetails;
    return this.http.post(url, filters);
  }
  GetCBPPatientList(filters:CommonFilters):Observable<any> {
    
    const url=GLOBAL.GetCBPPatientList;
    return this.http.post(url, filters);
  }

  GetAllMeasureTypeMaster():Observable<any>{
    
    const url=GLOBAL.GetAllMeasureTypeMaster;
    return this.http.get(url);
  }
  GetABAWellCareDetails(filters:CommonFilters):Observable<any> {
    const url=GLOBAL.GetABAWellCareDetails;
    return this.http.post(url, filters);
  }
  GetCHBPWellCareDetails(filters:CommonFilters):Observable<any> {
    debugger;
    const url=GLOBAL.GetCBPWellCareDetails;
    return this.http.post(url, filters);
  }
  
  GetBCSWellCareAllDetails(filters:CommonFilters):Observable<any> {
    
    const url=GLOBAL.GetBCSWellCareDetails;
    return this.http.post(url, filters);
  }

  GetCBPWellCareAllDetails(filters:CommonFilters):Observable<any> {
    const url=GLOBAL.GetCBPWellCareDetails;
    return this.http.post(url, filters);
  }
  GetMeasureWellCarePatientList(filters:CommonFilters):Observable<any> {
    const url=GLOBAL.GetMeasureWellCarePatientList;
    return this.http.post(url, filters);
  }
  GethbpWellCarePatientList(filters:CommonFilters):Observable<any> {
    const url = GLOBAL.GetCBPWellCarePatientsList;
    return this.http.post(url, filters);
  }
  

  GetMeasureAnalysisWellCare(filters:CommonFilters):Observable<any>{
    
    const url=GLOBAL.GetMeasureAnalysisWellCare;
    return this.http.post(url, filters);
  }

  
  GetCOLWellCareDetails(filters:CommonFilters):Observable<any> {
    
    const url=GLOBAL.GetCOLWellCareDetails;
    return this.http.post(url, filters);
  }

  GetColWellCarePatientList(filters:CommonFilters):Observable<any>{
    const url=GLOBAL.GetColWellCarePatientList;
    return this.http.post(url, filters);
  }
//**************This 2 function is used for getting calculated BCS and Patent List*********
  GetCalculatedCBPWellCareAllDetails(filters:CommonFilters):Observable<any> {
    const url=GLOBAL.GetCBPWellCareDetails;
    return this.http.post(url, filters);
  }

  GetCBPWellCarePatientsDetails(filters:CommonFilters):Observable<any> {
    const url=GLOBAL.GetCBPWellCarePatientsList;
    return this.http.post(url, filters);
  }
  //****************************************End**********************************************
  GetCCSWellCarePatientList(filters:CommonFilters):Observable<any>{
    const url=GLOBAL.GetCCSWellCarePatientList;
    return this.http.post(url, filters);
}
GetBCSWellCarePatientList(filters:CommonFilters):Observable<any>{
  const url=GLOBAL.GetBCSWellCarePatientList;
  return this.http.post(url, filters);
}
GetCBPWellCarePatientList(filters:CommonFilters):Observable<any>{
  const url=GLOBAL.GetCBPWellCarePatientsList;
  return this.http.post(url, filters);
}
GetCCSWellCareDetails(filters:CommonFilters):Observable<any>{
  const url=GLOBAL.GetCCSWellCareDetails;
  return this.http.post(url, filters);
}
GetMeasureDescription(measureId:number,measuerType:number):Observable<any>{
  debugger;
const url=GLOBAL.GetMeasureDescription;
return this.http.get(url,{params:{measureId:measureId.toString(),entityId:measuerType.toString() }});

}

GetAttributionPatientList(filters:CommonFilters):Observable<any>{
  const url=GLOBAL.GetAttributionPatientDetails;
  return this.http.post(url,filters);
}


}