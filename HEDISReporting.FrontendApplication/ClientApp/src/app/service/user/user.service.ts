import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import {GLOBAL} from '../../shared/global';
import{user} from '../../model/user/user';
import{environment} from '../../../environments/environment'
import 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  private dataSource = new BehaviorSubject('true');
  showDashboard = this.dataSource.asObservable();

  
  private organizationSource = new BehaviorSubject('0');
  organizationId = this.organizationSource.asObservable();

  constructor( private http: Http) { 
    
  }
  getUserMenus(userModel:user) {
    return this.http.get(GLOBAL.UserMenus, {params: {userId: userModel.userId.toString(),baseUrl:environment.clientBaseUrl}})
    .map((response: Response) => {
    const result = response.json();
    return result;
    });
    //return this.http.get(GLOBAL.UserMenus,{params:{userId: userModel.userId.toString(),baseUrl:environment.clientBaseUrl}});

  }

  changeScreen(message: string) {
    this.dataSource.next(message)
  }

  
  getEditOrganizationID(message: string) {
    this.organizationSource.next(message)
  }
 
  
}
