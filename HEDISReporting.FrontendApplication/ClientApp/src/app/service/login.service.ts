import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import {login} from '../model/login';
import {GLOBAL} from '../shared/global';
import 'rxjs/Rx';
import {SettingService}  from '../service/setting.service';
// import {Buffer} from 'buffer/';
import * as crypto from "crypto-browserify";
@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    private settings: SettingService,
    private http: Http
  ) { }

  
  login(loginDetails: login) {
  
    const url = GLOBAL.Login;
    return this.http.post(url, loginDetails)
    .map((response: Response) => {
    const loginResponse = response.json();
    return loginResponse;
    });
    // return this.http.post(GLOBAL.Login, JSON.stringify(loginDetails))
    // .map((response:Response)=>{
    //   return response.json();
    // })
  
  }
   

}
