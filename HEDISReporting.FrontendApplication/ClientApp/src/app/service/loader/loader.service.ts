import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {
  loaderShow = false;

  constructor() { }

  Start() {
    this.loaderShow = true;
    document.getElementById('loader').style.display = 'block';
  }
  Stop() {
    this.loaderShow = false;
    document.getElementById('loader').style.display = 'none';
  }
}
