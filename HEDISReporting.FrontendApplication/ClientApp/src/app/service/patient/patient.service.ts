import { Injectable } from '@angular/core';
// import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { HttpClient, HttpHeaders,HttpRequest, HttpErrorResponse } from '@angular/common/http'
import {GLOBAL} from '../../shared/global';
import 'rxjs/Rx';
import { Router } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs/Rx';
import { ResponseType } from '@angular/http';
import { PatientClaim } from 'src/app/model/patient/PatientClaim';

@Injectable({
  providedIn: 'root'
})
export class PatientService {
  //options = new RequestOptions({ headers: new Headers({ 'Content-Type': 'application/json' }) });
  private messageSource = new BehaviorSubject('default message');
  currentMessage = this.messageSource.asObservable();
  // options = new RequestOptions({ headers: new Headers({ 'Content-Type': 'application/json' }) });
  userName: string;
  userId: string;
  authToken: string;
  constructor(private http: HttpClient,private router: Router) {
    this.userId = localStorage.getItem('LoginUserID');
    this.userName=localStorage.getItem('LoginUserName');
    this.authToken=  localStorage.getItem('authenticationtoken');
  }

  getAllPatientList(listOperand):Observable<any>{
    debugger;
    const url= GLOBAL.GetAllPatientList;
    return this.http.post(url, listOperand);
  }

  Uploadccda(file):Observable<any>{
    return this.http.post(GLOBAL.UploadCcdaFile,file);
  }
  GetEthinicity(EthnicityId:any):Observable<any> {
    return this.http.get(GLOBAL.GetEthnicity, {params: {EthnicityId: EthnicityId}});
  }
  GetRace(RaceId:any):Observable<any> {
    return this.http.get(GLOBAL.GetRace, {params: {RaceId: RaceId}});
   
  }
  GetOrganization(OrganizationId:any):Observable<any> {
    return this.http.get(GLOBAL.GetOrganization, {params: {OrganizationId: OrganizationId}})
    
  }
  GetFilteredPatientList(model):Observable<any>{
    return this.http.get(GLOBAL.GetFilteredPatientList, {params: {EthnicityId: model.EthnicityID, RaceId: model.RaceID, OrganizationId: model.OrganizationID}});
  
  }
  GetGender(GenderId:any):Observable<any> {
    return this.http.get(GLOBAL.GetGender, {params: {GenderId: GenderId}});
  }
  GetPatientChart(PatientId:any,TabId:any):Observable<any>
  {
    return this.http.get(GLOBAL.GetPatientChart,{params:{paientId:PatientId,tabId:TabId}});
  }
  GetPatientChartTabs(PatientId:any,TabId:any):Observable<any>
  {
    return this.http.get(GLOBAL.GetPatientChartTabs,{params:{paientId:PatientId,tabId:TabId}});
  }
  GetOrganizationFolderDetail(organizationId:any):Observable<any>
  {
    return this.http.get(GLOBAL.GetOrganizationFolderDetail,{params:{organizationId:organizationId}});
  }
  GetPatientCda(OrganizationId,FileName,IsSuccessFiles,PatientID)
  {
    return this.http.get(GLOBAL.GetPatientCda,{params:{organizationId:OrganizationId,fileName:FileName,isSuccessFiles:IsSuccessFiles,patientId:PatientID}});
  }
  SyncCCDAFiles(OrganizationId,UserId)
  {
    return this.http.get(GLOBAL.SyncSFTPClient,{params:{organizationId:OrganizationId,userId:UserId}});
  }
  changeMessage(message: string) {
    this.messageSource.next(message)
  }
  updateIsConsentTag(PatientID:string, IsConsent: boolean ) {
    debugger;
    let data :any={}
    data.PatientId=PatientID;
    data.IsConsent=IsConsent;
    data.UserId=localStorage.getItem('LoginUserID');
    return this.http.post(GLOBAL.UpdateIsConsentTag,data)
  //  .map((response:Response)=>{
  //   return response.json();
  //  })
   .catch(this.handleError);
  }
  private handleError(error: HttpErrorResponse):
      Observable<any> {
        console.error('observable error: ', error);
        return Observable.throw(error);
    }
    
  GetClinicsByOrganization(OrganizationId:any):Observable<any> {
    return this.http.get(GLOBAL.GetClinics, {params: {OrganizationId: OrganizationId}})
    
  }
  GetProviderdatabyClinicId(ClinicId:any):Observable<any> {
    return this.http.get(GLOBAL.GetProviders, {params: {ClinicId: ClinicId}})
    
  }
  GetPayers():Observable<any>{
    const url= GLOBAL.GetAllPayers;
    return this.http.get(url);
  }
  GetPatientClaimData(claimdata:any):Observable<any>{
    debugger;
    const url= GLOBAL.GetAllClaimDetail;
    return this.http.post(url,claimdata);
  }
  GetPatientDetailsLab(PatientId:any):Observable<any>
  {
    return this.http.get(GLOBAL.GetPatientDetailsLab,{params:{patientId:PatientId}});
  }

  GetLabDetailsPatient(listOperand):Observable<any>
  {
    debugger;
    return this.http.post(GLOBAL.GetLabDetailsPatient,listOperand);
  }
  DownloadPdf(parentClaimDetail:PatientClaim):Observable<any>
  {
    return this.http.post(GLOBAL.DownloadPdf,parentClaimDetail);
  }
  /**
  ** Get Patient All status and Reason
  **/
 GetstatusAndReason():Observable<any>
 {
   return this.http.get(GLOBAL.GetStatusReasonCode);
 }
}
