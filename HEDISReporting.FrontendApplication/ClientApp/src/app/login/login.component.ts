import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import {login} from '../model/login';
import {LoginService} from '../service/login.service';
import {RouterState,Router,Route} from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm:FormGroup;

  constructor(private fb: FormBuilder,private loginService: LoginService, private router:Router,private toastr:ToastrService ) {

    this.loginForm = fb.group({
      'Username' :[null,Validators.required],
      'Password' :[null,Validators.required],
    });
  }

  ngOnInit() {

  }

  onFormSubmit(LoginData:login){
   return  this.loginService.login(LoginData).subscribe(response=>{
     debugger;
      if(response.message=="Success"){
        localStorage.setItem('LoginUserID',response.data.userId );
        localStorage.setItem('OrganizationId',response.data.organizationId);
        localStorage.setItem('LoginUserName',response.data.userName )
        localStorage.setItem('authenticationtoken', response.data.token);
        localStorage.setItem('Role', response.data.role);
        this.router.navigate(['/Admin/dashboard']);
        this.toastr.success("Login Successfully !");
      }else{
        this.toastr.error("Invalid Username or Password !");
      }



    },
    error => {
      this.toastr.error("Invalid Username or Password !");
    });


  }


}
