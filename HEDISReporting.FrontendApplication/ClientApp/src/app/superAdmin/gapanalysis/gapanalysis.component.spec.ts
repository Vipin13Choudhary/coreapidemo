import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GapanalysisComponent } from './gapanalysis.component';

describe('GapanalysisComponent', () => {
  let component: GapanalysisComponent;
  let fixture: ComponentFixture<GapanalysisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GapanalysisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GapanalysisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
