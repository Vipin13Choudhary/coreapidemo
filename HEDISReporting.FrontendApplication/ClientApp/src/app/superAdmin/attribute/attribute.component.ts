import { Component, OnInit, Output, EventEmitter  } from '@angular/core';
import { AttributeService } from 'src/app/service/attribute/attribute.service';
import { ToastrService } from 'ngx-toastr';
import { LoaderService } from 'src/app/service/loader/loader.service';

@Component({
  selector: 'app-attribute',
  templateUrl: './attribute.component.html',
  styleUrls: ['./attribute.component.css']
})
export class AttributeComponent implements OnInit {
  public progress: number;
  public message: string;
  public Organization:any=[];
  public Payer:any=[];
  OrganizationId:string;
  PayerId:string;
  @Output() public onUploadFinished = new EventEmitter();
  constructor(private attribute : AttributeService,
              private toastr: ToastrService
              ,private loader: LoaderService)
   { }
  ngOnInit() {
    this.getAllOrganizationPayer();
  }
  //function is used to get all Organization and Payers
  getAllOrganizationPayer(){
    this.Organization=[];
    this.Payer=[];
    this.attribute.organizationPayer().subscribe( 
        response => {
          debugger;
          if (response.Item1.length > 0){
              this.Organization=response.Item2;
          }        
          if(response.Item2.length > 0){
            this.Payer=response.Item1;
           
          }
        });
  }
  public uploadFile = (files) => {
    if (files.length === 0) {
      return;
    }
    if(this.OrganizationId==null || this.PayerId==null){
      return;
    }
 this.loader.Start();
    let fileToUpload = <File>files[0];
    const formData = new FormData();
    let UserId=localStorage.getItem("LoginUserID");
    let FileName=fileToUpload.name+"#"+this.OrganizationId+"#"+this.PayerId+"#"+UserId;
    formData.append('file', fileToUpload, fileToUpload.name);
    formData.append('file1', fileToUpload, FileName);
    this.attribute.UploadExcelFile(formData).subscribe(response => {
        if (response.Result == "Sucess"){
          this.loader.Stop();
          this.toastr.success('Attribution file uploaded sucessfully.','Success');
        }        
         else if(response.Result == "Error") {
          this.loader.Stop();
          this.toastr.error('Something went wrong. Please try again','Error');
        }
      });
  }

}
