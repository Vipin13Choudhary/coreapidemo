import { Component, OnInit } from '@angular/core';
import {UserService} from '../../service/user/user.service';




@Component({
  selector: 'app-super-admin',
  templateUrl: './super-admin.component.html',
  styleUrls: ['./super-admin.component.css']
})
export class SuperAdminComponent implements OnInit {
public scrollbarOptions = { axis: 'yx', theme: 'minimal-dark' };

  showDashboard:string='true';
  constructor(private userService: UserService) { 

    this.userService.showDashboard.subscribe(message => this.showDashboard = message)
  }
  
  ngOnInit() {

  }
 

}
