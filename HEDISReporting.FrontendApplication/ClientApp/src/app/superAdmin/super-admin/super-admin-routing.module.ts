import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import {SuperAdminComponent } from '../super-admin/super-admin.component';
import {OrganizationComponent} from '../organization/organization.component';
import {OrganizationSetUpComponent} from '../organization-set-up/organization-set-up.component';
import {DashboardComponent} from '../dashboard/dashboard.component';
import { PatientsComponent } from '../patients/patients.component';
import { PatientschartComponent } from '../patientschart/patientschart.component';
import {MeasuresPatientComponent} from '../measures-patient/measures-patient.component';
import {DMeasureAnalysisComponent} from '../dmeasure-analysis/dmeasure-analysis.component';
import { ProviderDetailComponent} from '../provider-detail/provider-detail.component';
import { SettingsComponent } from '../settings/settings.component';
import {ProviderViewComponent} from '../provider-view/provider-view.component';
import {MeasureDetailComponent} from '../measure-detail/measure-detail.component';
import {OrganizationGoalComponent} from '../organization-goal/organization-goal.component';
import { SearchComponent } from '../search/search.component';
import { GapanalysisComponent } from '../gapanalysis/gapanalysis.component';
import { DashboardProviderComponent } from '../dashboard-provider/dashboard-provider.component';
import { AttributeComponent } from '../attribute/attribute.component';
import { MessageComponent } from '../message/message.component';
import { SmsComponent } from '../sms/sms.component';
import { GapManagementComponent } from '../gap-management/gap-management.component';
import { AdvanceSearchComponent } from '../advance-search/advance-search.component';


const routes: Routes=[
{path:'',component:SuperAdminComponent,
children: [
{path:'organization',component:OrganizationComponent},
{path:'setUp',component:OrganizationSetUpComponent},
{path:'dashboard',component:DashboardComponent},
{path:'editOrganization',component:OrganizationSetUpComponent},
{path:'patient',component:PatientsComponent},
{path:'patientchart',component:PatientschartComponent},
{path:'patientList',component:MeasuresPatientComponent},
{path:'measureAnalysis',component:DMeasureAnalysisComponent},
{path:'gapAnalysis',component:GapanalysisComponent},
{path:'dashboardProvider',component:DashboardProviderComponent},
{path:'detailMeasureAnalysis',component:MeasuresPatientComponent},
{path:'providerDetail',component:ProviderDetailComponent},
{path:'providerview',component:ProviderViewComponent},
{path:'measuredetails',component:MeasureDetailComponent},
{path:'Setting',component:SettingsComponent},
{path:'goals',component:OrganizationGoalComponent},
{path:'search',component:SearchComponent},
{path:'attribute',component:AttributeComponent},
{path:'message',component:MessageComponent},
{path:'sms',component:SmsComponent},
{path:'GapManagement',component:GapManagementComponent},
{path:'advance-search',component:AdvanceSearchComponent}

]
}
]

@NgModule({
  imports:[RouterModule.forChild(routes)],
  exports:[RouterModule]
})
export class SuperAdminRoutingModule { }


