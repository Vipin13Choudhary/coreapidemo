import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SuperAdminComponent } from '../super-admin/super-admin.component';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SuperAdminRoutingModule } from './super-admin-routing.module';
import { SidebarComponent } from '../../shared/sidebar/sidebar.component';
import { HeaderComponent } from '../../shared/header/header.component';
import { OrganizationComponent } from '../organization/organization.component';
import { MatTableModule } from '@angular/material/table';
import { CdkTableModule } from '@angular/cdk/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { MatSelectModule } from '@angular/material/select';
import { OrganizationSetUpComponent } from '../organization-set-up/organization-set-up.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatStepperModule } from '@angular/material/stepper';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TextMaskModule } from 'angular2-text-mask';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { MatExpansionModule } from '@angular/material/expansion';
import { TimezonePickerModule } from 'ng2-timezone-selector';
// import { TimepickerModule } from 'ngx-bootstrap/timepicker';
// import { PopoverModule } from 'ngx-bootstrap/popover';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { FooterComponent } from '../../shared/footer/footer.component';
import { OnlyNumber } from '../../shared/directive/OnlyNumber';
import { PatientsComponent } from '../patients/patients.component';
import { FileUploadModule } from 'ng2-file-upload';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PatientschartComponent } from '../patientschart/patientschart.component';
import { TabsModule } from "ngx-tabs";
import { MeasuresPatientComponent } from '../measures-patient/measures-patient.component';
import { FilterComponent } from '../../shared/filter/filter.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { DMeasureAnalysisComponent } from '../dmeasure-analysis/dmeasure-analysis.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { NgxMaskModule } from 'ngx-mask'
import { ProviderDetailComponent } from '../provider-detail/provider-detail.component';
// import { NgScrollbarModule } from 'ngx-scrollbar';
// import {ScrollDispatchModule} from '@angular/cdk/scrolling';
import { ShContextMenuModule } from 'ng2-right-click-menu';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { ChartModule } from 'primeng/chart';
import { SettingsComponent } from '../settings/settings.component';
import { ProviderViewComponent } from '../provider-view/provider-view.component';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MeasureDetailComponent } from '../measure-detail/measure-detail.component';
import { OrganizationGoalComponent } from '../organization-goal/organization-goal.component';
import { MalihuScrollbarModule } from 'ngx-malihu-scrollbar';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { SafehtmlPipe } from 'src/app/pipe/safehtml.pipe';
import {MatTooltipModule} from '@angular/material/tooltip';
import { CareGapComponent } from '../care-gap/care-gap.component';
import { GapanalysisComponent } from '../gapanalysis/gapanalysis.component';
import { DashboardProviderComponent } from '../dashboard-provider/dashboard-provider.component';
import { MessageComponent } from '../message/message.component';
// import {MdDatepickerModule} from '@angular/material';
import {MatDatepickerModule} from '@angular/material/datepicker'
import { MatNativeDateModule, MatAutocompleteModule } from '@angular/material';
 import { SearchComponent } from '../search/search.component';
import { AttributeComponent } from '../attribute/attribute.component';
import {MatDialogModule} from '@angular/material';
// import { AttributeComponent } from '../attribute/attribute.component';
import { SmsComponent } from '../sms/sms.component'
import { DeviceDetectorModule } from 'ngx-device-detector';
import { NeedAttentionComponent } from '../need-attention/need-attention.component';
import {SpeechRecognitionModule} from '@kamiazya/ngx-speech-recognition';
  import { from } from 'rxjs';
//import { SanitizeHtmlDirective } from 'src/app/shared/sidebar/sanitize-html.directive';
import { GapManagementComponent } from '../gap-management/gap-management.component';
import { AttributionManagementComponent } from '../attribution-management/attribution-management.component';
import { AdvanceSearchComponent } from '../advance-search/advance-search.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

@NgModule({
  declarations: [SuperAdminComponent,
    SidebarComponent,
    HeaderComponent,
    OrganizationComponent,
    OrganizationSetUpComponent,
    DashboardComponent,
    FooterComponent,
    OnlyNumber,
    PatientschartComponent,
    PatientsComponent,
    MeasuresPatientComponent,
    FilterComponent,
    DMeasureAnalysisComponent,
    ProviderDetailComponent,
    ProviderViewComponent,
    MeasureDetailComponent,
    SettingsComponent,
    SafehtmlPipe,
    CareGapComponent,
    OrganizationGoalComponent,   
    GapanalysisComponent,
    SearchComponent,
    AttributeComponent,
    DashboardProviderComponent,
    MessageComponent,
    SmsComponent,
    NeedAttentionComponent,
    //SanitizeHtmlDirective,
    NeedAttentionComponent,
    GapManagementComponent,
    AttributionManagementComponent,
    AdvanceSearchComponent
  ],
  imports: [
    CommonModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    SuperAdminRoutingModule,
    MatTableModule,
    CdkTableModule,
    MatPaginatorModule,
    MatSortModule,
    AngularFontAwesomeModule,
    MatSelectModule,
    MatFormFieldModule,
    MatSelectModule,
    MatStepperModule,
    MatMenuModule,
    TextMaskModule,
    ModalModule.forRoot(),
    MatCheckboxModule,
    NgMultiSelectDropDownModule.forRoot(),
    MatExpansionModule,
    TimezonePickerModule,
    NgxMaterialTimepickerModule.forRoot(),
    FileUploadModule,
    NgbModule,
    TabsModule,
    BsDatepickerModule.forRoot(),
    MDBBootstrapModule.forRoot(),
    NgxMaskModule.forRoot(),
    ShContextMenuModule,
    PopoverModule.forRoot(),
    // NgScrollbarModule,
    // ScrollDispatchModule
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    ChartModule,
    MatIconModule,
    MalihuScrollbarModule.forRoot(),
    DeviceDetectorModule.forRoot(),
    MatTooltipModule,
  AngularEditorModule,
  MatDatepickerModule,
  MatNativeDateModule,
  PerfectScrollbarModule,
  MatDialogModule,
    MatAutocompleteModule,
    SpeechRecognitionModule.withConfig({
      lang: 'en-US',
      interimResults: true,
      maxAlternatives: 100,
    }),
  ],
    providers: [ 
    MatDatepickerModule,  
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    }
  ],
})
export class SuperAdminModule { }
