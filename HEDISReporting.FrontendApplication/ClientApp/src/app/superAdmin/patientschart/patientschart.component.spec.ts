import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientschartComponent } from './patientschart.component';

describe('PatientschartComponent', () => {
  let component: PatientschartComponent;
  let fixture: ComponentFixture<PatientschartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientschartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientschartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
