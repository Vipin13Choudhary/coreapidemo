import { Component, OnInit, Input, TemplateRef, ViewChild } from '@angular/core';
import { UserService } from 'src/app/service/user/user.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { PatientService } from 'src/app/service/patient/patient.service';
import { PatientModel, PatientDemographics, PatientAllergies, PatientLabTest, SnomedCodeModel, PatientNavigationScreens } from 'src/app/model/patient/patientModel';
import { PatientClaimFilters } from 'src/app/model/patient/PatientClaimFilters';
import { PatientClaim } from 'src/app/model/patient/PatientClaim';
import { Location } from '@angular/common';
import { listOperation } from '../../model/common/common';
//import { AccordionTemplate, AccordionData } from 'ngx-accordion-table';
import * as JSZip from 'jszip/dist/jszip.min';
import { saveAs } from 'file-saver';
import { PageEvent } from '@angular/material';
import { MatTableDataSource } from '@angular/material';
import * as _ from 'underscore';
import { MatDatepickerModule } from '@angular/material/datepicker'
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { LoaderService } from 'src/app/service/loader/loader.service';
import { DeviceDetectorService } from 'ngx-device-detector';
import { browser } from 'protractor';
//import * as html2canvas from "html2canvas"
import { stringify } from '@angular/compiler/src/util';
import { RxSpeechRecognitionService, resultList } from '@kamiazya/ngx-speech-recognition';
import { environment } from 'src/environments/environment';
import { Observable, Subject } from 'rxjs';
import { NeedAttentionComponent } from '../need-attention/need-attention.component';
import { NeedAttentionService } from 'src/app/service/NeedAttention/need-attention.service';
import { isNullOrUndefined } from 'util';
import { CareGapComponent } from '../care-gap/care-gap.component';
import { NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
declare let jsPDF;
declare let JSZipUtils;
declare let $;

@Component({
  selector: 'app-patientschart',
  templateUrl: './patientschart.component.html',
  styleUrls: ['./patientschart.component.css'],
  providers: [
    RxSpeechRecognitionService,
  ],
})
export class PatientschartComponent implements OnInit {
  public scrollbarOptions = { axis: 'y', theme: 'minimal-dark' };
  patientId: number;
  patientChartDetail: PatientModel;
  GetPatientChartBasicDetails: any;
  previousUrl: string = "";
  message: any;
  data:any;
  addClass: string = "";
  status: boolean = false;
  img: string = "";
  PatientName: string = "";
  PatientAddress: string = "";
  modalRef: BsModalRef;
  DateOfBirth: Date;
  charges: string = null;
  clinicName: string = null;
  diagnosisDate: Date = null;
  contactNo: string = null;
  modifier: string = null;
  pageSizeOptions: number[] = [1, 5, 10, 20];
  serviceCode: string = null;
  serviceName: string = null;
  unit: string = null;
  payerName: string;
  month: string = null;
  day: string = null;
  year: string = null;
  areaCode: string = null;
  phoneNumber: string = null;
  filteringData: any;
  claimDetail: any;
  fafahide: number = null;
  fafashow: boolean = true
  fafaanchor: boolean = false;
  Paging: boolean = false;
  parentClaimDetail: PatientClaim[] = [];
  base64textString: string = null;
  //Field is kept for toggling the subsection;
  ptDiagnosisId: number = 0;
  base64Image: any;
  templateShow: boolean = false;
  // img:string="";
  PhysicianName: string = "";
  LabName: string = "";
  zip = new JSZip();
  Pdfblob: any[] = [];
  DateCollected: string = "";
  DateRecived: string = "";
  GetPatientLabBasicDetails: any[] = [];
  GetFilteredPatientLabDetail: any[] = [];
  MessageType: any[] = [{ id: 1, Name: 'Email' }, { id: 2, Name: 'Text' }, { id: 3, Name: 'Voice' }]
  TemplateType: any[] = [{ id: 1, Name: 'Appointment' }, { id: 2, Name: 'Care Gap' }]
  DestinationType: any[] = [{ id: 1, Name: 'Home' }, { id: 2, Name: 'Work' }]
  displayedColumns = ['ORDER/REQNO.', 'TestPerformed', 'ReportDate', 'LonicCode', 'Lab', 'Unit', 'Result', 'Notes'];
  pageEvent: PageEvent;
  search: "";
  DateOFBirth: string = "";
  ReportName: string = "";
  UniqueId: string = "";
  PdfNames: any[] = [];
  Pdfs: any[] = [];
  counter: number = 0
  ELEMENT_DATA: any;
  dataSource: any;
  OrganizationName: string = "";
  listOperands: any;
  OrderNo: string = "";
  TestName: string = "";
  FromDate: Date;
  ToDate: Date;
  length: Number;
  pageSize: Number = 10;
  NoteName: any;
  MessageText: string = '';
  base64Img: string = "";
  link: any;
  imageBlobUrl: string | null = null;
  imgPath: string = null;
  Phone: string;
  Email: string;
  Address: string;
  OrgName: string;
  PatientReason: any[] = [];
  PatientStatus: any[] = [];
  GetFilteredNeedAttentionDetail: any[] = [];
  StatusCodeId:number=0;
  ReasonCodeId:number=0;
  PatientComment:string=null;
  GetFilteredNeedAttentionDetails:any[]=[];
  needAttention:boolean=false;
  caregaps:boolean=true;
  SelectedCareGap:string=null;
  multipleselected:boolean=false;
  StatusCareGapCodeId:number=0;
  ReasonCareGapCodeId:number=0;
  PatientcareGapComment:string=null;
  GetclearNeedAttentionDetail:any[]=[];
  PatientStatusCode:string=null;
  PatientReasonStatusCode:string=null;
  PatientClearNeedAttentionComment:string=null;
  PatientClearCareGapComment:string=null;
  public phoneMask = [/\d/, ' ', '(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];

  modalOption: NgbModalOptions = {};
  constructor(private deviceService: DeviceDetectorService,
    private loader: LoaderService,
    private toaster: ToastrService,
    private userService: UserService,
    private patientService: PatientService,
    private route: ActivatedRoute,
    private router: Router,
    private _location: Location,
    private modalService: BsModalService,
    private service: RxSpeechRecognitionService,
    private needAttentionService: NeedAttentionService
  ) {
    this.userService.changeScreen("false");
    this.patientChartDetail = new PatientModel();
    this.patientChartDetail.patientAllergies = [new PatientAllergies];
    this.patientChartDetail.snomedCodeModel = [new SnomedCodeModel];
    this.patientChartDetail.patientLabTest = [new PatientLabTest];
    this.patientChartDetail.patientDemographics = new PatientDemographics();
    this.patientChartDetail.patientclaimData = [new PatientClaim];
    this.claimDetail = new PatientClaimFilters();
    this.claimDetail.offset = 1;
    this.claimDetail.limit = 10;
    this.claimDetail.order = "desc";
    this.claimDetail.sort = null;
    this.claimDetail.OrganizationId = 0;
    this.claimDetail.FromDate = null;
    this.claimDetail.ToDate = null;
    this.claimDetail.PatientId = 0;
    this.claimDetail.offset = 1;
    this.claimDetail.limit = 10;
    this.claimDetail.order = "desc";
    this.claimDetail.sort = null;
    this.claimDetail.OrganizationId = 0;
    this.claimDetail.FromDate = null;
    this.claimDetail.ToDate = null;
    this.claimDetail.PatientId = 0;
    this.listOperands = new listOperation();
  }
  @ViewChild(NeedAttentionComponent) private myChild: NeedAttentionComponent;
  @ViewChild(CareGapComponent) private myCareGapChild: CareGapComponent;
  ngOnInit() {
    debugger;
    this.needAttentionService.currentMessage.subscribe(message => this.message = message);
    // this.needAttentionService.ClearcurrentMessage.subscribe(data => this.data = data);
    var url = environment.clientBaseUrl + 'assets/img/LabLogo.PNG';
    url = url.replace('/#', '');
    this.toDataUrl(url, (myBase64) => {
      this.imgPath = myBase64;
    })

    this.patientService.currentMessage.subscribe(message => this.message = message);
    this.route.queryParams.subscribe(params => {

      if (params.id != undefined)
        this.patientId = params.id;
      else
        this.patientId = 0;

    });
    this.claimDetail.OrganizationId = 0;
    this.claimDetail.PatientId = 0;
    this.claimDetail.FromDate = null;
    this.claimDetail.ToDate = null;
    this.claimDetail.offset = 1;
    this.claimDetail.limit = 10;
    this.claimDetail.order = null;
    this.claimDetail.sort = null;
    this.claimDetail.Search = null;


    this.GetPatientChartBasicDetail(this.patientId);

    this.listOperands.offset = 1;
    this.listOperands.limit = 10;
    this.listOperands.order = "desc";
    this.listOperands.sort = null;
    this.listOperands.LabName = null;
    this.listOperands.OrderNo = null;
    this.listOperands.TestName = null;
    this.listOperands.FromDate = null;
    this.listOperands.ToDate = null;
    this.listOperands.search = null;
    this.listOperands.PatientId = 0;
  }
  openMessageModal(modelName, id) {
    debugger;
    this.MessageText = "";
    this.Address = "";
    let template: TemplateRef<any> = modelName;
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: 'modal-lg' })
    );
  }

  //This function is used to open dtatus modal
  //This function is used to open popup for changing the status of Modal
  fnShowMessageModal(modelName, id) {
    debugger;
    this.MessageText = "";
    this.GetFilteredNeedAttentionDetail = [];
    // if(modelName==="templatemessagestatus"){
    this.GetFilteredNeedAttentionDetail = _.filter(this.message,
      function (element) {
        return element.AttentionFlag == true;
      });
    this.patientService.GetstatusAndReason().subscribe(response => {
      /**
       * This if is used to fill data in Patient status dropdown for Popup
       */
      if (response.result.Item1.length > 0 && response.result != null) {
        this.PatientStatus = [];
        (response.result.Item1).forEach(element => {
          this.PatientStatus.push({
            CodeId: element.CodeId,
            CodeDescription: element.CodeDescription
          })
        });
      }
      /**
       * End
       */
      /**
       * *This if condition is used to insert data in Reason Dropdown for Popup
       */
      if (response.result.Item2.length > 0 && response.result != null) {
        this.PatientReason = [];
        (response.result.Item2).forEach(element => {
          this.PatientReason.push({
            CodeId: element.CodeId,
            CodeDescription: element.CodeDescription
          })
        });
      }
      /**
       * *End
       */
    });
    // }
    if (this.GetFilteredNeedAttentionDetail.length < 1)
    {
      this.toaster.warning('Please check atleast one checkbox');
      return true;
    }
    this.modalOption.backdrop = 'static';
    this.modalOption.keyboard = false;
    let template: TemplateRef<any> = modelName;
    this.modalRef = this.modalService.show(template,this.modalOption);
  }
  //End
  
  //This function is used to close popup and submit the chages
  changeStatus() {
    if (this.GetFilteredNeedAttentionDetail.length > 0) {
      this.GetFilteredNeedAttentionDetails=[];
      this.GetFilteredNeedAttentionDetail.forEach(element => {
        this.GetFilteredNeedAttentionDetails.push({
          PatientWorkItemsId:element.PatientWorkItemsId,
          PatientId:element.PatientId,
          PatientStatusCode:element.PatientStatusCode,
          ReasonCode:element.ReasonCode,
          StatusCodeId:this.StatusCodeId,
          ReasonCodeId:this.ReasonCodeId,
          PatientComment:this.PatientComment
        });
      });
      if(!isNullOrUndefined(this.PatientComment) && !isNullOrUndefined(this.ReasonCodeId) && !isNullOrUndefined(this.StatusCodeId))
      {
        this.needAttentionService.UpdateNeedAttention(this.GetFilteredNeedAttentionDetails).subscribe(
        response => {
          if (JSON.stringify(response).indexOf('Success') > -1) {
            this.toaster.success('Status updated successfully');
            this.modalRef.hide();
            this.StatusCodeId=null;
            this.ReasonCodeId=null;
            this.PatientComment=null;
            this.message=[];
            this.GetFilteredNeedAttentionDetail=[];
            this.myChild.GetNeedAttentionPatienList();
          }
        }, err => {
          this.toaster.error('Something went wrong');
        })
      }else{
        this.StatusCodeId=null;
            this.ReasonCodeId=null;
            this.PatientComment=null;
            this.toaster.error("Please select all fields")
      }
    }else {
            this.StatusCodeId=null;
            this.ReasonCodeId=null;
            this.PatientComment=null;
            this.toaster.error("Please select all fields")
    }
  }
  //End
  //End

  //This function is used to open caregap modal
  //This function is used to open popup for changing the Caregap status of Modal
  fnShowcaregapMessageModal(modelName, id) {
    debugger;
    this.MessageText = "";
    this.GetFilteredNeedAttentionDetail = [];
    // if(modelName==="templatemessagestatus"){
    this.GetFilteredNeedAttentionDetail = _.filter(this.message,
      function (element) {
        return element.CareGapFlag == true;
      });
    this.patientService.GetstatusAndReason().subscribe(response => {
      /**
       * This if is used to fill data in Patient status dropdown for Popup
       */
      if (response.result.Item1.length > 0 && response.result != null) {
        this.PatientStatus = [];
        (response.result.Item1).forEach(element => {
          this.PatientStatus.push({
            CodeId: element.CodeId,
            CodeDescription: element.CodeDescription
          })
        });
      }
      /**
       * End
       */
      /**
       * *This if condition is used to insert data in Reason Dropdown for Popup
       */
      if (response.result.Item2.length > 0 && response.result != null) {
        this.PatientReason = [];
        (response.result.Item2).forEach(element => {
          this.PatientReason.push({
            CodeId: element.CodeId,
            CodeDescription: element.CodeDescription
          })
        });
      }
      /**
       * *End
       */
    });
    // }
    debugger;
    if(this.GetFilteredNeedAttentionDetail.length==1){
      this.multipleselected=false;
      this.SelectedCareGap=this.GetFilteredNeedAttentionDetail[0].Name;
    }else{
      this.multipleselected=true;
    }
    if (this.GetFilteredNeedAttentionDetail.length < 1 )
    {
      this.toaster.warning('Please check atleast one checkbox');
      return true;
    }
    this.modalOption.backdrop = 'static';
    this.modalOption.keyboard = false;
    let template: TemplateRef<any> = modelName;
    this.modalRef = this.modalService.show(template, this.modalOption);
  }
  //END
  changeCareGapStatus() {
    if (this.GetFilteredNeedAttentionDetail.length > 0) {
      debugger;
      this.GetFilteredNeedAttentionDetails=[];
      this.GetFilteredNeedAttentionDetail.forEach(element => {
        this.GetFilteredNeedAttentionDetails.push({
          PatientWorkItemsId:element.EthnicityId,
          PatientId:element.PatientId,
          PatientStatusCode:element.RaceId,
          ReasonCode:element.Gap,
          StatusCodeId:this.StatusCareGapCodeId,
          ReasonCodeId:this.ReasonCareGapCodeId,
          PatientComment:this.PatientcareGapComment
        });
      });
      if(!isNullOrUndefined(this.PatientcareGapComment) && !isNullOrUndefined(this.ReasonCareGapCodeId) && !isNullOrUndefined(this.StatusCareGapCodeId))
      {
        this.needAttentionService.UpdateCareGaps(this.GetFilteredNeedAttentionDetails).subscribe(
        response => {
          if (JSON.stringify(response).indexOf('Success') > -1) {
            this.toaster.success('Status updated successfully');
            this.modalRef.hide();
            this.StatusCareGapCodeId=null;
            this.ReasonCareGapCodeId=null;
            this.PatientcareGapComment=null;
            this.message=[];
            this.GetFilteredNeedAttentionDetail=[];
            this.myCareGapChild.GetCareGapPatientList();
          }
        }, err => {
          this.toaster.error('Something went wrong');
        })
      }else{
        this.StatusCareGapCodeId=null;
        this.ReasonCareGapCodeId=null;
        this.PatientcareGapComment=null;
            this.toaster.error("Please select all fields")
      }
    }else {
      this.StatusCareGapCodeId=null;
      this.ReasonCareGapCodeId=null;
      this.PatientcareGapComment=null;
            this.toaster.error("Please select all fields")
    }
  }
  //END
  //END

    //This function is used to open caregap modal
  //This function is used to open popup for changing the Caregap status of Modal
  fnShowNeedAttentionModals(modelName, id) {
    debugger;
    if (this.message.length < 1)
    {
      this.toaster.warning('Please check atleast one checkbox');
      return true;
    }
    else if (this.message.length>0 && this.message[0].AttentionFlag==true)
    {
    this.MessageText = "";
    this.GetclearNeedAttentionDetail = [];
    this.PatientStatusCode=this.message[0].PatientStatus;
    this.PatientReasonStatusCode=this.message[0].Reason;
    let template: TemplateRef<any> = modelName;
    this.modalOption.backdrop = 'static';
    this.modalOption.keyboard = false;
    this.modalRef = this.modalService.show(template,this.modalOption);
    }
    else{
      this.toaster.warning('Please check atleast one checkbox');
      return true;
    }
  }
  //END
  /**
   * *For Clearing the care Gap 
   */

  fnShowcaregapMessageModals(modelName, id) {
    debugger;
    this.MessageText = "";
    this.PatientClearCareGapComment=null;
    
    this.GetclearNeedAttentionDetail = [];
    this.GetFilteredNeedAttentionDetail = [];
    this.GetFilteredNeedAttentionDetail = _.filter(this.message,
      function (element) {
        return element.CareGapFlag == true;
      });
      if (this.GetFilteredNeedAttentionDetail.length < 1)
      {
        this.toaster.warning('Please check atleast one checkbox');
        return true;
      }
      this.modalOption.backdrop = 'static';
      this.modalOption.keyboard = false;
    let template: TemplateRef<any> = modelName;
    this.modalRef = this.modalService.show(template,this.modalOption);
   
  }
  //END

  hideModal(){
    // this.message=[];
    this.modalRef.hide();
  }
  
  clearNeedAttentionStatus(){
    if (this.message.length > 0) {
      debugger;
      this.GetFilteredNeedAttentionDetails=[];
      this.message.forEach(element => {
        this.GetFilteredNeedAttentionDetails.push({
          PatientWorkItemsId:element.PatientWorkItemsId,
          PatientId:element.PatientId,
          PatientStatusCode:element.PatientStatusCode,
          ReasonCode:element.ReasonCode,
          PatientComment:this.PatientClearNeedAttentionComment
        });
      });
      if(!isNullOrUndefined(this.PatientClearNeedAttentionComment))
      {
        /**
         * ! Need to make new function for clearcare gap in C# code side 
         * ! Storeprocedure is already created 
         * ! Angular Service also have to created
         */
        this.needAttentionService.UpdateNeedAttentiondata(this.GetFilteredNeedAttentionDetails).subscribe(
        response => {
          if (JSON.stringify(response).indexOf('Success') > -1) {
            this.toaster.success('Status updated successfully');
            this.modalRef.hide();
            this.PatientClearNeedAttentionComment=null;
            this.message=[];
            this.GetFilteredNeedAttentionDetail=[];
            this.myChild.GetNeedAttentionPatienList();
          }
        }, err => {
          this.toaster.error('Something went wrong');
        })
      }else{
        this.PatientClearNeedAttentionComment=null;
            this.toaster.error("Something went wrong")
      }
    }else {
      this.PatientClearNeedAttentionComment=null;
            this.toaster.error("something went wrong")
    }
  }
  
  clearCareGapStatus(){
    if (this.GetFilteredNeedAttentionDetail.length > 0) {
      debugger;
      this.GetFilteredNeedAttentionDetails=[];
      this.GetFilteredNeedAttentionDetail.forEach(element => {
        this.GetFilteredNeedAttentionDetails.push({
          PatientWorkItemsId:element.EthnicityId,
          PatientId:element.PatientId,
          PatientStatusCode:element.RaceId,
          ReasonCode:element.Gap,
          StatusCodeId:element.status,
          ReasonCodeId:element.PatientId,
          PatientComment:this.PatientClearNeedAttentionComment
        });
      });
      if(!isNullOrUndefined(this.PatientClearNeedAttentionComment))
      {
        /**
         * ! Need to make new function for clearcare gap in C# code side 
         * ! Storeprocedure is already created 
         * ! Angular Service also have to created
         */
        this.needAttentionService.UpdateCareGaps(this.GetFilteredNeedAttentionDetails).subscribe(
        response => {
          if (JSON.stringify(response).indexOf('Success') > -1) {
            this.toaster.success('Status updated successfully');
            this.modalRef.hide();
            this.PatientClearNeedAttentionComment=null;
            this.message=[];
            this.GetFilteredNeedAttentionDetail=[];
            this.myCareGapChild.GetCareGapPatientList();
          }
        }, err => {
          this.toaster.error('Something went wrong');
        })
      }else{
        this.PatientClearNeedAttentionComment=null;
            this.toaster.error("Something went wrong")
      }
    }else {
      this.PatientClearNeedAttentionComment=null;
            this.toaster.error("something went wrong")
    }
  }
  
  //END

  GetTemplate(id: number) {
    let provider: string;
    if (id == 1) {
      provider = this.patientChartDetail.patientDemographics.provider == '' ? 'your physician' : this.patientChartDetail.patientDemographics.provider;
    }
    else {
      provider = this.patientChartDetail.patientDemographics.provider == '' ? 'Your physician' : this.patientChartDetail.patientDemographics.provider;
    }
    if (id == 1) {
      this.MessageText = "Hi " + this.patientChartDetail.patientDemographics.name +",\n \nThis is a reminder of your upcoming appointment with " + provider + ". \n \n If you would like to speak to an associate, please press 1 at any time during this message. Otherwise, please arrive at 9:00 AM on Monday, May 20th. We look forward to seeing you."
    }
    else {
      this.MessageText = "Hi " + this.patientChartDetail.patientDemographics.name +",\n \nThis is the scheduling department calling from "+this.OrgName+". \n \n If you would like to speak to an associate, please press 1 at any time during this message. " + provider + " wanted me to reach out to you about scheduling a wellness visit for you in the near future."
    }
  }

  listen() {
    debugger;
    this.service
      .listen()
      .pipe(resultList)
      .subscribe((list: SpeechRecognitionResultList) => {
        this.MessageText = list.item(0).item(0).transcript;
        //console.log('RxComponent:onresult', this.MessageText, list);
      });
  }

  Voice() {
    debugger;
    let text: string;
    let text2: string;
    let text3: string;
    // text='Hi Allen Jared, this is Missy Begley calling from Juniper Health.  Steve Gruner wanted me to reach out to you about scheduling your chronic disease check-up.Have you by chance had that done at another location in the past one month.';
    // text2='If no then,Steve Gruner would like to get you scheduled at your convenience. What day of the week and time works best for you? Do you have any transportation needs for this appointment that we could help coordinate to make it easier for you?';
    // text3='We look forward to seeing you on Monday 6th Of May.Please don’t hesitate to give us a call or message us through the portal if you have any questions.';

    text = this.MessageText.substring(0, 250);
    let textnum: number;
    textnum = text.lastIndexOf('.');
    textnum += 1;
    text = this.MessageText.substring(0, textnum);
    text2 = this.MessageText.substring(textnum, 500);
    // text2=this.MessageText.substring(251,500);
    // text3=this.MessageText.substring(501,550);
    //console.log(window.speechSynthesis.getVoices());
    const utterance = new SpeechSynthesisUtterance(text);
    const utterance1 = new SpeechSynthesisUtterance(text2);
    // const utterance2 = new SpeechSynthesisUtterance(text3);

    window.speechSynthesis.cancel();
    // utterance.lang="en-US";
    utterance.pitch = 1;
    utterance.rate = 1;
    utterance1.pitch = 1;
    utterance1.rate = 1;
    // // var v=window.speechSynthesis.getVoices();
    // // utterance.voice=v[0];
    // utterance.volume=1;
    // console.log(utterance);
    window.speechSynthesis.speak(utterance);
    window.speechSynthesis.speak(utterance1);
    // window.speechSynthesis.speak(utterance2);
    //setTimeout(() => { speechSynthesis.cancel(); }, 50);

    // speechSynthesis.cancel();
  }

  clickEvent() {
    debugger
    this.status = !this.status;
  }

  openPopup(notes, templatename) {
    let template: TemplateRef<any> = templatename;
    this.modalRef = this.modalService.show(template);
    this.NoteName = notes;
  }
  GetPhEmail(Id: number) {
    if (Id == 1) {
      this.Address = this.Email;
    }
    else if (Id == 2) {
      this.Address = this.Phone;
    }
    else {
      this.Address = '';
    }
  }

  GetPatientChartBasicDetail(PatientId: any) {
    this.patientService.GetPatientChart(PatientId, 0).subscribe(response => {
      debugger;
      //console.log(response);
      this.Phone = response.patientDemographicsModel.phone;
      this.Email = response.patientDemographicsModel.email;
      this.OrgName = response.patientDemographicsModel.organizationName;
      this.patientChartDetail.patientDemographics = response.patientDemographicsModel;
      this.patientChartDetail.snomedCodeModel = response.snomedCodeModels;
      if (this.patientChartDetail.snomedCodeModel.length > 0) {
        this.patientChartDetail.snomedCodeModel[0].isVisible = true;
        this.ptDiagnosisId = this.patientChartDetail.snomedCodeModel[0].patientDiagnosisId;

      }
      //console.log(this.patientChartDetail);

    });
  }


  GetPatientTabsDetail(tabId: any) {
    debugger;
    this.patientService.GetPatientChartTabs(this.patientId, tabId).subscribe(response => {
      debugger;
      if (tabId == 0) {
        this.patientChartDetail.snomedCodeModel = response.snomedCodeModels;
        if (this.patientChartDetail.snomedCodeModel.length > 0) {
          this.patientChartDetail.snomedCodeModel[0].isVisible = true;
          this.ptDiagnosisId = this.patientChartDetail.snomedCodeModel[0].patientDiagnosisId;
          this.needAttention=false;
          this.caregaps=true;
          this.message=[];
        }
        this.myCareGapChild.GetCareGapPatientList();
        this.myCareGapChild.GetImmunizations();
        this.Paging = false;
      }
      if (tabId == 4) {
        this.fnLoadPatientLabData();
        this.patientChartDetail.patientLabTest = response.patientLabTest;
        //console.log(this.patientChartDetail.patientLabTest);
        this.Paging = false;
      }
      if (tabId == 2) {
        this.patientChartDetail.patientAllergies = response.patientAllergies;
        this.Paging = false;
      }
      if (tabId == 5) {
        this.patientChartDetail.patientclaimData = this.setClaimData();
      }
      if(tabId==1){
        this.needAttention=true;
        this.caregaps=false;
        this.message=[];
        this.myChild.GetNeedAttentionPatienList();
      }
      // this.setClaimData();
    });
  }

  GetPatientsTabsDetail(tabId: any) {
    debugger;
    this.patientService.GetPatientChartTabs(this.patientId, tabId).subscribe(response => {
      if (tabId == 0)
      {
        this.patientChartDetail.snomedCodeModel = response.snomedCodeModels;
        //console.log(this.patientChartDetail.snomedCodeModel);
      }
      if (tabId == 4)
        this.fnLoadPatientLabData();
      this.patientChartDetail.patientLabTest = response.patientLabTest;
      if (tabId == 2)
        this.patientChartDetail.patientAllergies = response.patientAllergies;
      if (tabId == 5) {
        this.patientChartDetail.patientclaimData = this.setClaimData();
      }

    });
  }
  //this compoent is being shared between below component urls using rxjs message subscription
  fnbackToBaseUrl() {
    debugger
    if ("/Admin/patientList" == this.message.split(";")[0])
      this.router.navigate([this.message.split(";")[0], { "id": this.message.split(";")[1].split("=")[1] }])
    else if ("/Admin/patient" == this.message.split(";")[0])
      this.router.navigate([this.message.split(";")[0]]);
    else if ("/Admin/detailMeasureAnalysis" == this.message.split(";")[0])
      this.router.navigate([this.message.split(";")[0]]);
  }


  toDataUrl(url, callback) {
    debugger;
    var xhr = new XMLHttpRequest();
    xhr.onload = function () {
      var reader = new FileReader();
      reader.onloadend = function () {
        callback(reader.result);
      }
      reader.readAsDataURL(xhr.response);
    };
    xhr.open('GET', url);
    xhr.responseType = 'blob';
    xhr.send();
  }

  genratePDFLab(GetFilteredPatientLabDetail) {
    this.GetFilteredPatientLabDetail.forEach(element => {
      var doc = new jsPDF();
      var img = localStorage.getItem('imagedata');
      doc.addImage(this.imgPath, 'PNG', 10, 2, 100, 18);
      doc.setFontSize(11);
      doc.setTextColor(100)
      doc.text(145, 18, "LAB RESULT-Patient Report");
      doc.setDrawColor(0, 128, 128);
      doc.setLineWidth(0.5);
      doc.line(10, 20, 200, 20); // horizontal line
      doc.setFontSize(9);
      doc.setTextColor(0, 128, 128);
      doc.text(10, 28, "CLINIC INFORMATION");
      doc.text(80, 28, "PHYSICIAN INFORMATION");
      doc.text(140, 28, "PATIENT INFORMATION");
      doc.setTextColor(100);
      doc.setFontSize(8.5);
      doc.text(10, 35, "Name");
      doc.text(10, 40, "Account");
      doc.text(10, 45, "Address");
      doc.text(10, 50, "Fax");
      doc.setTextColor(0, 0, 0);
      doc.text(26, 35, "clinictest");
      doc.text(26, 40, "1312312313");
      doc.text(26, 45, "parksttreet");
      doc.text(26, 50, "35353453453");
      doc.setDrawColor(0, 128, 128);
      doc.setLineWidth(0.5);
      doc.line(75, 30, 75, 60);// vertical line 
      doc.setTextColor(100);
      doc.text(80, 35, "Order");
      doc.text(80, 40, "NPI");
      doc.text(80, 45, "Refer");
      doc.text(80, 50, "NPI");
      doc.setTextColor(0, 0, 0);
      doc.text(100, 35, "45435345");
      doc.text(100, 40, "3453453453");
      doc.text(100, 45, "testing");
      doc.text(100, 50, "5464564646");
      doc.setDrawColor(0, 128, 128);
      doc.setLineWidth(0.5);
      doc.line(135, 30, 135, 60);// vertical line 
      doc.setTextColor(100);
      doc.text(140, 35, "Name");
      doc.text(140, 40, "DOB");
      doc.text(140, 45, "MRN");
      doc.text(140, 50, "ACCT");
      doc.setTextColor(0, 0, 0);
      doc.text(160, 35, element.FirstName);
      doc.text(160, 40, "");
      doc.text(160, 45, "67868658568");
      doc.text(160, 50, "8568568658");
      doc.setDrawColor(0, 0, 0);
      doc.setLineWidth(0.2);
      doc.line(10, 65, 200, 65); // horizontal line
      doc.setFontSize(9);
      doc.setTextColor(0, 128, 128);
      doc.text(10, 72, "LAB INFORMATION");
      doc.text(80, 72, "LAB DIRECTOR");
      doc.text(140, 72, "SPECIMEN INFORMATION");
      doc.setTextColor(100);
      doc.setFontSize(8.5);
      doc.text(10, 79, "CLIA#");
      doc.text(10, 84, "Name");
      doc.text(10, 89, "Address");
      doc.text(10, 94, "Fax");
      doc.setTextColor(0, 0, 0);
      doc.text(26, 79, "45634653463");
      doc.text(26, 84, element.LabName);
      doc.text(26, 89, "park street");
      doc.text(26, 94, "5646456546");
      doc.setDrawColor(0, 128, 128);
      doc.setLineWidth(0.5);
      doc.line(75, 75, 75, 105);// vertical line 
      doc.setTextColor(100);
      doc.text(80, 79, "Name");
      doc.text(80, 84, "Phone");
      doc.text(80, 89, "Branch");
      doc.setTextColor(0, 0, 0);
      doc.text(100, 79, "");
      doc.text(100, 84, "");
      doc.text(100, 89, "");
      doc.setDrawColor(0, 128, 128);
      doc.setLineWidth(0.5);
      doc.line(135, 75, 135, 105);// vertical line 
      doc.setTextColor(100);
      doc.text(140, 79, "Req Number");
      doc.text(140, 84, "Lab Accession");
      doc.text(140, 89, "Date Collected");
      doc.text(140, 94, "Date Recieved");
      doc.text(140, 99, "Date Reported");
      doc.text(140, 104, "Clinic Order ID");
      doc.setTextColor(0, 0, 0);
      doc.text(164, 79, "565464543");
      doc.text(164, 84, element.LOINcode);
      doc.text(164, 89, element.DateCollected);
      doc.text(164, 94, element.DateReceived);
      doc.text(164, 99, element.DateRecevied);
      doc.text(164, 104, "564564564");
      doc.setDrawColor(0, 0, 0);
      doc.setLineWidth(0.2);
      doc.line(10, 110, 200, 110);// horizontal line
      doc.setFontSize(9);
      doc.setTextColor(0, 128, 128);
      doc.text(10, 118, "Additional Information");
      doc.setTextColor(100);
      doc.setFontSize(8.5);
      doc.text(10, 123, "Reason for Testing");
      doc.text(10, 128, "Collector's Name");
      doc.text(10, 133, "Collector's Phone");
      doc.text(10, 138, "Clinical Info");
      doc.text(10, 143, "Ordered Items");
      doc.setTextColor(0, 0, 0);
      doc.text(43, 123, "testpurpose");
      doc.text(43, 128, "");
      doc.text(43, 133, "");
      doc.text(43, 138, "");
      doc.text(43, 143, "");
      doc.setDrawColor(0, 0, 0);
      doc.setLineWidth(0.2);
      doc.line(10, 148, 200, 148);// horizontal line
      doc.setTextColor(100);
      doc.text(10, 155, "Test Performed");
      doc.text(70, 155, "Lab Result");
      doc.text(100, 155, "Lab Result");
      doc.text(125, 155, "Units");
      doc.text(145, 155, "Reference Interval");
      doc.text(178, 155, "Flag");
      doc.text(70, 158, "(Qualitative)");
      doc.text(100, 158, "(Observed)");
      doc.setDrawColor(0, 0, 0);
      doc.setLineWidth(0.3);
      doc.line(10, 160, 200, 160);// horizontal line
      doc.setFontSize(8.5);
      doc.text(10, 170, "Treatment related medications reported");
      doc.text(10, 175, "NOTES:Here are the notes for the above test");
      doc.line(10, 182, 200, 182);// horizontal line
      doc.text(70, 175, "Abnormal");
      doc.text(100, 175, "40");
      doc.text(125, 175, "Ng/mL");
      doc.text(145, 175, "");
      doc.text(178, 175, "");
      doc.text(10, 187, "Alpha-Hydroxylaprazolam(LCMSMS)");
      doc.text(10, 193, "NOTES:Not expected result");
      doc.text(70, 194, "Abnormal");
      doc.text(100, 194, "40");
      doc.text(125, 194, "Ng/mL");
      doc.text(145, 194, "");
      doc.text(178, 194, "");
      var file = doc.output('blob')
      this.GetPatientLabBasicDetails.push(file);
      if (this.GetFilteredPatientLabDetail.length == this.GetPatientLabBasicDetails.length) {
        this.genrateZip();
      }
    }
    )
  }

  genrateZip() {
    this.counter = 0;
    const OrganizationName = this.OrganizationName;
    if (this.GetPatientLabBasicDetails.length > 0) {
      this.GetPatientLabBasicDetails.forEach(element => {
        this.zip.file(this.makeid(7) + ".pdf", element, { base64: true });
      });
      this.zip.generateAsync({ type: "blob" }).then((content) => {
        // see FileSaver.js
        saveAs(content, OrganizationName + ".zip");
        // $('#PDFTemplateLab').css('display', 'none');
      });
    }
  }
  makeid(length) {
    debugger
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    text = this.PdfNames[this.counter];
    this.counter++;
    return text + this.counter;
  }
  DownloadPDFLab($event) {
    debugger
    this.PdfNames = [];
    this.counter = 0;
    this.GetPatientLabBasicDetails = [];
    this.GetFilteredPatientLabDetail = [];
    this.GetFilteredPatientLabDetail = _.filter(this.GetPatientChartBasicDetails,
      function (element) {
        return element.status == true;
      });
    this.GetFilteredPatientLabDetail.forEach(element => {
      this.PdfNames.push(element.FirstName + " " + element.LastName);
    });
    this.OrganizationName = this.GetFilteredPatientLabDetail[0].OrganizationName;
    this.genratePDFLab(this.GetFilteredPatientLabDetail);

  }




  fnLoadPatientLabData() {
    debugger
    // this.patientService.GetPatientDetailsLab(this.patientId).subscribe(response=>{
    // });
    this.ELEMENT_DATA = [];
    this.counter = 0;
    this.GetPatientChartBasicDetails = [];
    this.listOperands.PatientId = this.patientId;
    this.patientService.GetLabDetailsPatient(this.listOperands).subscribe(response => {
      debugger;
      console.log(response);
      if (response.status = 200 && response.result.Item1.length > 0) {
        response.result.Item1.forEach(element => {
          response.result.Item1[this.counter].status = false;
          this.counter++;
        });
        debugger;
        this.GetPatientChartBasicDetails = response.result.Item1;
       
        this.ELEMENT_DATA = this.GetPatientChartBasicDetails;
        this.dataSource = new MatTableDataSource<Element>(this.ELEMENT_DATA);
        // this.pageSize=response.result.Item1.length;
        this.length = this.GetPatientChartBasicDetails[0].TotalRows;
      }
      else {
        this.pageSize = 0;
        this.length=0;
        this.dataSource = new MatTableDataSource<Element>(this.ELEMENT_DATA);
      }
    });

  }

  getServerDataLab(pageData) {
    debugger;
    this.listOperands.offset = pageData.pageIndex + 1;
    this.listOperands.limit = pageData.pageSize;
    this.fnLoadPatientLabData();

  }

  sortDataLab(header, order) {
    debugger;
    this.listOperands.sort = header;
    this.listOperands.order = order;
    this.fnLoadPatientLabData();
  }

  getSearchedData() {
    this.listOperands.LabName = this.LabName == "" ? null : this.LabName;
    this.listOperands.OrderNo = this.OrderNo == "" ? null : this.OrderNo;
    this.listOperands.TestName = this.TestName == "" ? null : this.TestName;
    this.listOperands.FromDate = this.FromDate == undefined ? null : this.FromDate;
    this.listOperands.ToDate = this.ToDate == undefined ? null : this.ToDate;
    this.listOperands.HL7TransactionId = this.patientId;
    this.fnLoadPatientLabData();
  }

  //-----------------End-----------------------------------
  showHide(event) {
    debugger;
    if (this.fafahide == null) {
      this.fafashow = true;
      this.fafaanchor = false;
      this.fafahide = event;
    } else {
      this.fafahide = null;
      this.fafaanchor = true;
      this.fafashow = false;
    }
  }
  genratePDF() {
    this.loader.Start();
    var anum = this.deviceService.getDeviceInfo()
    // this.deviceInfo = this.deviceService.getDeviceInfo();
    localStorage.setItem('deviceInfo', anum.browser);
    // this.templateShow=true;
    debugger;
    // $('#claimTemplate').css('display','block');
    // this.img = "";
    // this.img = '../../../assets/img/qrcode.png';
    // console.log(this.parentClaimDetail);
    this.parentClaimDetail.forEach(element => {
      this.PatientName = element.patientName;
      this.PatientAddress = element.patientAddress;
      this.payerName = element.payerName;
      element.patientClaimChildrenDetails.forEach(elements => {
        this.charges = elements.Charges;
        this.clinicName = elements.ClinicName;
        this.diagnosisDate = elements.diagnosisDate;
        this.month = new Date(elements.diagnosisDate).getMonth().toString();
        this.year = new Date(elements.diagnosisDate).getFullYear().toString();
        this.day = new Date(elements.diagnosisDate).getDate().toString();
        this.modifier = elements.modifier;
        this.serviceCode = elements.serviceCode;
        this.serviceName = elements.ServiceName;
        this.unit = elements.Unit;
        this.areaCode = elements.contactNo.split('-')[0];
        this.phoneNumber = elements.contactNo.split('-')[1];
      });
    });
    this.claimDetail.PatientId = this.patientId;
    this.parentClaimDetail = [];
    this.claimDetail.OrganizationId = 71;
    this.patientService.GetPatientClaimData(this.claimDetail).subscribe(response => {
      debugger;
      if (response.length > 0) {
        //console.log(response)
        this.parentClaimDetail = response;
        this.Paging = true;
      }


      this.patientService.DownloadPdf(this.parentClaimDetail[0]).subscribe(response => {
        debugger;
        if (response.length > 0) {
          //console.log(response)
          debugger;
          // base64ToArrayBuffer(response)

          const linkSource = 'data:application/pdf;base64,' + response;
          const downloadLink = document.createElement("a");
          const fileName = "claims.pdf";

          downloadLink.href = linkSource;
          downloadLink.download = fileName;


          var deviceinfo = (localStorage.getItem('deviceInfo'));
          if (deviceinfo == "Firefox") {
            window.open($(downloadLink).attr('href'), '_blank');
          } else if (deviceinfo == "Chrome") {

            downloadLink.click();

          }



        }
        this.loader.Stop();
      }



      )


    });

    this.fafahide = 0;
    this.fafashow = true;
    this.fafaanchor = false;
    return this.parentClaimDetail;



  }
  setClaimData(): PatientClaim[] {
    debugger;
    this.claimDetail.PatientId = this.patientId;
    this.parentClaimDetail = [];
    this.claimDetail.OrganizationId = 71;
    this.patientService.GetPatientClaimData(this.claimDetail).subscribe(response => {
      debugger;
      //console.log(response);

      if(response!=null)
      {
        if (response.length > 0) {

          this.parentClaimDetail = response;
          this.Paging = true;
        }
      }



    });




    this.fafahide = 0;
    this.fafashow = true;
    this.fafaanchor = false;
    return this.parentClaimDetail;
  }

  getServerData(pageData) {
    debugger;
    this.claimDetail.offset = pageData.pageIndex + 1;
    this.claimDetail.limit = pageData.pageSize;
    this.setClaimData();

  }

  sortData(header, order) {
    debugger;
    this.claimDetail.sort = header;
    this.claimDetail.order = order;
    this.setClaimData();
  }

  //Go back URL
  backClicked() {
    this._location.back();
  }
  toggleclasstable(patientDiagId, patientDiagnosisId) {
    debugger;
    var oldresult = this.patientChartDetail.snomedCodeModel.filter(x => x.patientDiagnosisId == patientDiagId)[0];
    var newResult = this.patientChartDetail.snomedCodeModel.filter(x => x.patientDiagnosisId == patientDiagnosisId)[0];
    if (oldresult != undefined) {
      oldresult.isVisible = false;
      this.ptDiagnosisId = 0;

    }
    if (newResult != undefined) {
      newResult.isVisible = true;
      this.ptDiagnosisId = patientDiagnosisId;
    }
    // this.dstatus = !this.dstatus;
  }

}
