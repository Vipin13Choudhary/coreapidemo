import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganizationSetUpComponent } from './organization-set-up.component';

describe('OrganizationSetUpComponent', () => {
  let component: OrganizationSetUpComponent;
  let fixture: ComponentFixture<OrganizationSetUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganizationSetUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganizationSetUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
