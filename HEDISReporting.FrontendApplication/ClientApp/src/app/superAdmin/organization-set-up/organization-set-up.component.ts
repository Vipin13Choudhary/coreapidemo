import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import {
  statesDropDown, organizationDropDown, CountriesStates, listOperation, multiselectDropDown, Direction
  , Frequencies,
  AdminOrganizationTabs,
  TypeSave
} from '../../model/common/common';
import { OrganizationsService } from '../../service/organization/organizations.service';
import { Organization } from '../../model/Organization';
import { orgSftpModal } from '../../model/organization/orgSftpModal';
import { MatStepperNext, PageEvent, MatTableDataSource,MatSort,MatPaginator } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { department } from '../../model/organization/orgDepartment';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { organizationClinic } from '../../model/organization/organizationClinic';
import { organizationListFilter } from '../../model/organization/OrganizationListFilters';
import { UserService } from '../../service/user/user.service';
import { ActivatedRoute } from '@angular/router';
import { orgDataSourceModal, GetDataSourceSftp, ReceiveDataSource, GetDataSource } from '../../model/organization/orgDataSourceModal';
import * as _ from 'underscore';
import * as moment from 'moment';
import { elementStart } from '@angular/core/src/render3';
import { LoaderService } from '../../service/loader/loader.service';
import {Location} from '@angular/common';
//import {CalendarModule} from 'primeng/calendar';
//import {NgbTimeStruct} from '@ng-bootstrap/ng-bootstrap';



@Component({
  selector: 'app-organization-set-up',
  templateUrl: './organization-set-up.component.html',
  styleUrls: ['./organization-set-up.component.css']
})
export class OrganizationSetUpComponent implements OnInit {
  @ViewChild(MatStepperNext) stepNext: MatStepperNext;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sorting: MatSort;
  // GetTime = new Date();
  meridian = true;
  spinners = true;
  organizationForm: FormGroup;
  departmentForm: FormGroup;
  OrganizationDropDown: organizationDropDown[];
  StatesDropDown: statesDropDown[];
  VendorDropDown: multiselectDropDown[];
  RegionTypeDropDown: organizationDropDown[];
  clinicDropDown: organizationDropDown[];
  listOperands: any;
  ELEMENT_DATA_Department: Element[];
  ELEMENT_DATA_Sftp: Element[];
  ELEMENT_DATA_Clinic: Element[];
  length = 10;
  pageSize = 10;
  pageSizeOptions: number[] = [1, 5, 10, 20];
  displayedColumns = ['DepartmentName', 'ClinicName', 'DepartmentAddress', 'DepartmentPhoneNumber'];
  pageEvent: PageEvent;
  search: "";
  DepartmentList = [];
  dataSource_Department: any;
  dataSource_Sftp: any;
  SelectedTemplate: string;
  TabSequenceOrg: boolean = true;
  TabSequenceClinic: boolean = true;
  TabSequenceDept: boolean = true;
  TabSequenceDS: boolean = true;
  dataSource = [];
  SftAddNew: boolean = true;
  // modalRef_Sftp: BsModalRef;
  // modalRef_Department:BsModalRef;
  DepartmenetClinicDropDown: organizationDropDown[];
  // modalRef_Clinic:BsModalRef;
  modalRef: BsModalRef;
  search_Sftp: "";
  search_Clinic: "";
  search_Region: "";
  showList: boolean;
  showList_department: boolean;
  showList_clinic: boolean;
  showList_region: boolean
  countryStateDropDown: CountriesStates;
  IPAddress: string;
  sftpListOperands: any;
  regionListOperands: any;
  showAddRegion: boolean = false;
  dataSourceTypes: organizationDropDown[];
  vendorsList: organizationDropDown[];
  payorsList: organizationDropDown[];
  // organization Configuration model
  orgConfigModel: FormGroup;
  OrganizationId: any = 0;
  OrganizationRegionList = [];
  showValid: boolean = false;
  EditOrganization: boolean = false;
  EditRegion: number = -1;
  SelectedEditableRegion: number = 0;
  // dataSourceFromSettings={showHideform:false,buttonText:"",SubmitButtonText:"",DataSourceDropDownText:""};
  //dataSource=[];
  dataTypeformat: organizationDropDown[];
  acquisitionMethods: Organization
  dataTypeformatDisable: boolean;
  dataSourceFrequencies: organizationDropDown[];
  dataSourceFrequencyDays: organizationDropDown[];
  dataSourceWeeks: organizationDropDown[];
  organizationDataSource: orgDataSourceModal;
  // dataSourceFromSettings={showHideform:false,buttonText:"",SubmitButtonText:""};
  clinicFromSettings = { showHideform: false, buttonText: "", SubmitButtonText: "", IsEmpty: true };
  departmentFromSettings = { showHideform: false, buttonText: "", SubmitButtonText: "", IsEmpty: true };
  checkedDays: string;
  IsDataSourceEdit: boolean = false;
  checkUncheck: boolean = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup;
  Urlpattern = "https?://.+";
  SelecteddatasourceId: number = 0;
  //dataSourceFromSettings:{showHideform:boolean,buttonText:string,SubmitButtonText:string,IsEmpty:true};
  dataSourceFromSettings = { showHideform: false, buttonText: "", SubmitButtonText: "", IsEmpty: true };

  tt: string;

  OrganizationData: any;
  regionName: "";

  public postalMask = [/\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/]
  //public mask = ['+', /\d/, /\d/, ' ', '(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  public mask = [/\d/, ' ', '(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  public phoneMask = [/\d/, ' ', '(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  public portMask = [/\d/, /\d/, /\d/, /\d/, /\d/];
  organizationClinicModel: FormGroup;
  sftpListFilters: organizationListFilter;
  organizationSftpList = [];
  clinicListFilters: organizationListFilter;
  regionListFilters: organizationListFilter;
  departmentListFilters: organizationListFilter;
  organizationClinicList = [];
  clinicListOperands: any;
  dataSource_Clinic: any;
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
  IsRegionActive: false;
  timezone: any;
  placeholderString = 'Select timezone';
  countryIsoCode: any;
  editableOrganizationID: string = '0';
  SelectedClinicId: number = 0;
  SelectedDepartmentId: number = 0;
  DeleteItem: number = 0;
  SelectedTable: string = '';
  Pagetitle: string = 'Add Organization';
  NoRegionFound: boolean = false;
  getOrgConfigModel: FormGroup;
  receiveOrgConfigModel: FormGroup;
  receiveSelectedFrequencyDays = [];
  receiveOrgConfigModelDQ: FormGroup;
  getSelectedFrequencyDays = [];
  dataquerySelectedFrequencyDays = [];
  DataType = null;
  SourceOfData = null;
  AcquisitionMethod = 1;
  selectedDataSourceItems = "";
  DataSourceId = 0;
  LoginUserId: string;
  OrganizationTypeDropDown: organizationDropDown[];

  //Show tabs As Per conditions:
  ShowOrganizationTab: boolean = true;
  ShowDataSourcesTab: boolean = false;
  ShowClinicsTab: boolean = false;
  ShowDepartmentTab: boolean = false;
  ShowSupportedOrganizationsTab: boolean = false;
  manageSection: AdminOrganizationTabs;
  panelOpenState: boolean = true;

  constructor(private _location:Location, private loader:LoaderService,private fb: FormBuilder, private organizationService: OrganizationsService, private toastr: ToastrService,
    private modalService: BsModalService, private userService: UserService, private route: ActivatedRoute) {
    this.manageSection = new AdminOrganizationTabs();
    this.manageSection.Organization = true;
    this.manageSection.Clinic = false;
    this.manageSection.DataSource = false;
    this.manageSection.Department = false;
    this.userService.changeScreen("false")
    this.listOperands = new listOperation();
    this.organizationForm = fb.group({
      OrganizationName: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(50)])],
      OrganizationWebSite: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(250), Validators.pattern(this.Urlpattern)])],
      // OrganizationEmail:[null,Validators.compose([Validators.required, Validators.email])],
      OrganizationPhoneNumber: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(20)])],
      OrganizationFax: [null],
      OrganizationAddress: [null, Validators.compose([Validators.minLength(4), Validators.required, Validators.maxLength(200)])],
      PostalCode: [null, Validators.required],
      StateId: [null, Validators.required],
      City: [null, Validators.compose([Validators.minLength(0), Validators.required, Validators.maxLength(20)])],
      OrganizationType: [null, Validators.required],
      Vendor: [null],
      Address2: [null, Validators.compose([Validators.minLength(4), Validators.maxLength(200)])],
      ContactPersonName: [null, Validators.compose([Validators.minLength(4), Validators.maxLength(50)])],
      ContactPersonEmail: [null, Validators.compose([Validators.minLength(4), Validators.email, Validators.maxLength(20)])],
      ContactPersonPhonenumber: [null],
      RegionId: [null, Validators.required],
      CountryId: [null, Validators.required],
      OrganizationDisplayName: [null],
      OrganizationTaxID: [null, Validators.required]
    });

    this.getOrgConfigModel = fb.group({
      //Get Section
      'GetFrequency': [null, Validators.compose([Validators.required])],
      'GetTime': [new Date(), Validators.compose([Validators.required])],
      'GetFileType': [null, Validators.compose([Validators.required])],
      //List of days for week will be handled in respective modal
      'GetFrequencyDay': [null],
      'GetWeekOfDay': [null],
      'Host': [null, Validators.compose([Validators.required, Validators.minLength(0), Validators.maxLength(200)])],
      'Port': [null, Validators.compose([Validators.required, Validators.minLength(0), Validators.maxLength(5)])],
      'UserName': [null, Validators.compose([Validators.required, Validators.minLength(0), Validators.maxLength(200)])],
      'Password': [null, Validators.compose([Validators.required, Validators.minLength(0), Validators.maxLength(200)])],
      //Token is kept here for future of FHIR
    });
    this.receiveOrgConfigModel = fb.group({
      //receive section
      'ReceiveFrequency': [null, Validators.compose([Validators.required])],
      'ReceiveTime': [new Date()],
      'ReceiveDirectory': [null, Validators.compose([Validators.required])],
      'ReceiveFileType': [null, Validators.compose([Validators.required])],
      'ReceiveFrequencyDay': [null],
      'ReceiveWeekOfDay': [null],

    })

    this.receiveOrgConfigModelDQ = fb.group({
          //receive section
          'ReceiveFrequency': [null, Validators.compose([Validators.required])],
          'ReceiveTime': [new Date()],
          'ReceiveFrequencyDay': [null],
          'ReceiveWeekOfDay': [null],
          'DatabaseName': [null],
          'UserName': [null],
          'Password': [null]
    
        })

    this.departmentForm = fb.group({
      DepartmentName: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(50)])],
      ClinicId: [null, Validators.required],
      DepartmentEmail: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(4), Validators.maxLength(50)])],
      DepartmentPhoneNumber: [null, Validators.compose([Validators.required, Validators.minLength(0), Validators.maxLength(20)])],
      DepartmentFax: [null],
      // DepartmentAddress:[null,Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(500)])],
      // PostalCode:[null,Validators.required],
      // StateId:[null,Validators.required],
      // City:[null,Validators.compose([Validators.required, Validators.minLength(0), Validators.maxLength(20)])],
      // DepartmentAddress2:[null,Validators.compose([ Validators.minLength(4), Validators.maxLength(20)])],
      ContactPersonName: [null, Validators.compose([Validators.minLength(4), Validators.maxLength(20)])],
      ContactPersonEmail: [null, Validators.compose([Validators.minLength(4), Validators.email, Validators.maxLength(50)])],
      ContactPersonPhonenumber: [null],
    });

    this.organizationClinicModel = fb.group({
      ClinicName: [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(249)])],
      ClinicAddress1: [null, Validators.compose([Validators.required, Validators.minLength(0), Validators.maxLength(499)])],
      ClinicAddress2: [null, Validators.compose([Validators.minLength(0), Validators.maxLength(499)])],
      ClinicPhoneNumber: [null, Validators.compose([Validators.required, Validators.minLength(0), Validators.maxLength(20)])],
      ClinicFax: [null],
      RegionId: [null],
      StateId: [null, Validators.required],
      City: [null, Validators.compose([Validators.required, Validators.maxLength(149)])],
      PostalCode: [null, Validators.required],
      ClinicWebsite: [null, Validators.compose([Validators.minLength(0), Validators.maxLength(250), Validators.pattern(this.Urlpattern)])],
      OrganizationId: [null],
      ContactPerson: [null, Validators.compose([Validators.minLength(0), Validators.maxLength(249)])],
      ContactPersonEmail: [null, Validators.compose([Validators.email, Validators.maxLength(100)])],
      ContactPersonPhone: [null, Validators.compose([Validators.minLength(0), Validators.maxLength(20)])],
      ClinicDisplayName: [null],
      ClinicLocationId: [null]
    });
    //this.OrganizationId=1;
    this.sftpListFilters = new organizationListFilter();
    this.sftpListFilters.length = 5;
    this.sftpListFilters.pageSize = 10;
    this.sftpListFilters.pageSizeOptions = [5, 10, 20, 50];
    this.sftpListFilters.displayedColumns = ["Name", "DataSource", "MethodName", "FreTime", "Action"];
    this.sftpListFilters.pageEvent = PageEvent;
    this.sftpListOperands = new listOperation();
    this.sftpListOperands.offset = 1;
    this.sftpListOperands.limit = 10;
    this.sftpListOperands.order = "desc"
    this.sftpListOperands.sort = ""
    this.sftpListOperands.Search = ""
    this.sftpListOperands.AcquisitionMethodId = 0
    this.sftpListOperands.DataSourceId = 0
    this.sftpListOperands.OrganizationId = this.OrganizationId;
    //--Organization Clinic List Configuration
    this.clinicListFilters = new organizationListFilter();
    this.clinicListFilters.length = 5;
    this.clinicListFilters.pageSize = 10;
    this.clinicListFilters.pageSizeOptions = [5, 10, 20, 50];
    this.clinicListFilters.displayedColumns = ["clinicName", "ClinicAddress1", "clinicPhoneNumber", "ClinicFax", "ContactPerson", "Action"];
    this.clinicListFilters.pageEvent = PageEvent;
    this.clinicListOperands = new listOperation();
    this.clinicListOperands.offset = 1;
    this.clinicListOperands.limit = 10;
    this.clinicListOperands.order = "desc"
    this.clinicListOperands.sort = ""
    this.clinicListOperands.Search = ""
    this.clinicListOperands.OrganizationId = this.OrganizationId;
    this.regionListFilters = new organizationListFilter();
    this.regionListFilters.length = 5;
    this.regionListFilters.pageSize = 10;
    this.regionListFilters.pageSizeOptions = [5, 10, 20, 50];
    //this.regionListOperands.displayedColumns=["clinicName","clinicPhoneNumber","ClinicAddress1","ClinicFax","ContactPerson"];
    this.regionListFilters.pageEvent = PageEvent;
    this.regionListOperands = new listOperation();
    this.regionListOperands.offset = 1;
    this.regionListOperands.limit = 10;
    this.regionListOperands.order = "desc"
    this.regionListOperands.sort = ""
    this.regionListOperands.Search = ""
    this.regionListOperands.OrganizationId = this.OrganizationId;

    //department

    this.departmentListFilters = new organizationListFilter();
    this.departmentListFilters.length = 5;
    this.departmentListFilters.pageSize = 10;
    this.departmentListFilters.pageSizeOptions = [5, 10, 20, 50];
    this.departmentListFilters.pageEvent = PageEvent;
    this.listOperands.offset = 1;
    this.listOperands.limit = 10;
    this.listOperands.order = "desc";
    this.listOperands.sort = "";
    this.listOperands.Search = "";
    this.listOperands.ClinicId = 0;

    //editableOrganizationID:number=0;
    this.userService.organizationId.subscribe(message => this.editableOrganizationID = message);

    // var tt=this.editableOrganizationID

  }

  ngOnInit() {
    debugger;

    //this.dataSourceFromSettings.showHideform=false;
    this.LoginUserId = localStorage.getItem("LoginUserID");
    this.route.queryParams.subscribe(params => {
      if (params.id != undefined) {
        this.editableOrganizationID = params.id;
      }
      else {
        this.editableOrganizationID = '0';
        this.ShowDataSourcesTab = false;
        this.ShowOrganizationTab = true;
        this.ShowDepartmentTab = false;
        this.ShowClinicsTab = false;
        this.ShowSupportedOrganizationsTab = false;
      }

      this.initOrganization();
    })
    this.userService.changeScreen("false")

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
    this.countryIsoCode = 'US';
    this.GetDataSourceDropDowns();
    //this.dataSourceFromSettings=new this.dataSourceFromSettings();
    this.dataSourceFromSettings.buttonText = "Add New";
    this.dataSourceFromSettings.showHideform = false;
    this.dataSourceFromSettings.SubmitButtonText = "Save";

    this.GetIP();

  }
  toggleMeridian() {
    this.meridian = !this.meridian;
  }




  GetIP() {
    debugger
    this.organizationService.getIpAddress().subscribe(data => {
      debugger;
      // console.log(data); 
      let jObjGetIP = JSON.parse(data._body);
      this.IPAddress = jObjGetIP.ip;
      this.GetCountryCodeFromIP(this.IPAddress);
    });

  }

  GetCountryCodeFromIP(IPAddress) {
    debugger
    this.organizationService.getCountryCodeFromIP(IPAddress).subscribe(data => {
      debugger;
      //if (Number(this.editableOrganizationID) == 0)
      //this.organizationForm.controls["OrganizationPhoneNumber"].setValue(data.result.code);
      //this.organizationClinicModel.controls["ClinicPhoneNumber"].setValue(data.result.code);
      //this.departmentForm.controls["DepartmentPhoneNumber"].setValue(data.result.code);
    
    });
  }


  initOrganization() {


    //get editable organization form
    this.clinicListFilters.displayedColumns = ["clinicName", "ClinicAddress1", "clinicPhoneNumber", "ClinicFax", "ContactPerson", "Action"];
    // this.displayedColumns=["clinicName","clinicPhoneNumber","ClinicAddress1","ClinicFax","ContactPerson","Action"];
    this.displayedColumns = ['DepartmentName', 'ClinicName', 'DepartmentAddress', 'DepartmentPhoneNumber', 'Action'];

    if (this.editableOrganizationID != '0') {
      this.Pagetitle = '';
      this.getEditableOrganizationform();
      this.clinicListOperands.OrganizationId = this.editableOrganizationID;
      //this.getAllDropDown();

    }
    else {
      this.clinicListFilters.displayedColumns = ["clinicName", "ClinicAddress1", "clinicPhoneNumber", "ClinicFax", "ContactPerson", "Action"];

    }
    this.getAllDropDown();
    this.getCountriesStates(0);



    //      this.organizationForm.setValue({
    //     //  formControlName1: myValue1, 
    //     //  formControlName2: myValue2
    //  });


    //} 





  }
  getCountriesStates(countryId: any) {

    this.countryStateDropDown = new CountriesStates();
    this.countryStateDropDown.countries = new Array<organizationDropDown>();
    this.countryStateDropDown.states = new Array<organizationDropDown>();
    this.countryStateDropDown.region = new Array<organizationDropDown>();
    this.organizationService.GetCountriesStates(countryId).subscribe(
      response => {
        debugger;
        this.countryStateDropDown.countries = response.result.countries;
        this.countryStateDropDown.states = response.result.states;
        this.countryStateDropDown.region = response.result.regions;
        this.OrganizationTypeDropDown = response.result.organizationDropDown;
      }
    )
  }

  getStatesByCountry(countryId: any) {
    this.organizationService.GetCountriesStates(countryId).subscribe(
      response => {
        this.countryStateDropDown.states = response.result.states;
        this.countryStateDropDown.region = response.result.regions;
      })
  }
  getAllDropDown() {
    //  let obj:any={};
    //  obj.
    if (this.editableOrganizationID != '0') {
      this.OrganizationId = this.editableOrganizationID;
    }
    this.organizationService.getAllDropDown(localStorage.getItem("LoginUserID"), this.OrganizationId).subscribe(response => {


      this.RegionTypeDropDown = response.organizationRegion;
      this.OrganizationDropDown = response.organizationsList;
      this.StatesDropDown = response.statesList;
      this.VendorDropDown = response.vendorType;
      this.clinicDropDown = response.clinicList;

      //  var test=response.clinicList;
      //   this.DepartmenetClinicDropDown=test;
      this.DepartmenetClinicDropDown = [];
      this.DepartmenetClinicDropDown.push({ id: 0, name: 'All' });
      this.clinicDropDown.forEach(element => {
        this.DepartmenetClinicDropDown.push({ id: element.id, name: element.name });
      });

      this.VendorDropDown = [];
      response.vendorType.forEach(element => {
        this.VendorDropDown.push({ 'item_id': element.id, 'item_text': element.name });

      });

      //  this.clinicDropDown.shift();
    });
  }


  // save organization

  onOrganizationFormSubmit(orgModal: Organization, saveType) {
    debugger
    orgModal.UserId = 1; // this.organizationService.getOrganizationList(this.listOperands).subscribe(response=>{
    if (this.editableOrganizationID != '0') {
      orgModal.OrganizationId = parseInt(this.editableOrganizationID);
      this.organizationService.editOrganization(orgModal).subscribe(Response => {
        if (Response.status == 200) {
          this.sftpListOperands.OrganizationId = this.OrganizationId;
          this.clinicListOperands.OrganizationId = this.OrganizationId;
          this.OrganizationId = Response.result;
          if (saveType == 0)
            this.toastr.success('Organization detail auto updated successfully.', 'Success')
          else
            this.toastr.success('Organization detail updated successfully.', 'Success');
          this.getAllDropDown();
        }
        else {

          this.toastr.error('There is error in updating organization detail.Please try after sometime.', 'Error');
        }
      });


    } else {
      this.organizationService.saveOrganization(orgModal).subscribe(Response => {

        if (Response.status == 200) {
          this.OrganizationId = Response.result;
          this.sftpListOperands.OrganizationId = this.OrganizationId;
          this.clinicListOperands.OrganizationId = this.OrganizationId;
          if (this.OrganizationId > 0) {
            this.editableOrganizationID = this.OrganizationId;
            this.Pagetitle = '';
            this.EditOrganization = true;
          }
          if (saveType == 0)
            this.toastr.success('Organization detail auto saved successfully.', 'Success')
          else
            this.toastr.success('Organization detail saved successfully.', 'Success');
          this.fnSetNext();
          this.getAllDropDown();
        }
        else {
          this.toastr.error('There is error in saving organization detail.Please try after sometime.', 'Error');
        }
      });
    }

    return true;
  }



  ondepartmentFormSubmit(departmentModal: department, saveType) {
    //this.LoginUserId=localStorage.getItem("LoginUserID");

    departmentModal.UserId = parseInt(this.LoginUserId);
    if (this.SelectedDepartmentId > 0) {
      departmentModal.DepartmentId = this.SelectedDepartmentId;
      this.organizationService.saveOrganizationDepartment(departmentModal).subscribe(Response => {
        if (Response.status == 200) {
          //this.modalRef_Department.hide();
          this.getAllDepartmentList();
          this.departmentForm.reset();
          if (saveType == 0)
            this.toastr.success('Organization department detail autosaved successfully.', 'Success');
          else
            this.toastr.success('Organization department detail saved successfully.', 'Success');

          this.departmentFromSettings.buttonText = "Add New";
          this.departmentFromSettings.showHideform = false;
          this.departmentFromSettings.SubmitButtonText = "Save";
        }
        else {

          this.toastr.error('There is error in saving department detail.Please try after sometime.', 'Error');

        }

      });
    } else {

      this.organizationService.saveOrganizationDepartment(departmentModal).subscribe(Response => {
        if (Response.status == 200) {
          //this.modalRef_Department.hide();
          this.getAllDepartmentList();
          this.departmentForm.reset();
          this.toastr.success('Organization department  detail saved successfully.', 'Success');

          this.departmentFromSettings.buttonText = "Add New";
          this.departmentFromSettings.showHideform = false;
          this.departmentFromSettings.SubmitButtonText = "Save";
        }
        else {

          this.toastr.error('There is error in saving department detail.Please try after sometime.', 'Error');

        }

      });
    }

  }

  //get department list

  getAllDepartmentList() {
    debugger
    // if(!this.organizationClinicModel.valid)
    // {
    // this.stepNext._stepper.next();
    // this.stepNext._stepper.previous();
    //this.toastr.error('Please fill all the mandatory fields before proceeding to another tab.', 'Error'); 
    // }
    // else{
    if (this.TabSequenceDept == false && this.TabSequenceClinic == true && this.organizationClinicModel.valid) {

      this.onOrgClinicFormSubmit(this.organizationClinicModel.value, 0);
    }
    this.getAllDropDown();
    if (this.SelectedDepartmentId > 0) {
      this.SelectedDepartmentId = 0;
      this.departmentForm.reset();
    }
    // this.departmentFromSettings.showHideform=false;
    this.showList_department = false;
    this.ELEMENT_DATA_Department = [];
    this.DepartmentList = [];
    this.listOperands.OrganizationId = this.OrganizationId;
    this.organizationService.getDepartmentList(this.listOperands).subscribe(response => {
      if (response.length > 0) {
        this.showList_department = true;
        this.departmentListFilters.length = response[0].TotalRows;
        response.forEach(element => {
          this.DepartmentList.push({
            DepartmentId: element.DepartmentId, DepartmentName: element.DepartmentName,
            ClinicName: element.ClinicName, DepartmentAddress: element.DepartmentAddress,
            DepartmentPhoneNumber: element.DepartmentPhoneNumber, DepartmentEmail: element.DepartmentEmail,
            ContactPersonName: element.ContactPersonName
          });
        });

        this.departmentFromSettings.buttonText = "Add New";
        this.departmentFromSettings.showHideform = false;
        this.departmentFromSettings.SubmitButtonText = "Save";
        this.departmentFromSettings.IsEmpty = false;

      }
      else {
        //this.departmentFromSettings.pageSize = 0;
        if ((this.search == "" || this.search == undefined) && (this.listOperands.ClinicId == 0)) {
          this.departmentFromSettings.buttonText = "Back To List";
          this.departmentFromSettings.showHideform = true;
          this.departmentFromSettings.SubmitButtonText = "Save";
          this.departmentFromSettings.IsEmpty = true;;
          this.departmentListFilters.pageSize = 0;
        }
      }
      //  else{
      //   this.pageSize=0;
      //  }
      this.ELEMENT_DATA_Department = this.DepartmentList;
      this.dataSource_Department = new MatTableDataSource<Element>(this.ELEMENT_DATA_Department);
    });
    //}
  }

  // add department

  AddDepartment() {
    this.SelectedDepartmentId = 0;
    // let template: TemplateRef<any>=modelName;
    // this.modalRef_Department = this.modalService.show(template);
    // this.OrganizationId=element.OrganizationId;
    if (this.departmentFromSettings.showHideform == false) {
      this.departmentFromSettings.buttonText = "Back To List";
      this.departmentFromSettings.showHideform = true;
      this.departmentFromSettings.SubmitButtonText = "Save";
    }
    else {
      this.departmentFromSettings.buttonText = "Add New";
      this.departmentFromSettings.showHideform = false;
      this.departmentFromSettings.SubmitButtonText = "Save";
    }



  }

  //Paging
  setPageSizeOptions(setPageSizeOptionsInput: string) {
    this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
  }

  getServerData(pageData, type) {
    if (type == "Department") {
      this.listOperands.offset = pageData.pageIndex + 1;
      this.listOperands.limit = pageData.pageSize;
      this.getAllDepartmentList();
    }
    else if (type == "Sftp") {
      debugger;
      this.sftpListOperands.offset = pageData.pageIndex + 1;
      this.sftpListOperands.limit = pageData.pageSize;
      this.getOrganizationconfigurationList();
    }
    else if (type == "Clinic") {
      this.clinicListOperands.offset = pageData.pageIndex + 1;
      this.clinicListOperands.limit = pageData.pageSize;
      this.getOrganizationsClinicList();
    }
    else if (type == 'Region') {
      this.regionListOperands.offset = pageData.pageIndex + 1;
      this.regionListOperands.limit = pageData.pageSize;
      this.getOrganizationRegionList();

    }
    else if (type == 'Data Source') {
      this.sftpListOperands.offset = pageData.pageIndex + 1;
      this.sftpListOperands.limit = pageData.pageSize;
      this.getOrganizationconfigurationList();

    }
  }

  sortData(OrderType,header, order, type) {
    debugger
    if(OrderType.currentTarget.attributes.getNamedItem('aria-sort')!=null){
      OrderType=OrderType.currentTarget.attributes.getNamedItem('aria-sort').value;
    if(OrderType=="descending"){
      OrderType='desc';
    }
    else
    {
      OrderType='asc';
    }
      }
    else{
       this.loader.Stop();
         return;
     }
    debugger;
    if (type == 'Department') {
      this.listOperands.sort = header;
      this.listOperands.order = OrderType;
      this.getAllDepartmentList();
    }
    else if (type == "Sftp") {
      this.sftpListOperands.sort = header;
      this.sftpListOperands.order = OrderType;
      this.getOrganizationconfigurationList();
    }
    else if (type == "Clinic") {
      this.clinicListOperands.sort = header;
      this.clinicListOperands.order = OrderType;
      this.getOrganizationsClinicList();
    }
    else if (type == "Region") {
      this.regionListOperands.sort = header;
      this.regionListOperands.order = OrderType;
      this.getOrganizationRegionList();
    }

    else if (type == "Data Source") {
      this.sftpListOperands.sort = header;
      this.sftpListOperands.order = OrderType;
      this.getOrganizationconfigurationList();
    }

  }

  getSearchedData() {
    this.listOperands.Search = this.search;
    this.getAllDepartmentList();
  }

  //As per drop downs (further added on future)
  getFilteredList(ClinicId) {
    this.listOperands.ClinicId = ClinicId;
    this.getAllDepartmentList();
  }


  // closeAddDepartmentForm(){
  //   this.departmentForm.reset();
  //  // this.modalRef_Department.hide(); 
  // }
  //#region --Organization DataSource Settings
  AddOrganizationSftp() {

    if (this.dataSourceFromSettings.showHideform == false) {
      this.dataSourceFromSettings.buttonText = "Back To List";
      this.dataSourceFromSettings.showHideform = true;
      this.dataSourceFromSettings.SubmitButtonText = "Save";
    }
    else {
      this.dataSourceFromSettings.buttonText = "Add New";
      this.dataSourceFromSettings.showHideform = false;
      this.dataSourceFromSettings.SubmitButtonText = "Save";
    }
    this.getOrgConfigModel.reset();
    this.getOrgConfigModel.get('GetTime').setValue(new Date());
    this.receiveOrgConfigModel.reset();
    this.receiveOrgConfigModel.get('ReceiveTime').setValue(new Date());
    this.receiveOrgConfigModelDQ.reset();
    this.receiveOrgConfigModelDQ.get('ReceiveTime').setValue(new Date());
    this.DataType = 1;
    this.onDataSourceChange(1);
  }


  getSftpSearchedData() {
    this.sftpListOperands.Search = this.search_Sftp;
    this.getOrganizationconfigurationList();
  }

  isOptional = false;


  getOrganizationconfigurationList() {
    debugger;
    if (this.TabSequenceDept == true && this.TabSequenceDS == false && this.departmentForm.valid) {

      this.ondepartmentFormSubmit(this.departmentForm.value, 0);
    }
    if (this.TabSequenceOrg == true && this.TabSequenceDS == false && this.organizationForm.valid) {

      this.onOrganizationFormSubmit(this.organizationForm.value, 0);
    }
    this.showList = false;
    this.ELEMENT_DATA_Sftp = [];
    this.organizationSftpList = [];
    this.sftpListOperands.OrganizationId = this.OrganizationId;
    this.organizationService.GetOrganizationConfigList(this.sftpListOperands).subscribe(response => {
      debugger;
      if (response.length > 0) {
        debugger;
        this.showList = true;
        this.sftpListFilters.length = response[0].TotalRows;
        response.forEach(element => {
          this.organizationSftpList.push({
            Id: element.DataSourceId, Name: element.SourceOfData, DataSource: element.DataSource,
            MethodName: element.MethodName, FreTime: element.FreTime, Frequency: element.Frequency
          });
          //  this.searchOrgDropDown.push({id:element.OrganizationId,name:element.OrganizationName})
        });

      }
      else {
         this.sftpListFilters.pageSize = 0;
         this.dataSourceFromSettings.showHideform = true;
         this.SftAddNew = false;
      }
      debugger;
      this.ELEMENT_DATA_Sftp = this.organizationSftpList;
      this.dataSource_Sftp = new MatTableDataSource<Element>(this.ELEMENT_DATA_Sftp);
    });
    //}
  }

  //#endregion
  //Organzation Clinic--Start
  AddOrganizationClinic() {
    debugger
    this.SelectedClinicId = 0;
    if (this.clinicFromSettings.showHideform == false) {
      this.clinicFromSettings.buttonText = "Back To List";
      this.clinicFromSettings.showHideform = true;
      this.clinicFromSettings.SubmitButtonText = "Save";

    }
    else {
      this.clinicFromSettings.buttonText = "Add New";
      this.clinicFromSettings.showHideform = false;
      this.clinicFromSettings.SubmitButtonText = "Save";
    }

    // let template: TemplateRef<any>=modelName;
    // this.modalRef_Clinic = this.modalService.show(template);
  }
  resetOrganizationClinic() {
    this.organizationClinicModel.reset();
    // this.modalRef_Clinic.hide(); 
  }
  onOrgClinicFormSubmit(organizationClinicModel: organizationClinic, saveType) {
    debugger;
    if (this.SelectedClinicId > 0) {
      organizationClinicModel.OrganizationId = parseInt(this.editableOrganizationID);

      organizationClinicModel.CreatedBy = parseInt(this.LoginUserId);
      organizationClinicModel.ClinicId = this.SelectedClinicId;
      this.organizationService.saveOrganizationClinic(organizationClinicModel).subscribe(response => {

        if (response.status == 200) {
          if (saveType == 0)
            this.toastr.success('Organization Clinic detail auto updated successfully.', 'Success');
          else
            this.toastr.success('Organization Clinic detail updated successfully.', 'Success');

          this.clinicFromSettings.buttonText = "Add New";
          this.clinicFromSettings.showHideform = false;
          this.clinicFromSettings.SubmitButtonText = "Save";
          this.resetOrganizationClinic();
          this.getOrganizationsClinicList();
        }
        else {

          this.toastr.error('There is error in saving Clinic detail.Please try after sometime.', 'Error');

        }


      })
    }
    else {
      organizationClinicModel.OrganizationId = this.OrganizationId;
      organizationClinicModel.CreatedBy = parseInt(this.LoginUserId);;

      this.organizationService.saveOrganizationClinic(organizationClinicModel).subscribe(response => {

        if (response.status == 200) {
          this.toastr.success('Organization Clinic detail saved successfully.', 'Success');
          this.clinicFromSettings.buttonText = "Add New";
          this.clinicFromSettings.showHideform = false;
          this.clinicFromSettings.SubmitButtonText = "Save";
          this.resetOrganizationClinic();
          this.getOrganizationsClinicList();
        }
        else {

          this.toastr.error('There is error in saving Clinic detail.Please try after sometime.', 'Error');

        }


      })

    }
  }
  getClinicSearchedData() {
    this.clinicListOperands.Search = this.search_Clinic;
    this.getOrganizationsClinicList();
  }
  getOrganizationsClinicList() {
    debugger;

    if (this.TabSequenceOrg == true && this.TabSequenceClinic == false && this.organizationForm.valid) {
      this.onOrganizationFormSubmit(this.organizationForm.value, 0);

    }
    if (this.SelectedClinicId > 0) {
      this.SelectedClinicId = 0;
      this.organizationClinicModel.reset();
    }
    this.getAllDropDown();
    this.showList_clinic = false;
   
    this.organizationService.GetOrganizationsClinicList(this.clinicListOperands).subscribe(response => {
      debugger;
      this.ELEMENT_DATA_Clinic = [];
      this.organizationClinicList = [];
      if (response.length > 0) {
        debugger
        this.showList_clinic = true;;
        this.clinicListFilters.length = response[0].TotalRecords;
        response.forEach(element => {
          this.organizationClinicList.push({
            clinicId: element.clinicId,
            clinicName: element.clinicName,
            clinicPhoneNumber: element.clinicPhoneNumber,
            ClinicAddress1: element.ClinicAddress1,
            ClinicFax: element.ClinicFax != null ? element.ClinicFax : 0,
            ContactPerson: element.ContactPerson,
            ContactPersonEmail: element.ContactPersonEmail,

            Address2Row:element.Address2Row,
            contactPersonPhone: element.contactPersonPhone ? element.contactPersonPhone : 0,

          });
        });
        //clinic
        this.clinicFromSettings.buttonText = "Add New";
        this.clinicFromSettings.showHideform = false;
        this.clinicFromSettings.SubmitButtonText = "Save";
        this.clinicFromSettings.IsEmpty = false;

      }
      else {
        //   this.OrganizationData.forEach(element => {
        //     this.organizationClinicList.push({
        //       // clinicId:element.clinicId,
        //       // clinicName: element.clinicName,
        //       clinicPhoneNumber: element.organizationPhoneNumber,
        //       ClinicAddress1: element.organizationAddress,
        //       ClinicFax:element.organizationFax,
        //       // ContactPerson:element.ContactPerson,
        //       // ContactPersonEmail:element.ContactPersonEmail,
        //       // contactPersonPhone:element.contactPersonPhone

        //     });
        // });
        // if(this.organizationClinicList.length == 0)
        // {
        // this.organizationClinicModel.setValue
        // ({
        //   ClinicAddress1:this.OrganizationData.clinicAddress,
        //   ClinicAddress2:this.OrganizationData.address2,
        //   ClinicPhoneNumber:this.OrganizationData.organizationPhoneNumber,
        //   ClinicFax:this.OrganizationData.organizationFax,
        //   PostalCode:this.OrganizationData.postalCode,
        //   StateId:this.OrganizationData.stateId,
        //   City:this.OrganizationData.city

        //  })
        //}

        if (this.search_Clinic == "" || this.search_Clinic == undefined) {
          this.clinicListFilters.pageSize = 0;
          this.clinicFromSettings.buttonText = "Back To List";
          this.clinicFromSettings.showHideform = true;
          this.clinicFromSettings.SubmitButtonText = "Save";
          this.clinicFromSettings.IsEmpty = true;
        }
      }

      this.ELEMENT_DATA_Clinic = this.organizationClinicList;
      this.dataSource_Clinic = new MatTableDataSource<Element>(this.ELEMENT_DATA_Clinic);
    });
    //}
  }

  onItemSelect(item: any) {
  }
  onSelectAll(items: any) {
  }
  // GetDataSourceDropDowns()
  // {

  //   this.organizationService.GetDataSourceDropDowns(1).subscribe(response=>{
  //     this.dataSourceTypes=response.Item1;
  //     this.vendorsList=response.Item2;
  //     this.payorsList=response.Item3;
  //     // veGetDataSourceDropDownsndorsList;
  //     // payorsList;
  //   })
  // }
  beforeSelectionChange(event) {

  }


  //region get oragnization region list

  getOrganizationRegionList() {


    // this.regionListOperands=new listOperation();
    this.regionListOperands.OrganizationId = this.OrganizationId;
    this.regionListOperands.IsActive = "";
    this.showList_region = false;
    this.ELEMENT_DATA_Department = [];
    this.OrganizationRegionList = [];
    this.organizationService.getOrganizationRegionList(this.regionListOperands).subscribe(response => {
      if (response.length > 0) {

        this.NoRegionFound = false;
        this.showList_region = true;
        this.regionListFilters.length = response[0].TotalRows;
        response.forEach(element => {
          this.OrganizationRegionList.push({
            OrganizationRegionId: element.OrganizationRegionId,
            RegionName: element.RegionName,
            IsActivate: element.IsActivate, IsDeleted: element.IsDeleted
          });
        });


      }
      else {
        this.NoRegionFound = true;
        this.regionListFilters.pageSize = 0;
      }

    });
  }

  AddNewRegion() {
    this.showAddRegion = true;
    this.regionName = "";
  }

  saveOrganizationRegion(regionName, Active) {

    if (Active == undefined) {
      Active = false;
    }

    if (regionName != '' && regionName != undefined) {
      var regionDetail: any = {};
      regionDetail.RegionName = regionName;
      regionDetail.IsActivate = Active;
      regionDetail.OrganizationId = this.OrganizationId;
      regionDetail.AspNetUserId = localStorage.getItem("LoginUserID");
      regionDetail.RegionId = 0
      if (this.EditRegion >= 0) {
        regionDetail.RegionId = this.SelectedEditableRegion
      }

      this.organizationService.saveOrganizationRegion(regionDetail).subscribe(response => {
        if (response.result > 0) {
          this.showAddRegion = false;
          this.getOrganizationRegionList();
          if (this.EditRegion >= 0) {
            this.toastr.success('Organization region updated successfully.', 'Success');
            this.EditRegion = -1;
          } else {
            this.toastr.success('Organization region saved successfully.', 'Success');
          }
        }
        else {
          this.toastr.error('There is error in saving region.Please try after sometime.', 'Error');

          this.pageSize = 0;
        }

      });
    }
    else {
      this.showValid = true;
      this.toastr.warning('Region is required !');
    }
  }

  CancelSave() {
    if (this.EditRegion == -1) {
      this.showAddRegion = false;
    }
    else {
      this.EditRegion = -1;
      this.getOrganizationRegionList();
    }
  }

  getRegionSearchedData() {

    this.regionListOperands.Search = this.search_Region;
    this.getOrganizationRegionList();
  }


  changeTimezone(timezone) {
    // this.user=
    this.timezone = timezone;
  }
  // pageSizeOptionRegion(setPageSizeOptionsInput: string) {
  //   this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
  // }


  //Edit form

  getEditableOrganizationform() {
    debugger;
    this.EditOrganization = true;
    //this.OrganizationId=this.editableOrganizationID;
    this.organizationService.getOrganizationDetailById(this.editableOrganizationID).subscribe(
      Response => {
        debugger;
        let selectedOrganization = Response.result.model[0];
        this.OrganizationData = Response.result.model;

        this.showTabsAsPerType(selectedOrganization.organizationType);
        debugger;
        this.organizationService.GetCountriesStates(selectedOrganization.countryId).subscribe(
          response => {
            this.countryStateDropDown.states = response.result.states;
            this.countryStateDropDown.region = response.result.regions;
            //  this.organizationTypeDropDown=response.OrganizationTypeList;

            // setTimeout(
            this.organizationForm.setValue
              ({              
                OrganizationName: selectedOrganization.organizationName,
                OrganizationWebSite: selectedOrganization.organizationWebSite,
                OrganizationPhoneNumber: selectedOrganization.organizationPhoneNumber,
                OrganizationFax: selectedOrganization.organizationFax,
                OrganizationAddress: selectedOrganization.organizationAddress,
                PostalCode: selectedOrganization.postalCode,
                StateId: selectedOrganization.stateId,
                City: selectedOrganization.city,
                Vendor: Response.result.vendor,
                Address2: selectedOrganization.address2,
                ContactPersonName: selectedOrganization.contactPersonName,
                ContactPersonEmail: selectedOrganization.contactPersonEmail,
                ContactPersonPhonenumber: selectedOrganization.contactPersonPhonenumber,
                RegionId: parseInt(selectedOrganization.regionId),
                CountryId: selectedOrganization.countryId,
                OrganizationType: parseInt(selectedOrganization.organizationType),
                OrganizationTaxID: selectedOrganization.organizationTaxID,
                OrganizationDisplayName: selectedOrganization.organizationDisplayName

              })
            //,
            //  1000);

          })

        this.organizationForm.patchValue({
          Vendor: Response.result.vendor,
          StateId: selectedOrganization.stateId,
          RegionId: parseInt(selectedOrganization.regionId),
          //  OrganizationPhoneNumber:"34523452345"
          // OrganizationType:parseInt(selectedOrganization.OrganizationType)
        });
        //this.organizationForm.controls["OrganizationPhoneNumber"].setValue(selectedOrganization.organizationPhoneNumber);
        //   this.getStatesByCountry(selectedOrganization.countryId);





      }


    )
  }

  getEditableOrganizationClinicform(element) {
    debugger;
    this.EditOrganization = true;

    //  this.clinicFromSettings.SubmitButtonText="Save";
    //this.OrganizationId=this.editableOrganizationID;
    this.organizationService.GetOrganizationClinicDetailById(element.clinicId).subscribe(
      Response => {

        this.SelectedClinicId = element.clinicId;
        let selectedClinic = Response.result;
        debugger;

        //  this.organizationService.getAllDropDown(localStorage.getItem("LoginUserID"),this.OrganizationId ).subscribe(response=>{


        //    this.RegionTypeDropDown=response.organizationRegion;
        //    this.StatesDropDown=response.statesList;


        this.organizationClinicModel.setValue
          ({
            ClinicName: selectedClinic.clinicName,
            ClinicAddress1: selectedClinic.clinicAddress1,
            ClinicAddress2: selectedClinic.clinicAddress2,
            ClinicPhoneNumber: selectedClinic.clinicPhoneNumber,
            ClinicFax: selectedClinic.clinicFax,
            PostalCode: selectedClinic.postalCode,
            StateId: selectedClinic.stateId,
            City: selectedClinic.city,
            ClinicWebsite: selectedClinic.clinicWebsite,
            OrganizationId: selectedClinic.organizationId,
            ContactPerson: selectedClinic.contactPerson,
            ContactPersonEmail: selectedClinic.contactPersonEmail,
            ContactPersonPhone: selectedClinic.contactPersonPhone,
            RegionId: parseInt(selectedClinic.regionId),
            ClinicDisplayName: selectedClinic.clinicDisplayName,
            ClinicLocationId: selectedClinic.clinicLocationId
          })


        this.clinicFromSettings.buttonText = "Back To List";
        this.clinicFromSettings.showHideform = true;
        this.clinicFromSettings.SubmitButtonText = "Update";
        this.clinicFromSettings.IsEmpty = true;;


        this.organizationClinicModel.patchValue({
          StateId: selectedClinic.stateId
        });
        // })
        //   this.getStatesByCountry(selectedOrganization.countryId);





      }


    )
  }

  getEditableOrganizationDepartmentform(element) {

    this.EditOrganization = true;
    //this.OrganizationId=this.editableOrganizationID;
    this.organizationService.GetOrganizationDepartmentDetailById(element.DepartmentId).subscribe(
      Response => {

        this.SelectedDepartmentId = element.DepartmentId;
        let selectedDepartment = Response.result;


        this.departmentForm.setValue
          ({
            DepartmentName: selectedDepartment.departmentName,
            ClinicId: selectedDepartment.clinicId,
            DepartmentEmail: selectedDepartment.departmentEmail,
            DepartmentPhoneNumber: selectedDepartment.departmentPhoneNumber,
            DepartmentFax: selectedDepartment.departmentFax,
            ContactPersonName: selectedDepartment.contactPersonName,
            ContactPersonEmail: selectedDepartment.contactPersonEmail,
            ContactPersonPhonenumber: selectedDepartment.contactPersonPhonenumber,



          })

        this.departmentFromSettings.buttonText = "Back To List";
        this.departmentFromSettings.showHideform = true;
        this.departmentFromSettings.SubmitButtonText = "Update";
        this.departmentFromSettings.IsEmpty = true;;


        //   this.getStatesByCountry(selectedOrganization.countryId);





      }


    )
  }



  DeleteTemplate(item, element, section) {

    this.SelectedTemplate = section;
    let template: TemplateRef<any> = element;
    this.modalRef = this.modalService.show(template);
    if (section == 'department') {
      this.DeleteItem = item.DepartmentId;
      this.SelectedTable = 'OrganizationDepartment';
    }
    else if (section == 'clinic') {
      this.DeleteItem = item.clinicId;
      this.SelectedTable = 'OrganizationClinics';
    }
    else if (section == 'region') {
      this.DeleteItem = item.OrganizationRegionId;
      this.SelectedTable = 'OrganizationRegion';
    }
    else if (section == 'data Source') {
      this.DeleteItem = item.Id;
      this.SelectedTable = 'OrganizationDataSource';
    }
  }




  deleteSeletedItem() {



    // let obj:any={};

    // obj.Id=DeleteItem;
    // obj.TableName=SelectedTable;
    // obj.UserId=1;
    this.organizationService.deleteSelectedItem(this.DeleteItem, this.SelectedTable).subscribe(response => {

      if (response.status == 200) {
        this.toastr.success(this.SelectedTemplate + " " + 'deleted successfully.', 'Success');
        if (this.SelectedTemplate == "department")
          this.getAllDepartmentList();
        else if (this.SelectedTemplate == "clinic")
          this.getOrganizationsClinicList();
        else if (this.SelectedTemplate == 'region')
          this.getOrganizationRegionList();
        else if (this.SelectedTemplate == 'data Source')
          this.getOrganizationconfigurationList();
        // this.getAllDepartmentList ();

        this.modalRef.hide();

      }// this.OrganizationDropDown.push({id:6,name:'All'})
      else
        this.toastr.error('There is error in deleting ' + this.SelectedTemplate + '.Please try after sometime.', 'Error');

    });
  }

  openEditOrganizationRegion(item, i) {

    this.EditRegion = i;
    this.SelectedEditableRegion = item.OrganizationRegionId;
    //this.EditRegionName=item.RegionName;
  }
  updateOrganizationRegion(item) {
    this.saveOrganizationRegion(item.RegionName, item.IsActivate);

  }

  onDataSourceSubmit(sftpModal, type) {
    debugger;
    this.organizationDataSource = new orgDataSourceModal();
    this.organizationDataSource.GetDataSource = new GetDataSource();

    this.organizationDataSource.ReceiveDataSource = new ReceiveDataSource();
    this.organizationDataSource.DataType = this.DataType;
    this.organizationDataSource.SourceOfData = this.SourceOfData;
    this.organizationDataSource.AcquisitionMethod = this.AcquisitionMethod;
    if (this.IsDataSourceEdit == true) {
      this.organizationDataSource.DataSourceId = this.SelecteddatasourceId;
    }
    // if(this.editableOrganizationID!='0')
    // this.organizationDataSource.OrganizationId=parseInt(this.editableOrganizationID);
    // else
    this.organizationDataSource.OrganizationId = parseInt(this.OrganizationId);
    this.organizationDataSource.CreatedBy = parseInt(this.LoginUserId);;
    this.organizationDataSource.IsEdit = this.IsDataSourceEdit;
    if (this.IsDataSourceEdit == true) {
      if (this.checkUncheck == false) {
        if (this.checkedDays.indexOf(",") > 0) {
          this.checkedDays.split(",").forEach(element => {
            if (type == "receive") {

              this.receiveSelectedFrequencyDays.push(parseInt(element));
            }
            if (type == "get")
              this.getSelectedFrequencyDays.push(parseInt(element))
              if (type == "dataquery")
                 debugger;
               this.dataquerySelectedFrequencyDays.push(parseInt(element))
          });

          this.getSelectedFrequencyDays = _.uniq(this.getSelectedFrequencyDays);
          this.receiveSelectedFrequencyDays = _.uniq(this.receiveSelectedFrequencyDays);
          this.dataquerySelectedFrequencyDays = _.uniq(this.dataquerySelectedFrequencyDays);
        }
      }
      //  this.checkedDays="";

    }
    if (type == "get") {
      this.organizationDataSource.GetDataSourceSftp = new GetDataSourceSftp();
      this.organizationDataSource.ReceiveDataSource = null;
      this.organizationDataSource.GetDataSource.Frequency = sftpModal.GetFrequency;
      let selectedFrequency = _.filter(this.dataSourceFrequencies, function (data) { return data.id == sftpModal.GetFrequency; })[0];

      if (selectedFrequency.name == Frequencies[Frequencies.Weekly]) {
        if (this.getSelectedFrequencyDays.length == 0) {
          this.toastr.warning("Please select days for weekly frequency.")
          return false;
        }
        this.organizationDataSource.GetDataSource.FrequencyDays = this.getSelectedFrequencyDays.join(",");
        this.organizationDataSource.GetDataSource.FrequencyWeek = "";

      }
      else if (selectedFrequency.name == Frequencies[Frequencies.Monthly]) {
        if (sftpModal.GetFrequencyDay == null || sftpModal.GetWeekOfDay == null) {
          this.toastr.warning("Please select day and week number  for Monthly frequency.")
          return false;
        }
        this.organizationDataSource.GetDataSource.FrequencyDays = sftpModal.GetFrequencyDay;
        this.organizationDataSource.GetDataSource.FrequencyWeek = sftpModal.GetWeekOfDay;
      }
      else {
        this.organizationDataSource.GetDataSource.FrequencyDays = "";
        this.organizationDataSource.GetDataSource.FrequencyWeek = "";
      }

      this.organizationDataSource.GetDataSourceSftp.Host = sftpModal.Host;
      this.organizationDataSource.GetDataSourceSftp.Port = sftpModal.Port;
      this.organizationDataSource.GetDataSourceSftp.UserName = sftpModal.UserName;
      this.organizationDataSource.GetDataSourceSftp.Password = sftpModal.Password;
      this.organizationDataSource.GetDataSource.FileType = this.dataTypeformat[0].id;
      // if(typeof(sftpModal.GetTime)!="object"){
      //   this.organizationDataSource.GetDataSource.FrequencyTime=(sftpModal.GetTime).toString();

      //   }else{
      //     this.organizationDataSource.GetDataSource.FrequencyTime=(sftpModal.GetTime.getHours()+":"+sftpModal.GetTime.getMinutes()).toString();

      //   } 
      this.organizationDataSource.GetDataSource.FrequencyTime =
        moment(sftpModal.GetTime, 'h:mm:ss').format('h:mm:ss');
      //   this.organizationDataSource.GetDataSource.FrequencyTime=(sftpModal.GetTime.getHours()+":"+sftpModal.GetTime.getMinutes()).toString();
      //this.organizationDataSource.GetFileType
      //--set form and same
    }
    else if (type == "receive") {
      this.organizationDataSource.GetDataSource = null;

      let selectedFrequency = _.filter(this.dataSourceFrequencies, function (data) { return data.id == sftpModal.ReceiveFrequency; })[0];

      if (selectedFrequency.name == Frequencies[Frequencies.Weekly]) {
        if (this.receiveSelectedFrequencyDays.length == 0) {
          this.toastr.warning("Please select days for weekly frequency.")
          return false;
        }
        this.organizationDataSource.ReceiveDataSource.FrequencyDays = this.receiveSelectedFrequencyDays.join(",");
        this.organizationDataSource.ReceiveDataSource.FrequencyWeek = "";

      }
      else if (selectedFrequency.name == Frequencies[Frequencies.Monthly]) {
        if (sftpModal.ReceiveFrequencyDay == null || sftpModal.ReceiveWeekOfDay == null) {
          this.toastr.warning("Please select day and week number  for Monthly frequncy.")
          return false;
        }
        this.organizationDataSource.ReceiveDataSource.FrequencyDays = sftpModal.ReceiveFrequencyDay;
        this.organizationDataSource.ReceiveDataSource.FrequencyWeek = sftpModal.ReceiveWeekOfDay;
      }
      else {
        this.organizationDataSource.ReceiveDataSource.FrequencyDays = "";
        this.organizationDataSource.ReceiveDataSource.FrequencyWeek = "";
      }
      //--set form fields and save
      this.organizationDataSource.ReceiveDataSource.Directory = sftpModal.ReceiveDirectory;
      this.organizationDataSource.ReceiveDataSource.Frequency = sftpModal.ReceiveFrequency;
      // if(typeof(sftpModal.ReceiveTime)!="object"){
      // this.organizationDataSource.ReceiveDataSource.FrequencyTime= (sftpModal.ReceiveTime).toString();
      // }else{
      //   this.organizationDataSource.ReceiveDataSource.FrequencyTime= (sftpModal.ReceiveTime.hour+":"+sftpModal.ReceiveTime.minute).toString();
      // }
      //moment(sftpModal.ReceiveTime,'h:mm:ss')
      debugger;

      this.organizationDataSource.ReceiveDataSource.FrequencyTime = moment(sftpModal.ReceiveTime, 'h:mm:ss').format('h:mm:ss');
      this.organizationDataSource.ReceiveDataSource.FileType = this.dataTypeformat[0].id;

    }
    else if (type == "dataquery") {
       debugger;
       this.organizationDataSource.GetDataSource = null;
     
       let selectedFrequency = _.filter(this.dataSourceFrequencies, function (data) { return data.id == sftpModal.ReceiveFrequency; })[0];
       this.organizationDataSource.ReceiveDataSource.Frequency=sftpModal.ReceiveFrequency;
       if (selectedFrequency.name == Frequencies[Frequencies.Weekly]) {
         if (this.dataquerySelectedFrequencyDays.length == 0) {
           this.toastr.warning("Please select days for weekly frequency.")
           return false;
         }
         this.organizationDataSource.ReceiveDataSource.FrequencyDays = this.dataquerySelectedFrequencyDays.join(",");
         this.organizationDataSource.ReceiveDataSource.FrequencyWeek = "";
       }
       else if (selectedFrequency.name == Frequencies[Frequencies.Monthly]) {
         if (sftpModal.ReceiveFrequencyDay == null || sftpModal.ReceiveWeekOfDay == null) {
           this.toastr.warning("Please select day and week number  for Monthly frequncy.")
           return false;
         }
         this.organizationDataSource.ReceiveDataSource.FrequencyDays = sftpModal.ReceiveFrequencyDay;
         this.organizationDataSource.ReceiveDataSource.FrequencyWeek = sftpModal.ReceiveWeekOfDay;
       }
       else {
         this.organizationDataSource.ReceiveDataSource.FrequencyDays = "";
         this.organizationDataSource.ReceiveDataSource.FrequencyWeek = "";
       }
       //--set form fields and save
       // this.organizationDataSource.ReceiveDataSource.Directory = sftpModal.ReceiveDirectory;
       // this.organizationDataSource.ReceiveDataSource.Frequency = sftpModal.ReceiveFrequency;
       // if(typeof(sftpModal.ReceiveTime)!="object"){
       // this.organizationDataSource.ReceiveDataSource.FrequencyTime= (sftpModal.ReceiveTime).toString();
       // }else{
       //   this.organizationDataSource.ReceiveDataSource.FrequencyTime= (sftpModal.ReceiveTime.hour+":"+sftpModal.ReceiveTime.minute).toString();
       // }
       //moment(sftpModal.ReceiveTime,'h:mm:ss')
       debugger;
     
       this.organizationDataSource.ReceiveDataSource.FrequencyTime = moment(sftpModal.ReceiveTime, 'h:mm:ss').format('h:mm:ss');
       this.organizationDataSource.ReceiveDataSource.FileType = this.dataTypeformat[0].id;
       this.organizationDataSource.ReceiveDataSource.DatasourceName=sftpModal.DatabaseName;
       this.organizationDataSource.ReceiveDataSource.Username=sftpModal.UserName;
       this.organizationDataSource.ReceiveDataSource.Password=sftpModal.Password;
     
     }
    //save sftp data source
    this.organizationService.SaveOrganizationConfiguration(this.organizationDataSource).subscribe(response => {
      debugger;
      if (response.result.dataSourceId > 0) {
        this.showAddRegion = false;
        // this.getOrganizationRegionList();
        if (this.IsDataSourceEdit == false) {
          this.toastr.success('Data source detail saved successfully.', 'Success');
          this.getOrganizationconfigurationList();
          // this.EditSftp=-1; 
        } else {
          this.getOrganizationconfigurationList();
          this.toastr.success('Data source detail updated successfully.', 'Success');
        }
      }
      else {
        if (this.IsDataSourceEdit == false)
          this.toastr.error('There is error in saving region.Please try after sometime.', 'Error');
        else
          this.toastr.error('There is error in updating region.Please try after sometime.', 'Error');
        this.pageSize = 0;
      }
      this.resetSftpConfiguration();
      //this.AddOrganizationSftp();

    });



  }
  resetSftpConfiguration() {
    debugger;
    this.getOrgConfigModel.reset();
    this.getOrgConfigModel.get('GetTime').setValue(new Date());
    this.receiveOrgConfigModel.reset();
    this.receiveOrgConfigModel.get('ReceiveTime').setValue(new Date());
    this.DataType = 1;
    this.onDataSourceChange(1);
    debugger;
    this.dataSourceFromSettings.buttonText = "Add New";
    this.dataSourceFromSettings.showHideform = false;
    this.dataSourceFromSettings.SubmitButtonText = "Save";

  }
  GetDataSourceDropDowns() {

    this.organizationService.GetDataSourceDropDowns(0).subscribe(response => {


      this.dataSourceTypes = response.Item1;
      this.vendorsList = response.Item2;
      this.payorsList = response.Item3;
      //this.dataTypeformat=response.Item4;
      this.acquisitionMethods = response.Item4;


      this.DataType = 1;
      this.onDataSourceChange(1);
    })
    this.organizationService.GetDataSourceFrequencyAndDays().subscribe(response => {
      this.dataSourceFrequencies = response.frequencies;
      this.dataSourceFrequencyDays = response.frequencyDays;
      this.dataSourceWeeks = response.weeks
    })
  }
  onDataSourceChange(selectedSource) {


    let selectedDataType = _.filter(this.dataSourceTypes, function (data) { return data.id == selectedSource; })[0];
    this.selectedDataSourceItems = selectedDataType.name;
    //&& (selectedDataType.name !=Direction.OtherCCD)
    if ((selectedDataType.name != Direction.CCDA)) {
      this.toastr.warning("This feature is under development.");
      this.dataSource = [];
    }
    else {
      this.dataSource = this.vendorsList;

      this.organizationService.GetDataSourceDropDowns(selectedDataType.id).subscribe(response => {

        this.dataTypeformat = response.Item1;
        if (this.dataTypeformat.length == 1) {
          this.receiveOrgConfigModel.get('ReceiveFileType').enable();
          this.receiveOrgConfigModel.get('ReceiveFileType').setValue(this.dataTypeformat[0].id);
          this.receiveOrgConfigModel.get('ReceiveFileType').disable();
          this.getOrgConfigModel.get('GetFileType').enable();
          this.getOrgConfigModel.get('GetFileType').setValue(this.dataTypeformat[0].id);
          this.getOrgConfigModel.get('GetFileType').disable();
        }
        else {


          this.receiveOrgConfigModel.get('ReceiveFileType').enable();
          this.receiveOrgConfigModel.get('ReceiveFileType').reset();
          this.getOrgConfigModel.get('GetFileType').enable();
          this.getOrgConfigModel.get('GetFileType').reset();

        }




      })


    }
  }

  checkUncheckFrequencyDays(event, itemCheck, sectionType) {
    if (this.IsDataSourceEdit == true) {
      if (this.checkedDays.indexOf(",") > 0) {
        this.checkUncheck = true;
        this.checkedDays.split(",").forEach(element => {
          if (sectionType == "receive") {

            this.receiveSelectedFrequencyDays.push(parseInt(element));
          }
          if (sectionType == "get")
            this.getSelectedFrequencyDays.push(parseInt(element))
          if (sectionType == "dataquery")
            this.dataquerySelectedFrequencyDays.push(parseInt(element));
            
        });

        this.getSelectedFrequencyDays = _.uniq(this.getSelectedFrequencyDays);
        this.receiveSelectedFrequencyDays = _.uniq(this.receiveSelectedFrequencyDays);
        this.dataquerySelectedFrequencyDays = _.uniq(this.dataquerySelectedFrequencyDays);
      }
      //  this.checkedDays="";

    }
    if (sectionType == "receive") {
      let index: number = this.receiveSelectedFrequencyDays.indexOf(itemCheck);
      if (event.checked == true) { if (index == -1) this.receiveSelectedFrequencyDays.push(itemCheck); }
      else { if (index !== -1) { this.receiveSelectedFrequencyDays.splice(index, 1); } }
    }
    else if (sectionType == "get") {
      let index: number = this.getSelectedFrequencyDays.indexOf(itemCheck);
      if (event.checked == true) { if (index == -1) this.getSelectedFrequencyDays.push(itemCheck); }
      else { if (index !== -1) { this.getSelectedFrequencyDays.splice(index, 1); } }
    }
    else {
       let index: number = this.dataquerySelectedFrequencyDays.indexOf(itemCheck);
       if (event.checked == true) { debugger; if (index == -1) this.dataquerySelectedFrequencyDays.push(itemCheck);this.checkedDays=this.dataquerySelectedFrequencyDays.toString(); }
       else { if (index !== -1) { let dataquerytempSelectedFrequencyDays:any[];
         debugger;
         dataquerytempSelectedFrequencyDays =this.dataquerySelectedFrequencyDays;
         dataquerytempSelectedFrequencyDays.splice(index, 1);
         this.dataquerySelectedFrequencyDays=dataquerytempSelectedFrequencyDays;
         this.checkedDays=this.dataquerySelectedFrequencyDays.toString();
       } }
     }

  }

  getOrganizationsDataSourceDetail(item) {
    debugger;
    this.dataSourceFromSettings.showHideform = true;
    this.DataSourceId = item;
    this.organizationService.GetOrganizationsDataSourceDetail(parseInt(this.OrganizationId), this.DataSourceId).subscribe(
      Response => {
        debugger;
        if (Response.result.organizationDataSource.length != 0) {
          this.IsDataSourceEdit = true;

          this.SelecteddatasourceId = Response.result.organizationDataSource[0].dataSourceId;
          this.DataType = Response.result.organizationDataSource[0].dataType;
          this.SourceOfData = Response.result.organizationDataSource[0].sourceOfData;
          this.AcquisitionMethod = Response.result.organizationDataSource[0].acquisitionMethod;

          if (this.AcquisitionMethod == 1) {
            let getDataSource = Response.result.getDataSource[0];
            let getDataSourceSftp = Response.result.getDataSourceSftp[0];
            this.checkedDays = getDataSource.frequencyDays;
            this.getOrgConfigModel.setValue
              ({
                GetFrequency: getDataSource.frequency,
                // tslint:disable-next-line:max-line-length
                GetTime: getDataSource.frequencyTime,
                GetFileType: getDataSource.fileType,
                GetFrequencyDay: parseInt(getDataSource.frequencyDays),
                GetWeekOfDay: parseInt(getDataSource.frequencyWeek == undefined ? 0 : getDataSource.frequencyWeek),
                Host: getDataSourceSftp.host,
                Port: getDataSourceSftp.port,
                UserName: getDataSourceSftp.userName,
                Password: getDataSourceSftp.password
              })
            //this.getOrgConfigModel.patchValue(['GetTime']);
          }

          else if (this.AcquisitionMethod == 2) {
            debugger;
            let recieveDataSource = Response.result.receiveDataSource[0];
            this.checkedDays = recieveDataSource.frequencyDays;
            this.receiveOrgConfigModel.setValue({
              //receive section
              ReceiveFrequency: recieveDataSource.frequency,
              ReceiveTime: recieveDataSource.frequencyTime,
              ReceiveDirectory: recieveDataSource.directory,
              ReceiveFileType: recieveDataSource.fileType,
              ReceiveFrequencyDay: parseInt(recieveDataSource.frequencyDays),
              ReceiveWeekOfDay: parseInt(recieveDataSource.frequencyWeek),
            })
          }
          else {
              debugger;
              let recieveDataSource = Response.result.receiveDataSource[0];
              this.checkedDays = recieveDataSource.frequencyDays;
              this.receiveOrgConfigModelDQ.setValue({
                //receive section
                ReceiveFrequency: recieveDataSource.frequency,
                ReceiveTime: recieveDataSource.frequencyTime,
                ReceiveFrequencyDay: parseInt(recieveDataSource.frequencyDays),
                ReceiveWeekOfDay: parseInt(recieveDataSource.frequencyWeek),
                DatabaseName: recieveDataSource.datasourceName,
                UserName: recieveDataSource.userName,
                Password: recieveDataSource.password,
            
              })     
            }	
          this.dataSourceFromSettings.buttonText = "Back To List";
          this.dataSourceFromSettings.showHideform = true;
          this.dataSourceFromSettings.SubmitButtonText = "Update";



        }
        else {
          this.IsDataSourceEdit = false;
          this.checkedDays = "";
        }
      })

  }

  GetValue(item) {
    if (this.checkedDays != "" && this.checkedDays != undefined) {
      if (this.checkedDays.indexOf(item) > -1) {
        return true;
      } else {
        return false;
      }
    }
  }

  changeAcquisitionMethod() {
    this.getOrganizationsDataSourceDetail(this.AcquisitionMethod);
  }


  showTabsAsPerType(organizationType) {
    debugger;
    if (organizationType == undefined)
      organizationType = 0;
    switch (parseInt(organizationType)) {
      case 1:

        this.ShowDataSourcesTab = true;
        this.ShowOrganizationTab = true;
        this.ShowDepartmentTab = false;
        this.ShowClinicsTab = false;
        this.ShowSupportedOrganizationsTab = false;

        break;
      case 2:
        this.ShowDataSourcesTab = true;
        this.ShowOrganizationTab = true;
        this.ShowDepartmentTab = true;
        this.ShowClinicsTab = true;
        this.ShowSupportedOrganizationsTab = false;
        this.dataSourceFromSettings.showHideform = false;
        break;
      case 3:
        this.ShowDataSourcesTab = true;
        this.ShowOrganizationTab = true;
        this.ShowDepartmentTab = false;
        this.ShowClinicsTab = false;
        this.ShowSupportedOrganizationsTab = false;
        break;
      case 4:
        this.ShowDataSourcesTab = true;
        this.ShowOrganizationTab = true;
        this.ShowDepartmentTab = false;
        this.ShowClinicsTab = false;
        this.ShowSupportedOrganizationsTab = true;
        break;
      case 5:
        this.ShowDataSourcesTab = true;
        this.ShowOrganizationTab = true;
        this.ShowDepartmentTab = false;
        this.ShowClinicsTab = false;
        this.ShowSupportedOrganizationsTab = false;
        break;
      case 0:
        this.ShowDataSourcesTab = false;
        this.ShowOrganizationTab = true;
        this.ShowDepartmentTab = false;
        this.ShowClinicsTab = false;
        this.ShowSupportedOrganizationsTab = false;

    }
  }



  showDetailTabsAsPerType(organizationType) {
    debugger;
    if (organizationType == undefined)
      organizationType = 0;
    switch (parseInt(organizationType)) {
      case 1:
        this.manageSection.DataSource = true;
        this.manageSection.Organization = true;
        this.manageSection.Department = false;
        this.manageSection.Clinic = false;
        this.ShowSupportedOrganizationsTab = false;
        break;
      case 2:
        this.manageSection.DataSource = true;
        this.manageSection.Organization = true;
        this.manageSection.Department = true;
        this.manageSection.Clinic = true;
        this.ShowSupportedOrganizationsTab = false;
        this.dataSourceFromSettings.showHideform = false;
        break;
      case 3:
        this.manageSection.DataSource = true;
        this.manageSection.Organization = true;
        this.manageSection.Department = false;
        this.manageSection.Clinic = false;
        this.ShowSupportedOrganizationsTab = false;
        break;
      case 4:
        this.manageSection.DataSource = true;
        this.manageSection.Organization = true;
        this.manageSection.Department = false;
        this.manageSection.Clinic = false;
        this.ShowSupportedOrganizationsTab = true;
        break;
      case 5:
        this.manageSection.DataSource = true;
        this.manageSection.Organization = true;
        this.manageSection.Department = false;
        this.manageSection.Clinic = false;
        this.ShowSupportedOrganizationsTab = false;
        break;
      case 0:
        this.manageSection.DataSource = false;
        this.manageSection.Organization = true;
        this.manageSection.Department = false;
        this.manageSection.Clinic = false;
        this.ShowSupportedOrganizationsTab = false;

    }
  }

  fnDisplaySection(tabType) {
    debugger

    switch (parseInt(tabType)) {

      case 1:

        this.TabSequenceOrg = true;
        this.TabSequenceClinic = false;
        this.TabSequenceDept = false;
        this.TabSequenceDS = false;

        this.manageSection.Organization = true;
        this.manageSection.Clinic = false;
        this.manageSection.DataSource = false;
        this.manageSection.Department = false;

        break;

      case 2:




        if (this.organizationForm.valid) {
          this.getOrganizationsClinicList();
          this.manageSection.Organization = false;
          this.manageSection.Clinic = true;
          this.manageSection.DataSource = false;
          this.manageSection.Department = false;

        }
        else {
          this.getOrganizationsClinicList();
          this.manageSection.Organization = true;
          this.manageSection.Clinic = false;
          this.manageSection.DataSource = false;
          this.manageSection.Department = false;

        }
        this.TabSequenceOrg = false;
        this.TabSequenceClinic = true;
        this.TabSequenceDept = false;
        this.TabSequenceDS = false;
        break;

      case 3:



        // if(this.organizationForm.valid)
        // {
        if (!this.clinicFromSettings.showHideform) {
          this.getAllDepartmentList();
          this.manageSection.Organization = false;
          this.manageSection.Clinic = false;
          this.manageSection.DataSource = false;
          this.manageSection.Department = true;
        }
        else {
          if (this.organizationClinicModel.valid) {
            this.getAllDepartmentList();
            // this.getAllDepartmentList();
            //this.onOrgClinicFormSubmit(this.organizationClinicModel.value);
            this.manageSection.Organization = false;
            this.manageSection.Clinic = false;
            this.manageSection.DataSource = false;
            this.manageSection.Department = true;
          }
        }
        this.TabSequenceOrg = false;
        this.TabSequenceClinic = false;
        this.TabSequenceDept = true;
        this.TabSequenceDS = false;
        break;
      case 4:
        debugger



        if (this.organizationForm.get('OrganizationType').value == 2) {
          this.TabSequenceOrg = false;
          this.TabSequenceClinic = false;
          this.TabSequenceDept = true;
          this.TabSequenceDS = false;

          if (this.departmentFromSettings.showHideform && this.departmentForm.valid) {

            this.getOrganizationconfigurationList();
            this.manageSection.Organization = false;
            this.manageSection.Clinic = false;
            this.manageSection.DataSource = true;
            this.manageSection.Department = false;


            //  else
            //  {
            //   this.getAllDepartmentList();
            //   this.manageSection.Organization=false;
            //   this.manageSection.Clinic=false;
            //   this.manageSection.DataSource=false;
            //   this.manageSection.Department=true;

            //  }

          }
          else {
            this.TabSequenceOrg = false;
            this.TabSequenceClinic = false;
            this.TabSequenceDept = false;
            this.TabSequenceDS = true;
            this.getOrganizationconfigurationList();
            this.manageSection.Organization = false;
            this.manageSection.Clinic = false;
            this.manageSection.DataSource = true;
            this.manageSection.Department = false;

          }
        }
        else {
          if (this.organizationForm.valid) {
            this.TabSequenceOrg = true;
            this.TabSequenceClinic = false;
            this.TabSequenceDept = false;
            this.TabSequenceDS = false;
            this.getOrganizationconfigurationList();
            this.manageSection.Organization = false;
            this.manageSection.Clinic = true;
            this.manageSection.DataSource = false;
            this.manageSection.Department = false;
          }
          else {
            this.TabSequenceOrg = false;
            this.TabSequenceClinic = false;
            this.TabSequenceDept = false;
            this.TabSequenceDS = true;
            this.getOrganizationconfigurationList();
            this.manageSection.Organization = true;
            this.manageSection.Clinic = false;
            this.manageSection.DataSource = false;
            this.manageSection.Department = false;
          }

        }


        // if(this.departmentFromSettings.showHideform)
        // {
        //     if(this.organizationForm.get('OrganizationType').value!=null)
        //     {   
        //     if(this.organizationForm.get('OrganizationType').value==3)
        //     {
        //     if(this.organizationForm.valid)
        //     {
        //     // this.ondepartmentFormSubmit(this.departmentForm.value);
        //     this.getOrganizationconfigurationList();
        //     this.manageSection.Organization=false;
        //     this.manageSection.Clinic=false;
        //     this.manageSection.DataSource=true;
        //     this.manageSection.Department=false;
        //     }
        //   }

        //   else
        //   {
        //     if(this.organizationForm.valid)
        //     {
        //     if(this.departmentForm.valid)
        //     {
        //     //this.onOrganizationFormSubmit(this.organizationForm.value);
        //     this.getOrganizationconfigurationList();
        //     this.manageSection.Organization=false;
        //     this.manageSection.Clinic=false;
        //     this.manageSection.DataSource=true;
        //     this.manageSection.Department=false;
        //     }
        //   }
        //   }
        // }
        // }
        //     else
        //     {

        //     if(this.organizationForm.get('OrganizationType').value==3)
        //     {
        //       if(this.organizationForm.valid)
        //       {
        //     this.getOrganizationconfigurationList();
        //     this.manageSection.Organization=false;
        //     this.manageSection.Clinic=false;
        //     this.manageSection.DataSource=true;
        //     this.manageSection.Department=false;
        //       }

        //   }
        //   else
        //   {

        //     this.getOrganizationconfigurationList();
        //     this.manageSection.Organization=false;
        //     this.manageSection.Clinic=false;
        //     this.manageSection.DataSource=true;
        //     this.manageSection.Department=false;  
        //   }
        //     }  

        break;
    }
  }

  fnSetNext() {
    if (this.organizationForm.get('OrganizationType').value != null) {
      var checkType = this.organizationForm.get('OrganizationType').value;
      switch (checkType) {

        case 2:
          this.getOrganizationsClinicList()
          this.manageSection.Organization = false;
          this.manageSection.Clinic = true;
          this.manageSection.DataSource = false;
          this.manageSection.Department = false;
          break;
        default:
          this.manageSection.Organization = false;
          this.manageSection.Clinic = false;
          this.manageSection.DataSource = true;
          this.manageSection.Department = false;
          break;
      }
    }

  }
  // Go back URL
  backClicked() {
     this._location.back();
  }

}
