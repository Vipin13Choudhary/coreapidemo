import { Component, OnInit ,ViewChild} from '@angular/core';
import { Provider } from '../../model/Provider';
import "rxjs"
import { ProviderService } from '../../service/provider/provider.service';
import { listOperation, statesDropDown, organizationDropDown } from '../../model/common/common'
import {MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { PageEvent } from '@angular/material';

import { ToastrService } from 'ngx-toastr';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { UserService } from '../../service/user/user.service';
import { Router } from '@angular/router';
import { OrganizationsService } from '../../service/organization/organizations.service';
import {LoaderService} from '../../service/loader/loader.service'
import * as _ from 'underscore';
@Component({
  selector: 'app-provider-detail',
  templateUrl: './provider-detail.component.html',
  styleUrls: ['./provider-detail.component.css']
})
export class ProviderDetailComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sorting: MatSort;
  ProviderList = [];
  ELEMENT_DATA: any;
  listOperands: any;
  length = 5;
  pageSize = 10;
  pageSizeOptions: number[] = [1, 5, 10, 20];
  dataSource: any;
  LoginUserId: string;
  displayedColumns = ['Name', 'NPI', 'LicenseName', 'ProviderType', 'CredentialDate', 'ExpirationDate', 'LastCredentialDate','Action'];
  OrganizationDropDown = [];
  search:"";
  LoginOrganizationId:number;
  Role:string;

  constructor(private loader:LoaderService,private fbuilder: FormBuilder, private providerService: ProviderService, private organizationService: OrganizationsService) {
    this.listOperands = new listOperation();
  }

  ngOnInit() {
    debugger;
    this.LoginUserId = localStorage.getItem("LoginUserID");
    this.listOperands.offset = 1;
    this.listOperands.limit = 10;
    this.listOperands.order = "desc";
    this.listOperands.sort = "";
    this.listOperands.Search = "";
    this.listOperands.ProviderId = 0;
    this.Role=localStorage.getItem("Role");
    this.LoginOrganizationId=parseInt(localStorage.getItem("OrganizationId"));
    this.listOperands.OrganizationId=this.Role=="SuperAdmin"?0:this.LoginOrganizationId;
    this.listOperands.ProviderId=this.Role=="SuperAdmin"?0:this.LoginOrganizationId;
    this.getProviderList(this.listOperands);
    this.getAllDropDown(this.LoginUserId);
    
  }
  getProviderList(data) {
    debugger;
    this.loader.Start();
    this.ELEMENT_DATA = [];
    this.ProviderList = [];
    this.providerService.getProviderList(this.listOperands).subscribe(response => {
      debugger;
      if (response.length > 0) {
        this.length = response[0].TotalRows;
        debugger;
        response.forEach(element => {
          this.ProviderList.push({
            ProviderId: element.ProviderId, DisplayName: element.DisplayName, NPI: element.NPI, LicenseName: element.LicenseName, ProviderType: element.ProviderType,
            InitialCredentialDate: element.InitialCredentialDate, CredentialExpirationDate: element.CredentialExpirationDate,
            LastCredentialDate: element.LastCredentialDate
          });
        });
        

      }
      else {
        this.pageSize = 0;
        this.length=0;
      }
      this.ELEMENT_DATA = this.ProviderList;
      //console.log(this.ELEMENT_DATA);
      this.dataSource = new MatTableDataSource<Element>(this.ELEMENT_DATA);
      this.loader.Stop();
      //console.log(this.dataSource);
    });
  }
  ViewProvider(element){
    //
    this.providerService.ViewProvider(element.OrganizationId);
     // this.router.navigate(['/Admin', 'editOrganization']);
    
    }

    sortData(header,OrderType){
      this.loader.Start();
      if(OrderType.currentTarget.attributes.getNamedItem('aria-sort')!=null){
        OrderType=OrderType.currentTarget.attributes.getNamedItem('aria-sort').value;
      if(OrderType=="descending"){
        OrderType='desc';
      }
      else
      {
        OrderType='asc';
      }
        }
      else{
         this.loader.Stop();
           return;
       }
          this.listOperands.sort=header;
          this.listOperands.order=OrderType;
          this.getProviderList(this.listOperands);
          this.loader.Stop();
    }

  // sortData(header, order) {
  //   debugger;
  //   this.listOperands.sort = header;
  //   this.listOperands.order = order;
  //   this.getProviderList(this.listOperands);
  // }

  getServerData(pageData) {
    this.listOperands.offset = pageData.pageIndex + 1;
    this.listOperands.limit = pageData.pageSize;
    this.getProviderList(this.listOperands);
  }

  getAllDropDown(userId) {
    debugger;
    this.organizationService.getAllDropDown(localStorage.getItem("LoginUserID"), this.Role=="SuperAdmin"?0:this.LoginOrganizationId).subscribe(response => {
      this.OrganizationDropDown=response.organizationsList;
    });
  }

  //As per drop downs (further added on future)
  getFilteredList(organizationId) {
    this.listOperands.ProviderId = organizationId;
    this.getProviderList(this.listOperands);
  }

  getSearchedData(){
    this.listOperands.Search=this.search;
    this.getProviderList(this.listOperands);
  }
}
