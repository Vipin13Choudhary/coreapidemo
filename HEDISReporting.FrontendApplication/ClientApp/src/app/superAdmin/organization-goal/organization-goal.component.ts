import { Component, OnInit, TemplateRef } from '@angular/core';
import { OrganizationsService } from 'src/app/service/organization/organizations.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DashboardService } from 'src/app/service/dashboard/dashboard.service';
import { from } from 'rxjs';
import { GoalDetails } from 'src/app/model/dashboard/dashboard';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { OrganizationGoalModal, MasterGoal, AddCopyMeasure } from 'src/app/model/dashboard/MeasureRules';
import { promise } from 'protractor';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { type } from 'os';
import { StringifyOptions } from 'querystring';
import { resolve } from 'dns';
import { organizationDropDown } from 'src/app/model/common/common';
import { and } from '@angular/router/src/utils/collection';
import * as _ from 'underscore';
import { find } from 'rxjs/operators';
import { equalSegments } from '@angular/router/src/url_tree';
import { elementStart } from '@angular/core/src/render3';
import {LoaderService} from '../../service/loader/loader.service'

declare let $;
@Component({
  selector: 'app-organization-goal',
  templateUrl: './organization-goal.component.html',
  styleUrls: ['./organization-goal.component.css']
})
export class OrganizationGoalComponent implements OnInit {
  OrganizationDropDown = [];
  OrganizationSDropDown = [];
  OrganizationPDropDown = [];
  goalForm: FormGroup;
  IsDataSourceEdit: boolean = false;
  MeasureId:number;
  panelOpenState1: boolean = true;
  OrganizationGoals:any;
  modalRef: BsModalRef;
  DataType:string;
  EntityId:number;
  EntityType:number;
  ABAflag: boolean;
  CCflag: boolean;
  BCflag: boolean;
  CCSflag: boolean;
  CHBPflag: boolean;
  HeaderClass:string;
  ChildClass:string;
  Error:boolean;
  Eg:boolean;
  ButtonFlag:boolean;
  MainScreenFlag:boolean;
  DetailScreenFlag:boolean;
  EntityGoal:any;
  IsEdit:boolean;
  EditId:number;
  counterValue = 0;
  minValue = 0;
  maxValue = 100;
  EntityMeasure:any;
  Goalflag:boolean;
  GoalMsg:boolean;
  OrganizationTypeDropDown: [];
  MeasureAddForm:FormGroup;
  IsCopy:number;
  search:string;
  SEntityType:number=0;
  SEntityId:number=0;
  EditType:string;
  DeleteId:number;
  DeleteAllId:number;
  DeleteAllType:number;
  HeadingName:string;
  MeasureListFlag:boolean;
  MeasureModalFlag:string;
  IsCopyFlag:boolean;
  public mask=[/\d/,/\d/,'.',/\d/,/\d/];//'^\d{0,2}(\.\d{1,2})?$'
  EditMeasure:string;
  CopyFrom:string;
  LoginOrganizationId:number=0;
Role:string
  // public mask=['0','0','.','0','0'];
  constructor(private loder:LoaderService, private organizationService: OrganizationsService, private fb: FormBuilder, private dashboardService: DashboardService,private toastr: ToastrService,private router:Router,private modalService: BsModalService) { }

  ngOnInit() {
    //this.GetOrganizationData();
    //this.getAllDropDown();
    this.search="";
    this.ABAflag=false;
    this.Role=localStorage.getItem("Role");
    this.CCflag = false;
    this.BCflag = false;
    this.CCSflag = false;
    this.CHBPflag = false;
    this.HeaderClass="fa fa-plus";
    this.MainScreenFlag=true;
    this.DetailScreenFlag=false;
    this.GetOrganizationData();
    this.IsEdit=false;
    this.Goalflag=false;
    this.GoalMsg=false;
    this.HeadingName="Goal Management";
    this.goalForm=this.fb.group({
      Goal: [null, Validators.required],
      MeasureId: [null],
    });
    this.MeasureAddForm=this.fb.group({
      EntityTypeId:[null,Validators.required],
      EntityId:[null,Validators.required]
    });
    this.BindAllDropdown();
    this.EditType="Create New";
    // debugger;
    // this.SEntityType=0;
    // this.SEntityId=0;
  }
  AddCopyMeasure(modal:AddCopyMeasure)
  {
    debugger;
    let modelName:any;
    modelName="template1";
    let template: TemplateRef<any>=modelName;
    // this.modalRef = this.modalService.show(template);
    this.modalService.hide(1);
    if(this.IsCopy==0)
    {
      // this.dashboardService.CopyMersure(modal.EntityId,modal.EntityTypeId, parseInt(localStorage.getItem("LoginUserID")),this.EntityId,this.EntityType).subscribe(response => {
      this.dashboardService.CopyMersure( modal.EntityId, modal.EntityTypeId, parseInt(localStorage.getItem("LoginUserID")),this.EntityId, this.EntityType).subscribe(response => {
        console.log(response);
        debugger;
        if(response>0)
        {
          this.toastr.success('Measure copied successfully.','Success');
        }
        else if(response = -1)
        {
          this.toastr.warning('There is no common measure in parent entity.', 'Waring');
        }
        else
        {
          this.toastr.error('There is error in copying measure.Please try after sometime.', 'Error');
        }
        this.GetOrganizationData();
      });
    }
    else
    {
      this.Details(modal.EntityId,modal.EntityTypeId)
    }
  }
  Details(Id:number,Type:number)
  {
    debugger;
    this.EntityId=Id;
    this.EntityType=Type;
    this.dashboardService.GetMeasureGoalsForOrganizaton(Id,Type,this.search).subscribe(response => {
      debugger;
      this.search="";
      this.EntityGoal=response.list;
      this.HeadingName=response.name +" " + "Measures and Goals";
      console.log(this.EntityGoal);
      if(this.EntityGoal.length>0)
      {
        this.MeasureListFlag=false;
      }
      else
      {
        this.MeasureListFlag=true;
      }

    });
    debugger;
   
    this.MainScreenFlag=false;
    this.DetailScreenFlag=true;
  }
  getSearchedData()
  {
    this.dashboardService.GetMeasureGoalsForOrganizaton(this.EntityId,this.EntityType,this.search).subscribe(response => {
      debugger;
      //_.find()
      this.EntityGoal=response.list;//.filter(x=>x.name.includes(this.search));
      if(this.EntityGoal.length>0)
      {
        this.MeasureListFlag=false;
      }
      else
      {
        this.MeasureListFlag=true;
      }

    });
  }
  BackToMainList()
  {
    this.GetOrganizationData();
    this.search="";
    this.HeadingName="Goal Management";
    this.MainScreenFlag=true;
    this.DetailScreenFlag=false;
  }
  getAllSDropDown(TypeId: number) {
    debugger;
      this.LoginOrganizationId = parseInt(localStorage.getItem("OrganizationId"));
      this.OrganizationSDropDown = [];
    this.organizationService.GetAllEntity(TypeId,this.Role=="SuperAdmin"?0:this.LoginOrganizationId).subscribe(response => {
      // this.OrganizationDropDown.push({id:6,name:'All'})
      console.log(response.organizationsList);
      this.OrganizationSDropDown = response.organizationsList;
      this.SEntityId = 0;
      this.GetOrganizationData();
    });
  }
  getAllEDropDown()
  {
    this.GetOrganizationData();
  }
  getAllDropDown(TypeId:number) {
debugger;
this.LoginOrganizationId=parseInt(localStorage.getItem("OrganizationId"));
    this.organizationService.GetAllEntity(TypeId,this.Role=="SuperAdmin"?0:this.LoginOrganizationId).subscribe(response => {
      // this.OrganizationDropDown.push({id:6,name:'All'})
      console.log(response.organizationsList);
      this.OrganizationPDropDown = response.organizationsList;
    });
  }

  GetGoal(item: number) {
    
    if (item > 0) {
      this.GetOrganizationData();
    }
  }
  SearchData()
  {
    this.GetOrganizationData();
  }
  GetOrganizationData()
  {
    this.loder.Start();
    debugger;
    this.LoginOrganizationId=parseInt(localStorage.getItem("OrganizationId"));
    this.dashboardService.GetClinicPayerForOrganizaton(this.SEntityType,this.SEntityId,this.Role=="SuperAdmin"?0:this.LoginOrganizationId).subscribe(response => {
      this.OrganizationGoals= response;
      // this.SEntityId=0;
      // this.SEntityType=0;
      //this.OrganizationSDropDown=[];
      this.loder.Stop();
    });
  }

  hideshow(event) {
    debugger;
    if ($(event.target).parents('tr').eq(0).attr('data') == 'hide' && $(event.target).parents('tr').eq(0).nextAll('tr').length > 0) {
      $(event.target).parents('tr').eq(0).nextAll('tr').show();
      $(event.target).parents('tr').eq(0).attr('data', 'show');
      if ($(event.target).is('a')) {
        $(event.target).children('i').removeClass('fa fa-plus');
        $(event.target).children('i').addClass('fa fa-minus');
      } else {
        $(event.target).removeClass('fa fa-plus');
        $(event.target).addClass('fa fa-minus');
      }
    } else if ($(event.target).parents('tr').eq(0).attr('data') == 'show' && $(event.target).parents('tr').eq(0).nextAll('tr').length > 0) {
      $(event.target).parents('tr').eq(0).nextAll('tr').hide();
      $(event.target).parents('tr').eq(0).attr('data', 'hide');
      if ($(event.target).is('a')) {
        $(event.target).children('i').removeClass('fa fa-minus');
        $(event.target).children('i').addClass('fa fa-plus');
      } else {
        $(event.target).removeClass('fa fa-minus');
        $(event.target).addClass('fa fa-plus');
      }
      // this.HeaderClass="fa fa-plus";
    }
  }

  hideshowheader(event){
    debugger;
    if($(event.target).parents('tr').attr('data')=='hide' && $(event.target).parents('tr').eq(0).nextAll('tr').length>0){
      $(event.target).parents('tr').nextAll('tr').show();
      $(event.target).parents('tr').attr('data','show');
      if ($(event.target).is('a')) {
        $(event.target).children('i').removeClass('fa fa-plus');
        $(event.target).children('i').addClass('fa fa-minus');
      } else {
        $(event.target).removeClass('fa fa-plus');
        $(event.target).addClass('fa fa-minus');
      }
      this.HeaderClass="fa fa-minus";
    }else if($(event.target).parents('tr').attr('data')=='show' && $(event.target).parents('tr').eq(0).nextAll('tr').length>0){
      $(event.target).parents('tr').nextAll('tr').hide();
      $(event.target).parents('tr').attr('data','hide');
      this.HeaderClass="fa fa-plus";
      if ($(event.target).is('a')) {
        $(event.target).children('i').removeClass('fa fa-minus');
        $(event.target).children('i').addClass('fa fa-plus');
      } else {
        $(event.target).removeClass('fa fa-minus');
        $(event.target).addClass('fa fa-plus');
      }
    }
  }


  onGoalFormSubmit(orgModal: GoalDetails)
  {
    
    orgModal.IsEdit=this.IsDataSourceEdit;
    orgModal.OrganizationId=this.MeasureId;
    this.dashboardService.SubmitGoalData(orgModal).subscribe(response => {
      this.toastr.success('Goal data saved successfully.', 'Success')
      
      document.getElementById("form").setAttribute("style", "visibility: hidden;");
    });
  }
  OnAssociationGoal(orgModal: GoalDetails)
  {
    
  }
  OnClinicFormSubmit(orgModal: GoalDetails,id:number)
  {
    
  }
  BindAllDropdown()
  {
    this.organizationService.GetCountriesStates(1).subscribe(
      response => {
        this.OrganizationDropDown = response.result.organizationDropDown;
      });
  }
  fnShowCreateMeasureModal(modelName,Edit:number,Id:number,Type:number,CopyFrom:string)
  {
    debugger;
    this.MeasureAddForm=this.fb.group({
      EntityTypeId:[null,Validators.required],
      EntityId:[null,Validators.required]
    });
    if (Edit === 1) {
      this.EditType = "Create New";
    this.MeasureModalFlag="Add";
      this.OrganizationTypeDropDown=[];
    this.OrganizationPDropDown=[];
    this.IsCopyFlag=false;
    }
    else {
      this.EditType = "Copy New";
      this.MeasureModalFlag="Copy";
      this.OrganizationTypeDropDown=[];
      this.OrganizationPDropDown=[];
      this.IsCopyFlag=true;
      this.CopyFrom=CopyFrom;
    }
    this.IsCopy=Edit;
    this.EntityId=Id;
    this.EntityType=Type;
    this.organizationService.GetCountriesStates(1).subscribe(
      response => {
        this.OrganizationTypeDropDown =response.result.organizationDropDown.filter((x=>
          {
            debugger;
            if(Edit===0)
            {
              if (Type === 1) {
                if (x.id === 1) {
                  return x.id
                }
              }
              else if (Type === 2) {
                if (x.id === 1 || x.id === 2) {
                  return x.id
                }
                // if (x.id === 2) {
                //   return x.id
                // }
              }
              else if (Type === 3) {
                if (x.id === 3) {
                  return x.id
                }
              }
              else if (Type === 4) {
                if (x.id === 4) {
                  return x.id
                }
              }
              else if (Type === 5) {
                if (x.id === 5) {
                  return x.id
                }
              }
              else if (Type === 6) {
                if (x.id === 6 || x.id === 2) {
                  return x.id
                }
              }
              else if (Type === 7) {
                if (x.id === 7 || x.id === 6) {
                  return x.id
                }
              } 
            }
            else
            {
              return x.id
            }            
        }))
        // debugger;
        // this.OrganizationTypeDropDown = response.result.organizationDropDown;
        // this.OrganizationTypeDropDown.filter((x=>
        //   {
        //     debugger;
        //     if(x.id===8 || x.id===9 )
        //     {
        //       return x.id
        //     }
             
        //   }))
      }
    )
    let template: TemplateRef<any>=modelName;
    this.modalRef = this.modalService.show(template);
  }
  DeleteMeasure()
  {
    this.dashboardService.DeleteMeasure(this.DeleteId).subscribe(response => {
      debugger;
      this.Details(this.EntityId,this.EntityType);
      this.dashboardService.GetEntityMeasure(this.EntityId,this.EntityType).subscribe(response => {
        console.log(response);
        debugger;
        this.EntityMeasure=response;
        if(this.EntityMeasure.length>0)
        {
          this.toastr.success('Goal data deleted successfully.', 'Success')
          this.modalService.hide(1);
          this.Goalflag=true;
          this.GoalMsg=false;
        }
        else
        {
          this.Goalflag=false;
          this.GoalMsg=true;
        }
      });
    });
  }
  ShowDeleteMeasureTemplate(modelName,Id:number)
  {
    this.DeleteId=Id;
    let template: TemplateRef<any>=modelName;
    this.modalRef = this.modalService.show(template);
  }
  DeleteAllMeasure()
  {
    this.dashboardService.DeleteAllMeasure(this.DeleteAllId,this.DeleteAllType).subscribe(response => {
      this.toastr.success('All goal data deleted successfully.', 'Success')
      this.modalRef.hide();
      this.GetOrganizationData();
    });
  }
  ShowDeleteAllMeasureTemplate(modelName,Id:number,Type:number)
  {
    debugger;
    this.DeleteAllId=Id;
    this.DeleteAllType=Type;
    let template: TemplateRef<any>=modelName;
    this.modalRef = this.modalService.show(template);
  }
  EditCreateMeasure(modelName,Id:number,Flag:boolean)
  {
    debugger;
    if (Id > 0 && Flag == true) {
      this.IsEdit = false;
      this.EditId = Id;
      debugger;
      this.dashboardService.GetEntityGoal(Id).subscribe(response => {
        debugger;
        let GoalData:any;
        GoalData=response;
        this.EditMeasure=GoalData[0].Name
        // let g:string;
        // g=GoalData[0].Goal;
        // g=g.toString();
        // if (g.split('.')[0].length == 1) {
        //   if (g.split('.')[1] != null) {
        //     if (g.split('.')[1].length == 1) {
        //       g = g + '0';
        //     }
        //     else if (g.split('.')[1].length == 0) {
        //       g = g + '00';
        //     }
        //     else {
        //       g = '0' + g;
        //     }
        //   }
        //   else
        //   {
        //     g = '00'+g + '.00';
        //   }
        // }
        // else if(g.split('.')[0].length == 2)
        // {
        //   if (g.split('.')[1] != null) {
        //     if (g.split('.')[1].length == 1) {
        //       g = '0'+g + '.0';
        //     }
        //     else if (g.split('.')[1].length == 0) {
        //       g = '0'+g + '.00';
        //     }
        //     else {
        //       g = '0' + g;
        //     }
        //   }
        //   else
        //   {
        //     g = '0'+g;
        //   }
        // }
        // else
        // {
        //   if (g.split('.')[1] != null) {
        //     if (g.split('.')[1].length == 1) {
        //       g = g + '.0';
        //     }
        //     else if (g.split('.')[1].length == 0) {
        //       g = g + '.00';
        //     }
        //   }
        //   else
        //   {
        //     g=g+'.00';
        //   }
        // }
        // GoalData[0].Goal=g;
        this.goalForm.setValue({
          Goal:GoalData[0].Goal,
          MeasureId:null
        });
        this.Goalflag=true;
      });
    }
    else
    {
      this.dashboardService.GetEntityMeasure(this.EntityId,this.EntityType).subscribe(response => {
        console.log(response);
        debugger;
        this.EntityMeasure=response;
        this.EditMeasure="Define Measure Goal";
        if(this.EntityMeasure.length>0)
        {
          
          this.Goalflag=true;
          this.GoalMsg=false;
        }
        else
        {
          this.Goalflag=false;
          this.GoalMsg=true;
        }
      });
      this.EditId=0;
      this.IsEdit=true;
      this.goalForm.setValue({
        Goal:'0',
        MeasureId:0
      });
      this.goalForm.patchValue({
        MeasureId:0
      });
    }
    let template: TemplateRef<any>=modelName;
    this.modalRef = this.modalService.show(template);
  }
  
  fnShowModel(modelName,Id:number,Type:number,Subchildcode:string)
  {
    debugger;

    this.goalForm = this.fb.group({
      ABAgoal: 0.00,
      CCgoal: 0.00,
      BCSgoal: 0.00,
      CCSgoal: 0.00,
      CHBPgoal: 0.00,
    });

    this.Error=false;
    this.Eg=true;
    this.ButtonFlag=true;
    this.ABAflag=false;
    this.CCflag = false;
    this.BCflag = false;
    this.CCSflag = false;
    this.CHBPflag = false;
    this.dashboardService.GetMeasureForOrganizaton(Id,Type).subscribe(response => {
      
      if (response.length > 0) {
        if (response[0].MeasureId == 1 && response[0].IsActive == true) {
          this.ABAflag = true;
        }
        else {
          this.ABAflag = false;
        }
        if (response[1].MeasureId == 2 && response[1].IsActive == true) {
          this.CCflag = true;
        }
        else {
          this.CCflag = false;
        }
        if (response[2].MeasureId == 3 && response[2].IsActive == true) {
          this.BCflag = true;
        }
        else {
          this.BCflag = false;
        }
        if (response[3].MeasureId == 4 && response[3].IsActive == true) {
          this.CCSflag = true;
        }
        else {
          this.CCSflag = false;
        }
        if (response[4].MeasureId == 5 && response[4].IsActive == true) {
          this.CHBPflag = true;
        }
        else {
          this.CHBPflag = false;
        }
      }
      else
      {
        this.Error=true;
        this.Eg=false;
        this.ButtonFlag=false;
      }
    });
    this.dashboardService.GetMeasureGoalsForOrganizaton(Id,Type,this.search).subscribe(response => {
      if(response.length>0)
      {
        this.goalForm = this.fb.group({
          ABAgoal: response[0].Goal,
          CCgoal: response[1].Goal,
          BCSgoal: response[2].Goal,
          CCSgoal: response[3].Goal,
          CHBPgoal: response[4].Goal,
        });
      }
    });
    debugger;
    let template: TemplateRef<any>=modelName;
    this.modalRef = this.modalService.show(template);
    this.EntityId=Id;
    this.EntityType=Type;
    if(Subchildcode=="Provider")
    {
      Type=7;
    }
    if (Type == 2) {
      this.DataType = "Corporate"
    }
    else if (Type == 3) {
      this.DataType = "Payer"
    }
    else if (Type == 6) {
      this.DataType = "Clinic"
    }
    else if (Type == 7) {
      this.DataType = "Provider"
    }
    else {
      this.router.navigate(['/Admin/goals']);
    }
  }
  onMasterGoalSubmit(modal:MasterGoal)
  {
    debugger;
    modal.EntityId=this.EntityId;
    modal.EntityTypeId=this.EntityType;
    modal.Id=this.EditId;
    modal.UserId= parseInt(localStorage.getItem("LoginUserID"));
    if ((modal.MeasureId=== undefined || modal.MeasureId=== 0) && this.EditId===0)
    {
      this.toastr.error('Please select Measure','Error');
    }
    else if(modal.Goal.toString().includes('_'))
    {
      this.toastr.error('Please enter correct number','Error');
    }
    else if(modal.Goal>100)
    {
      this.toastr.error('Please enter goal less than 100','Error');
    }
    else
    {
      this.dashboardService.SubmitMasterGoalData(modal).subscribe(response => {
        let result: number;
        result = response[0];
  debugger;
        if (result > 0) {
          this.toastr.success('Saved successfully.','Success');
          this.Details(this.EntityId, this.EntityType);
          // this.goalForm = this.fb.group({
          //   ABAgoal: 0,
          //   CCgoal: 0,
          //   BCSgoal: 0,
          //   CCSgoal: 0,
          //   CHBPgoal: 0,
          // });
          this.modalRef.hide();
        }
        else {
          this.toastr.error('There is error in adding measure.Please try after sometime.', 'Error');
        }
      });
    }
   
    
  }
}
