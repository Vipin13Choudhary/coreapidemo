import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganizationGoalComponent } from './organization-goal.component';

describe('OrganizationGoalComponent', () => {
  let component: OrganizationGoalComponent;
  let fixture: ComponentFixture<OrganizationGoalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganizationGoalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganizationGoalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
