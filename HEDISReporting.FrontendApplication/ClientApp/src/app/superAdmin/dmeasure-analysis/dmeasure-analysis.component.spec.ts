import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DMeasureAnalysisComponent } from './dmeasure-analysis.component';

describe('DMeasureAnalysisComponent', () => {
  let component: DMeasureAnalysisComponent;
  let fixture: ComponentFixture<DMeasureAnalysisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DMeasureAnalysisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DMeasureAnalysisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
