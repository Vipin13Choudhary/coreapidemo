import { Component, OnInit } from '@angular/core';
import { CommonFilters } from 'src/app/model/dashboard/dashboard';
import { MatTableDataSource } from '@angular/material';
import { AttributeService } from 'src/app/service/attribute/attribute.service';
import { LoaderService } from 'src/app/service/loader/loader.service';
import { PatientService } from 'src/app/service/patient/patient.service';

@Component({
  selector: 'app-attribution-management',
  templateUrl: './attribution-management.component.html',
  styleUrls: ['./attribution-management.component.css']
})
export class AttributionManagementComponent implements OnInit {
  pageAttributeSizeOptions: number[] = [1, 5, 10, 20];
  displayedAttributeColumns = ['Name', 'DOB', 'PCPName', 'Ethnicity', 'Race', 'Note', 'Action'];
  pageAttributionMatchedSize = 10;
  pageAttributionNotMatchedSize = 10;
  ELEMENT_DATA: any = [];
  AttributionMatchedPatient: any = [];
  EmptyAttributionMatchedlist: boolean = false;
  length1: Number = 5;
  length2: Number = 5;
  pageSize1: Number = 10;
  pageSize2: Number = 10;
  ABAFilters: CommonFilters;
  dataSource: any = [];
  AttributionNotMatched: any = [];
  EmptyAttributionNotMatchedlist: boolean = false;
  pageSizeOptions1: number[] = [1, 5, 10, 20];
  pageSizeOptions2: number[] = [1, 5, 10, 20];
  Id: string;
  constructor(private attributeService: AttributeService, private loader: LoaderService, private patientService: PatientService) { }

  ngOnInit() {
    this.ABAFilters = new CommonFilters();
    this.ABAFilters.OrganizationId = Number(localStorage.getItem('OrganizationId'));
    this.ABAFilters.UserId = Number(localStorage.getItem('LoginUserID'));
    this.ABAFilters.Search = '';
    this.ABAFilters.sort = '';
    this.ABAFilters.order = 'asc';
    this.ABAFilters.offset = 1;
    this.ABAFilters.limit = 10;
    this.FilterPatientAsPerMismatch('1');
  }
  getServerData1(page) {
    debugger;
    this.pageSize1 = page;
    this.ABAFilters.offset = page.pageIndex + 1;
    this.ABAFilters.limit = page.pageSize;
    this.FilterPatientAsPerMismatch('1');
  }
  getServerData2(page) {
    debugger;
    this.pageSize2 = page;
    this.ABAFilters.offset = page.pageIndex + 1;
    this.ABAFilters.limit = page.pageSize;
    this.FilterPatientAsPerMismatch('2');
  }
  sortData(header, OrderType) {
    debugger;
    this.loader.Start();
    if (OrderType.currentTarget.attributes.getNamedItem('aria-sort') != null) {
      OrderType = OrderType.currentTarget.attributes.getNamedItem('aria-sort').value;
      OrderType = OrderType == "descending" ? 'desc' : 'asc';
    }
    else {
      this.loader.Stop();
      return;
    }
    this.ABAFilters.sort = header;
    this.ABAFilters.order = OrderType;
    if(this.Id=='1')
      this.FilterPatientAsPerMismatch('1');
    else
    this.FilterPatientAsPerMismatch('2');
  }

  //This function is used open CDA file for particular Patient
  fnGetPatientCDA(element) {
    debugger;
    element.OrganizationId
    element.FileName
    if (element.OrganizationId && element.FileName) {
      this.patientService.GetPatientCda(element.OrganizationId, element.FileName, true, element.PatientId).subscribe(
        response => {
          var win = window.open(response.toString(), '_blank');
          win.focus();
        }
      )
    }
  }
  //End

  //This code is used to get all data of Matched and Not Matched Atrribution Patient
  FilterPatientAsPerMismatch(Id: string) {
    this.loader.Start();
    debugger;
    this.Id = Id;
    this.ELEMENT_DATA = [];
    this.attributeService.GetAttributionPatientList(this.ABAFilters).subscribe(
      response => {
        debugger;
        // Thses if condition is used show Mismatch patient
        if (response.Item1.length > 0 && Id == "1") {
          this.AttributionMatchedPatient = [];
          this.EmptyAttributionMatchedlist = false;
          this.length1 = response.Item1[0].TotalRows;
          response.Item1.forEach(element => {
            this.AttributionMatchedPatient.push({
              Name: element.PatientName, PrimaryProvider: element.PatientName, FileName: element.FileName == null ? false : element.FileName,
              EthnicityName: element.EthnicityName, RaceName: element.RaceName, Status: element.Status, EthnicityId: element.EthnicityId,
              DOB: element.DOB, Age: element.Age, OrganizationId: element.OrganizationId, Gap: element.Gap, Days: element.Days, ClinicName: element.ClinicName,
              ProviderName: element.ProviderName, PatientId:element.PatientID
            })
          });
          this.pageSize1 = this.ABAFilters.limit;
          this.ELEMENT_DATA = this.AttributionMatchedPatient;
          this.dataSource = new MatTableDataSource<Element>(this.ELEMENT_DATA);
        }
        else if (response.Item2.length == 0 && Id == "1") {
          this.AttributionMatchedPatient = [];
          this.ELEMENT_DATA = this.AttributionMatchedPatient;
          this.dataSource = new MatTableDataSource<Element>(this.ELEMENT_DATA);
          this.pageAttributionMatchedSize = 0;
          this.EmptyAttributionMatchedlist = true;
          this.pageSize1 = this.ABAFilters.limit;
        }
        // End
        // Thses if condition is used show Not Confirmed patient
        else if (response.Item2.length > 0 && Id == "2") {
          this.AttributionNotMatched = [];
          this.EmptyAttributionNotMatchedlist = false;
          this.length2 = response.Item2[0].TotalRows;
          response.Item2.forEach(element => {
            this.AttributionNotMatched.push({
              Name: element.PatientName,
              PrimaryProvider: element.PatientName,
              EthnicityName: element.EthnicityName,
              RaceName: element.RaceName,
              Status: element.Status,
              EthnicityId: element.EthnicityId,
              FileName: element.FileName == null ? false : element.FileName,
              DOB: element.DOB,
              Age: element.Age,
              OrganizationId: element.OrganizationId,
              Gap: element.Gap,
              Days: element.Days,
              ClinicName: element.ClinicName,
              ProviderName: element.ProviderName,
              PatientId:element.PatientID
            })
            this.pageSize2 = this.ABAFilters.limit;
          });
          this.ELEMENT_DATA = this.AttributionNotMatched;
          this.dataSource = new MatTableDataSource<Element>(this.ELEMENT_DATA);
        }
        else if (response.Item2.length == 0 && Id == "2") {
          this.AttributionNotMatched = [];
          this.ELEMENT_DATA = this.AttributionNotMatched;
          this.dataSource = new MatTableDataSource<Element>(this.ELEMENT_DATA);
          this.pageAttributionNotMatchedSize = 0;
          this.EmptyAttributionNotMatchedlist = true;
          this.pageSize2 = this.ABAFilters.limit;

        }//End
        this.loader.Stop();
      }
    )
  }
  //END
}
