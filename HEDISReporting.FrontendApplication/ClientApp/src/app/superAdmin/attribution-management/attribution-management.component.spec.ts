import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttributionManagementComponent } from './attribution-management.component';

describe('AttributionManagementComponent', () => {
  let component: AttributionManagementComponent;
  let fixture: ComponentFixture<AttributionManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttributionManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttributionManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
