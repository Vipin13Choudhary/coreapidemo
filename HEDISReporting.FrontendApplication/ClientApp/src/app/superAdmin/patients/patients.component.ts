import { Organization } from './../../model/Organization';
import { Component, OnInit, AfterViewInit, Directive, ElementRef, Output, EventEmitter, HostListener, TemplateRef,ViewChild } from '@angular/core';
import {PatientService} from '../../service/patient/patient.service';
import {listOperation, statesDropDown, organizationDropDown,
  EthnicityDropDown, RaceDropDown, OrganizationListDropDown, GenderDropDown} from '../../model/common/common';
import {MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import {PageEvent} from '@angular/material';
import { ToastrService, Toast } from 'ngx-toastr';
import { UserService } from './../../service/user/user.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Config } from 'protractor';
import { FileUploader } from 'ng2-file-upload';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Router } from '@angular/router';
import { LoaderService } from 'src/app/service/loader/loader.service';


@Component({
  selector: 'app-patients',
  templateUrl: './patients.component.html',
  styleUrls: ['./patients.component.css'],
})

export class PatientsComponent implements OnInit, AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sorting: MatSort;
  config: Config;
  PatientList = [];
  listOperands:any;
  ELEMENT_DATA: any;
  dataSource: any;
  length = 5;
  pageSize = 10;
  pageSizeOptions: number[] = [1, 5, 10, 20];
  displayedColumns = ['PatientName', 'OrganizationName','DOB','Gender', 'PrimaryProvider','EthnicityName','RaceName','Action'];
  pageEvent: PageEvent;
  search: "";
  PatientFilterForm:FormGroup;
  EthnicityDropDown:EthnicityDropDown;
  RaceDropDown:RaceDropDown;
  GenderDropDown:GenderDropDown;
  OrganizationListDropDown:OrganizationListDropDown;
  hide = false;
  modalRef: BsModalRef;
  Race:any;
  OrgID:"";
  LoginUserId:string;
  message:string;
  LoginOrganizationId:number;
  Role:string;
  
  // tslint:disable-next-line:max-line-length
  constructor(private loader: LoaderService, private patientService:PatientService,private userService: UserService,private fb:FormBuilder,
    private _elementRef : ElementRef,private modalService: BsModalService,private toastr: ToastrService,private router:Router) {
    this.userService.changeScreen("false")
    this.listOperands=new listOperation();
    this.PatientFilterForm= fb.group({
      EthnicityID:0,
      RaceID:0,
      OrganizationID:0,
      GenderID:0,
    });
   }
   public uploader: FileUploader = new FileUploader({ url: 'api/your_upload', removeAfterUpload: false, autoUpload: true, });
   public file = this.uploader.queue[0];
   public fileData:FormData;

  ngOnInit() {
    debugger;
    this.loader.Start();
    this.patientService.currentMessage.subscribe(message => this.message = message);
    this.fnPassPageUrlToClientChart()
    this.LoginUserId =localStorage.getItem('LoginUserID');
    this.listOperands.offset=1;
    this.listOperands.limit=10;
    this.listOperands.order="desc";
    this.listOperands.sort="";
    this.listOperands.Search="";
    this.listOperands.OrganizationId=0;
    this.Role=localStorage.getItem("Role");
    this.LoginOrganizationId=parseInt(localStorage.getItem("OrganizationId"));
    this.listOperands.OrganizationId=this.Role=="SuperAdmin"?0:this.LoginOrganizationId;
    this.getAllPatientList(this.listOperands);
    this.getEthnicityDropDown(0);
    this.getRaceDropDown(0);
    this.getOrganizationDropDown(this.Role=="SuperAdmin"?0:this.LoginOrganizationId);
    this.getGenderDropDown(0);
  }
  fnPassPageUrlToClientChart() {
    this.patientService.changeMessage(this.router.url)
  }
  onFileSelected(event: EventEmitter<File[]>)
  {

      const file: File = event[0];
      this.fileData= new FormData();
      this.fileData.append('uploadFile', file, file.name);
  }
  UploadFile() {
    debugger;
    if(typeof(this.fileData)=='undefined')
    {
      this.toastr.warning("Please choose C-CDA File.","Warning");
      return;
    }
   

    if(typeof this.OrgID.toString()=='undefined' || this.OrgID.toString()=='0' )
    {
      this.toastr.warning("Please select organization first.","Warning");
      return;
    }
   
    this.patientService.GetOrganizationFolderDetail(this.OrgID).subscribe(response=>{
      console.log(response);
      if(response==null)
      {
        this.toastr.warning("Please fill out Data Source C-CDA detail for this organization  to proceed with uploading patient's C-CDA.","warning");
      }
      else
      {
        
        this.fileData.append('UserId',localStorage.getItem('LoginUserID'));
            this.fileData.append('OrganizationId',this.OrgID.toString());
            this.patientService.Uploadccda(this.fileData).subscribe(response => {
              if(response.statusCode==200){
                this.modalRef.hide();
                this.toastr.success('CCDA has been upload successfully..', 'Success');
                let model:{EthnicityID:number,GenderID:number,OrganizationID:number,RaceID:number}={
                  EthnicityID:0,
                  GenderID:0,
                  OrganizationID: parseInt(this.OrgID.toString()) ,
                  RaceID:0
                };
                this.PatientFilterForm.get('OrganizationID').setValue(parseInt(this.OrgID.toString()));
                this.onFilterFormSubmit(model,1);
              }
              else{
                this.modalRef.hide();
               this.toastr.error('There is error in uploading CCDA.Please try after sometime.', 'Error');
              }
            });
      }
    });
  
  
    }
  ngAfterViewInit() {    
    // $.getScript('../../assets/js/sidebar.js', function() { });
  }

  setPageSizeOptions(setPageSizeOptionsInput: string) {
    this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
  }

  getServerData(pageData){
    this.listOperands.offset=pageData.pageIndex+1;
    this.listOperands.limit=pageData.pageSize;
    this.getAllPatientList(this.listOperands);
  }
  //short data as per coulumn 
  sortData(header,OrderType){
    debugger;
    this.loader.Start();
    if(OrderType.currentTarget.attributes.getNamedItem('aria-sort')!=null){
      OrderType=OrderType.currentTarget.attributes.getNamedItem('aria-sort').value;
    if(OrderType=="descending"){
      OrderType='desc';
    }
    else
    {
      OrderType='asc';
    }
      }
    else{
       this.loader.Stop();
         return;
     }
        this.listOperands.sort=header;
        this.listOperands.order=OrderType;
        this.getAllPatientList(this.listOperands);
        this.loader.Stop();
  }



  getSearchedData(){
    this.listOperands.Search=this.search;
    this.getAllPatientList(this.listOperands);
  }
  getFilteredList(PatientID){
    this.listOperands.PatientID=PatientID;
    this.getAllPatientList(this.listOperands);
  }
  getAllPatientList(data){
    debugger;
    this.ELEMENT_DATA=[];
    this.PatientList=[];
    this.dataSource=[];
    this.patientService.getAllPatientList(data).subscribe(
      response=>{
        debugger;
            if(response.length>0){
              this.length=response[0].TotalRows;
              this.PatientList=[];
              response.forEach(element => {
              this.PatientList.push({
              PatientID:element.PatientID,
              PatientName:element.PatientName,
              OrganizationName:element.OrganizationName,
              DOB:element.DOB,
              Gender:element.Gender,
              PrimaryProvider:element.PrimaryProvider,
              Ethnicity:element.EthnicityName,
              Race:element.RaceName,
              OrganizationId:element.OrganizationId,
              FileName:element.FileName == null ? false : element.FileName,
             // FileName:element.FileName == Null ? true : element.FileName,
              TagCount:element.TagCount,
              });
                });
              }
            else
            {
            this.pageSize=0;
            }
            this.ELEMENT_DATA=[];
            this.dataSource=[];
            this.ELEMENT_DATA = this.PatientList;
            this.dataSource = new MatTableDataSource<Element>(this.ELEMENT_DATA);
        //console.log(response);
        this.loader.Stop();
      }
    );

  }
  EditPatient(element){
    //
     // this.router.navigate(['/Admin', 'editOrganization']);

    }

    DeletePatient(element,modelName){

    }
    // sidebarExp(){
    //   // this.hide = !this.hide;
    //   const elems = document.getElementsByClassName('sidebarMain') as HTMLCollectionOf<HTMLElement>;
    // for ( let i = 0, l = elems.length; i < l; i++ ) {
    //     elems[ i ].style["width"] = "900px";
    //     elems[ i ].style["height"] = "1000px";
    // }
    // }
    sidebarCollapse(){

       this.hide = !this.hide;
    }
    sidebarExp(){
      this.hide = !this.hide;
    }
    // sidebarCollapse(){
    //   document.getElementById("sidebarMain").style.display = 'none';
    //   $("#overlaySidebar").remove();
    // }

    // sidebarExp(){
    //   document.getElementById("sidebarMain").style.display = 'block';
    //   $("body").append("<div id='overlaySidebar'></div>");
    // }
    onFilterFormSubmit(model,typeOfCall)
    {

      this.ELEMENT_DATA=[];
      this.PatientList=[];
      this.patientService.GetFilteredPatientList(model).subscribe(response=>{
        if(response.length>0){
          this.length=response[0].TotalRows;
          response.forEach(element => {
            this.PatientList.push({PatientID:element.PatientID,PatientName:element.PatientName,
              OrganizationName:element.OrganizationName,
              DOB:element.DOB,
              Gender:element.Gender,
              PrimaryProvider:element.PrimaryProvider,
              Ethnicity:element.EthnicityName,
              Race:element.RaceName,
           });
          });

          }
          else{
           this.pageSize=0;
          }
         this.ELEMENT_DATA = this.PatientList;
         this.dataSource = new MatTableDataSource<Element>(this.ELEMENT_DATA);
         if(typeOfCall==0)
         this.hide = !this.hide;

         });
    }
    getRaceDropDown(RaceId:any){
     this.RaceDropDown = new RaceDropDown();
     this.RaceDropDown.Race=new Array<organizationDropDown>();
     this.patientService.GetRace(RaceId).subscribe(
     (response)=>{
       console.log(response);
        this.RaceDropDown.Race=response.result.race;
     }
   )
    }
    getEthnicityDropDown(EthnicityId:any){

      this.EthnicityDropDown = new EthnicityDropDown();
      this.EthnicityDropDown.ethnicity=new Array<organizationDropDown>();
      this.patientService.GetEthinicity(EthnicityId).subscribe(
      (response)=>{
       this.EthnicityDropDown.ethnicity=response.result.ethnicity;
      }
    )
     }
     getOrganizationDropDown(OrganizationId:any){
      this.OrgID = OrganizationId;
      this.OrganizationListDropDown = new OrganizationListDropDown();
      this.OrganizationListDropDown.Organization=new Array<organizationDropDown>();
      this.patientService.GetOrganization(OrganizationId).subscribe(
      (response)=>{
       this.OrganizationListDropDown.Organization=response.result.organization;
      });
    }
    getGenderDropDown(GenderId:any){

      this.GenderDropDown = new GenderDropDown();
      this.GenderDropDown.Gender=new Array<organizationDropDown>();
      this.patientService.GetGender(GenderId).subscribe(
      response=>{
        this.GenderDropDown.Gender=response.result.gender;
        this.loader.Stop();
      }
    )
     }
    ClearFilters()
    {
      this.getAllPatientList(this.listOperands);
      this.PatientFilterForm.controls['OrganizationID'].setValue(0);
      this.PatientFilterForm.controls['EthnicityID'].setValue(0);
      this.PatientFilterForm.controls['RaceID'].setValue(0);
      this.PatientFilterForm.controls['GenderID'].setValue(0);
    }

    fnShowModel(modelName)
  {
    let template: TemplateRef<any>=modelName;
    this.modalRef = this.modalService.show(template);

  }
 //View Patient CDA Files
 fnGetPatientCDA(element)
 {
  debugger;
  //  if(element.TagCount > 0){
  //   this.toastr.warning("You are not authorized to view patient CCDA");
  //    return true
  //  }
  
  let test=element;
  element.OrganizationId
  element.FileName
  if(element.OrganizationId && element.FileName)
  {
    debugger;
    this.patientService.GetPatientCda(element.OrganizationId,element.FileName,true,element.PatientID).subscribe(
      response=>{
        debugger;
        var win = window.open(response.toString(), '_blank');
        win.focus();
      }
    )
  }
  
 }
 fnSyncFile() {
  debugger;
  if(typeof this.OrgID.toString()=='undefined' || this.OrgID.toString()=='0' )
  {
    this.toastr.warning("Please select organization first.","Warning");
    return;
  }
  this.toastr.info("We will notify you once the syncing is done.","Alert");
  this.modalRef.hide();
  this.patientService.SyncCCDAFiles(this.OrgID,this.LoginUserId).subscribe(response=>{
    let model:{EthnicityID:number,GenderID:number,OrganizationID:number,RaceID:number}={
      EthnicityID:0,
      GenderID:0,
      OrganizationID: parseInt(this.OrgID.toString()) ,
      RaceID:0
    };
    if(response==1)
    {
      this.toastr.success("C-CDA files has been synced.Please refresh the Patient list");
      this.onFilterFormSubmit(model,1);
    }
    else if(response==-1)
    this.toastr.warning("Please complete the datasource detail of the organizaion");
    else
    this.toastr.error("There was some error in syncing the C-CDA Files.Please try syncing again.");
    
   
  });

// Added on 13032019
// updateIsConsentTag(element,isConsent : boolean): void{
//   debugger;
//   //alert(element.IsConsent);
//     this.patientService.updateIsConsentTag(element.PatientID, isConsent).subscribe(
//       response=>{
//         debugger;
//         this.toastr.success("Consent Tag Status Updated Successfully");
//         this.getAllPatientList(this.listOperands);
//         // var win = window.open(response.toString(), '_blank');
//         // win.focus();
//       }
//     )
// }

 }
}
