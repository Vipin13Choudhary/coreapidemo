export class organizationClinic
{
    ClinicId          :number  ;
    ClinicName        :string  ;
    ClinicAddress     :string  ;
    ClinicAddress1    :string  ;
    ClinicAddress2    :string  ;
    ClinicPhoneNumber :string  ;
    ClinicFax         :string  ;
    CountryId         :number  ;
    StateId           :number  ;
    City              :string  ;
    PostalCode        :string  ;
    OrganizationId    :number  ;
    ContactPerson     :string  ;
    ContactPersonEmail:string  ;
    ContactPersonPhone:string  ;
    CreatedBy:number;
    ClinicDisplayName:string;
    ClinicLocationId:string;
   
}