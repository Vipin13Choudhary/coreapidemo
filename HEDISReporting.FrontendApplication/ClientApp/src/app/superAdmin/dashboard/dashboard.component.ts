import { Component, OnInit, TemplateRef, AfterViewInit } from '@angular/core';
import { DashboardService } from '../../service/dashboard/dashboard.service';
import { CommonFilters } from '../../model/dashboard/dashboard'
import { UserService } from '../../service/user/user.service';
import { PatientService } from '../../service/patient/patient.service';
import { Router } from '@angular/router';
import { ToastrService, ToastRef } from 'ngx-toastr';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { organizationDropDown, commonDropDown } from '../../model/common/common';
import * as moment from 'moment';
import { timeout } from 'q';
import { debug } from 'util';
import { IfStmt } from '@angular/compiler';
import { LoaderService } from 'src/app/service/loader/loader.service';
import { filter } from 'rxjs-compat/operator/filter';
declare var $: any;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, AfterViewInit {


  public scrollbarOptions = { axis: 'x', theme: 'minimal-dark' };
  ProviderItem: organizationDropDown
  OrganizationItem: organizationDropDown;
  AssociationItem: organizationDropDown;
  clinicItem: organizationDropDown;
  payerItem: organizationDropDown;
  percentagefactor: number;
  ABAFilters: CommonFilters;

  Numerator: number;
  Denominator: number;
  EligiblePatient: any = []
  QualifiedPatient: any = []
  NonQualifiedPatient: any = []
  Result: number;
  POPResult: number;
  POPResult1: number;
  DivId:any;
  range:string;
  POPNumerator: number;
  showfilter: any = 'false';
  ClearFilters: string = 'false';
  filteringData: any;
  date: Date = new Date();
  sync: boolean = false;
  totalPatient: number = 0;
  filteredDate = (this.date.getMonth() + 1) + '/' + this.date.getDate() + '/' + this.date.getFullYear();
  // associationGoal: any = [];
  corporateGoal: any;

  // corporateGoal: any
  AssPatientNeeded: number;
  CoPatientNeeded: number;
  PayPatientNeeded: number;
  ProPatientNeeded: number;
  ClinicPatientNeeded: number;
  Selectedcolumn: number = 0;
  AssGoal: boolean = true;
  CopGoal: boolean = true;
  modalRef: BsModalRef;
  SelectedComparableItem: number;
  GoalSelectedOrganization: number = 0;
  Numerator_BCS: number = 0;
  Denominator_BCS: number = 0;
  Numerator_CCS: number = 0;
  Denominator_CCS: number = 0;
  Result_CCS: number = 0;
  Denominator_CBP: number = 0;
  Numerator_CBP: number = 0;
  Result_CBP: number = 0;
  item: number = 1;
  selectedMasterMeasure: number = 1;
  OrganizationList: organizationDropDown[];
  compareToObject = [
    { "Id": 1, "Name": "Association Goal" },
    { "Id": 2, "Name": "Corporate Goal" },

    { "Id": 4, "Name": "Payer Goal" },
    { "Id": 5, "Name": "Provider Goal" },
    { "Id": 6, "Name": "Clinic Goal" }
  ];
  

  // obj= [
  //   { "id": 2, "name": "lata" }];
  associationGoal: any;
  listdata: any;
  OrgGoal: boolean = false;
  ProGoal: boolean = false;
  ClinicGoal: boolean = false;
  PayGoal: boolean = false;
  ClinicGoalPerF: boolean = true;
  CopGoalPerF: boolean = true;
  AssGoalPerF: boolean = true;
  OrgGoalPerF: boolean = true;
  PayGoalPerF: boolean = true;
  ProGoalPerF: boolean = true;
  AssGoalPer: boolean = false;
  Role:string

  CopGoalPer: boolean = false;
  OrgGoalPer: boolean = false;
  PayGoalPer: boolean = false;
  ProGoalPer: boolean = false;
  ClinicGoalPer: boolean = false;
  payerGoal: any
  providerGoal: any
  clinicGoal: any
  MasterMeasuretypeList: commonDropDown[];
  DeletedSelectedGoal: number = 0;
  Numerator_COL: number = 0;
  Denominator_COL: number = 0;
  Result_COL: number = 0;
  Result_BCS: number = 0;
  bsValue = new Date();
  maxDate = new Date();
  bsValue1 = new Date();
  maxDate1 = new Date();
  html: string = '<span class="btn btn-danger">Never trust not sanitized HTML!!!</span>';
  MeasureList: any;
  constructor(private loader: LoaderService, private dashboardService: DashboardService, private userService: UserService, private router: Router,
    private toaster: ToastrService, private modalService: BsModalService, private patientService: PatientService
  ) {
    this.userService.changeScreen("false")
  }
  ngAfterViewInit() {

  }
  ngOnInit() {
    debugger;
    this.loader.Start();
    this.Role=localStorage.getItem('Role');
    this.dashboardService.currentFilter.subscribe(message => this.showfilter = message);
    this.dashboardService.currentClearFilter.subscribe(message => this.ClearFilters = message);
    this.ABAFilters = new CommonFilters();
    this.ABAFilters.OrganizationId =this.Role=="SuperAdmin"?0:parseInt(localStorage.getItem("OrganizationId"));
this.ABAFilters.MeasureId = 0;
    // this.OrganizationItem=this.obj;
    this.ABAFilters.VendorId = 0;
    this.ABAFilters.AgeRange = '1-200';
    this.ABAFilters.Gendor = 0;
    this.bsValue=new Date('01/01/2018');
    this.maxDate=new Date('12/31/2018');

    this.bsValue1=new Date('01/01/2019');
    this.maxDate1=new Date('12/31/2019');
    if(Number(localStorage.getItem('MasterMeaure'))==1|| Number(localStorage.getItem('MasterMeaure'))==2 || Number(localStorage.getItem('MasterMeaure'))==0 || isNaN(Number(localStorage.getItem('MasterMeaure'))))
    {
      this.ABAFilters.PeriodEnd =  moment(this.maxDate).format('L').toString() 
        this.ABAFilters.PeriodStart =  moment(this.bsValue.setFullYear(this.bsValue.getFullYear())).format('L')
        }
    
    else 
    {
      this.ABAFilters.PeriodEnd =  moment(this.maxDate1).format('L').toString() 
      this.ABAFilters.PeriodStart =  moment(this.bsValue1.setFullYear(this.bsValue1.getFullYear())).format('L') 
      }
    // this.ABAFilters.PeriodEnd = moment(this.maxDate).format('L').toString();
    // this.ABAFilters.PeriodStart = moment(this.bsValue).format('L');;
    this.ABAFilters.ProviderId = 0;
    this.ABAFilters.PayerId = 0;
    // this.ABAFilters.OrganizationId = 0;

    this.ABAFilters.ClinicId = 0;
    this.ABAFilters.RaceId = 0;
    this.ABAFilters.EthnicityId = 0;
    this.ABAFilters.NewCal = 0;
    this.ABAFilters.IsFilter = 0;
    this.ABAFilters.UserId = Number(localStorage.getItem("LoginUserID"));
    this.ABAFilters.Type = 1;
    if(Number(localStorage.getItem("MasterMeaure"))==0)
    {
    localStorage.setItem('MasterMeaure', this.selectedMasterMeasure.toString());
    this.selectedMasterMeasure=Number(localStorage.getItem("MasterMeaure"));
    }
    else{
      this.ABAFilters.Type=Number(localStorage.getItem("MasterMeaure"));
      this.selectedMasterMeasure=Number(localStorage.getItem("MasterMeaure"));
    }

    this.dashboardService.GetAllMeasureTypeMaster().subscribe(response => {
      this.MasterMeasuretypeList = response;


    })
    this.GetMeasures();
   // localStorage.setItem("MasterMeaure", "1");


    // this.GetABAAllDetails();
    // this.GetCOLAllDetails();

  }


  GetABAAllDetails() {
    this.dashboardService.getAllABADetails(this.ABAFilters).subscribe(
      response => {

        this.Numerator = response.numericalData[0].numerator;
        this.Denominator = response.numericalData[0].denominator;
        this.Result = response.result[0];

        // response.allEligiblePatient.forEach(element => {
        //   this.EligiblePatient.push({
        //     Name: element.patientName, NextVisit: element.nextVisit,
        //     LastVisit: element.lastVisit, RaceId: element.raceId, Status: element.status, EthnicityId: element.ethnicityId,
        //     DOB: element.dob, Age: element.age, OrganizationId: element.organizationId
        //   })
        // });
        // this.totalPatient = response.allEligiblePatient.length;

        // response.nonQualifiedPatient.forEach(element => {
        //   this.NonQualifiedPatient.push({
        //     Name: element.patientName, NextVisit: element.nextVisit,
        //     LastVisit: element.lastVisit, RaceId: element.raceId, Status: element.status, EthnicityId: element.ethnicityId,
        //     DOB: element.dob, Age: element.age, OrganizationId: element.organizationId
        //   })
        // });
        // response.qualifiedPatient.forEach(element => {
        //   this.QualifiedPatient.push({
        //     Name: element.patientName, NextVisit: element.nextVisit,
        //     LastVisit: element.lastVisit, RaceId: element.raceId, Status: element.status, EthnicityId: element.ethnicityId,
        //     DOB: element.dob, Age: element.age, OrganizationId: element.organizationId
        //   })
        // });


        var AbaBar = document.querySelector("#AbaProgressBar") as HTMLElement;
        AbaBar.style.backgroundColor = "#D93600";
        AbaBar.style.width = (this.Result + "%").toString();
        //  this.html = '<span>Current ('+(this.Result).toFixed(2)  +'% increase)<span></br><div class="col-sm-12"><div class="col-sm-8 progress"><div class="progress-bar" role="progressbar" aria-valuenow="50"aria-valuemin="0" aria-valuemax="100" style="width:50%"></div></div><div class="col-sm-4">'+this.Numerator+'</div></div><span>Prior</span><br/><div class="progress" style="width:70%"><div class="progress-bar" role="progressbar" aria-valuenow="50"aria-valuemin="0" aria-valuemax="100" style="width:50%"></div></div> 0';
        //this.html='<span>Current ('+(this.Result).toFixed(2)  +'% increase)<span></br><div class="row"><div class="col-sm-8 progress"><div class="progress-bar" role="progressbar" aria-valuenow="50"aria-valuemin="0" aria-valuemax="100" style="width:50%"></div></div><div class="col-sm-4"><span>'+this.Numerator+'</span></div></div>'
        // this.html='<div class="row"><div class="col-sm-6"><small>Current ( <span class="text-success">23.6%</span> Increase)</small><div class="row"><div class="col-sm-8"><div class="progress"><div class="progress-bar bg-danger" role="progressbar" style="width:40%"></div></div> </div><div class="col-sm-4"><small>22.5 K</small></div></div><small>Prior</small><div class="row"><div class="col-sm-8"> <div class="progress"><div class="progress-bar bg-danger" role="progressbar" style="width:40%"></div></div></div><div class="col-sm-4"><small>18.2 K</small></div></div> </div></div> '
        this.getDefaultGoalsDetails();
        if (this.sync == true) {
          this.toaster.success("Measure successfully calculated with latest data !");
          this.sync = false;

        }
      }
    )
  }
  ClearFilter() {

    this.ABAFilters.MeasureId = 0;
    this.ABAFilters.VendorId = 0;
    this.ABAFilters.AgeRange = '1-200';
    this.ABAFilters.Gendor = 0;
    if(Number(localStorage.getItem("MasterMeaure"))==1 ||(Number(localStorage.getItem("MasterMeaure"))== 2))
    {
    this.ABAFilters.PeriodEnd = this.filteringData.DateRange == null ? moment(this.maxDate).format('L').toString() : this.filteringData.DateRange[1];
    this.ABAFilters.PeriodStart = this.filteringData.DateRange == null? moment(this.bsValue.setFullYear(this.bsValue.getFullYear())).format('L') : this.filteringData.DateRange[0];
    }
    else 
  {
    this.ABAFilters.PeriodEnd = this.filteringData.DateRange == null ? moment(this.maxDate1).format('L').toString() : this.filteringData.DateRange[1];
    this.ABAFilters.PeriodStart = this.filteringData.DateRange == null? moment(this.bsValue1.setFullYear(this.bsValue1.getFullYear())).format('L') : this.filteringData.DateRange[0];
  }
    // this.ABAFilters.PeriodEnd = moment(this.maxDate).format('L').toString();
    // this.ABAFilters.PeriodStart = this.ABAFilters.PeriodStart;
    this.ABAFilters.ProviderId = 0;
    this.ABAFilters.RaceId = 0;
    this.ABAFilters.EthnicityId = 0;
    this.ABAFilters.UserId = Number(localStorage.getItem("LoginUserID"));
    this.ABAFilters.offset = this.ABAFilters.offset;
    this.ABAFilters.limit = this.ABAFilters.limit;
    this.ABAFilters.order = this.ABAFilters.order;
    this.ABAFilters.sort = "";
    this.ABAFilters.Search = "";
    this.ABAFilters.IsFilter = 0;
    this.ABAFilters.OrganizationId = 0;
    this.ABAFilters.ClinicId = 0;
    this.ABAFilters.PayerId = 0;
    this.ABAFilters.ProviderId = 0;
    this.ABAFilters.Search = null;
    this.ABAFilters.sort = null;
    // this.GetABAAllDetails();
    // this.GetCOLAllDetails();
    this.GetMeasures();


  }
  GetDashboardAsPer(MasterMeasure: number) {
    debugger;
    localStorage.setItem('MasterMeaure', MasterMeasure.toString());
    MasterMeasure=MasterMeasure==3?1:MasterMeasure==4?2:MasterMeasure
    this.ABAFilters.Type = MasterMeasure;
    this.selectedMasterMeasure = MasterMeasure;
   if((Number(localStorage.getItem('MasterMeaure'))==1||(Number(localStorage.getItem('MasterMeaure'))==2)))
{
  this.ABAFilters.PeriodEnd =  moment(this.maxDate).format('L').toString() 
    this.ABAFilters.PeriodStart =  moment(this.bsValue.setFullYear(this.bsValue.getFullYear())).format('L')
    }

else 
{
  this.ABAFilters.PeriodEnd =  moment(this.maxDate1).format('L').toString() 
  this.ABAFilters.PeriodStart =  moment(this.bsValue1.setFullYear(this.bsValue1.getFullYear())).format('L') 
  }

   this.GetMeasures();
    // switch (MasterMeasure) {
    //   case 1:
    //     this.GetAllHedisDashboardData();
    //     break;

    //   case 2:
    //     this.GetAllWellCareDashboardData();
    //     break;
    // }
  }
  GetBCSAllDetails() {

    this.dashboardService.GetBCSAllDetails(this.ABAFilters).subscribe(
      response => {
        this.Numerator_BCS = response.numericalData[0].numerator;
        this.Denominator_BCS = response.numericalData[0].denominator;
        this.Result_BCS = response.result[0];
        var BCSBar = document.querySelector("#BCSProgressBar") as HTMLElement;
        BCSBar.style.backgroundColor = "#008000";
        BCSBar.style.width = (this.Result_BCS + "%").toString();
      }
    )

  }
  GetABAWellCareDetails() {

    this.dashboardService.GetABAWellCareDetails(this.ABAFilters).subscribe(
      response => {
        this.Numerator = response.numericalData[0].numerator;
        this.Denominator = response.numericalData[0].denominator;
        this.Result = response.result[0];
        var AbaBar = document.querySelector("#AbaProgressBar") as HTMLElement;
        if (response.result[0] <= 25) {
          AbaBar.style.backgroundColor = "#FFC300";
        }
        else if (response.result[0] >= 26 && response.result[0] <= 50) {
          AbaBar.style.backgroundColor = "#008000";
        }
        else if (response.result[0] >= 51 && response.result[0] <= 75) {
          AbaBar.style.backgroundColor = "#8b0000";
        }
        else if (response.result[0] >= 76 && response.result[0] <= 100) {
          AbaBar.style.backgroundColor = "#6495ED";
        }
        // AbaBar.style.backgroundColor = "#FA8072";
        AbaBar.style.width = (this.Result + "%").toString();
      }
    )

  }
  GetCOLWellCareDetails() {
    this.dashboardService.GetCOLWellCareDetails(this.ABAFilters).subscribe(
      response => {
        this.Numerator_COL = response.numericalData[0].numerator;;
        this.Denominator_COL = response.numericalData[0].denominator;
        this.Result_COL = response.result[0];
        var COLBar = document.querySelector("#COLProgressBar") as HTMLElement;
        if (response.result[0] <= 25) {
          COLBar.style.backgroundColor = "#FFC300";
        }
        else if (response.result[0] >= 26 && response.result[0] <= 50) {
          COLBar.style.backgroundColor = "#008000";
        }
        else if (response.result[0] >= 51 && response.result[0] <= 75) {
          COLBar.style.backgroundColor = "#8b0000";
        }
        else if (response.result[0] >= 76 && response.result[0] <= 100) {
          COLBar.style.backgroundColor = "#6495ED";
        }
        // COLBar.style.backgroundColor = "#FFC300";
        COLBar.style.width = (this.Result_COL + "%").toString();
      }
    )

  }
  GetBCSWellCareDetails() {

    this.dashboardService.GetBCSWellCareAllDetails(this.ABAFilters).subscribe(
      response => {
        this.Numerator_BCS = response.numericalData[0].numerator;
        this.Denominator_BCS = response.numericalData[0].denominator;
        this.Result_BCS = response.result[0];
        var BCSBar = document.querySelector("#BCSProgressBar") as HTMLElement;
        if (response.result[0] <= 25) {
          BCSBar.style.backgroundColor = "#FFC300";
        }
        else if (response.result[0] >= 26 && response.result[0] <= 50) {
          BCSBar.style.backgroundColor = "#008000";
        }
        else if (response.result[0] >= 51 && response.result[0] <= 75) {
          BCSBar.style.backgroundColor = "#8b0000";
        }
        else if (response.result[0] >= 76 && response.result[0] <= 100) {
          BCSBar.style.backgroundColor = "#6495ED";
        }
        // BCSBar.style.backgroundColor = "#008000";
        BCSBar.style.width = (this.Result_BCS + "%").toString();
      }
    )


  }
  GetCCSWellCareDetails() {

    // this.Numerator_CCS = 0;
    // this.Denominator_CCS = 0;
    // this.Result_CCS = 0;
    // var CCSBar = document.querySelector("#CCSProgressBar") as HTMLElement;
    // CCSBar.style.backgroundColor = "#6495ED";
    // CCSBar.style.width = (this.Result_CCS + "%").toString();
    this.dashboardService.GetCCSWellCareDetails(this.ABAFilters).subscribe(
      response => {
        this.Numerator = response.numericalData[0].numerator;
        this.Denominator = response.numericalData[0].denominator;
        this.Result = response.result[0];
        var CCSBar = document.querySelector("#ccsProgressBar") as HTMLElement;
        // AbaBar.style.backgroundColor = "#FA8072";
        if (response.result[0] <= 25) {
          CCSBar.style.backgroundColor = "#FFC300";
        }
        else if (response.result[0] >= 26 && response.result[0] <= 50) {
          CCSBar.style.backgroundColor = "#008000";
        }
        else if (response.result[0] >= 51 && response.result[0] <= 75) {
          CCSBar.style.backgroundColor = "#8b0000";
        }
        else if (response.result[0] >= 76 && response.result[0] <= 100) {
          CCSBar.style.backgroundColor = "#6495ED";
        }
        CCSBar.style.width = (this.Result + "%").toString();
      }
    )

  }


GetCBPWellCareDetails()
{
  this.dashboardService.GetCalculatedCBPWellCareAllDetails(this.ABAFilters).subscribe(
    response => {
      this.Numerator_CBP = response.numericalData[0].numerator;
      this.Denominator_CBP = response.numericalData[0].denominator;
      this.Result_CBP = response.result[0];
      var CBPBar = document.querySelector("#CBPProgressBar") as HTMLElement;
      CBPBar.style.backgroundColor = "#008000";
      CBPBar.style.width = (this.Result_CBP + "%").toString();
    }
  )
}

  GetAllWellCareDashboardData() {
    this.GetABAWellCareDetails();
    this.GetCOLWellCareDetails();
    this.GetBCSWellCareDetails();
    this.GetCCSWellCareDetails();
    this.GetCBPWellCareDetails();

  }
  GetAllHedisDashboardData() {
    this.GetMeasures();
    // this.GetABAAllDetails();
    // this.GetCOLAllDetails();
    // this.GetBCSAllDetails();
    // this.GetCCSAllDetails();
  }
  GetCCSAllDetails() {
    debugger
    this.dashboardService.GetCCSAllDetails(this.ABAFilters).subscribe(
      response => {
        this.Numerator_CCS = response.numericalData[0].numerator;
        this.Denominator_CCS = response.numericalData[0].denominator;
        this.Result_CCS = response.result[0];
        var CCSBar = document.querySelector("#CCSProgressBar") as HTMLElement;
        if (response.result[0] <= 25) {
          CCSBar.style.backgroundColor = "#FFC300";
        }
        else if (response.result[0] >= 26 && response.result[0] <= 50) {
          CCSBar.style.backgroundColor = "#008000";
        }
        else if (response.result[0] >= 51 && response.result[0] <= 75) {
          CCSBar.style.backgroundColor = "#8b0000";
        }
        else if (response.result[0] >= 76 && response.result[0] <= 100) {
          CCSBar.style.backgroundColor = "#6495ED";
        }
        // CCSBar.style.backgroundColor = "#6495ED";
        CCSBar.style.width = (this.Result_CCS + "%").toString();
      }
    )
  }

  showAndHideFilters(value) {
    this.dashboardService.changeScreenSetting(value)
  }
  // sidebarExp(){
  //   var element = document.getElementById("sidebarMain");
  //   element.classList.add("expandedSidebar");
  // }
  receiveMessage($event) {
 
    this.filteringData = $event
    if (this.filteringData.AgeRange == null) {
      this.filteringData.AgeRange = '1-200';
    }
    this.ABAFilters.EthnicityId = this.filteringData.EthnicityId;
    this.ABAFilters.Gendor = this.filteringData.GenderID;
    this.ABAFilters.MeasureId = this.filteringData.MeasureId;
    this.ABAFilters.OrganizationId = this.filteringData.OrganizationID;
    this.ABAFilters.ProviderId = this.filteringData.ProviderID;
    this.ABAFilters.ClinicId = this.filteringData.ClinicID;
    this.ABAFilters.PayerId = this.filteringData.PayerID;
    if(Number(localStorage.getItem("MasterMeaure"))==1 ||(Number(localStorage.getItem("MasterMeaure"))== 2))
    {
    this.ABAFilters.PeriodEnd = this.filteringData.DateRange == null ? moment(this.maxDate).format('L').toString() : this.filteringData.DateRange[1];
    this.ABAFilters.PeriodStart = this.filteringData.DateRange == null? moment(this.bsValue.setFullYear(this.bsValue.getFullYear())).format('L') : this.filteringData.DateRange[0];
    }
    else 
  {
    this.ABAFilters.PeriodEnd = this.filteringData.DateRange == null ? moment(this.maxDate1).format('L').toString() : this.filteringData.DateRange[1];
    this.ABAFilters.PeriodStart = this.filteringData.DateRange == null? moment(this.bsValue1.setFullYear(this.bsValue1.getFullYear())).format('L') : this.filteringData.DateRange[0];
  }
    this.ABAFilters.RaceId = this.filteringData.RaceID;
    this.ABAFilters.VendorId = this.filteringData.VendorId;
    this.ABAFilters.AgeRange = this.filteringData.AgeRange;
    this.ABAFilters.IsFilter = 1;
    this.ABAFilters.UserId = Number(localStorage.getItem("LoginUserID"));
     //var selectedGoals:any =this.GetSelectedColumnArray(this.ABAFilters);
    this.SaveDashboardGoalDetails(this.ABAFilters, "Add");


   
    // this.GetABAAllDetails();
    // this.GetCOLAllDetails();
    // this.GetMeasures();
  }
  GetSelectedColumnArray(filters:CommonFilters):[]
  {
    var selectedGoals:any =[];
    if(filters.OrganizationId!=null)
    selectedGoals.push(2)
    if(filters.PayerId!=null)
    selectedGoals.push(4)
    if(filters.ProviderId!=null)
    selectedGoals.push(5)    
    if(filters.ClinicId!=null)
    selectedGoals.push(6)
    return selectedGoals;
  }
  GetListForMeasure(item: number, flag: number) {
    this.ABAFilters.MeasureId = item;


    localStorage.setItem('item', this.ABAFilters.MeasureId.toString());
    // if (item == 1) {
    this.router.navigate(['/Admin/patientList'], {
      queryParams: { id: item, status: flag,filters:true }
    });
    //}
    // else if (item == 2) {
    //   //this.dashboardService.ShowDataWithFilters(this.ABAFilters);
    //   // this.router.navigate(['/Admin/colPatientList', { id: 2,Status:item }]);
    //   this.router.navigate(['/Admin/patientList'], {
    //     queryParams: { id: item, status: flag }
    //   });
    // }
    // else if (item == 3) {
    //   //this.dashboardService.ShowDataWithFilters(this.ABAFilters);
    //   // this.router.navigate(['/Admin/colPatientList', { id: 2,Status:item }]);
    //   this.router.navigate(['/Admin/patientList'], {
    //     queryParams: { id: item, status: flag }
    //   });
    // }
    // else if (item == 4) {
    //   //this.dashboardService.ShowDataWithFilters(this.ABAFilters);
    //   // this.router.navigate(['/Admin/colPatientList', { id: 2,Status:item }]);
    //   this.router.navigate(['/Admin/patientList'], {
    //     queryParams: { id: item, status: flag }
    //   });
    // }
    // else if (item == 2) {
    //   //this.dashboardService.ShowDataWithFilters(this.ABAFilters);
    //   // this.router.navigate(['/Admin/colPatientList', { id: 2,Status:item }]);
    //   this.router.navigate(['/Admin/patientList'], {
    //     queryParams: { id: item, status: flag }
    //   });
    // }
  }

  SyncToLatestRecord() {
    this.toaster.success("Syncing started successfully !")
    this.ABAFilters.MeasureId = 0;
    this.ABAFilters.VendorId = 0;
    this.ABAFilters.AgeRange = '1-200';
    this.ABAFilters.Gendor = 0;
    if(Number(localStorage.getItem("MasterMeaure"))==1 ||(Number(localStorage.getItem("MasterMeaure"))== 2))
    {
    this.ABAFilters.PeriodEnd = this.filteringData.DateRange == null ? moment(this.maxDate).format('L').toString() : this.filteringData.DateRange[1];
    this.ABAFilters.PeriodStart = this.filteringData.DateRange == null? moment(this.bsValue.setFullYear(this.bsValue.getFullYear())).format('L') : this.filteringData.DateRange[0];
    }
    else 
  {
    this.ABAFilters.PeriodEnd = this.filteringData.DateRange == null ? moment(this.maxDate1).format('L').toString() : this.filteringData.DateRange[1];
    this.ABAFilters.PeriodStart = this.filteringData.DateRange == null? moment(this.bsValue1.setFullYear(this.bsValue1.getFullYear())).format('L') : this.filteringData.DateRange[0];
  }
    // this.OrganizationItem=this.obj;
    // this.ABAFilters.PeriodEnd = moment(this.maxDate).format('L').toString();
    // this.ABAFilters.PeriodStart = this.ABAFilters.PeriodStart;
    this.ABAFilters.ProviderId = 0;

    this.ABAFilters.PayerId = 0;
    this.ABAFilters.OrganizationId = 0;

    this.ABAFilters.ClinicId = 0;
    this.ABAFilters.RaceId = 0;
    this.ABAFilters.EthnicityId = 0;
    this.ABAFilters.UserId = Number(localStorage.getItem("LoginUserID"));
    this.ABAFilters.offset = this.ABAFilters.offset;
    this.ABAFilters.limit = this.ABAFilters.limit;
    this.ABAFilters.order = this.ABAFilters.order;
    this.ABAFilters.sort = "";
    this.ABAFilters.Search = "";
    this.ABAFilters.NewCal = 1;
    this.sync = true;
    // this.GetABAAllDetails();
    // this.GetCOLAllDetails();
    this.GetMeasures();

  }






  GetMeasures() {
    this.loader.Start();
    // console.log(this.MeasureList)
    
    this.AssGoal = true;
    this.CopGoal = this.ABAFilters.OrganizationId > 0 ? true : false;
    this.ProGoal = this.ABAFilters.ProviderId > 0 ? true : false;
    this.PayGoal = this.ABAFilters.PayerId > 0 ? true : false;
    this.ClinicGoal = this.ABAFilters.ClinicId > 0 ? true : false;
    this.dashboardService.getMeasurename(this.ABAFilters.UserId).subscribe(response => {
      //this.OrganizationItem = response.associationItem;
      this.ProviderItem = response.providerItem;
      this.OrganizationItem = response.organizationItem;
      this.clinicItem = response.clinicItem;
      this.payerItem = response.payerItem;
      let filterValues :CommonFilters=new CommonFilters();
      //---initialize new object to pass
      filterValues.OrganizationId=this.Role=="SuperAdmin"?0:response.organizationItem.id;;
      filterValues.ProviderId=response.providerItem.id;
      filterValues.ClinicId=response.clinicItem.id;
      filterValues.PayerId=response.payerItem.id;
      filterValues.UserId=this.ABAFilters.UserId;
     
      filterValues.Type=this.ABAFilters.Type;
      filterValues.AgeRange=this.ABAFilters.AgeRange;
      filterValues.PeriodStart=this.ABAFilters.PeriodStart;
      filterValues.PeriodEnd=this.ABAFilters.PeriodEnd;
     
      //-------------Load User selected goals along with other items on dahsboard-----
      this.dashboardService.getMeasuresList(filterValues).subscribe(response => {
       
        this.MeasureList = response;
  
        this.AssGoal = response[0].userDashboardSetting[0].associationGoal==0 ? true : false;
        this.CopGoal = response[0].userDashboardSetting[0].corporateGoal >0  ? true : false;
        this.ProGoal = response[0].userDashboardSetting[0].providerGoal >0 ? true : false;
        this.PayGoal = response[0].userDashboardSetting[0].payerGoal > 0 ? true : false;
        this.ClinicGoal = response[0].userDashboardSetting[0].clinicGoal > 0 ? true : false;
  
        this.Result = response[0].masterMeasureList[0].met;
        this.Result_COL = response[0].masterMeasureList[1].met;
        this.Result_BCS = response[0].masterMeasureList[2].met;
        this.Result_CCS = response[0].masterMeasureList[3].met;
        this.Result_CBP = response[1].masterMeasureList[0].met;
  
        // this.percentagefactor=response[0].masterMeasureList[0].met-response[0].userDashboardSetting[0].associationGoal;
   
        this.loader.Stop();
        setTimeout(() => { this.ColorCodeFunction(); }, 50);
      });

    }
   

    );  
  }
 
  ColorCodeFunction() {
    var CBPBar = document.querySelector("#CHBP") as HTMLElement;
    var AbaBar = document.querySelector("#ABA") as HTMLElement;
    var COLBar = document.querySelector("#CC") as HTMLElement;
    var CCSBar = document.querySelector("#CCS") as HTMLElement;
    var BCSBar = document.querySelector("#BC") as HTMLElement;
    
    var CBPBar11 = document.querySelector("#CHBP11") as HTMLElement;
    var AbaBar11 = document.querySelector("#ABA11") as HTMLElement;
    var COLBar11 = document.querySelector("#CC11") as HTMLElement;
    var CCSBar11= document.querySelector("#CCS11") as HTMLElement;
    var BCSBar11= document.querySelector("#BC11") as HTMLElement;

   


    
    if (this.Selectedcolumn == 0) {


  var measureAssGoalV = (this.Result - this.MeasureList[0].masterMeasureList[0].associationGoal);
  var measureAssGoal = Math.abs(this.Result - this.MeasureList[0].masterMeasureList[0].associationGoal);
//  AbaBar.style.width = (measureAssGoal + "%").toString();
  AbaBar.style.width = (this.Result + "%").toString();
    // AbaBar.style.width = (this.Result + "%").toString();
//   AbaBar11.style.width = (this.Result + "%").toString();
  //AbaBar1.style.width = (this.Result + "%").toString();
 //this.fnColorCodeing1(AbaBar.style, measureAssGoalV, this.Result,AbaBar11.style);
   this.fnColorCodeingN(measureAssGoalV,AbaBar.style)




  var measureCOLAssGoalV = (this.Result_COL - this.MeasureList[0].masterMeasureList[1].associationGoal)
  var measureCOLAssGoal = Math.abs(this.Result_COL - this.MeasureList[0].masterMeasureList[1].associationGoal)

 // COLBar.style.width = (measureCOLAssGoal + "%").toString();
  //COLBar11.style.width = (this.Result_COL + "%").toString();
  COLBar.style.width = (this.Result_COL  + "%").toString();
  // COLBar11.style.width = (this.Result_COL + "%").toString();
  // this.fnColorCodeing(COLBar.style, measureCOLAssGoalV);
  //this.fnColorCodeing1(COLBar.style, measureCOLAssGoalV, this.Result_COL,COLBar11.style);
   this.fnColorCodeingN(measureCOLAssGoalV,COLBar.style)




  var measureCCSLAssGoalV = (this.Result_CCS - this.MeasureList[0].masterMeasureList[3].associationGoal)
  var measureCCSLAssGoal = Math.abs(this.Result_CCS - this.MeasureList[0].masterMeasureList[3].associationGoal)
 // CCSBar.style.width = (measureCCSLAssGoal + "%").toString();
  CCSBar.style.width = (this.Result_CCS + "%").toString();
  // CCSBar.style.width = (this.Result_CCS + "%").toString();
  // CCSBar11.style.width = (this.Result_CCS + "%").toString();

   //this.fnColorCodeing1(CCSBar.style, measureCCSLAssGoalV,this.Result_CCS,CCSBar11.style);
 this.fnColorCodeingN(measureCCSLAssGoalV,CCSBar.style)




  var measureCPBLAssGoalV = (this.Result_CBP - this.MeasureList[1].masterMeasureList[0].associationGoal)
  var measureCPBLAssGoal = Math.abs(this.Result_CBP - this.MeasureList[1].masterMeasureList[0].associationGoal)
  CBPBar.style.width = (this.Result_CBP  + "%").toString();
  //CBPBar11.style.width = (this.Result_CBP  + "%").toString();
  //CBPBar.style.width = (this.Result_CBP + "%").toString();
  // CBPBar11.style.width = (this.Result_CBP  + "%").toString();
 // this.fnColorCodeing1(CBPBar.style, measureCPBLAssGoalV,this.Result_CBP,CBPBar11.style);
   this.fnColorCodeingN( measureCPBLAssGoalV,CBPBar.style)



  var measureBCSLAssGoalV = (this.Result_BCS - this.MeasureList[0].masterMeasureList[2].associationGoal);
  var measureBCSLAssGoal = (Math.abs(this.Result_BCS - this.MeasureList[0].masterMeasureList[2].associationGoal));
  BCSBar.style.width = (this.Result_BCS + "%").toString();
 // BCSBar11.style.width = (this.Result_BCS + "%").toString();
  // BCSBar.style.width = (this.Result_BCS + "%").toString();
  // BCSBar11.style.width = (this.Result_BCS + "%").toString();
   //this.fnColorCodeing1(BCSBar.style, measureBCSLAssGoalV,this.Result_BCS, BCSBar11.style);
   this.fnColorCodeingN( measureBCSLAssGoalV, BCSBar.style)

    





    }
    else if (this.Selectedcolumn == 1) {

    
    
    
    
      var measureAssGoalV = (this.Result - this.MeasureList[0].masterMeasureList[0].associationGoal);
      var measureAssGoal = Math.abs(this.Result - this.MeasureList[0].masterMeasureList[0].associationGoal);
    //  AbaBar.style.width = (measureAssGoal + "%").toString();
      AbaBar.style.width = (this.Result + "%").toString();
        // AbaBar.style.width = (this.Result + "%").toString();
    //   AbaBar11.style.width = (this.Result + "%").toString();
      //AbaBar1.style.width = (this.Result + "%").toString();
     //this.fnColorCodeing1(AbaBar.style, measureAssGoalV, this.Result,AbaBar11.style);
       this.fnColorCodeingN(measureAssGoalV,AbaBar.style)
    
    
    
    
      var measureCOLAssGoalV = (this.Result_COL - this.MeasureList[0].masterMeasureList[1].associationGoal)
      var measureCOLAssGoal = Math.abs(this.Result_COL - this.MeasureList[0].masterMeasureList[1].associationGoal)
    
     // COLBar.style.width = (measureCOLAssGoal + "%").toString();
      //COLBar11.style.width = (this.Result_COL + "%").toString();
      COLBar.style.width = (this.Result_COL  + "%").toString();
      // COLBar11.style.width = (this.Result_COL + "%").toString();
      // this.fnColorCodeing(COLBar.style, measureCOLAssGoalV);
      //this.fnColorCodeing1(COLBar.style, measureCOLAssGoalV, this.Result_COL,COLBar11.style);
       this.fnColorCodeingN(measureCOLAssGoalV,COLBar.style)
    
    
    
    
      var measureCCSLAssGoalV = (this.Result_CCS - this.MeasureList[0].masterMeasureList[3].associationGoal)
      var measureCCSLAssGoal = Math.abs(this.Result_CCS - this.MeasureList[0].masterMeasureList[3].associationGoal)
     // CCSBar.style.width = (measureCCSLAssGoal + "%").toString();
      CCSBar.style.width = (this.Result_CCS + "%").toString();
      // CCSBar.style.width = (this.Result_CCS + "%").toString();
      // CCSBar11.style.width = (this.Result_CCS + "%").toString();
    
       //this.fnColorCodeing1(CCSBar.style, measureCCSLAssGoalV,this.Result_CCS,CCSBar11.style);
     this.fnColorCodeingN(measureCCSLAssGoalV,CCSBar.style)
    
    
    
    
      var measureCPBLAssGoalV = (this.Result_CBP - this.MeasureList[1].masterMeasureList[0].associationGoal)
      var measureCPBLAssGoal = Math.abs(this.Result_CBP - this.MeasureList[1].masterMeasureList[0].associationGoal)
      CBPBar.style.width = (this.Result_CBP  + "%").toString();
      //CBPBar11.style.width = (this.Result_CBP  + "%").toString();
      //CBPBar.style.width = (this.Result_CBP + "%").toString();
      // CBPBar11.style.width = (this.Result_CBP  + "%").toString();
     // this.fnColorCodeing1(CBPBar.style, measureCPBLAssGoalV,this.Result_CBP,CBPBar11.style);
       this.fnColorCodeingN( measureCPBLAssGoalV,CBPBar.style)
    
    
    
      var measureBCSLAssGoalV = (this.Result_BCS - this.MeasureList[0].masterMeasureList[2].associationGoal);
      var measureBCSLAssGoal = (Math.abs(this.Result_BCS - this.MeasureList[0].masterMeasureList[2].associationGoal));
      BCSBar.style.width = (this.Result_BCS + "%").toString();
     // BCSBar11.style.width = (this.Result_BCS + "%").toString();
      // BCSBar.style.width = (this.Result_BCS + "%").toString();
      // BCSBar11.style.width = (this.Result_BCS + "%").toString();
       //this.fnColorCodeing1(BCSBar.style, measureBCSLAssGoalV,this.Result_BCS, BCSBar11.style);
       this.fnColorCodeingN( measureBCSLAssGoalV, BCSBar.style)
    
      
    }

    else if (this.Selectedcolumn == 2) {

    



  


   

/// NEW WAY --\\

var measureCorpAbaV = (this.Result - this.MeasureList[0].masterMeasureList[0].corporateGoal);
      var measureCorpAba = Math.abs(this.Result - this.MeasureList[0].masterMeasureList[0].corporateGoal);

      // AbaBar.style.width = (measureCorpAba + "%").toString();

      AbaBar.style.width = (this.Result + "%").toString();
  
     // this.fnColorCodeing1(AbaBar.style, measureCorpAbaV, this.Result,AbaBar11.style);
      this.fnColorCodeingN(measureCorpAbaV,AbaBar.style);


      


      var measureCOLCorpGoalV = (this.Result_COL - this.MeasureList[0].masterMeasureList[1].corporateGoal)
      var measureCOLCorpGoal = Math.abs(this.Result_COL - this.MeasureList[0].masterMeasureList[1].corporateGoal)

      // COLBar.style.width = (measureCOLCorpGoal + "%").toString();
      // this.fnColorCodeing(COLBar.style, measureCOLCorpGoalV);


      COLBar.style.width = (this.Result_COL + "%").toString();

      // this.fnColorCodeing(COLBar.style, measureCOLAssGoalV);
      this.fnColorCodeingN(measureCOLCorpGoalV,COLBar.style);




      var measureCorpCCSV = (this.Result_CCS - this.MeasureList[0].masterMeasureList[3].corporateGoal);
      var measureCorpCCS = Math.abs(this.Result_CCS - this.MeasureList[0].masterMeasureList[3].corporateGoal);

      // CCSBar.style.width = (measureCorpCCS + "%").toString();
      CCSBar.style.width = (this.Result_CCS + "%").toString();

      this.fnColorCodeingN(measureCorpCCSV,CCSBar.style);
      // this.fnColorCodeing(CCSBar.style, measureCorpCCSV);



      var measureCorpCBPV = (this.Result_CBP - this.MeasureList[1].masterMeasureList[0].corporateGoal)
      var measureCorpCBP = (Math.abs(this.Result_CBP - this.MeasureList[1].masterMeasureList[0].corporateGoal))

      // CBPBar.style.width = (measureCorpCBP + "%").toString();
      CBPBar.style.width = (this.Result_CBP  + "%").toString();
      this.fnColorCodeingN(measureCorpCBPV,CBPBar.style);



      var measureCorpBCSV = ((this.Result_BCS - this.MeasureList[0].masterMeasureList[2].corporateGoal));
      var measureCorpBCS = (Math.abs(this.Result_BCS - this.MeasureList[0].masterMeasureList[2].corporateGoal));

      // BCSBar.style.width = (measureCorpBCS + "%").toString();
     // this.fnColorCodeing(BCSBar.style, measureCorpBCSV);
      BCSBar.style.width = (this.Result_BCS + "%").toString();
      // this.fnColorCodeing(BCSBar.style, measureCorpBCSV);
      this.fnColorCodeingN(measureCorpBCSV, BCSBar.style);




    }

    else if (this.Selectedcolumn == 4) {

      // var AbapayerGoalV = (this.Result - this.MeasureList[0].masterMeasureList[0].payerGoal
      // );
      // var AbapayerGoal = Math.abs(this.Result - this.MeasureList[0].masterMeasureList[0].payerGoal
      // );
      // AbaBar.style.width = (AbapayerGoal + "%").toString();
      // AbaBar11.style.width = (this.Result + "%").toString();
  
      // this.fnColorCodeing1(AbaBar.style, AbapayerGoalV, this.Result,AbaBar11.style);

      // // this.fnColorCodeing(AbaBar.style, AbapayerGoalV);



      // var measurepayerCOLV = (this.Result_COL - this.MeasureList[0].masterMeasureList[1].payerGoal);
      // var measurepayerCOL = Math.abs(this.Result_COL - this.MeasureList[0].masterMeasureList[1].payerGoal);
      // COLBar.style.width = (measurepayerCOL + "%").toString();
      // // this.fnColorCodeing(COLBar.style, measurepayerCOLV);
      // COLBar11.style.width = (this.Result_COL + "%").toString();

      // // this.fnColorCodeing(COLBar.style, measureCOLAssGoalV);
      // this.fnColorCodeing1(COLBar.style, measurepayerCOLV, this.Result_COL,COLBar11.style);



      // var measurepayerCCSV = ((this.Result_CCS - this.MeasureList[0].masterMeasureList[3].payerGoal));
      // var measurepayerCCS = (Math.abs(this.Result_CCS - this.MeasureList[0].masterMeasureList[3].payerGoal));
      // CCSBar.style.width = (measurepayerCCS + "%").toString();
      // CCSBar11.style.width = (this.Result_CCS + "%").toString();

      // this.fnColorCodeing1(CCSBar.style, measurepayerCCSV,this.Result_CCS,CCSBar11.style);

      // // this.fnColorCodeing(CCSBar.style, measurepayerCCSV);



      // var measurepayerCBPV = ((this.Result_CBP - this.MeasureList[1].masterMeasureList[0].payerGoal));
      // var measurepayerCBP = (Math.abs(this.Result_CBP - this.MeasureList[1].masterMeasureList[0].payerGoal));
      // CBPBar.style.width = (measurepayerCBP + "%").toString();
      // CBPBar11.style.width = (this.Result_CBP  + "%").toString();
      // this.fnColorCodeing1(CBPBar.style, measurepayerCBPV,this.Result_CBP,CBPBar11.style);
      // // this.fnColorCodeing(CBPBar.style, measurepayerCBPV);




      // var measurepayerBCSV = ((this.Result_BCS - this.MeasureList[0].masterMeasureList[2].payerGoal));
      // var measurepayerBCS = (Math.abs(this.Result_BCS - this.MeasureList[0].masterMeasureList[2].payerGoal));
      // BCSBar.style.width = (measurepayerBCS + "%").toString();

      // BCSBar11.style.width = (this.Result_BCS + "%").toString();
      // // this.fnColorCodeing(BCSBar.style, measureCorpBCSV);
      // this.fnColorCodeing1(BCSBar.style, measurepayerBCSV,this.Result_BCS, BCSBar11.style);
      // // this.fnColorCodeing(BCSBar.style, measurepayerBCSV);



      ///new way ---\

      
      var AbapayerGoalV = (this.Result - this.MeasureList[0].masterMeasureList[0].payerGoal
        );
        var AbapayerGoal = Math.abs(this.Result - this.MeasureList[0].masterMeasureList[0].payerGoal
        );
        // AbaBar.style.width = (AbapayerGoal + "%").toString();
        AbaBar.style.width = (this.Result + "%").toString();
    
        this.fnColorCodeingN(AbapayerGoalV,AbaBar.style);
  
        // this.fnColorCodeing(AbaBar.style, AbapayerGoalV);
  
  
  
        var measurepayerCOLV = (this.Result_COL - this.MeasureList[0].masterMeasureList[1].payerGoal);
        var measurepayerCOL = Math.abs(this.Result_COL - this.MeasureList[0].masterMeasureList[1].payerGoal);
        // COLBar.style.width = (measurepayerCOL + "%").toString();
        // this.fnColorCodeing(COLBar.style, measurepayerCOLV);
        COLBar.style.width = (this.Result_COL + "%").toString();
  
        // this.fnColorCodeing(COLBar.style, measureCOLAssGoalV);
        this.fnColorCodeingN(measurepayerCOLV,COLBar.style);
  
  
  
        var measurepayerCCSV = ((this.Result_CCS - this.MeasureList[0].masterMeasureList[3].payerGoal));
        var measurepayerCCS = (Math.abs(this.Result_CCS - this.MeasureList[0].masterMeasureList[3].payerGoal));
        // CCSBar.style.width = (measurepayerCCS + "%").toString();
        CCSBar.style.width = (this.Result_CCS + "%").toString();
  
        this.fnColorCodeingN(measurepayerCCSV,CCSBar.style);
  
        // this.fnColorCodeing(CCSBar.style, measurepayerCCSV);
  
  
  
        var measurepayerCBPV = ((this.Result_CBP - this.MeasureList[1].masterMeasureList[0].payerGoal));
        var measurepayerCBP = (Math.abs(this.Result_CBP - this.MeasureList[1].masterMeasureList[0].payerGoal));
        // CBPBar.style.width = (measurepayerCBP + "%").toString();
        CBPBar.style.width = (this.Result_CBP  + "%").toString();
        this.fnColorCodeingN(measurepayerCBPV,CBPBar.style);
        // this.fnColorCodeing(CBPBar.style, measurepayerCBPV);
  
  
  
  
        var measurepayerBCSV = ((this.Result_BCS - this.MeasureList[0].masterMeasureList[2].payerGoal));
        var measurepayerBCS = (Math.abs(this.Result_BCS - this.MeasureList[0].masterMeasureList[2].payerGoal));
        // BCSBar.style.width = (measurepayerBCS + "%").toString();
  
        BCSBar.style.width = (this.Result_BCS + "%").toString();
        // this.fnColorCodeing(BCSBar.style, measureCorpBCSV);
        this.fnColorCodeingN(measurepayerBCSV, BCSBar.style);
        // this.fnColorCodeing(BCSBar.style, measurepayerBCSV);
  

    }
    else if (this.Selectedcolumn == 5) {

    

      /// new way--


      var AbaproviderGoalV = ((this.Result - this.MeasureList[0].masterMeasureList[0].providerGoal
        ));
        var AbaproviderGoal = (Math.abs(this.Result - this.MeasureList[0].masterMeasureList[0].providerGoal
        ));
        // AbaBar.style.width = (AbaproviderGoal + "%").toString();
        
  
        // this.fnColorCodeing(AbaBar.style, AbaproviderGoalV);
  
        AbaBar.style.width = (this.Result + "%").toString();
    
        this.fnColorCodeingN(AbaproviderGoalV,AbaBar.style);
  
  
  
  
        var COLproviderGoalV = (this.Result_COL - this.MeasureList[0].masterMeasureList[1].providerGoal);
        var COLproviderGoal = Math.abs(this.Result_COL - this.MeasureList[0].masterMeasureList[1].providerGoal);
        // COLBar.style.width = (COLproviderGoal + "%").toString();
        COLBar.style.width = (this.Result_COL + "%").toString();
  
        // this.fnColorCodeing(COLBar.style, measureCOLAssGoalV);
        this.fnColorCodeingN(COLproviderGoalV,COLBar.style);
        // this.fnColorCodeing(COLBar.style, COLproviderGoalV);
  
  
  
        var CCSproviderGoalV = (this.Result_CCS - this.MeasureList[0].masterMeasureList[3].providerGoal);
        var CCSproviderGoal = Math.abs(this.Result_CCS - this.MeasureList[0].masterMeasureList[3].providerGoal);
        // CCSBar.style.width = (CCSproviderGoal + "%").toString();
        CCSBar.style.width = (this.Result_CCS + "%").toString();
  
        this.fnColorCodeingN(CCSproviderGoalV,CCSBar.style);
  
  
        // this.fnColorCodeing(CCSBar.style, CCSproviderGoalV);
  
  
        var CPBproviderGoalV = (this.Result_CBP - this.MeasureList[1].masterMeasureList[0].providerGoal);
        var CPBproviderGoal = Math.abs(this.Result_CBP - this.MeasureList[1].masterMeasureList[0].providerGoal);
        // CBPBar.style.width = (CPBproviderGoal + "%").toString();
        CBPBar.style.width = (this.Result_CBP  + "%").toString();
        this.fnColorCodeingN(CPBproviderGoalV,CBPBar.style);
        // this.fnColorCodeing(CBPBar.style, CPBproviderGoalV);
  
  
  
        var BCSproviderGoalV = ((this.Result_BCS - this.MeasureList[0].masterMeasureList[2].providerGoal));
        // var BCSproviderGoal = (Math.abs(this.Result_BCS - this.MeasureList[0].masterMeasureList[2].providerGoal));
        // BCSBar.style.width = (BCSproviderGoal + "%").toString();
        
        BCSBar.style.width = (this.Result_BCS + "%").toString();
        // this.fnColorCodeing(BCSBar.style, measureCorpBCSV);
        this.fnColorCodeingN(BCSproviderGoalV, BCSBar.style);
        // this.fnColorCodeing(BCSBar.style, BCSproviderGoalV);



    }
    else if (this.Selectedcolumn == 6) {
    
      ///new way ---\



      var AbaclinicGoalV = ((this.Result - this.MeasureList[0].masterMeasureList[0].clinicGoal
        ));
        var AbaclinicGoal = (Math.abs(this.Result - this.MeasureList[0].masterMeasureList[0].clinicGoal
        ));
        // AbaBar.style.width = (AbaclinicGoal + "%").toString();
  
        AbaBar.style.width = (this.Result + "%").toString();
    
        this.fnColorCodeingN( AbaclinicGoalV,AbaBar.style);
        // this.fnColorCodeing(AbaBar.style, AbaclinicGoalV);
  
        var COLclinicGoalV = ((this.Result_COL - this.MeasureList[0].masterMeasureList[1].clinicGoal));
        var COLclinicGoal = (Math.abs(this.Result_COL - this.MeasureList[0].masterMeasureList[1].clinicGoal));
        // COLBar.style.width = (COLclinicGoal + "%").toString();
  
        COLBar.style.width = (this.Result_COL + "%").toString();
  
        // this.fnColorCodeing(COLBar.style, measureCOLAssGoalV);
        this.fnColorCodeingN(COLclinicGoalV,COLBar.style);
        // this.fnColorCodeing(COLBar.style, COLclinicGoalV);
  
  
        var CCSclinicGoalV = ((this.Result_CCS - this.MeasureList[0].masterMeasureList[3].clinicGoal));
        var CCSclinicGoal = (Math.abs(this.Result_CCS - this.MeasureList[0].masterMeasureList[3].clinicGoal));
        // CCSBar.style.width = (CCSclinicGoal + "%").toString();
  
        CCSBar.style.width = (this.Result_CCS + "%").toString();
  
        this.fnColorCodeingN(CCSclinicGoalV,CCSBar.style);
        // this.fnColorCodeing(CCSBar.style, CCSclinicGoalV);
  
  
        var CBPproviderGoalV = ((this.Result_CBP - this.MeasureList[1].masterMeasureList[0].clinicGoal));
        var CBPproviderGoal = (Math.abs(this.Result_CBP - this.MeasureList[1].masterMeasureList[0].clinicGoal));
        // CBPBar.style.width = (CBPproviderGoal + "%").toString();
        // this.fnColorCodeing(CBPBar.style, CBPproviderGoalV);
        CBPBar.style.width = (this.Result_CBP  + "%").toString();
        this.fnColorCodeingN(CBPproviderGoalV,CBPBar.style);
  
        var BCSproviderGoalV = ((this.Result_BCS - this.MeasureList[0].masterMeasureList[2].clinicGoal));
        var BCSproviderGoal = (Math.abs(this.Result_BCS - this.MeasureList[0].masterMeasureList[2].clinicGoal));
        // BCSBar.style.width = (BCSproviderGoal + "%").toString();
        BCSBar.style.width = (this.Result_BCS + "%").toString();
        // this.fnColorCodeing(BCSBar.style, measureCorpBCSV);
        this.fnColorCodeingN(BCSproviderGoalV, BCSBar.style);
        // this.fnColorCodeing(BCSBar.style, BCSproviderGoalV);




    }
  }
  fnColorCodeing1(style,measureGoal,Result,style1)
  {
   

    if (measureGoal >= 0) {
      
      style.backgroundColor = "#008000";
     style1.backgroundColor="#f98666"
      //green
    }

    else
    {style.width=0;
    
      style1.backgroundColor="#f98666"
     
    }
    // else if (measureGoal >= -10 && measureGoal < 0) {
    //   style.backgroundColor = "#ffc300";
    //   //yellow
    // }
    // if (measureGoal < -10) {
    //   style.backgroundColor = "#8b0000";
    //   //red
    // }
  }
  fnColorCodeingN(Result,style1)
  {
    if (Result>=0) {
      style1.backgroundColor = "#008000";
    }
     //green
    else if (Result>=-10  && Result<0) {
      style1.backgroundColor = "#ffc300";
    }
    //yellow
    else if (Result <-10) {
      style1.backgroundColor = "#8b0000";
    }
      
  }

  // fnColorCodeing(style, measureGoal) {


  //   if (measureGoal >= 0) {
  //     style.backgroundColor = "#008000";
  //     //green
  //   }
  //   else if (measureGoal >= -10 && measureGoal < 0) {
  //     style.backgroundColor = "#ffc300";
  //     //yellow
  //   }
  //   if (measureGoal < -10) {
  //     style.backgroundColor = "#8b0000";
  //     //red
  //   }

    // if ((measureGoal) > 0) {
    //   if ((measureGoal) == 0) {
    //     style.backgroundColor = "#008000";
    //   }
    //   if ((measureGoal) <= 10) {
    //     style.backgroundColor = "#ffc300";
    //   }

    //   if ((measureGoal) > 10) {
    //     style.backgroundColor = "#8b0000";
    //   }
    // }
    // else {

    //   style.backgroundColor = "#8b0000";

    // }

    // }
  //}



  getDefaultGoalsDetails() {

 //-------------Load User selected goals along with other items on dahsboard-----
 this.dashboardService.getMeasuresList(this.ABAFilters).subscribe(response => {

  this.MeasureList = response;

  this.AssGoal = response[0].userDashboardSetting[0].associationGoal==0 ? true : false;
  this.CopGoal = response[0].userDashboardSetting[0].corporateGoal >0  ? true : false;
  this.ProGoal = response[0].userDashboardSetting[0].providerGoal >0 ? true : false;
  this.PayGoal = response[0].userDashboardSetting[0].payerGoal > 0 ? true : false;
  this.ClinicGoal = response[0].userDashboardSetting[0].clinicGoal > 0 ? true : false;

  this.Result = response[0].masterMeasureList[0].met;
  this.Result_COL = response[0].masterMeasureList[1].met;
  this.Result_BCS = response[0].masterMeasureList[2].met;
  this.Result_CCS = response[0].masterMeasureList[3].met;
  this.Result_CBP = response[1].masterMeasureList[0].met;

  // this.percentagefactor=response[0].masterMeasureList[0].met-response[0].userDashboardSetting[0].associationGoal;

  this.loader.Stop();
  setTimeout(() => { this.ColorCodeFunction(); }, 50);
    this.dashboardService.getMeasurename(this.ABAFilters.UserId).subscribe(response => {
    this.AssociationItem = response.associationItem;
    this.ProviderItem = response.providerItem;
    this.OrganizationItem = response.organizationItem;
    this.clinicItem = response.clinicItem;
    this.payerItem = response.payerItem;
  });
});

}
 
  getSelectedcolumn(item) {
    this.Selectedcolumn = item;
  }



  // ClinicGoalPerF: boolean = true;
  // CopGoalPerF:boolean=true;
  // AssGoalPerF:boolean=true;
  // OrgGoalPerF:boolean=true;
  // PayGoalPerF:boolean=true;
  // ProGoalPerF:boolean=true;


  AddNewColumn(selectedGoal, element) {


    let template: TemplateRef<any> = element;
    this.modalRef = this.modalService.show(template);
    switch (parseInt(selectedGoal)) {
      case 1:
        this.AssGoal = true;
        this.AssGoalPer = false;
        this.AssGoalPerF = true;
        break;
      case 2:
        this.CopGoal = true;
        this.CopGoalPer = false;
        this.CopGoalPerF = true;
        //this.ABAFilters.OrganizationId=this.GoalSelectedOrganization;
        break;
      case 3:
        this.OrgGoal = true;
        this.OrgGoalPer = false;
        this.OrgGoalPerF = true;
        break;
      case 4:
        this.PayGoal = true;
        this.PayGoalPerF = true;
        this.PayGoalPer = false;
        break;
      case 5:
        this.ProGoal = true;
        this.ProGoalPer = false;
        this.ProGoalPerF = true;
        break;
      case 6:
        this.ClinicGoal = true;
        this.ClinicGoalPer = false;
        this.ClinicGoalPerF = true;
    }

  }



  RemoveCurrentColumn(selectedGoal, element) {

    this.dashboardService.getMeasurename(this.ABAFilters.UserId).subscribe(response => {


    
      this.OrganizationItem = response.associationItem;
      this.ProviderItem = response.providerItem;
      this.OrganizationItem = response.organizationItem;
      this.clinicItem = response.clinicItem;
      this.payerItem = response.payerItem;
    })
    ;

    if (selectedGoal == 1 ) {

      this.toaster.warning(" Default Association goal is required!")
    }



    else if(selectedGoal==2 && (this.clinicItem.id>0 || this.ProviderItem.id>0))
     {
       this.toaster.warning("Firstly remove the clinic and provider associated with the organization")
     }

   else  if(selectedGoal==6&&  this.ProviderItem.id>0)
     {
      this.toaster.warning("Firstly  remove the provider associated with the clinic")
     }

     
    else {
      let template: TemplateRef<any> = element;
      this.modalRef = this.modalService.show(template);


      // }
      this.DeletedSelectedGoal = selectedGoal;
    }
    // this.deleteSeletedItem(selectedGoal);
  }

  getSelectedComparingObject(item) {
    // if()
    // {
    //   this.SetPerformanceBaseLine(item)z
    // }


    this.SelectedComparableItem = item;


    if (this.AssGoal == true && item == 1) {
      this.toaster.warning("Association Goal is already present !");
      this.modalRef.hide();
    }
    else if (this.CopGoal == true && item == 2) {
      this.toaster.warning("Corporate Goal is already present !");
      this.modalRef.hide();
    }
    else
     if (this.PayGoal == true && item == 4) {
      this.toaster.warning("Payer Goal is already present !");
      this.modalRef.hide();
      
    }
    else if (this.ProGoal == true && item == 5) {
      this.toaster.warning("Provider Goal is already present !");
      this.modalRef.hide();
    }
    else if (this.ClinicGoal == true && item == 6) {
      this.toaster.warning("Clinic Goal is already present !");
      this.modalRef.hide();
    }
    else {
      this.getAllFilterDropDown();
    }

    switch (parseInt(item))
   
    {

    
  case 2:
  if (this.OrganizationItem.id==undefined) {
    this.toaster.warning("please select a particular organization from the filters");
  } 
  break;
  case 4:
  if (this.payerItem.id==undefined )  {
    this.toaster.warning("please select a particular payer from the filters");
  } 
  break;
  case 5:
  if (this.ProviderItem.id==undefined) {
    this.toaster.warning("please select a particular provider from the filters");
  } 
  break;
  case 6:
  if (this.clinicItem.id==undefined) {
    this.toaster.warning("please select a particular clinic from the filters");
  } 
 
  }
  this.modalRef.hide();
  }
  getAllFilterDropDown() {
    this.patientService.GetOrganization(localStorage.getItem('LoginUserID')).subscribe(
      (response) => {
        this.OrganizationList = response.result.organization;

      });
      

  }

  ShowSelectedGoal(selectedGoal) {

    switch (parseInt(selectedGoal)) {
      case 1:
        this.AssGoal = true;
        this.AssociationItem = null;
        this.AssGoalPer = false;
        this.AssGoalPerF = true;

        break;
      case 2:
        // if (this.OrganizationItem.id==undefined) {
        //   this.toaster.warning("please select a particular organization from the filters");
        // } else {
          this.CopGoal = true;


          this.CopGoalPer = false;
          this.CopGoalPerF = true;
        // }
        break;
      case 3:
        this.OrgGoal = true;


        // this.ABAFilters.OrganizationId=this.GoalSelectedOrganization;
        // this.GetABAAllDetails();
        break;
      case 4:
        // if (this.payerItem.id == undefined) {
        //   this.toaster.warning("please select a particular payer  from the filters");
        // } else {
          this.PayGoal == true;

          this.PayGoalPer = false;
          this.PayGoalPerF = true;
        // }
        break;
      case 5:
        // if (this.ProviderItem.id == undefined) {
        //   this.toaster.warning("please select a particular provider  from the filters");
        // } else {
          this.ProGoal = true;
         
          this.ProGoalPer = false;
          this.ProGoalPerF = true;
        // }
        break;
      case 6:
        // if (this.clinicItem.id == undefined) {
        //   this.toaster.warning("please select a particular clinic  from the filters");
        // } else {
          this.ClinicGoal = true;

          this.ClinicGoalPer = false;
          this.ClinicGoalPerF = true;
        // }
    }
    this.SelectedComparableItem=null;
    this.Selectedcolumn=0;
    this.modalRef.hide();
    this.SaveDashboardGoalDetails(selectedGoal, "Add");
  }

  SetPerformanceBaseLine(selectedGoal) {
    
    this.AssGoalPer = false;
    this.CopGoalPer = false;
    this.OrgGoalPer = false;
    this.PayGoalPer = false;
    this.ProGoalPer = false;
    this.ClinicGoalPer = false;
    this.AssGoalPerF = true;
    this.CopGoalPerF = true;
    this.PayGoalPerF = true;
    this.ProGoalPerF = true;
    this.ClinicGoalPerF = true;
    switch (parseInt(selectedGoal)) {
      case 1:


       


      this.DivId=null;
      this.AssGoalPer = true;
      this.AssGoalPerF = false;
      this.DivId='AssGoalId';
        break;
      case 2:
      this.DivId=null;
      this.CopGoalPer = true;
      this.CopGoalPerF = false;
      this.DivId='CopGoalId';

       



        break;
      case 3:

        // if(this.OrganizationItem.length>0)
        //   {this.CopGoalPer = true;
        //   this.CopGoalPerF=false;}
        //   else {
        //     this.CopGoalPer = true;
        //     this.CopGoalPerF=true;
        //   }
        this.DivId = null;
        this.OrgGoalPer = true;
        this.OrgGoalPerF = false;
        this.DivId='OrgGoalId';
        break;
      case 4:
      this.DivId=null;
      this.PayGoalPer = true;
      this.PayGoalPerF = false;
      this.DivId='PayGoalId';


       



        break;
      case 5:
      this.DivId=null;
      this.ProGoalPer = true;
       this.ProGoalPerF = false;
      this.DivId='ProGoalId';

       



        break;
      case 6:
      this.DivId=null;
      this.ClinicGoalPer = true;
      this.ClinicGoalPerF = false;
      this.DivId='ClinicGoalId';




    }
   this.ColorCodeFunction();
   var Elements=document.getElementById(this.DivId)
   var IDs=$('tr.summary-head').children('th').not(':first').eq(0).prop('id')
   var movingId=$('.'+$(Elements).attr('id'));
   $(movingId).eq(0).insertAfter('.staticHeader');
   $(movingId).eq(1).insertAfter('.staticHeaders');
   $(movingId).eq(2).insertAfter($(movingId).eq(1));
   $(movingId).eq(3).insertAfter($('.staticHeader0').eq(0));
   $(movingId).eq(4).insertAfter($('.staticHeader1').eq(0));
   $(movingId).eq(5).insertAfter($(movingId).eq(4));
   $(movingId).eq(6).insertAfter('.staticHeader2');
   $(movingId).eq(7).insertAfter($(movingId).eq(6));
   $(movingId).eq(8).insertAfter('.staticHeader3');
   $(movingId).eq(9).insertAfter( $(movingId).eq(8));
   $(movingId).eq(10).insertAfter('.staticHeader4');
   $(movingId).eq(11).insertAfter($(movingId).eq(10));
   $(movingId).eq(12).insertAfter($('.staticHeader0').eq(1));
   $(movingId).eq(13).insertAfter($('.staticHeader1').eq(1));
   $(movingId).eq(14).insertAfter($(movingId).eq(13));
    // this.SaveDashboardGoalDetails(selectedGoal, "baseLine");
  }

  getOrganizationSelected(value) {
    this.GoalSelectedOrganization = value;
  }

  SaveDashboardGoalDetails(selectedGoal, Case) {
    this.GoalSelectedOrganization=this.ABAFilters.OrganizationId
    this.dashboardService.SaveDashboardGoalDetails(selectedGoal, Case, this.GoalSelectedOrganization).subscribe(
      response => {
       
        this.getDefaultGoalsDetails();
       
        var Elements=document.getElementById(this.DivId)
        var IDs=$('tr.summary-head').children('th').not(':first').eq(0).prop('id')
        var movingId=$('.'+$(Elements).attr('id'));
        $(movingId).eq(0).insertAfter('.staticHeader');
        $(movingId).eq(1).insertAfter('.staticHeaders');
        $(movingId).eq(2).insertAfter($(movingId).eq(1));
        $(movingId).eq(3).insertAfter($('.staticHeader0').eq(0));
        $(movingId).eq(4).insertAfter($('.staticHeader1').eq(0));
        $(movingId).eq(5).insertAfter($(movingId).eq(4));
        $(movingId).eq(6).insertAfter('.staticHeader2');
        $(movingId).eq(7).insertAfter($(movingId).eq(6));
        $(movingId).eq(8).insertAfter('.staticHeader3');
        $(movingId).eq(9).insertAfter( $(movingId).eq(8));
        $(movingId).eq(10).insertAfter('.staticHeader4');
        $(movingId).eq(11).insertAfter($(movingId).eq(10));
        $(movingId).eq(12).insertAfter($('.staticHeader0').eq(1));
        $(movingId).eq(13).insertAfter($('.staticHeader1').eq(1));
        $(movingId).eq(14).insertAfter($(movingId).eq(13));
      }
      
    )
  
  }

  deleteSeletedItem() {

    // }

    switch (this.DeletedSelectedGoal) {
      case 1:
        this.AssGoal = false;
        this.AssociationItem = null;
        this.AssGoalPer = false
        break;
      case 2:
        this.CopGoal = false;
        this.CopGoalPer = false;
        this.OrganizationItem = null;
        break;
      case 3:
        this.OrgGoal = false;
        break;
      case 4:
        this.PayGoal = false;
        this.PayGoalPer = false;
        this.payerItem = null;
        break;
      case 5:
        this.ProGoal = false;
        this.ProGoalPer = false;
        this.ProviderItem = null;
        break;
      case 6:
        this.ClinicGoal = false;
        this.ClinicGoalPer = false;
        this.clinicItem = null;

    }
    

    if(this.DeletedSelectedGoal==2)
    {
    this.ABAFilters.OrganizationId=2;
    this.ABAFilters.PayerId=0;
    this.ABAFilters.ProviderId=0;
    this.ABAFilters.ClinicId=0;
    }
    else if(this.DeletedSelectedGoal==4)
    {
    this.ABAFilters.OrganizationId=0;
    this.ABAFilters.PayerId=4;
    this.ABAFilters.ProviderId=0;
    this.ABAFilters.ClinicId=0;
    }else if(this.DeletedSelectedGoal==5)
    {
      this.ABAFilters.OrganizationId=0;
      this.ABAFilters.PayerId=0;
      this.ABAFilters.ProviderId=5;
      this.ABAFilters.ClinicId=0;
      }
      else if(this.DeletedSelectedGoal==6)
    {
      this.ABAFilters.OrganizationId=0;
      this.ABAFilters.PayerId=0;
      this.ABAFilters.ProviderId=0;
      this.ABAFilters.ClinicId=6;
      }




    this.SaveDashboardGoalDetails( this.ABAFilters, "Remove");
    //this.getDefaultGoalsDetails();
    this.modalRef.hide();
  }
  RemoveFilterSection(data) {

    if (data.target.id != 'sidebar') {
      this.dashboardService.changeScreenSetting('false')
      this.showfilter = false;
    }
  }
  SetProgressBar(list: any) {


    var AbaBar = document.querySelector("#ABA") as HTMLElement;
    AbaBar.style.backgroundColor = "#D93600";
    AbaBar.style.width = (list[0].masterMeasureList[0].met + "%").toString();
    // this.dashboardService.getAllABADetails(this.ABAFilters).subscribe(
    //   response => {
    //      ;
    //     this.Numerator = response.numericalData[0].numerator;
    //     this.Denominator = response.numericalData[0].denominator;

    //     this.Result = response.result[0];



    //     // response.allEligiblePatient.forEach(element => {
    //     //   this.EligiblePatient.push({
    //     //     Name: element.patientName, NextVisit: element.nextVisit,
    //     //     LastVisit: element.lastVisit, RaceId: element.raceId, Status: element.status, EthnicityId: element.ethnicityId,
    //     //     DOB: element.dob, Age: element.age, OrganizationId: element.organizationId
    //     //   })
    //     // });
    //     // this.totalPatient = response.allEligiblePatient.length;

    //     // response.nonQualifiedPatient.forEach(element => {
    //     //   this.NonQualifiedPatient.push({
    //     //     Name: element.patientName, NextVisit: element.nextVisit,
    //     //     LastVisit: element.lastVisit, RaceId: element.raceId, Status: element.status, EthnicityId: element.ethnicityId,
    //     //     DOB: element.dob, Age: element.age, OrganizationId: element.organizationId
    //     //   })
    //     // });
    //     // response.qualifiedPatient.forEach(element => {
    //     //   this.QualifiedPatient.push({
    //     //     Name: element.patientName, NextVisit: element.nextVisit,
    //     //     LastVisit: element.lastVisit, RaceId: element.raceId, Status: element.status, EthnicityId: element.ethnicityId,
    //     //     DOB: element.dob, Age: element.age, OrganizationId: element.organizationId
    //     //   })
    //     // });

    //     var AbaBar = document.querySelector("#ABA") as HTMLElement;
    //     AbaBar.style.backgroundColor = "#D93600";
    //     AbaBar.style.width = (list[0].masterMeasureList[0].met + "%").toString();


    //     //  this.html = '<span>Current ('+(this.Result).toFixed(2)  +'% increase)<span></br><div class="col-sm-12"><div class="col-sm-8 progress"><div class="progress-bar" role="progressbar" aria-valuenow="50"aria-valuemin="0" aria-valuemax="100" style="width:50%"></div></div><div class="col-sm-4">'+this.Numerator+'</div></div><span>Prior</span><br/><div class="progress" style="width:70%"><div class="progress-bar" role="progressbar" aria-valuenow="50"aria-valuemin="0" aria-valuemax="100" style="width:50%"></div></div> 0';
    //     //this.html='<span>Current ('+(this.Result).toFixed(2)  +'% increase)<span></br><div class="row"><div class="col-sm-8 progress"><div class="progress-bar" role="progressbar" aria-valuenow="50"aria-valuemin="0" aria-valuemax="100" style="width:50%"></div></div><div class="col-sm-4"><span>'+this.Numerator+'</span></div></div>'
    //     // this.html='<div class="row"><div class="col-sm-6"><small>Current ( <span class="text-success">23.6%</span> Increase)</small><div class="row"><div class="col-sm-8"><div class="progress"><div class="progress-bar bg-danger" role="progressbar" style="width:40%"></div></div> </div><div class="col-sm-4"><small>22.5 K</small></div></div><small>Prior</small><div class="row"><div class="col-sm-8"> <div class="progress"><div class="progress-bar bg-danger" role="progressbar" style="width:40%"></div></div></div><div class="col-sm-4"><small>18.2 K</small></div></div> </div></div> '
    //     //this.getDefaultGoalsDetails();
    //     if (this.sync == true) {
    //       this.toaster.success("Measure successfully calculated with latest data !");
    //       this.sync = false;

    //     }
    //   }
    // )

    this.dashboardService.GetCOLAllDetails(this.ABAFilters).subscribe(
      response => {

        this.Numerator_COL = response.numericalData[0].numerator;
        this.Denominator_COL = response.numericalData[0].denominator;

        this.Result_COL = response.result[0];
        var COLBar = document.querySelector("#CC") as HTMLElement;
        COLBar.style.backgroundColor = "#FFC300";
        COLBar.style.width = (this.Result_COL + "%").toString();
      }
    )

    this.dashboardService.GetBCSAllDetails(this.ABAFilters).subscribe(
      response => {
     
        this.Numerator_BCS = response.numericalData[0].numerator;
        this.Denominator_BCS = response.numericalData[0].denominator;
        this.Result_BCS = response.result[0];
        var BCSBar = document.querySelector("#BC") as HTMLElement;
        BCSBar.style.backgroundColor = "#008000";
        BCSBar.style.width = (this.Result_BCS + "%").toString();
      }
    )

    this.dashboardService.GetCCSAllDetails(this.ABAFilters).subscribe(
      response => {

        this.Numerator_CCS = response.numericalData[0].numerator;
        this.Denominator_CCS = response.numericalData[0].denominator;
        this.Result_CCS = response.result[0];
        var CCSBar = document.querySelector("#CCS") as HTMLElement;
        CCSBar.style.backgroundColor = "#6495ED";
        CCSBar.style.width = (this.Result_CCS + "%").toString();
      }
    )
    this.dashboardService.GetCBPAllDetails(this.ABAFilters).subscribe(
      response => {

        console.log(response);

        this.Numerator_CBP = response.numericalData[0].numerator;
        this.Denominator_CBP = response.numericalData[0].denominator;
        this.Result_CBP = response.result[0];
        var CBPBar = document.querySelector("#CHBP") as HTMLElement;
        CBPBar.style.backgroundColor = "#008000";
        CBPBar.style.width = (this.Result_CBP + "%").toString();
      }
    )

  }
  //col calculation details started
  GetCOLAllDetails() {

    this.dashboardService.GetCOLAllDetails(this.ABAFilters).subscribe(
      response => {
        this.Numerator_COL = response.numericalData[0].numerator;
        this.Denominator_COL = response.numericalData[0].denominator;
        this.Result_COL = response.result[0];
        var COLBar = document.querySelector("#COLProgressBar") as HTMLElement;
        COLBar.style.backgroundColor = "#FFC300";
        COLBar.style.width = (this.Result_COL + "%").toString();
      }
    )

  }

  GetListForMeasureCOL(item: number) {

    this.dashboardService.ShowDataWithFilters(this.ABAFilters);
    // this.router.navigate(['/Admin/colPatientList', { id: 2,Status:item }]);
    this.router.navigate(['/Admin/patientList'], {
      queryParams: { id: 2, status: item }
    });
  }
  //funcgoal(){
  // var ABAResult = (this.Result - this.MeasureList[0].masterMeasureList[0].goal);
  // var COLResult = (this.Result_COL - this.MeasureList[0].masterMeasureList[1].goal);
  // var CCSResult = (this.Result_BCS - this.MeasureList[0].masterMeasureList[3].goal);
  // var BCSResult = (this.Result_CCS - this.MeasureList[0].masterMeasureList[2].goal);
  // var CBPResult = (this.Result_CBP - this.MeasureList[1].masterMeasureList[0].goal);
  // }


  funcresult(POPResult, POPResult1) {
    if (this.POPResult1 > 0) {
      this.range = "Increase";
    }
    else {
      this.range = "Decrease";
    }
    var Bar = document.querySelector("#ABA1") as HTMLElement;


   


    if (Bar != null) {
      // COLBar.style.width = (response[0].masterMeasureList[1].met + "%").toString();
      if (this.POPResult1 >=0) {
        Bar.style.backgroundColor = "#008000";
      }
       //green
      else if (this.POPResult1 >=-10  && this.POPResult1<0) {
        Bar.style.backgroundColor = "#ffc300";
      }
      //yellow
      else if (this.POPResult1 <-10) {
        Bar.style.backgroundColor = "#8b0000";
      }
            //red
      
      Bar.style.width = (this.POPResult + "%").toString();

    }
  }
  ShowBarColor(showBarColor: string, MeasureId: number) {
    //  this.ColorCodeFunction();

  
    if (this.Selectedcolumn == 0) {
      // var ABAResult = this.Result;
      // var COLResult = this.Result_COL;
      // var BCSResult = this.Result_BCS;
      // var CCSResult = this.Result_CCS;
      // var CBPResult = this.Result_CBP;
      var ABAResult = (this.Result - this.MeasureList[0].masterMeasureList[0].associationGoal);
      var COLResult = (this.Result_COL - this.MeasureList[0].masterMeasureList[1].associationGoal);
      var CCSResult = (this.Result_CCS - this.MeasureList[0].masterMeasureList[3].associationGoal);
      var BCSResult = (this.Result_BCS - this.MeasureList[0].masterMeasureList[2].associationGoal);
      var CBPResult = (this.Result_CBP - this.MeasureList[1].masterMeasureList[0].associationGoal);
    }
    else if (this.Selectedcolumn == 1) {

      var ABAResult = (this.Result - this.MeasureList[0].masterMeasureList[0].associationGoal);
      var COLResult = (this.Result_COL - this.MeasureList[0].masterMeasureList[1].associationGoal);
      var CCSResult = (this.Result_CCS - this.MeasureList[0].masterMeasureList[3].associationGoal);
      var BCSResult = (this.Result_BCS - this.MeasureList[0].masterMeasureList[2].associationGoal);
      var CBPResult = (this.Result_CBP - this.MeasureList[1].masterMeasureList[0].associationGoal);
    }

    else if (this.Selectedcolumn == 2) {
      var ABAResult = (this.Result - this.MeasureList[0].masterMeasureList[0].corporateGoal);
      var COLResult = (this.Result_COL - this.MeasureList[0].masterMeasureList[1].corporateGoal);
      var BCSResult = (this.Result_BCS - this.MeasureList[0].masterMeasureList[2].corporateGoal);
      var CCSResult = (this.Result_CCS - this.MeasureList[0].masterMeasureList[3].corporateGoal);
      var CBPResult = (this.Result_CBP - this.MeasureList[1].masterMeasureList[0].corporateGoal);

    }
    else if (this.Selectedcolumn == 4) {
      var ABAResult = (this.Result - this.MeasureList[0].masterMeasureList[0].payerGoal);
      var COLResult = (this.Result_COL - this.MeasureList[0].masterMeasureList[1].payerGoal);
      var BCSResult = (this.Result_BCS - this.MeasureList[0].masterMeasureList[2].payerGoal);
      var CCSResult = (this.Result_CCS - this.MeasureList[0].masterMeasureList[3].payerGoal);
      var CBPResult = (this.Result_CBP - this.MeasureList[1].masterMeasureList[0].payerGoal);


    }

    else if (this.Selectedcolumn == 5) {

      var ABAResult = (this.Result - this.MeasureList[0].masterMeasureList[0].providerGoal);
      var COLResult = (this.Result_COL - this.MeasureList[0].masterMeasureList[1].providerGoal);
      var BCSResult = (this.Result_BCS - this.MeasureList[0].masterMeasureList[2].providerGoal);
      var CCSResult = (this.Result_CCS - this.MeasureList[0].masterMeasureList[3].providerGoal);
      var CBPResult = (this.Result_CBP - this.MeasureList[1].masterMeasureList[0].providerGoal);
    }
    else if (this.Selectedcolumn == 6) {
      var ABAResult = (this.Result - this.MeasureList[0].masterMeasureList[0].clinicGoal);
      var COLResult = (this.Result_COL - this.MeasureList[0].masterMeasureList[1].clinicGoal);
      var BCSResult = (this.Result_BCS - this.MeasureList[0].masterMeasureList[2].clinicGoal);
      var CCSResult = (this.Result_CCS - this.MeasureList[0].masterMeasureList[3].clinicGoal);
      var CBPResult = (this.Result_CBP - this.MeasureList[1].masterMeasureList[0].clinicGoal);

    }

    var measurearray: {};
    measurearray = this.MeasureList;
    if (MeasureId < 5) {
     
      // this.POPResult = measurearray[0].masterMeasureList[MeasureId - 1].met;
      this.POPNumerator = measurearray[0].masterMeasureList[MeasureId - 1].numerator;
  
      //this.POPResult=1;
    }
    else {
      // this.POPResult = measurearray[1].masterMeasureList[MeasureId - 5].met;
      this.POPNumerator = measurearray[1].masterMeasureList[MeasureId - 5].numerator;
    }



    setTimeout(() => {

      if (MeasureId == 1) {
        this.POPResult = Math.abs(ABAResult);
        this.POPResult1 = ABAResult;

        this.funcresult(this.POPResult, this.POPResult1)

      }
      if (MeasureId == 2) {
        this.POPResult = Math.abs(COLResult);
        this.POPResult1 = COLResult;
        this.funcresult(this.POPResult, this.POPResult1)
        // Bar.style.backgroundColor = "#FFC300";

      }


      if (MeasureId == 3) {

        this.POPResult = Math.abs(BCSResult);
        this.POPResult1 = BCSResult;
        this.funcresult(this.POPResult, this.POPResult1)
      }



      if (MeasureId == 4) {
        this.POPResult = Math.abs(CCSResult);
        this.POPResult1 = CCSResult;
        this.funcresult(this.POPResult, this.POPResult1)
      }



      if (MeasureId == 5) {

        this.POPResult = Math.abs(CBPResult);
        this.POPResult1 = CBPResult;
        this.funcresult(this.POPResult, this.POPResult1)
      }



    }, 50);



  }
}
