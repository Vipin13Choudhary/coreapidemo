import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { DashboardService } from '../../service/dashboard/dashboard.service';
import { CommonFilters } from '../../model/dashboard/dashboard'
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { PageEvent } from '@angular/material';
import { UserService } from '../../service/user/user.service';
import { listOperation, organizationDropDown } from '../../model/common/common';
import { Router, ActivatedRoute } from '@angular/router'
import { PatientService } from 'src/app/service/patient/patient.service';
import { PatientNavigationScreens } from 'src/app/model/patient/patientModel';
import * as moment from 'moment';
import { Key } from 'protractor';
import { LoaderService } from '../../service/loader/loader.service';
import { ELEMENT_MARKER } from '@angular/core/src/render3/interfaces/i18n';
import { type } from 'os';



@Component({
  selector: 'app-measures-patient',
  templateUrl: './measures-patient.component.html',
  styleUrls: ['./measures-patient.component.css']
})
export class MeasuresPatientComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sorting: MatSort;
  OrganizationItem: organizationDropDown;
  ABAFilters: CommonFilters;
  Numerator: number;
  Denominator: number;
  EligiblePatient: any = []
  QualifiedPatient: any = []
  NonQualifiedPatient: any = []
  Result: number;
  searchOrgDropDown = [];
  listOperands: any;
  ELEMENT_DATA: any;
  ELEMENT_DATA1 = [];
  dataSource1 = [];
  ELEMENT_DATA2 = [];
  dataSource2 = [];
  dataSource: any;
  length = 5;
  pageSize = 10;
  pageAttributionMatchedSize = 10;
  pageAttributionNotMatchedSize = 10;
  pageSizeOptions: number[] = [1, 5, 10, 20];
  displayedColumns = ['Name', 'DOB', 'ClinicName', 'ProviderName', 'LastVisit', 'Days', 'Note', 'Action'];
  pageAttributeSizeOptions: number[] = [1, 5, 10, 20];
  displayedAttributeColumns = ['Name', 'DOB', 'PCPName', 'Ethnicity', 'Race', 'Note', 'Action'];
  pageEvent: PageEvent;
  search: "";
  showfilter: any = 'false';
  filteringData: any;
  ClearFilters: string = 'false';
  filteringSpecification: number = 0;
  PageRefereshed: boolean = true;
  date: Date = new Date();
  filteredDate = (this.date.getMonth() + 1) + '/' + this.date.getDate() + '/' + this.date.getFullYear();
  Emptylist: boolean = true;
  IsDetailPage: boolean = true
  UrlStatus: number = 1;
  SelectedMeasureId: number = 0;
  message: string;
  DataTagWise: boolean = false;
  bsValue = new Date();
  maxDate = new Date();
  SelectedMasterMeasure: number = 1;
  flagvalue: boolean = false;
  filterValues: CommonFilters;
  LoginOrganizationId: number = 0;
  filters: boolean = false;
  Role: string
  bsValue1 = new Date();
  maxDate1 = new Date();
  MeasureName: string;
  Filter: boolean = false;
  CareGap: boolean = false;
  isMatGrid: boolean = true;
  EmptyAttributionMatchedlist: Boolean = false;
  AttributionMatchedPatient: any = [];
  dynamicclass: number = 3;
  EmptyAttributionNotMatchedlist: Boolean = false;
  AttributionNotMatched: any = [];
  // filters:Boolean=false;
  constructor(private loader: LoaderService, private dashboardService: DashboardService, private userService: UserService,
    private router: Router, private route: ActivatedRoute, private patientService: PatientService) {
    this.userService.changeScreen("false")

    this.listOperands = new listOperation();

  }


  ngOnInit() {
    debugger;
    this.ABAFilters = new CommonFilters();
    this.bsValue = new Date('01/01/2018');
    this.maxDate = new Date('12/31/2018');

    this.bsValue1 = new Date('01/01/2019');
    this.maxDate1 = new Date('12/31/2019');
    if ((Number(localStorage.getItem('MasterMeaure')) == 1 || (Number(localStorage.getItem('MasterMeaure')) == 2))) {
      this.ABAFilters.PeriodEnd = moment(this.maxDate).format('L').toString()
      this.ABAFilters.PeriodStart = moment(this.bsValue.setFullYear(this.bsValue.getFullYear())).format('L')
    }

    else {
      this.ABAFilters.PeriodEnd = moment(this.maxDate1).format('L').toString()
      this.ABAFilters.PeriodStart = moment(this.bsValue1.setFullYear(this.bsValue1.getFullYear())).format('L')
    }
    this.patientService.currentMessage.subscribe(message => this.message = message);
    this.fnPassPageUrlToClientChart()
    // detail measure patient page check : BEGIN

    this.SelectedMasterMeasure = Number(localStorage.getItem('MasterMeaure'));
    this.Role = localStorage.getItem('Role');
    // var t= this.route.snapshot.params;

    if (this.router.url.indexOf('detailMeasureAnalysis') > -1) {
      this.IsDetailPage = false;
      this.UrlStatus = -1;

      // this.ABAFilters.MeasureId=1;
      this.ABAFilters.MeasureId = Number(localStorage.getItem('item'));
    }
    else {
      this.UrlStatus = this.route.snapshot.queryParams.status;
      this.filters = this.route.snapshot.queryParams.filters;
      this.CareGap = Boolean(this.filters);
    }
    //END
    if (this.UrlStatus == -1) {
      this.Filter = true;
    }
    else {
      this.Filter = false;
    }



    this.dashboardService.currentFilter.subscribe(message => {
      this.showfilter = message
    });
    this.dashboardService.currentClearFilter.subscribe(message => this.ClearFilters = message);
    this.dashboardService.currentSharedFilter.subscribe(message => {
      debugger;
      this.PageRefereshed = false;
      if (message != undefined) {
        this.ABAFilters = message
        this.ABAFilters.UserId = Number(localStorage.getItem("LoginUserID"));
        this.ABAFilters.Status = this.UrlStatus;
        this.ABAFilters.Flag = this.UrlStatus == 0 ? -1 : 0;
      }
      else {
        this.PageRefereshed = true;
      }
    });
    if (this.PageRefereshed == true) {
      this.ABAFilters.UserId = Number(localStorage.getItem("LoginUserID"));
      // this.ABAFilters.MeasureId=1;
      this.ABAFilters.MeasureId = Number(localStorage.getItem('item'));


      this.ABAFilters.VendorId = 0;
      this.ABAFilters.AgeRange = '1-200';
      this.ABAFilters.Gendor = 0;
      // this.ABAFilters.PeriodEnd= moment(this.maxDate).format('L').toString();
      // this.ABAFilters.PeriodStart=moment(this.bsValue.setFullYear(this.bsValue.getFullYear())).format('L');
      this.ABAFilters.ProviderId = 0;
      this.ABAFilters.RaceId = 0;
      this.ABAFilters.EthnicityId = 0;


      this.ABAFilters.IsFilter = 0;
      //this.ABAFilters.UserId=Number(localStorage.getItem("LoginUserID"))==null?Number(localStorage.getItem("LoginUserID")) :0;
      this.ABAFilters.Status = this.UrlStatus;
      debugger;

      this.ABAFilters.Flag = this.UrlStatus == 0 ? -1 : 0;

      switch (this.ABAFilters.MeasureId) {
        case 1:
          this.MeasureName = "Adult BMI Assesment";
          break;
        case 2:
          this.MeasureName = "Colorectal Cancer";
          break;
        case 3:
          this.MeasureName = "Breast Cancer";
          break;
        case 4:
          this.MeasureName = "Cervical Cancer";
          break;
        case 5:
          this.MeasureName = "Controlling High Blood Pressure";


      }
    }
    this.ABAFilters.offset = 1;
    this.ABAFilters.limit = 10;
    this.ABAFilters.order = "desc";
    this.ABAFilters.sort = "";
    this.ABAFilters.Search = "";

    this.SelectedMeasureId = this.route.snapshot.queryParams.id == null ? 1 : this.route.snapshot.queryParams.id;
    debugger;
    this.SelectDataAsPerMasterMeasure(Number(localStorage.getItem('MasterMeaure')));
    // this.PatientListAsPerMeasure(this.SelectedMeasureId);

  }
  fnPassPageUrlToClientChart() {
    this.patientService.changeMessage(this.router.url)
  }




  GetAllMeasurePatientList() {
    debugger;
    this.loader.Start();
    this.EligiblePatient = [];
    this.ELEMENT_DATA = [];

    this.ABAFilters.OrganizationId = this.Role == "SuperAdmin" ? 0 : parseInt(localStorage.getItem("OrganizationId"));
    this.dashboardService.GetAllMeasurePatientList(this.ABAFilters).subscribe(
      response => {
        debugger;
        if (response.length > 0) {
          this.Emptylist = false;
          this.length = response[0].TotalRows;
          //  this.Numerator=response.numericalData[0].numerator;
          //  this.Denominator=response.numericbpalData[0].denominator;
          //  this.Result=response.result[0];
          debugger;
          this.EligiblePatient = [];
       
          response.forEach(element => {
            this.EligiblePatient.push({
              Name: element.PatientName, NextVisit: element.NextVisit,
              LastVisit: element.LastVisit, RaceName: element.RaceName, Status: element.Status, EthnicityName: element.EthnicityName,
              DOB: element.DOB, Age: element.Age, OrganizationId: element.OrganizationID, Gap: element.Gap, FileName: element.FileName,
              PatientId: element.PatientId, Days: element.Days, ClinicName: element.ClinicName, ProviderName: element.ProviderName,

            })
          });

        }

        else {
          this.pageSize = 0;
          this.Emptylist = true;
        }
        this.ELEMENT_DATA = this.EligiblePatient;
        this.dataSource = new MatTableDataSource<Element>(this.ELEMENT_DATA);
        this.dashboardService.clearFilterSetting('Clear Filter');
        // debugger;
        //  var AbaBar = document.querySelector("#AbaProgressBar") as HTMLElement;
        //  AbaBar.style.backgroundColor = "#D93600";
        //  AbaBar.style.width=(this.Result+"%").toString();

        this.loader.Stop();
      }

    )


  }

  setPageSizeOptions(setPageSizeOptionsInput: string) {
    this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
  }

  getServerData(pageData) {
    this.ABAFilters.offset = pageData.pageIndex + 1;
    this.ABAFilters.limit = pageData.pageSize;
    this.SelectDataAsPerMasterMeasure(Number(localStorage.getItem('MasterMeaure')));
  }

  //short data as per coulumn 
  sortData(header, OrderType) {
    this.loader.Start();
    if (OrderType.currentTarget.attributes.getNamedItem('aria-sort') != null) {
      OrderType = OrderType.currentTarget.attributes.getNamedItem('aria-sort').value;
      if (OrderType == "descending") {
        OrderType = 'desc';
      }
      else {
        OrderType = 'asc';
      }
    }
    else {
      this.loader.Stop();
      return;
    }
    // this.listOperands.sort=header;
    // this.listOperands.order=OrderType;
    this.ABAFilters.order = OrderType;
    this.ABAFilters.sort = header;
    this.SelectDataAsPerMasterMeasure(Number(localStorage.getItem('MasterMeaure')));
    this.loader.Stop();
  }

  getSearchedData() {
    this.ABAFilters.Search = this.search;
    this.SelectDataAsPerMasterMeasure(Number(localStorage.getItem('MasterMeaure')));
  }

  //As per drop downs (further added on future)
  getFilteredList(organizationId) {

    this.EligiblePatient(this.listOperands);
  }

  showAndHideFilters(value) {
    this.dashboardService.changeScreenSetting(value);

  }
  receiveMessage($event) {
    debugger;

    this.filteringData = $event
    if (this.filteringData.AgeRange == null) {
      this.filteringData.AgeRange = '1-200';
    }

    this.ABAFilters.EthnicityId = this.filteringData.EthnicityId;
    this.ABAFilters.Gendor = this.filteringData.GenderID;
    this.ABAFilters.MeasureId = this.filteringData.MeasureId;
    this.ABAFilters.OrganizationId = this.filteringData.OrganizationID;
    if (Number(localStorage.getItem("MasterMeaure")) == 1 || (Number(localStorage.getItem("MasterMeaure")) == 2)) {
      this.ABAFilters.PeriodEnd = this.filteringData.DateRange == null ? moment(this.maxDate).format('L').toString() : this.filteringData.DateRange[1];
      this.ABAFilters.PeriodStart = this.filteringData.DateRange == null ? moment(this.bsValue.setFullYear(this.bsValue.getFullYear())).format('L') : this.filteringData.DateRange[0];
    }
    else {
      this.ABAFilters.PeriodEnd = this.filteringData.DateRange == null ? moment(this.maxDate1).format('L').toString() : this.filteringData.DateRange[1];
      this.ABAFilters.PeriodStart = this.filteringData.DateRange == null ? moment(this.bsValue1.setFullYear(this.bsValue1.getFullYear())).format('L') : this.filteringData.DateRange[0];
    }
    // this.ABAFilters.PeriodEnd=this.filteringData.DateRange==null?moment(this.maxDate).format('L').toString():this.filteringData.DateRange[1];
    // this.ABAFilters.PeriodStart=this.filteringData.DateRange==null?moment(this.ABAFilters.PeriodStart).format('L').toString():this.filteringData.DateRange[0];
    this.ABAFilters.RaceId = this.filteringData.RaceID;
    this.ABAFilters.VendorId = this.filteringData.VendorId;
    this.ABAFilters.AgeRange = this.filteringData.AgeRange;
    this.ABAFilters.UserId = Number(localStorage.getItem("LoginUserID"));
    this.ABAFilters.IsFilter = this.filteringData.IsFilter;

    //this.filteringSpecification=;
    if (this.router.url.indexOf('detailMeasureAnalysis') == -1 && this.DataTagWise == false) {
      this.ABAFilters.Status = this.route.snapshot.queryParams.status;
      this.filteringSpecification = this.route.snapshot.queryParams.status == 0 ? 1 : 3

    }

    if (this.filteringSpecification == 1) {
      this.ABAFilters.Status = 0;
      this.ABAFilters.Flag = -1;
    }
    if (this.filteringSpecification == 2) {
      this.ABAFilters.Status = 1;
      this.ABAFilters.Flag = 1;
    }
    if (this.filteringSpecification == 3) {
      this.ABAFilters.Status = 1;
      this.ABAFilters.Flag = 0;
    }
    if (this.filteringSpecification == 4) {
      this.ABAFilters.Status = 1;
      this.ABAFilters.Flag = 2;
    }
    if (this.filteringSpecification == 0) {
      this.ABAFilters.Status = -1;
      this.ABAFilters.Flag = 0;

    }

    this.SelectDataAsPerMasterMeasure(Number(localStorage.getItem('MasterMeaure')));
    // if(this.router.url.indexOf('detailMeasureAnalysis')==-1)
    //   this.PatientListAsPerMeasure(this.SelectedMeasureId);
    //   else
    //   this.PatientListAsPerMeasure(this.ABAFilters.MeasureId);
  }

  ClearFilter() {

    // this.ABAFilters.MeasureId=1;
    this.ABAFilters.MeasureId = Number(localStorage.getItem('item'));
    this.ABAFilters.VendorId = 0;
    this.ABAFilters.AgeRange = '1-200';
    this.ABAFilters.Gendor = 0;
    if (Number(localStorage.getItem("MasterMeaure")) == 1 || (Number(localStorage.getItem("MasterMeaure")) == 2)) {
      this.ABAFilters.PeriodEnd = this.filteringData.DateRange == null ? moment(this.maxDate).format('L').toString() : this.filteringData.DateRange[1];
      this.ABAFilters.PeriodStart = this.filteringData.DateRange == null ? moment(this.bsValue.setFullYear(this.bsValue.getFullYear())).format('L') : this.filteringData.DateRange[0];
    }
    else {
      this.ABAFilters.PeriodEnd = this.filteringData.DateRange == null ? moment(this.maxDate1).format('L').toString() : this.filteringData.DateRange[1];
      this.ABAFilters.PeriodStart = this.filteringData.DateRange == null ? moment(this.bsValue1.setFullYear(this.bsValue1.getFullYear())).format('L') : this.filteringData.DateRange[0];
    }
    // this.ABAFilters.PeriodEnd= moment(this.maxDate).format('L').toString();
    // this.ABAFilters.PeriodStart=moment(this.bsValue.setFullYear(this.bsValue.getFullYear())).format('L');
    this.ABAFilters.ProviderId = 0;
    this.ABAFilters.RaceId = 0;
    this.ABAFilters.EthnicityId = 0;
    this.ABAFilters.UserId = Number(localStorage.getItem("LoginUserID"));
    this.ABAFilters.offset = this.ABAFilters.offset;
    this.ABAFilters.limit = this.ABAFilters.limit;
    this.ABAFilters.order = this.ABAFilters.order;
    this.ABAFilters.sort = "";
    this.ABAFilters.Search = "";
    this.ABAFilters.IsFilter = 0;
    this.ABAFilters.Status = this.route.snapshot.queryParams.status;
    this.ABAFilters.Flag = 0;

    this.SelectDataAsPerMasterMeasure(Number(localStorage.getItem('MasterMeaure')));


  }

  FilterPatientAsPerFlag(flag: string) {
    debugger;
    if (flag == "Select Deadline as per days")
      return false;
    this.DataTagWise = true;
    this.ABAFilters.offset = 1;
    this.ABAFilters.limit = 10;
    this.ABAFilters.PeriodStart = this.ABAFilters.PeriodStart;
    this.filteringSpecification = Number(flag);
    if (Number(flag) == 1) {
      this.ABAFilters.Status = 1;
      this.dynamicclass = 1;
    } else if (Number(flag) == 2) {
      this.dynamicclass = 2;
    } else if (Number(flag) == 3) {
      this.dynamicclass = 3;
    } else if (Number(flag) == 4) {
      this.dynamicclass = 4;
    }
    this.receiveMessage(this.ABAFilters);

  }
  FilterPatientAsPerSearch(Type: string) {
    debugger;
    if (Type == '2') {
      this.CareGap = true;
      this.isMatGrid = true;
      this.Emptylist = false;
      this.PatientListAsPerMeasure(Number(localStorage.getItem("MasterMeaure")));
    }
    else if (Type == '3') {
      this.CareGap = false;
      this.isMatGrid = false;
      this.Emptylist = true;
      this.FilterPatientAsPerMismatch("1");
    }
  }

  FilterPatientAsPerMismatch(Id: string) {
    this.ELEMENT_DATA2 = [];
    this.ELEMENT_DATA1 = [];
    this.dashboardService.GetAttributionPatientList(this.ABAFilters).subscribe(
      response => {
        debugger;
        if (response.Item1.length > 0 && Id == "1") {
          this.AttributionMatchedPatient = [];
          this.EmptyAttributionMatchedlist = false;
          this.length = response.Item1[0].TotalRows;
          response.Item1.forEach(element => {
            this.AttributionMatchedPatient.push({
              Name: element.PatientName, PrimaryProvider: element.PatientName,
              EthnicityName: element.EthnicityName, RaceName: element.RaceName, Status: element.Status, EthnicityId: element.EthnicityId,
              DOB: element.DOB, Age: element.Age, OrganizationId: element.OrganizationId, Gap: element.Gap, Days: element.Days, ClinicName: element.ClinicName,
              ProviderName: element.ProviderName
            })
          });

          this.ELEMENT_DATA1 = this.AttributionMatchedPatient;
          this.dataSource = new MatTableDataSource<Element>(this.ELEMENT_DATA1);
        }
        else {
          this.AttributionMatchedPatient = [];
          this.ELEMENT_DATA1 = this.AttributionMatchedPatient;
          this.dataSource = new MatTableDataSource<Element>(this.ELEMENT_DATA1);
          this.pageAttributionMatchedSize = 0;
          this.EmptyAttributionMatchedlist = true;
        }
        if (response.Item2.length > 0 && Id == "2") {
          this.AttributionNotMatched = [];
          this.EmptyAttributionNotMatchedlist = false;
          this.length = response.Item2[0].TotalRows;
          response.Item2.forEach(element => {
            this.AttributionNotMatched.push({
              Name: element.PatientName, PrimaryProvider: element.PatientName,
              EthnicityName: element.EthnicityName, RaceName: element.RaceName, Status: element.Status, EthnicityId: element.EthnicityId,
              DOB: element.DOB, Age: element.Age, OrganizationId: element.OrganizationId, Gap: element.Gap, Days: element.Days, ClinicName: element.ClinicName,
              ProviderName: element.ProviderName
            })
          });
          this.ELEMENT_DATA2 = this.AttributionNotMatched;
          this.dataSource = new MatTableDataSource<Element>(this.ELEMENT_DATA2);
          this.dashboardService.clearFilterSetting('Clear Filter');
        }
        else {
          this.AttributionNotMatched = [];
          this.ELEMENT_DATA2 = this.AttributionNotMatched;
          this.dataSource = new MatTableDataSource<Element>(this.ELEMENT_DATA2);
          this.pageAttributionNotMatchedSize = 0;
          this.EmptyAttributionNotMatchedlist = true;
        }
      }
    )
  }

  RemoveFilterSection(data) {
    debugger;
    if (data.target.id != 'sidebar') {
      this.dashboardService.changeScreenSetting('false');
      this.showfilter = false;
    }
  }

PatientListAsPerMeasure(MeasureId:number){
MeasureId=MeasureId==0?1:MeasureId;
this.ABAFilters.MeasureId=MeasureId;
debugger;
switch(Number(MeasureId)){
case 1:
this.GetAllMeasurePatientList();
break;
case 2:
this.GetColPatientList();
break;
case 3 :
this.GetBCSPatientList();
break;
case 4 :
this.GetCCSPatientList();
break;
case 5 :
this.GetCBPPatientList();

    }
  }

  GetColPatientList() {
    debugger;
    this.EligiblePatient = [];
    this.ELEMENT_DATA = [];
    Object.keys(this.ABAFilters).forEach(key => {
      if (this.ABAFilters[key] === undefined || this.ABAFilters[key] == null) {
        this.ABAFilters[key] = 0;
      }
    })

    //   this.dashboardService.getMeasurename(this.ABAFilters.UserId).subscribe(response =>{

    //     let filterValues :CommonFilters=new CommonFilters();
    //     // ---initialize new object to pass
    //     this.filterValues.OrganizationId=response.organizationItem.id;
    //     this.ABAFilters.OrganizationId= this.filterValues.OrganizationId;
    // }



    this.ABAFilters.OrganizationId = this.Role == "SuperAdmin" ? 0 : parseInt(localStorage.getItem("OrganizationId"));

    
    this.dashboardService.GetCOLPatientsList(this.ABAFilters).subscribe(
      response => {
        debugger;
        if (response.length > 0) {
          this.Emptylist = false;
          this.length = response[0].TotalRows;
          this.EligiblePatient = [];
          response.forEach(element => {
            this.EligiblePatient.push({
              Name: element.PatientName, NextVisit: element.NextVisit,
              LastVisit: element.LastVisit, RaceName: element.RaceName, Status: element.Status, EthnicityName: element.EthnicityName, FileName: element.FileName,
              DOB: element.DOB, Age: element.Age, OrganizationId: element.OrganizationID, Gap: element.Gap, Days: element.Days, ClinicName: element.ClinicName,
              ProviderName: element.ProviderName, PatientId: element.PatientId
            })
          });

        }

        else {
          this.pageSize = 0;
          this.Emptylist = true;
        }
        this.ELEMENT_DATA = this.EligiblePatient;
        this.dataSource = new MatTableDataSource<Element>(this.ELEMENT_DATA);
        this.dashboardService.clearFilterSetting('Clear Filter');


      }
    )
  }


  GetBCSPatientList() {
    debugger;
    this.EligiblePatient = [];
    this.ELEMENT_DATA = [];
    Object.keys(this.ABAFilters).forEach(key => {
      if (this.ABAFilters[key] === undefined || this.ABAFilters[key] == null) {
        this.ABAFilters[key] = 0;
      }
    })
    this.ABAFilters.OrganizationId = this.Role == "SuperAdmin" ? 0 : parseInt(localStorage.getItem("OrganizationId"));
    this.dashboardService.GetBCSPatientsList(this.ABAFilters).subscribe(
      response => {
        debugger;
        if (response.length > 0) {
          this.Emptylist = false;
          this.length = response[0].TotalRows;
          this.EligiblePatient = [];
          response.forEach(element => {
            this.EligiblePatient.push({
              Name: element.PatientName, NextVisit: element.NextVisit,
              LastVisit: element.LastVisit, RaceName: element.RaceName, Status: element.Status, EthnicityName: element.EthnicityName,
              DOB: element.DOB, Age: element.Age, OrganizationId: element.OrganizationID, Gap: element.Gap, Days: element.Days, ClinicName: element.ClinicName,
              ProviderName: element.ProviderName, FileName: element.FileName, PatientId: element.PatientId
            })
          });
        }

        else {
          this.pageSize = 0;
          this.Emptylist = true;
        }
        this.ELEMENT_DATA = this.EligiblePatient;
        this.dataSource = new MatTableDataSource<Element>(this.ELEMENT_DATA);
        this.dashboardService.clearFilterSetting('Clear Filter');


      }
    )
  }


  fnGetPatientCDA(element) {
    debugger;
    let test = element;
    element.OrganizationId
    element.FileName
    if (element.OrganizationId && element.FileName) {
      debugger;
      this.patientService.GetPatientCda(element.OrganizationId, element.FileName, true, element.PatientId).subscribe(
        response => {
          debugger;
          var win = window.open(response.toString(), '_blank');
          win.focus();
        }
      )
    }

  }

  GetCCSPatientList() {
    debugger;
    this.EligiblePatient = [];
    this.ELEMENT_DATA = [];
    Object.keys(this.ABAFilters).forEach(key => {
      if (this.ABAFilters[key] === undefined || this.ABAFilters[key] == null) {
        this.ABAFilters[key] = 0;
      }
    })
    this.ABAFilters.OrganizationId = this.Role == "SuperAdmin" ? 0 : parseInt(localStorage.getItem("OrganizationId"));
    this.dashboardService.GetCCSPatientList(this.ABAFilters).subscribe(
      response => {
        debugger;
        if (response.length > 0) {
          this.Emptylist = false;
          this.length = response[0].TotalRows;
          this.EligiblePatient = [];
          response.forEach(element => {
            this.EligiblePatient.push({
              Name: element.PatientName, NextVisit: element.NextVisit,
              LastVisit: element.LastVisit, RaceName: element.RaceName, Status: element.Status, EthnicityName: element.EthnicityName,
              DOB: element.DOB, Age: element.Age, OrganizationId: element.OrganizationID, Gap: element.Gap, Days: element.Days, ClinicName: element.ClinicName,
              ProviderName: element.ProviderName, FileName: element.FileName, PatientId: element.PatientId
            })
          });

        }

        else {
          this.pageSize = 0;
          this.Emptylist = true;
        }
        this.ELEMENT_DATA = this.EligiblePatient;
        this.dataSource = new MatTableDataSource<Element>(this.ELEMENT_DATA);
        this.dashboardService.clearFilterSetting('Clear Filter');


      }
    )
  }

  GetCBPPatientList() {
    debugger;
    this.EligiblePatient = [];
    this.ELEMENT_DATA = [];
    Object.keys(this.ABAFilters).forEach(key => {
      if (this.ABAFilters[key] === undefined || this.ABAFilters[key] == null) {
        this.ABAFilters[key] = 0;
      }
    })
    this.ABAFilters.OrganizationId = this.Role == "SuperAdmin" ? 0 : parseInt(localStorage.getItem("OrganizationId"));
    this.dashboardService.GetCBPPatientList(this.ABAFilters).subscribe(
      response => {
        debugger;
        if (response.length > 0) {
          this.Emptylist = false;
          this.length = response[0].TotalRows;
          this.EligiblePatient = [];
          response.forEach(element => {
            this.EligiblePatient.push({
              Name: element.PatientName, NextVisit: element.NextVisit,
              LastVisit: element.LastVisit, RaceName: element.RaceName, Status: element.Status, EthnicityName: element.EthnicityName,
              DOB: element.DOB, Age: element.Age, OrganizationId: element.OrganizationId, Gap: element.Gap, Days: element.Days, ClinicName: element.ClinicName,
              ProviderName: element.ProviderName, FileName: element.FileName, PatientId: element.PatientId
            })
          });

        }

        else {
          this.pageSize = 0;
          this.Emptylist = true;
        }
        this.ELEMENT_DATA = this.EligiblePatient;
        this.dataSource = new MatTableDataSource<Element>(this.ELEMENT_DATA);
        this.dashboardService.clearFilterSetting('Clear Filter');


      }
    )
  }




SelectDataAsPerMasterMeasure(SelectedMaterMeasure:number){
  debugger;
  switch(SelectedMaterMeasure){
    case 1:
    if(this.router.url.indexOf('detailMeasureAnalysis')==-1)
    this.PatientListAsPerMeasure(this.SelectedMeasureId);
    else
    this.PatientListAsPerMeasure(this.ABAFilters.MeasureId);
    break;
    case 2:
    if(this.router.url.indexOf('detailMeasureAnalysis')==-1)
    this.PatientListAsPerWellMeasure(this.SelectedMeasureId);
    else
    this.PatientListAsPerWellMeasure(this.ABAFilters.MeasureId);
    break;
    case 3:
    if(this.router.url.indexOf('detailMeasureAnalysis')==-1)
    this.PatientListAsPerMeasure(this.SelectedMeasureId);
    else
    this.PatientListAsPerMeasure(this.ABAFilters.MeasureId);
    break;
    case 4:
    if(this.router.url.indexOf('detailMeasureAnalysis')==-1)
    this.PatientListAsPerWellMeasure(this.SelectedMeasureId);
    else
    this.PatientListAsPerWellMeasure(this.ABAFilters.MeasureId);
    break;
  }
}

  PatientListAsPerWellMeasure(MeasureId: number) {
    debugger;
    switch (Number(MeasureId)) {
      case 1:
        this.GetMeasureWellCarePatientList();
        break;
      case 2:
        this.GetColWellCarePatientList();
        break;
      case 3:
        this.GetBCSWellCarePatientList();
        break;
      case 4:
        // this.GetCCSPatientList();GetCCSWellCarePatientList
        this.GetCCSWellCarePatientList();
        break;
        case 5:
        // this.GetCCSPatientList();GetCCSWellCarePatientList
        this.GetCBPWellCarePatientList();
        break;
    }
  }

  GetMeasureWellCarePatientList() {

    debugger;
    this.EligiblePatient = [];
    this.ELEMENT_DATA = [];
    Object.keys(this.ABAFilters).forEach(key => {
      if (this.ABAFilters[key] === undefined || this.ABAFilters[key] == null) {
        this.ABAFilters[key] = 0;
      }
    })

    this.ABAFilters.OrganizationId = this.Role == "SuperAdmin" ? 0 : parseInt(localStorage.getItem("OrganizationId"));
    this.dashboardService.GetMeasureWellCarePatientList(this.ABAFilters).subscribe(
      response => {
        debugger;
        if (response.length > 0) {
          this.Emptylist = false;
          this.length = response[0].TotalRows;
          this.EligiblePatient = [];
          response.forEach(element => {
            this.EligiblePatient.push({
              Name: element.PatientName, NextVisit: element.NextVisit,
              LastVisit: element.LastVisit, RaceName: element.RaceName, Status: element.Status, EthnicityName: element.EthnicityName,
              DOB: element.DOB, Age: element.Age, OrganizationId: element.OrganizationID, Gap: element.Gap, Days: element.Days, ClinicName: element.ClinicName,
              ProviderName: element.ProviderName, FileName: element.FileName, PatientId: element.PatientId
            })
          });

        }

        else {
          this.pageSize = 0;
          this.Emptylist = true;
        }
        this.ELEMENT_DATA = this.EligiblePatient;
        this.dataSource = new MatTableDataSource<Element>(this.ELEMENT_DATA);
        this.dashboardService.clearFilterSetting('Clear Filter');


      }
    )
  }

  GetColWellCarePatientList() {
    debugger;
    this.EligiblePatient = [];
    this.ELEMENT_DATA = [];
    Object.keys(this.ABAFilters).forEach(key => {
      if (this.ABAFilters[key] === undefined || this.ABAFilters[key] == null) {
        this.ABAFilters[key] = 0;
      }
    })
    this.ABAFilters.OrganizationId = this.Role == "SuperAdmin" ? 0 : parseInt(localStorage.getItem("OrganizationId"));
    this.dashboardService.GetColWellCarePatientList(this.ABAFilters).subscribe(
      response => {
        debugger;
        if (response.length > 0) {
          this.Emptylist = false;
          this.length = response[0].TotalRows;
          this.EligiblePatient = [];
          response.forEach(element => {
            this.EligiblePatient.push({
              Name: element.PatientName, NextVisit: element.NextVisit,
              LastVisit: element.LastVisit, RaceName: element.RaceName, Status: element.Status, EthnicityName: element.EthnicityName,
              DOB: element.DOB, Age: element.Age, OrganizationId: element.OrganizationID, Gap: element.Gap, Days: element.Days, ClinicName: element.ClinicName, ProviderName: element.ProviderName,

              FileName: element.FileName, PatientId: element.PatientId
            })
          });

        }

        else {
          this.pageSize = 0;
          this.Emptylist = true;
        }
        this.ELEMENT_DATA = this.EligiblePatient;
        this.dataSource = new MatTableDataSource<Element>(this.ELEMENT_DATA);
        this.dashboardService.clearFilterSetting('Clear Filter');

      }
    )
  }
  GetCCSWellCarePatientList() {
    debugger;
    this.EligiblePatient = [];
    this.ELEMENT_DATA = [];
    Object.keys(this.ABAFilters).forEach(key => {
      if (this.ABAFilters[key] === undefined || this.ABAFilters[key] == null) {
        this.ABAFilters[key] = 0;
      }
    })
    //  this.ABAFilters.MeasureId=4;   
    this.ABAFilters.OrganizationId = this.Role == "SuperAdmin" ? 0 : parseInt(localStorage.getItem("OrganizationId"));
    this.dashboardService.GetCCSWellCarePatientList(this.ABAFilters).subscribe(
      response => {
        debugger;
        if (response.length > 0) {
          this.Emptylist = false;
          this.length = response[0].TotalRows;
          this.EligiblePatient = [];
          response.forEach(element => {
            this.EligiblePatient.push({
              Name: element.PatientName, NextVisit: element.NextVisit,
              LastVisit: element.LastVisit, RaceName: element.RaceName, Status: element.Status, EthnicityName: element.EthnicityName,
              DOB: element.DOB, Age: element.Age, OrganizationId: element.OrganizationID, Gap: element.Gap, Days: element.Days, ClinicName: element.ClinicName
              , ProviderName: element.ProviderName, FileName: element.FileName, PatientId: element.PatientId
            })
          });

        }

        else {
          this.pageSize = 0;
          this.Emptylist = true;
        }
        this.ELEMENT_DATA = this.EligiblePatient;
        this.dataSource = new MatTableDataSource<Element>(this.ELEMENT_DATA);
        this.dashboardService.clearFilterSetting('Clear Filter');

      }
    )
  }
  GetBCSWellCarePatientList() {
    debugger;
    this.EligiblePatient = [];
    this.ELEMENT_DATA = [];
    Object.keys(this.ABAFilters).forEach(key => {
      if (this.ABAFilters[key] === undefined || this.ABAFilters[key] == null) {
        this.ABAFilters[key] = 0;
      }
    })
    //  this.ABAFilters.MeasureId=4;   
    this.ABAFilters.OrganizationId = this.Role == "SuperAdmin" ? 0 : parseInt(localStorage.getItem("OrganizationId"));
    this.dashboardService.GetBCSWellCarePatientList(this.ABAFilters).subscribe(
      response => {
        debugger;
        if (response.length > 0) {
          this.Emptylist = false;
          this.length = response[0].TotalRows;
          this.EligiblePatient = [];
          response.forEach(element => {
            this.EligiblePatient.push({
              Name: element.PatientName, NextVisit: element.NextVisit,
              LastVisit: element.LastVisit, RaceName: element.RaceName, Status: element.Status, EthnicityName: element.EthnicityName,
              DOB: element.DOB, Age: element.Age, OrganizationId: element.OrganizationID, Gap: element.Gap, Days: element.Days, ClinicName: element.ClinicName
              , ProviderName: element.ProviderName, FileName: element.FileName, PatientId: element.PatientId
            })
          });

        }

        else {
          this.pageSize = 0;
          this.Emptylist = true;
        }
        this.ELEMENT_DATA = this.EligiblePatient;
        this.dataSource = new MatTableDataSource<Element>(this.ELEMENT_DATA);
        this.dashboardService.clearFilterSetting('Clear Filter');

      }
    )
  }
  GetCBPWellCarePatientList()
    {
      debugger;
      this.EligiblePatient = [];
      this.ELEMENT_DATA = [];
      Object.keys(this.ABAFilters).forEach(key => {
        if (this.ABAFilters[key] === undefined || this.ABAFilters[key] == null) {
          this.ABAFilters[key] = 0;
        }
      })
      //  this.ABAFilters.MeasureId=4;   
      this.ABAFilters.OrganizationId = this.Role == "SuperAdmin" ? 0 : parseInt(localStorage.getItem("OrganizationId"));
      this.dashboardService.GetCBPWellCarePatientList(this.ABAFilters).subscribe(
        response => {
          debugger;
          if (response.length > 0) {
            this.Emptylist = false;
            this.length = response[0].TotalRows;
            this.EligiblePatient = [];
            response.forEach(element => {
              this.EligiblePatient.push({
                Name: element.PatientName, NextVisit: element.NextVisit,
                LastVisit: element.LastVisit, RaceName: element.RaceName, Status: element.Status, EthnicityName: element.EthnicityName,
                DOB: element.DOB, Age: element.Age, OrganizationId: element.OrganizationID, Gap: element.Gap, Days: element.Days, ClinicName: element.ClinicName
                , ProviderName: element.ProviderName, FileName: element.FileName, PatientId: element.PatientId
              })
            });
  
          }
  
          else {
            this.pageSize = 0;
            this.Emptylist = true;
          }
          this.ELEMENT_DATA = this.EligiblePatient;
          this.dataSource = new MatTableDataSource<Element>(this.ELEMENT_DATA);
          this.dashboardService.clearFilterSetting('Clear Filter');
  
        }
      )
    }
  
  goTodashboard() {
    debugger;
    this.router.navigate(['/Admin/dashboard']);
  }
}

