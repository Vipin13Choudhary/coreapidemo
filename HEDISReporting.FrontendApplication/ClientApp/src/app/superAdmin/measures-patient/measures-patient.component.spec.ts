import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeasuresPatientComponent } from './measures-patient.component';

describe('MeasuresPatientComponent', () => {
  let component: MeasuresPatientComponent;
  let fixture: ComponentFixture<MeasuresPatientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeasuresPatientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeasuresPatientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
