import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProviderService } from '../../service/Provider/provider.service';


@Component({
  selector: 'app-provider-view',
  templateUrl: './provider-view.component.html',
  styleUrls: ['./provider-view.component.css']
})
export class ProviderViewComponent implements OnInit {
  LoginUserId: string;
  editableOrganizationID: string = '0';
  ProviderData: any;
  ProviderForm: any;
  constructor(private route: ActivatedRoute, private providerService: ProviderService) {
  }

  ngOnInit() {
    this.LoginUserId = localStorage.getItem("LoginUserID");
    this.route.queryParams.subscribe(params => {
      if (params.id != undefined) {
        this.editableOrganizationID = params.id;
      }
    })
    this.GetProviderData();

  }
  GetProviderData() {
    debugger;
    this.providerService.getProviderDetailById(this.editableOrganizationID).subscribe(
      Response => {
        debugger;
        let selectedProvider = Response[0];
        this.ProviderForm=selectedProvider;
      }
    )
  }
}
