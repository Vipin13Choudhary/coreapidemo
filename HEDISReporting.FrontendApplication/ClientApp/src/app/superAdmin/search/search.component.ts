import { Component, OnInit } from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  length = 5;
  pageSize = 10;
  pageSizeOptions: number[] = [1, 5, 10, 20];
  displayedColumns = ['Name', 'DOB', 'Status','ClinicName','NextVisit','LastVisit','Days','Gap','Note','Action'];

  constructor() { }

  ngOnInit() {
  }

}
