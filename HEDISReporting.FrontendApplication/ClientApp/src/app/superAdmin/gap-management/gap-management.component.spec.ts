import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GapManagementComponent } from './gap-management.component';

describe('GapManagementComponent', () => {
  let component: GapManagementComponent;
  let fixture: ComponentFixture<GapManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GapManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GapManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
