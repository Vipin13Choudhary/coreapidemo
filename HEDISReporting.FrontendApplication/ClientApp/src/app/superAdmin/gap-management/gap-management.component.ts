import { Component, OnInit } from '@angular/core';
import { AttributeService } from 'src/app/service/attribute/attribute.service';
import { DashboardService } from 'src/app/service/dashboard/dashboard.service';
import { CommonFilters } from 'src/app/model/dashboard/dashboard';
import { MatTableDataSource } from '@angular/material';
import { LoaderService } from 'src/app/service/loader/loader.service';
import { PatientService } from 'src/app/service/patient/patient.service';
import { Toast, ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-gap-management',
  templateUrl: './gap-management.component.html',
  styleUrls: ['./gap-management.component.css']
})
export class GapManagementComponent implements OnInit {
  isMatGrid: boolean = true;
  Emptylist: boolean = false;
  CareGap: Boolean = false;
  Filter: boolean = false;
  ABAFilters: CommonFilters;
  CareGapPatient: any = [];
  length: number = 0;
  ELEMENT_DATA: any = [];
  pageSize: number = 10;
  dataSource: any = [];
  pageSizeOptions: number[] = [1, 5, 10, 20];
  displayedColumns = ['Name', 'Organization', 'DOB', 'Gender', 'PCP Name', 'Ethnicity', 'Race', 'Gap', 'Action'];
  constructor(private attributeService: AttributeService,
    private loader: LoaderService,
    private patientService: PatientService,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.isMatGrid = true;
    this.Emptylist = true;
    this.CareGap = false;
    this.Filter = true;
    this.ABAFilters = new CommonFilters();
    this.ABAFilters.OrganizationId = Number(localStorage.getItem('OrganizationId'));
    this.ABAFilters.UserId = Number(localStorage.getItem('LoginUserID'));
    this.ABAFilters.Search = '';
    this.ABAFilters.sort = '';
    this.ABAFilters.order = 'asc';
    this.ABAFilters.offset = 1;
    this.ABAFilters.limit = 10;
    this.getGapManagementPatientList();
  }
  // This function is used to show Gap Management Patient List
  getGapManagementPatientList() {
    this.loader.Start();
    this.attributeService.getGapPatientList(this.ABAFilters).subscribe(response => {
      debugger;
      if (response.length > 0) {
        this.CareGapPatient = [];
        this.length = response[0].TotalRows;
        response.forEach(element => {
          this.CareGapPatient.push({
            Name: element.PatientName,
            PatientId: element.PatientID,
            PrimaryProvider: element.PrimaryProvider,
            EthnicityName: element.EthnicityName,
            RaceName: element.RaceName,
            Organization: element.OrganizationName,
            OrganizationId: element.OrganizationId,
            EthnicityId: element.EthnicityId,
            DOB: element.DOB,
            Age: element.Age,
            Gap: element.Gap,
            Gender: element.Gender,
            FileName:element.FileName == null ? false : element.FileName,
          })
        });
        this.ELEMENT_DATA = this.CareGapPatient;
        this.Emptylist = false;
        this.pageSize = this.ABAFilters.limit;
        this.isMatGrid = true;
        this.dataSource = new MatTableDataSource<Element>(this.ELEMENT_DATA);
      } else {
        this.CareGapPatient = [];
        this.Emptylist = true;
        this.length = 0;
        this.pageSize = this.ABAFilters.limit;
        this.isMatGrid = true;
        this.ELEMENT_DATA = this.CareGapPatient;
        this.dataSource = new MatTableDataSource<Element>(this.ELEMENT_DATA);
      }
      this.loader.Stop();
    })
  }
  //ENd
  setPageSizeOptions(setPageSizeOptionsInput: string) {
    this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
  }
  getServerData(pageSize) {
    debugger;
    this.pageSize = pageSize;
    this.ABAFilters.offset = pageSize.pageIndex + 1;
    this.ABAFilters.limit = pageSize.pageSize;
    this.getGapManagementPatientList();
  }

  sortData(header, OrderType) {
    debugger;
    this.loader.Start();
    if (OrderType.currentTarget.attributes.getNamedItem('aria-sort') != null) {
      OrderType = OrderType.currentTarget.attributes.getNamedItem('aria-sort').value;
      OrderType = OrderType == "descending" ? 'desc' : 'asc';
    }
    else{
      this.loader.Stop();
        return;
    }
    this.ABAFilters.sort = header;
    this.ABAFilters.order = OrderType;
    this.getGapManagementPatientList();
  }

  // This function is used to show and hide child component
  FilterPatientAsPerSearch(Type: string) {
    if (Type == '2') {
      this.CareGap = true;
      this.isMatGrid = true;
      this.Emptylist = false;
    }
    else if (Type == '3') {
      this.CareGap = false;
      this.isMatGrid = false;
      this.Emptylist = true;
    }
  }
  //End
  //This function is used open CDA file for particular Patient
  fnGetPatientCDA(element) {
    element.OrganizationId
    element.FileName
    if (element.OrganizationId && element.FileName) {
      this.patientService.GetPatientCda(element.OrganizationId, element.FileName, true, element.PatientId).subscribe(
        response => {
          var win = window.open(response.toString(), '_blank');
          win.focus();
        }
      )
    }
  }
  //End
}
