import { Component, OnInit, Input } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { NeedAttentionService } from '../../service/NeedAttention/need-attention.service';
import { CommonFilters } from 'src/app/model/dashboard/dashboard';
import { NeedAttentionFilter } from '../../model/NeedAttention/NeedAttention';
import { ToastrService } from 'ngx-toastr';
import { LoaderService } from 'src/app/service/loader/loader.service';

@Component({
  selector: 'app-need-attention',
  templateUrl: './need-attention.component.html',
  styleUrls: ['./need-attention.component.css']
})
export class NeedAttentionComponent implements OnInit {
  @Input() patientId: number;
  dataSource: any;
  length = 5;
  pageSize = 5;
  pageSizeOptions: number[] = [1, 5, 10, 15, 20];
  displayedColumns = ['MPI', 'Organization', 'Account', 'Status', 'Reason', 'Queue'];
  ELEMENT_DATA: any;
  AttentionPatient: any = [];
  needAttentionList:boolean=false;
  message:string;
  CommonFilters: NeedAttentionFilter;
  constructor(private needAttentionService: NeedAttentionService,
    private loader: LoaderService,
     private toastr: ToastrService) { }
  ngOnInit() {
    debugger;
    this.CommonFilters = new NeedAttentionFilter();
    this.CommonFilters.patientId = this.patientId;
    this.CommonFilters.order = 'asc';
    this.CommonFilters.limit = 5;
    this.CommonFilters.offset = 1;
    this.CommonFilters.sort = null;
    this.GetNeedAttentionPatienList();
    this.needAttentionService.currentMessage.subscribe(message => this.message = message);
  }

  GetNeedAttentionPatienList() {
    debugger;
    this.loader.Start();
    this.AttentionPatient = [];
    this.ELEMENT_DATA = [];
    this.needAttentionService.GetNeedAttention(this.CommonFilters).subscribe(
      response => {
        debugger;
        if (response.Item1.length > 0) {
          this.AttentionPatient = [];
          response.Item1.forEach(element => {
            debugger;           
            this.needAttentionList=true;
            this.AttentionPatient.push({
              PatientWorkItemsId: element.PatientWorkItemsId,
              PatientStatusCode: element.PatientStatusCode,
              ReasonCode: element.ReasonCode,
              PatientStatus: element.PatientStatus,
              CreatedDate: element.CreatedDate,
              QueueStatus: element.QueueStatus,
              QueueStatusCode: element.QueueStatusCode,
              Reason: element.Reason,
              OrganizationName: element.OrganizationName,
              PatientId: element.PatientId,
              KMPI: element.KMPI,
              DaysPassed: element.DaysPassed,
              AttentionFlag:false
            })
          });
        }else{
          this.needAttentionList=false;
        }
      })
      this.loader.Stop();
  }

  changeStatus(event, Item) {
    debugger;
    this.loader.Start();
    this.needAttentionService.changeMessage(this.AttentionPatient);
  //   this.needAttentionService.UpdateNeedAttention(Item.PatientWorkItemsId, Item.PatientId, Item.PatientStatusCode, Item.ReasonCode).subscribe(
  //     response => {
  //       debugger;
  //       if (JSON.stringify(response).indexOf('Success') > -1) {
  //         this.toastr.success('Status updated successfully');
  //         this.GetNeedAttentionPatienList(this.CommonFilters);
  //       }
  //     }, err => {
  //       this.toastr.error('Something went wrong');
  //     })
  this.loader.Stop();
   }
}
