import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NeedAttentionComponent } from './need-attention.component';

describe('NeedAttentionComponent', () => {
  let component: NeedAttentionComponent;
  let fixture: ComponentFixture<NeedAttentionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NeedAttentionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NeedAttentionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
