import { Component, OnInit,TemplateRef,ViewChild } from '@angular/core';
import {Organization,Element} from '../../model/Organization';
import "rxjs"
import {OrganizationsService} from '../../service/organization/organizations.service';
  import {listOperation,statesDropDown,organizationDropDown} from '../../model/common/common'
  import {MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
  import {PageEvent} from '@angular/material';
  import * as _ from 'underscore';
  import { ToastrService } from 'ngx-toastr';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Validators, FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { UserService } from '../../service/user/user.service';
import { LoaderService } from '../../service/loader/loader.service';
import {Router} from '@angular/router';
import {map, startWith} from 'rxjs/operators';
import { Observable } from 'rxjs';


export interface Food {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-organization',
  templateUrl: './organization.component.html',
  styleUrls: ['./organization.component.css']
})
export class OrganizationComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sorting: MatSort;
  OrganizationList :any=[];
  searchOrgDropDown =[];
  listOperands:any;
   ELEMENT_DATA: any;
  dataSource: any;
  length = 5;
  pageSize = 10;
  pageSizeOptions: number[] = [1, 5, 10, 20];
  displayedColumns = ['OrganizationName', 'OrganizationAddress', 'OrganizationPhoneNumber','OrganizationFax', 'ContactPersonName','OrganizationWebSite','Action'];
  pageEvent: PageEvent;
  search:"";
  OrganizationDropDown=[];
  StatesDropDown:statesDropDown[];
  //Model Ref
  modalRef: BsModalRef;
  showDashboard:false;
  OrganizationId:number;
  LoginUserId:string;
  LoginOrganizationId:number;
  Role:string;

  //-=--------
  myControl = new FormControl();
  options: string[] = [];
  filteredOptions: Observable<string[]>;
  
  constructor(private loader:LoaderService, private fbuilder: FormBuilder,private organizationService:OrganizationsService,private modalService: BsModalService,
    private userService: UserService,private toastr: ToastrService,private router:Router) {  
    this.userService.changeScreen("false")
    this.listOperands=new listOperation();

   }


  ngOnInit() {
    debugger;
    //this.userService.showDashboard.subscribe(message => this.message = message)
   // this.userService.showDashboard.subscribe(message => this.showDashboard = this.showDashboard)
    this.LoginUserId=localStorage.getItem("LoginUserID");
    this.LoginOrganizationId=parseInt(localStorage.getItem("OrganizationId"));
    this.Role=localStorage.getItem("Role");
    this.listOperands.offset=1;
    this.listOperands.limit=10;
    this.listOperands.order="desc";
    this.listOperands.sort="";
    this.listOperands.Search="";
    this.listOperands.OrganizationId=this.Role=="SuperAdmin"?0:this.LoginOrganizationId;
    this.getOrganizationList(this.listOperands);
    this.getAllDropDown(this.LoginUserId);
    
    this.filteredOptions = this.myControl.valueChanges.pipe(
      map(value => this._filter(value))
    );
  }
  
  private _filter(value: string): string[] {
    debugger;
    const filterValue = value.toLowerCase();
    if(filterValue.length>=4 )
    {
    const datas=this.OrganizationList.filter(option => option.OrganizationName.toLowerCase().indexOf(filterValue) === 0);
    return datas;
    // debugger;
    // let lorglist=[];
    // debugger;
    // // this.OrganizationList.forEach(element => {
    // //   lorglist.push
    // // });
    // var org=this.OrganizationList;
    // var test=this.OrganizationList.filter(x=>function(item){
    //   debugger;
    //   if(item.OrganizationName.toLowerCase()==value.toLowerCase())
    //   return true;
    // });
    // return test;
      // return this.OrganizationList.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }
  else if (filterValue=="")
  {
    this.listOperands.OrganizationId=this.Role=="SuperAdmin"?0:this.LoginOrganizationId;;
    this.getOrganizationList(this.listOperands);
  }
}

  getOrganizationList(data){
    debugger;
    if(data.Search.length==0 || data.Search.length>=4)
    {  
    this.loader.Start();
    // this.LoginOrganizationId=parseInt(localStorage.getItem("OrganizationId"));
    // this.listOperands.OrganizationId=this.LoginOrganizationId;
     this.organizationService.getOrganizationList(this.listOperands).subscribe(response=>{
       debugger
       this.ELEMENT_DATA=[];
       this.OrganizationList=[];
       if(response.length>0){
         this.length=response[0].TotalRows;
       response.forEach(element => {
         this.OrganizationList.push({OrganizationId:element.OrganizationId,OrganizationName:element.OrganizationName,
         GlobalType:element.GlobalType,
          OrganizationAddress:element.OrganizationAddress,
          Address2Row:element.Address2Row,
  
         OrganizationPhoneNumber:element.OrganizationPhoneNumber,OrganizationFax:element.OrganizationFax,
         OrganizationWebSite:element.OrganizationWebSite,ContactPersonName:element.ContactPersonName
        });
       });
       


       }
       else{
        this.pageSize=0;
       }
      this.ELEMENT_DATA = this.OrganizationList;
      this.dataSource = new MatTableDataSource<Element>(this.ELEMENT_DATA);
      this.loader.Stop();
    });
  }
  }
  //Paging
  setPageSizeOptions(setPageSizeOptionsInput: string) {
    this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
  }

  getServerData(pageData){
    this.listOperands.offset=pageData.pageIndex+1;
    this.listOperands.limit=pageData.pageSize;
    this.getOrganizationList(this.listOperands);
  }

  //short data as per coulumn 
  sortData(header,OrderType){
    debugger
    this.loader.Start();
    if(OrderType.currentTarget.attributes.getNamedItem('aria-sort')!=null){
      OrderType=OrderType.currentTarget.attributes.getNamedItem('aria-sort').value;
    if(OrderType=="descending"){
      OrderType='desc';
    }
    else
    {
      OrderType='asc';
    }
      }
    else{
       this.loader.Stop();
         return;
     }
        this.listOperands.sort=header;
        this.listOperands.order=OrderType;
        this.getOrganizationList(this.listOperands);
        this.loader.Stop();
  }

  getSearchedData(option){
    debugger
    // this.listOperands.Search=this.search;
    // this.getOrganizationList(this.listOperands);
    this.listOperands.OrganizationId=option.OrganizationId;
    this.getOrganizationList(this.listOperands);
  }

 //As per drop downs (further added on future)
 getFilteredList(organizationId){
  this.listOperands.OrganizationId=organizationId;
  this.getOrganizationList(this.listOperands);
}

  //getting all drop downs

  getAllDropDown(userId){

    this.organizationService.getAllDropDown(localStorage.getItem("LoginUserID"),this.Role=="SuperAdmin"?0:parseInt(localStorage.getItem("OrganizationId"))).subscribe(response=>{
      this.OrganizationDropDown=response.organizationsList;
      this.StatesDropDown=response.statesList;

    });
  }

//#region --Organization Sftp and FHIR Settings
DeleteOrganization(element,modelName)
  {

    let template: TemplateRef<any>=modelName;
    this.modalRef = this.modalService.show(template);
    this.OrganizationId=element.OrganizationId;
  }

  DeleteSelectedOrganization(){
    this.organizationService.deleteSelectedItem(this.OrganizationId,"Organizations").subscribe(response=>{

      if(response.status==200){
      this.toastr.success('Organization deleted successfully.', 'Success');
      this.getOrganizationList(this.listOperands);
      this.modalRef.hide();

    }// this.OrganizationDropDown.push({id:6,name:'All'})
      else
      this.toastr.error('There is error in deleting organization.Please try after sometime.', 'Error');

    });
  }


//#endregion

//#region edit
EditOrganization(element){
  debugger;
//
this.userService.getEditOrganizationID(element.OrganizationId);
 // this.router.navigate(['/Admin', 'editOrganization']);

}
AddOrganization(){
  //
  this.userService.getEditOrganizationID('0');
    this.router.navigate(['/Admin', 'setUp']);
  }

//endregion

}
