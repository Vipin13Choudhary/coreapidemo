import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DashboardService } from 'src/app/service/dashboard/dashboard.service';
import { MasterRule } from 'src/app/model/dashboard/MeasureRules';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { ToastrService } from 'ngx-toastr';
import { LoaderService } from 'src/app/service/loader/loader.service';
import { getDate } from 'ngx-bootstrap/chronos/utils/date-getters';
import {Location} from '@angular/common';

@Component({
  selector: 'app-measure-detail',
  templateUrl: './measure-detail.component.html',
  styleUrls: ['./measure-detail.component.css']
})

export class MeasureDetailComponent implements OnInit {
  MeasureId: number = 0;
  edittable:boolean=false;
  //measureDetails=[MasterRule];
  measureDetails:MasterRule= {};
  ismeasureName = false;
  measureYear:boolean=false;
  measureNameEditor:boolean=true;
  measureYearEditor:boolean=true;
  trackingEnds:boolean=false;
  trackingEndsEditor:boolean=true;
  description:boolean=false;
  descriptionEditor:boolean=true;
  cptCodeHeader:boolean=false;
  cptCodeHeaderEditor:boolean=true;
  exclusionRules:boolean=false;
  exclusionRulesEditor:boolean=true;
  denominatorRules:boolean=false;
  denominatorRulesEditor:boolean=true;
  numeratorRules:boolean=false;
  numeratorRulesEditor:boolean=true;
  IsButtonDisable:boolean=false;
  IsNewMeasure:boolean=false;
  date:Date =new Date();
  constructor(private loader: LoaderService, private toastr: ToastrService,private route: ActivatedRoute, private router: Router, private dashboardService: DashboardService,private _location:Location,) { }


  ngOnInit() {
    debugger
    let body = document.getElementById('side_body');
    // this.htmlContent='<div>Hello test</div>'
    body.classList.add('page-measure-detail');
    this.route.queryParams.subscribe(params => {
      if (params.id != undefined) {
        this.MeasureId = params.id;
        debugger;
        if (Number(this.MeasureId) > 0) {
          this.GetMeasureDetails();
        }
        else {
          this.router.navigate(['/Admin/dashboard'])
        }
      }
    })    
  }

  EnableMeasureEditor(EditorName:string){
  if(EditorName=='measureName'){
      this.ismeasureName=true;
      this.measureNameEditor=false;
    }else if(EditorName=='measureYear'){
      this.measureYear=true;
      this.measureYearEditor=false;
    }else if(EditorName=='trackingEnds'){
      this.trackingEnds=true;
      this.trackingEndsEditor=false;
    }else if(EditorName=='description'){
      this.description=true;
      this.descriptionEditor=false;
    }else if(EditorName=='cptCodeHeader'){
      this.cptCodeHeader=true;
      this.cptCodeHeaderEditor=false;
    }else if(EditorName=='numeratorRules'){
      this.numeratorRules=true;
      this.numeratorRulesEditor=false;
    }else if(EditorName=='exclusionRules'){
      this.exclusionRules=true;
      this.exclusionRulesEditor=false;
    }else if(EditorName=='denominatorRules'){
      this.denominatorRules=true;
      this.denominatorRulesEditor=false;
    }else if(EditorName=='exclusionRules'){
      this.exclusionRules=true;
      this.exclusionRulesEditor=false;
    }
    this.IsButtonDisable=true;
  }

  DisableMeasureEditor(EditorName:string){
    debugger
    if(EditorName=='measureName'){
        this.ismeasureName=false;
        this.measureNameEditor=true;
      }else if(EditorName=='measureYear'){
        this.measureYear=false;
        this.measureYearEditor=true;
      }else if(EditorName=='trackingEnds'){
        this.trackingEnds=false;
        this.trackingEndsEditor=true;
      }else if(EditorName=='description'){
        this.description=false;
        this.descriptionEditor=true;
      }else if(EditorName=='cptCodeHeader'){
        this.cptCodeHeader=false;
        this.cptCodeHeaderEditor=true;
      }else if(EditorName=='numeratorRules'){
        this.numeratorRules=false;
        this.numeratorRulesEditor=true;
      }else if(EditorName=='exclusionRules'){
        this.exclusionRules=false;
        this.exclusionRulesEditor=true;
      }else if(EditorName=='denominatorRules'){
        this.denominatorRules=false;
        this.denominatorRulesEditor=true;
      }else if(EditorName=='exclusionRules'){
        this.exclusionRules=false;
        this.exclusionRulesEditor=true;
      }
      this.IsButtonDisable=false;
    }

  editorConfig: AngularEditorConfig = {
    editable: false,
    spellcheck: true,
    height: '35rem',
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    uploadUrl: 'v1/images', // if needed
    customClasses: [ // optional
      {
        name: "quote",
        class: "quote",
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: "titleText",
        class: "titleText",
        tag: "h1",
      },
    ]
  };
  GetMeasureDetails() {
    debugger;
    this.dashboardService.GetMeasureDetails(this.MeasureId).subscribe(response => {
      let measureDetails=response[0];
      this.measureDetails = measureDetails;
      if(measureDetails!=null && measureDetails!=undefined){
      this.IsNewMeasure=false;
      this.ViewMode();
        this.loader.Stop();
      }
      else{
        this.EmptyModel();
        this.IsNewMeasure=true;
        this.loader.Stop();
      }
    }, err=> { 
      this.loader.Stop();
    });
  }

    ViewMode(){
      this.ismeasureName=false;
      this.measureNameEditor=true;
      this.measureYear=false;
      this.measureYearEditor=true;
      this.trackingEnds=false;
      this.trackingEndsEditor=true;
      this.description=false;
      this.descriptionEditor=true;
      this.cptCodeHeader=false;
      this.cptCodeHeaderEditor=true;
      this.numeratorRules=false;
      this.numeratorRulesEditor=true;
      this.exclusionRules=false;
      this.exclusionRulesEditor=true;
      this.denominatorRules=false;
      this.denominatorRulesEditor=true;
      this.exclusionRules=false;
      this.exclusionRulesEditor=true;
    }
    EmptyModel()
    {
      this.measureDetails={};
      this.measureDetails.MeasureType=Number(localStorage.getItem('MasterMeaure'));
      this.measureDetails.description="";
      this.measureDetails.numeratorRules="";
      this.measureDetails.denominatorRules="";
      this.measureDetails.exclusionRules="";
      this.measureDetails.referenceCodes="";
      this.measureDetails.CPTCodeHeader="";
      this.measureDetails.trackingEnds=new Date();

    }

  SetMeasureDetails() {
    debugger;
    this.loader.Start();
    if(this.measureDetails.description!="<br>"){
    this.measureDetails.MeasureType=Number(localStorage.getItem('MasterMeaure'));
    this.dashboardService.SetMeasureDetails(this.measureDetails).subscribe(response => {
      this.GetMeasureDetails();
      this.IsButtonDisable=false;
      this.toastr.success("Data Saved Successfully!");
    },err => {
      this.loader.Stop();
    });}
    else{
      this.IsButtonDisable=true;
      this.loader.Stop();
      this.toastr.warning("Description can not be Empty!");
    }
   
  }
  backClicked() {
    this._location.back();
 }
}
