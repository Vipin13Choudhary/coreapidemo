import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { CareGapService } from 'src/app/service/careGap/care-gap.service';
import { CommonFilters } from 'src/app/model/dashboard/dashboard';
import { MatTableDataSource } from '@angular/material';
import { Subscription } from 'rxjs';
import { NeedAttentionService } from 'src/app/service/NeedAttention/need-attention.service';
import { LoaderService } from 'src/app/service/loader/loader.service';

@Component({
  selector: 'app-care-gap',
  templateUrl: './care-gap.component.html',
  styleUrls: ['./care-gap.component.css']
})
export class CareGapComponent implements OnInit {
  @Input() patientId: string;
  EligiblePatient: any = [];
  ELEMENT_DATA: any;
  Emptylist = true;
  dataSource: any;
  ImmunizationList:boolean=false;
  length = 5;
  CommonFilters: CommonFilters;
  ImunizationData:any=[];
  displayedColumns = ['Name', 'Deadline', 'Gap'];
  userId: string = localStorage.getItem('LoginUserID');
  constructor(private careGapService: CareGapService,
              private needAttentionService:NeedAttentionService,
              private loader:LoaderService) {
  }

  ngOnInit() {
    this.CommonFilters = new CommonFilters();
    this.CommonFilters.patientId = parseInt(this.patientId);
    this.CommonFilters.UserId = parseInt(this.userId);
    this.GetCareGapPatientList();
    this.GetImmunizations();
  }

  GetImmunizations(){
    this.loader.Start();
    this.careGapService.GetImunizationGap(this.CommonFilters).subscribe(
      response => {
        debugger;
        if (response.length > 0) {
          this.ImmunizationList=true;
          this.ImunizationData=[];
          response.forEach(element => {
            this.ImunizationData.push({
              Immunization: element.Immunization,
              ImmunizationDate: element.ImmunizationDate,
              ImmunizationDose: element.ImmunizationDose,
              ImmunizationUnits: element.ImmunizationUnits,
              PatientId: element.PatientId,
              ImmunizationFlag:false
            })
          });
         
        } else {
          this.ImmunizationList=false;
        }
      });
      this.loader.Stop();
  }


  GetCareGapPatientList() {
    this.EligiblePatient = [];
    this.ELEMENT_DATA = [];
    this.loader.Start();
    this.careGapService.GetABACareGap2018(this.CommonFilters).subscribe(
      response => {
        debugger;
        if (response.length > 0) {
          this.Emptylist = true;
          this.length = response[0].TotalRows;
          response.forEach(element => {
            this.EligiblePatient.push({
              Name: element.Name,
              NextVisit: element.NextVisit,
              LastVisit: element.LastVisit,
              RaceId: element.RaceId,
              Status: element.Status,
              EthnicityId: element.EthnicityId,
              DOB: element.DOB,
              Age: element.Age,
              OrganizationId: element.OrganizationID,
              Gap: element.Gap,
              FileName: element.FileName,
              PatientId: element.PatientId,
              Deadline: element.Deadline,
              CareGapFlag:false
            });
          });
        } else {
          this.Emptylist = false;
        }
        this.ELEMENT_DATA = this.EligiblePatient;
        this.dataSource = new MatTableDataSource<Element>(this.ELEMENT_DATA);
      }
    );
    this.loader.Stop();

  }

  changeStatus(event, Item) {
    this.needAttentionService.changeMessage(this.EligiblePatient);
   }
}
