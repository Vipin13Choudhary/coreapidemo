import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CareGapComponent } from './care-gap.component';

describe('CareGapComponent', () => {
  let component: CareGapComponent;
  let fixture: ComponentFixture<CareGapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CareGapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CareGapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
