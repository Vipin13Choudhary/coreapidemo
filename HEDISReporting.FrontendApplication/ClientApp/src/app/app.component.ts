import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public scrollbarOptions = { axis: 'y', theme: 'minimal-dark' };
  title = 'ClientApp';
}
