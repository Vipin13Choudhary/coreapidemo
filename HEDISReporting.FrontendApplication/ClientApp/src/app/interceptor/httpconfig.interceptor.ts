import { Injectable } from '@angular/core';
import {
    HttpInterceptor,
    HttpRequest,
    HttpResponse,
    HttpHandler,
    HttpEvent,
    HttpErrorResponse
} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { debug } from 'util';

@Injectable() 
export class HttpConfigInterceptor implements HttpInterceptor { 
    debugger;
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const  userId:string=localStorage.getItem('LoginUserID');
        const userName:string=localStorage.getItem('LoginUserName');
        const authToken:string=  localStorage.getItem('authenticationtoken');
    
        if (authToken) {
            
            request = request.clone({ headers: request.headers.set('Authorization', 'Bearer ' + authToken) });
        }
        if(userName)
        {
            request = request.clone({ headers: request.headers.set('UserName', userName) });
        }
        if (userId) {
            request = request.clone({ headers: request.headers.set('UserId', userId) });
        }
    
        //request = request.clone({ headers: request.headers.set('Accept', 'application/json') });
        return next.handle(request).pipe(
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    console.log('event--->>>', event);
                }
                return event;
            }));
    }
}
