import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuard} from './shared/auth-gaurd'
// import {LoginModule} from '../app/login/login.module'

const routes: Routes = [
     { path: '', redirectTo: '/Admin', pathMatch: 'full' },
     { path: 'login', loadChildren: './login/login.module#LoginModule' },
     { path: 'Admin', loadChildren: './superAdmin/super-admin/super-admin.module#SuperAdminModule' ,  canActivate: [AuthGuard] 
     //,canActivate:[AuthGuard]
    },

];


@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true,onSameUrlNavigation: 'reload' })],
  exports: [RouterModule]
})
export class AppRoutingModule {


 }
