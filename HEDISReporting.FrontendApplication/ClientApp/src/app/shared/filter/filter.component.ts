import { Component, OnInit,Output, EventEmitter,Input } from '@angular/core';
import {DashboardService} from '../../service/dashboard/dashboard.service';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { organizationDropDown, clinicDropDown, providerDropDown } from '../../model/common/common';
import { PatientService } from '../../service/patient/patient.service';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute} from '@angular/router';


@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {
  hide = true;
  showFilter:any;
  filterForm:FormGroup;
  EthinicityList:organizationDropDown[];
  VendorList:organizationDropDown[];
  RaceList:organizationDropDown[];
  MeasureList:organizationDropDown[];
  GendorList:organizationDropDown[];
  OrganizationList:organizationDropDown[];
  ClinicList:clinicDropDown[];
  ProviderList:providerDropDown[];
  PayerList:providerDropDown[];
  FilteringData:any;
  ClearFilter:string;
  showMeasureDropDown:boolean
  @Input() childMessage: string;
  @Output() messageEvent = new EventEmitter<any>();
  SelectedMeasureID:number=1
  bsValue:Date=new Date();
  maxDate:Date=new Date();
  //  = new Date();
  constructor(private DashboardService:DashboardService,private fb:FormBuilder,private patientService:PatientService,
  private Toaster:ToastrService,private route:ActivatedRoute) { 
      
    

    this.filterForm=fb.group({
    
      MeasureId:[null],
      // VendorId:[null],
      AgeRange:[null,Validators.compose([Validators.minLength(3), Validators.maxLength(20)])],
      DateRange:[null],
      RaceID:[null],
      OrganizationID:[null],
      ClinicID:[null],
      ProviderID:[null],
      PayerID:[null],
      GenderID:[null],
      EthnicityId:[null]

    })
    if(Number(localStorage.getItem('MasterMeaure'))==1 || Number(localStorage.getItem('MasterMeaure'))==2){
      this.bsValue=new Date('01/01/2018');
      this.maxDate=new Date('12/31/2018');
      //this.bsValue.setFullYear(this.bsValue.getFullYear() - 1);  
    }
    else if(Number(localStorage.getItem('MasterMeaure'))==3 || Number(localStorage.getItem('MasterMeaure'))==4)
   { this.bsValue=new Date('01/01/2019');
    this.maxDate=new Date('12/31/2019');
  }
    //this.bsValue.setFullYear(this.bsValue.getFullYear() - 1);
  }

  



  ngOnInit() {
  
  if(this.route.snapshot.routeConfig.path.indexOf('detailMeasureAnalysis')>-1){

    this.showMeasureDropDown=true;
  }
  else{
    this.showMeasureDropDown=false;
  }
      
   this.DashboardService.currentClearFilter.subscribe(message =>{    this.ClearFilter = message
    if( this.ClearFilter=="Clear Filter"){
      this.filterForm.reset();
     }
  })
    
   this.DashboardService.currentFilter.subscribe( 
    message =>{
        
       this.showFilter = message;
    }
  )
  if(this.route.snapshot.routeConfig.path.indexOf('patientList')>-1) 
    
  this.SelectedMeasureID= this.route.snapshot.queryParams.id;

     this.hide=this.showFilter;

    var t= this.hide
    // get all the filter dropdown
    this.getAllFilterDropDown();
    this.filterForm.patchValue({
      MeasureId:1,
      DateRange:[this.bsValue, this.maxDate]
    })

    
  }
 
 sidebarCollapse() {
   debugger
  this.DashboardService.changeScreenSetting('false')
}
 sidebarExp(){
   
}

getAllFilterDropDown(){

  debugger;
  this.DashboardService.getAllDropdowns().subscribe(response=>{
      
    this.EthinicityList=response.ethinicityList;
    this.VendorList=response.vendorList;
    this.RaceList=response.raceList;
    this.MeasureList=response.measureList;
    this.GendorList=response.gendorList;
      });

   this.patientService.GetOrganization(parseInt(localStorage.getItem('LoginUserID'))==2?0:localStorage.getItem('OrganizationId')).subscribe(
        (response)=>{
            
        
          this.OrganizationList=response.result.organization;
      });
   this.patientService.GetPayers().subscribe(
    (response)=>{
        
    
      this.PayerList=response.result.payers;
  });


  this.DashboardService.getMeasurename(localStorage.getItem('LoginUserID')).subscribe(response => {


      
    if(response!=undefined)
    {
      this.filterForm.get("OrganizationID").setValue(response.organizationItem.id) ;
      if(response.organizationItem.id!=0)
      {
        this.GetClinicdata(response.organizationItem.id);
        this.filterForm.get("ClinicID").setValue(response.clinicItem.id) ;
        if(response.clinicItem.id!=0)
        this.GetProviderdata(response.clinicItem.id);        
        this.filterForm.get("ProviderID").setValue(response.providerItem.id) ;
      }   
      //payer is independent entity;
      this.filterForm.get("PayerID").setValue(response.payerItem.id) ;
      
    }
    

  

}
  );
}

onFilterFormSubmit(filterData){
    
  this.FilteringData=null;
    
  this.FilteringData=filterData;
 // this.FilteringData.MeasureId=filterData.MeasureId this.SelectedMeasureID;

 if((this.FilteringData.OrganizationID)==null ||(this.FilteringData.OrganizationID ==undefined))
 {
  this.Toaster.warning("Please enter a specific Organization !")
 }
 else{
  let startRange:number=0;
  let endRange:number=0;

  // localStorage.setItem('OrganizationID', this.FilteringData.OrganizationID);
  if(filterData.AgeRange!="" && filterData.AgeRange!=undefined && filterData.AgeRange!=null && isNaN(filterData.AgeRange)==false){
  //  =filterData.AgeRange.substring(1, 2);
  startRange=Number(filterData.AgeRange.substring(0, 2));
  endRange=Number(filterData.AgeRange.substring(2, filterData.AgeRange.length));


  if(startRange>endRange){
    this.Toaster.warning("Please enter a valid age range !")
  }else{
    filterData.AgeRange=startRange+'-'+endRange;
   debugger
    this.messageEvent.emit(this.FilteringData)
  }
 
  }else{
    this.FilteringData.AgeRange='1-200';
  debugger;
    this.messageEvent.emit(this.FilteringData)
    
  }
  this.sidebarCollapse();
}

}

GetClinicdata(OrganizationId: number)
  {
    this.ProviderList=[];
    this.ClinicList=[];
    this.filterForm.patchValue({
      ClinicID:null,
      ProviderID:null
    });
    this.patientService.GetClinicsByOrganization(OrganizationId).subscribe(
      (response)=>{
          
        // this.ClinicList=response.result.organization;
        this.ClinicList=response.result.raceList;

        this.ProviderList=response.result.ethinicityList;
    });

  }

  GetProviderdata(ClinicId: number)
  {   
    this.ProviderList=[]; 
    this.filterForm.patchValue({
      ProviderID:null
    });
    this.patientService.GetProviderdatabyClinicId(ClinicId).subscribe(
      (response)=>{
          
        this.ProviderList=response.result.organization;
    });

  }


 
}
