import { Component, OnInit } from '@angular/core';
import {user} from '../../model/user/user';
import{environment} from '../../../environments/environment';
import {UserService} from '../../service/user/user.service';
import {Router} from '@angular/router';
// import {MatMenuModule} from '@angular/material/menu';
// import {MatIconModule} from '@angular/material/icon'


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  title: string = "test";
  mainMenus=[];
  private userModel:user;
  //className="fa fa-sitemap";
  Toggleclass:string;
  constructor(private userService: UserService,private router:Router) { 
    this.userModel=new user();
  }

  ngOnInit() {
    this.getUserMenus();
    
  }
  getUserMenus(){
   this.userModel.userId=parseInt(localStorage.getItem('LoginUserID'));
    this.userService.getUserMenus(this.userModel).subscribe(response=>{     
      debugger;
      
      this.mainMenus=response; 
      console.log("--------------Menu JASON------------------");
      console.log(this.mainMenus)
    });

  }

  Toggle()
  {
    debugger;
    this.Toggleclass = document.getElementById("submenu").getAttribute("class");
    if(this.Toggleclass=="show-sb-dropdown drpdwn-cstm")
    {
      document.getElementById("submenu").setAttribute("class","drpdwn-cstm");
    }
    else
    {
      document.getElementById("submenu").setAttribute("class","show-sb-dropdown drpdwn-cstm");
    }

  }

  
}
