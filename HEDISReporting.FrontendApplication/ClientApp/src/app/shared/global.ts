import {environment} from '../../environments/environment';
export class GLOBAL {
    public static Login =environment.baseUrl+"User/Login";
    public static getOrganizationList=environment.baseUrl+"Organization/GetOrganizationList";
    public static getProviderList=environment.baseUrl+"Provider/GetProviderList";
    public static UserMenus=environment.baseUrl+"User/GetUserMenus";
    public static getOrgDropDown=environment.baseUrl+"Organization/getAllDropDown";
    public static OrganizationConfigration=environment.baseUrl+"Organization/SaveOrganizationConfiguration";
    public static OrganizationSave=environment.baseUrl+"Organization/SaveOrganization";
    public static DepartmentSave=environment.baseUrl+"Organization/SaveOrganizationDepartment";
    public static getDepartmentList=environment.baseUrl+"Organization/GetDepartmentList";

    public static SaveOrgnizationClinic=environment.baseUrl+"Organization/SaveOrgnizationClinic";
    public static GetCountriesStates=environment.baseUrl+"Organization/GetCountriesStates"
    public static GetOrganizationConfigurationList=environment.baseUrl+"Organization/GetOrganizationConfigurationList";
    public static GetOrganizationsClinicsList=environment.baseUrl+"Organization/GetOrganizationsClinicsList";
    public static GetOrganizationRegionList=environment.baseUrl+"Organization/GetOrganizationRegionList";
    public static SaveOrganizationRegion=environment.baseUrl+"Organization/SaveOrganizationRegion";


    public static GetDataSourceDropDowns=environment.baseUrl+"Organization/GetDataSourceDropDowns";
    public static DeleteOrganization=environment.baseUrl+"Organization/DeleteOrganizationData";
    public static GetOrganizationDetailById=environment.baseUrl+"Organization/GetOrganizationDetailById";
    public static GetProviderDetailById=environment.baseUrl+"Provider/GetProviderById";

    public static GetOrganizationDepartmentDetailById=environment.baseUrl+"Organization/GetOrganizationDepartmentDetailById";
    public static GetOrganizationClinicDetailById=environment.baseUrl+"Organization/GetOrganizationClinicDetailById";
    public static SaveOrganizationConfiguration=environment.baseUrl+"Organization/SaveOrganizationConfiguration";
    public static GetOrganizationsDataSourceDetail=environment.baseUrl+"Organization/GetOrganizationsDataSourceDetail";
    public static GetDataSourceFrequencyAndDays=environment.baseUrl+"Organization/GetDataSourceFrequencyAndDays";
    //Patient API urls
    public static GetAllPatientList=environment.baseUrl+"Patient/GetAllPatientList";
    public static UpdateIsConsentTag=environment.baseUrl+"Patient/UpdateIsConsentTag";
    //Patient CCDA
    public static UploadCcdaFile = environment.baseUrl+"Patient/ImportPatientCCDA";
    public static GetEthnicity=environment.baseUrl+"Patient/GetEthnicity";
    public static GetRace=environment.baseUrl+"Patient/GetRace";
    public static GetOrganization=environment.baseUrl+"Patient/GetOrganization";
    public static GetClinics=environment.baseUrl+"Patient/GetClinics";
    public static GetProviders=environment.baseUrl+"Patient/GetProviders";
    public static GetAllPayers=environment.baseUrl+"Patient/GetPayers";

    
    public static GetGender=environment.baseUrl+"Patient/GetGender";
    public static GetFilteredPatientList=environment.baseUrl+"Patient/GetFilteredPatientList";
    public static GetPatientChart=environment.baseUrl+"Patient/GetPatientChart";
    public static GetPatientChartTabs=environment.baseUrl+"Patient/GetPatientChartTabs";   
    public static GetCountryCodeFromIP=environment.baseUrl+"Organization/GetCountryCodeByIP";
    public static GetOrganizationFolderDetail=environment.baseUrl+"Patient/GetOrganizationFolderDetail"
    //View Patient CDA for Parsed and exception Files
    public static GetPatientCda=environment.baseUrl+"Patient/GetPatientCda";
    //Sync Patient CDA Files 
    public static SyncSFTPClient=environment.baseUrl+"Patient/SyncSFTPClient"
    public static GetAllABADetails=environment.baseUrl+"Dashboard/GetAllABADetails";
    public static GetAllMeasurePatientList=environment.baseUrl+"Dashboard/GetAllMeasurePatientList";
    public static getAllDropdowns=environment.baseUrl+"Dashboard/GetAllDropDowm";
    public static GetMeasureAnalysis=environment.baseUrl+"Dashboard/GetMeasureAnalysis";
    public static GetDefaultGoalsDetails=environment.baseUrl+"Dashboard/GetDefaultGoalsDetails";
    public static SaveDashboardGoalDetails=environment.baseUrl+"Dashboard/SaveDashboardGoalDetails";
    public static GetCOLAllDetails=environment.baseUrl+"Dashboard/GetCOLAllDetails";
    public static GetCOLPatientsList=environment.baseUrl+"Dashboard/GetCOLPatientsList";
    public static GetBCSAllDetails=environment.baseUrl+"Dashboard/GetBCSAllDetails";
    public static GetBCSPatientsList=environment.baseUrl+"Dashboard/GetBCSPatientsList";
    public static GetCCSAllDetails=environment.baseUrl+"Dashboard/GetCCSAllDetails";
    public static GetCCSPatientList=environment.baseUrl+"Dashboard/GetCCSPatientsList";
    public static GetCBPAllDetails=environment.baseUrl+"Dashboard/GetCBPAllDetails";
    public static GetCBPPatientList=environment.baseUrl+"Dashboard/GetCBPPatientsList";
    public static GetAllMeasureTypeMaster=environment.baseUrl+"Dashboard/GetAllMeasureTypeMaster";
    public static GetABAWellCareDetails=environment.baseUrl+"Dashboard/GetABAWellCareDetails";
    //public static GetCHBPWellCareDetails=environment.baseUrl+"Dashboard/GetCBPWellCareDetails";
    public static GetMeasureWellCarePatientList=environment.baseUrl+"Dashboard/GetMeasureWellCarePatientList";
    public static GetMeasureAnalysisWellCare=environment.baseUrl+"Dashboard/GetMeasureAnalysisWellCare";
    public static GetCOLWellCareDetails=environment.baseUrl+"Dashboard/GetCOLWellCareDetails";
    public static GetColWellCarePatientList=environment.baseUrl+"Dashboard/GetColWellCarePatientList";
    //public static GetcbpWellCarePatientList=environment.baseUrl+"Dashboard/GetCBPWellCarePatientList";

    public static GetCCSWellCarePatientList=environment.baseUrl+"Dashboard/GetCCSWellCarePatientList";
    public static GetBCSWellCarePatientList=environment.baseUrl+"Dashboard/GetBCSWellCarePatientList";
    
    public static GetMeasureList=environment.baseUrl+"Dashboard/GetMeasures";
   
    public static GetMeasureName=environment.baseUrl+"Dashboard/GetMeasuresname";
    public static GetMeasureDetails=environment.baseUrl+"Dashboard/GetMeasureDetails";
    public static GetBCSWellCareDetails= environment.baseUrl+"Dashboard/GetBCSWellCareDetails";
    //These 2 URL used for getting BCS Welcare PatientList and Welcaredetail
    public static GetCBPWellCareDetails= environment.baseUrl+"Dashboard/GetCBPWellCareDetails";
    public static GetCBPWellCarePatientsList= environment.baseUrl+"Dashboard/GetCBPWellCarePatientList";
    //END
    public static GetGoalsDetails=environment.baseUrl+"Dashboard/GetMeasureGoals";
    public static SubmitGoalsDetails=environment.baseUrl+"Dashboard/SubmitGoalsDetails";
    public static GetClinicPayerForOrganizaton=environment.baseUrl+"Dashboard/GetClinicPayerForOrganizaton";
    public static SubmitMasterGoalsDetails=environment.baseUrl+"Dashboard/SubmitMasterGoalsDetails";
    public static MasterSelectedMeasure=environment.baseUrl+"Dashboard/MasterSelectedMeasure";
    public static MasterMeasureGoals=environment.baseUrl+"Dashboard/MasterMeasureGoals";
    public static GetEntityGoal=environment.baseUrl+"Dashboard/GetEntityGoal";
    public static GetEntityMeasure=environment.baseUrl+"Dashboard/GetEntityMeasure";
    public static GetAllEntity=environment.baseUrl+"Organization/GetAllEntity";
    public static CopyMersure=environment.baseUrl+"Dashboard/CopyMersure";
    public static DeleteMeasure=environment.baseUrl+"Dashboard/DeleteMeasure";
    public static DeleteAllMeasure=environment.baseUrl+"Dashboard/DeleteAllMeasure";
    public static GetAllClaimDetail=environment.baseUrl+"Dashboard/GetAllPatientClaimDetails";
    public static GetCCSWellCareDetails=environment.baseUrl+"Dashboard/GetCCSWellCareDetails";
    // For Care Gap
    public static GetABACareGap2018=environment.baseUrl+"CareGap/GetABACareGap2018";
    public static GetImunization2018=environment.baseUrl+"CareGap/GetImunizationData";
    // End for care Gap
    public static SetMeasuredetails=environment.baseUrl+"Dashboard/SetMeasureDetails";
    //--Get measure Description only
    public static GetMeasureDescription=environment.baseUrl+"Dashboard/GetMeasureDescription";
    //public static GetAllClaimDetail=environment.baseUrl+"Dashboard/GetAllPatientClaimDetails";
    public static GetPatientDetailsLab=environment.baseUrl+"Patient/GetPatientDetailsLab";
    public static GetLabDetailsPatient=environment.baseUrl+"Patient/GetLabDetailsPatient";
    // For Attribute
    public static UploadExcelFile=environment.baseUrl+"AttributionWorkflow/ImportExcelData";
    public static getOrganizationPayer=environment.baseUrl+"AttributionWorkflow/GetAllOrganizationPayer";
    public static NeedAttentionAtrribution=environment.baseUrl+"AttributionWorkflow/NeedAttentionata";
    public static UpdateNeedAttentionAtrribution=environment.baseUrl+"AttributionWorkflow/UpdateNeedAttentionata";
    public static UpdateNeedAttentionAtrributiondata=environment.baseUrl+"AttributionWorkflow/UpdateNeedattentiondata";
    public static UpdateCareGapAtrribution=environment.baseUrl+"AttributionWorkflow/UpdateCareGapdata";
    // End for attribute
    ///pdf //
    public static DownloadPdf=environment.baseUrl+"Patient/OnPost";
    
    public static CaregapPatientList=environment.baseUrl+"CareGap/GetCareGapPatientList";
    
    public static GetAttributionPatientDetails=environment.baseUrl+"AttributionWorkflow/PatientAttributionStatus";

    public static DownloadPdfLab=environment.baseUrl+"Patient/LabData";
    /**
     * For Getting Status and Reason Code
     */
    public static GetStatusReasonCode=environment.baseUrl+"Patient/GetStatusReasonCode";
}
