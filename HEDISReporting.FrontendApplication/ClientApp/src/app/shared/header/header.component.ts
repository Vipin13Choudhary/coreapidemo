import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  Count:number=0;
  loginUserName:string;
  constructor(private router:Router) { }

  ngOnInit() {
    this.loginUserName=localStorage.getItem("LoginUserName");
    document.getElementById('side_body').classList.add('side_full_body');
  }

  logOut(){
   
    localStorage.clear();
    this.router.navigate(['/login'])
  }
  HideAndShowSideBar(){
    debugger;
    this.Count=this.Count+1;
    if(this.Count%2==0){
    document.getElementById('side_body').classList.add('side_full_body');
    }
    else{
    document.getElementById('side_body').classList.remove('side_full_body');
    }
    
    }

}
