import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NgZone } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import {LoginModule} from './login/login.module';
import {HttpModule } from '@angular/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
 import { ToastrModule } from 'ngx-toastr';
 import {AuthGuard} from './shared/auth-gaurd';
 import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
 import { HttpConfigInterceptor} from './interceptor/httpconfig.interceptor';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { LoaderComponent } from './loader/loader.component';
import { MalihuScrollbarModule } from 'ngx-malihu-scrollbar';







// import { UserService } from './service/user/user.service';



@NgModule({
  declarations: [
    AppComponent,
    LoaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpModule,
    LoginModule,
    BrowserAnimationsModule,
   
    ToastrModule.forRoot(),
    MDBBootstrapModule.forRoot(),
    MalihuScrollbarModule.forRoot()
  ],
  providers: [AuthGuard,{ provide: HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi: true }],
  
  bootstrap: [AppComponent]
})
export class AppModule { }
