import {baseEntity} from "./baseEntity";

export class Organization extends baseEntity{
    OrganizationId : number;
    OrganizationName: string;
    OrganizationAddress1: string;
    OrganizationPhoneNumber: string;
    OrganizationFax: string;
    OrganizationWebSite: string;
    OrganizationType: string;
    OrganizationLogo: string;
    OrganizationEmail: string;
    CountryId: number;
    StateId: number;
    City: string;
    PostalCode: string;
    UserId:number;
    ContactPersonName:string;
    ContactPersonEmail:string;
    ContactPersonPhonenumber:string;
    Address2:string;
    OrganizationDisplayName:string;
    OrganizationTaxID:string;
    
}

export interface Element {
  
    OrganizationName: string;
    OrganizationAddress: string;
    OrganizationPhoneNumber?: string;
    OrganizationFax: string;
  
}

