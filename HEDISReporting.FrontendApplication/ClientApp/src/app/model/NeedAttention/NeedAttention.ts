import { listOperation } from '../common/common'
export class NeedAttentionFilter {
    patientId: number;
    sort: string;
    order: string;
    limit: number;
    offset: number
}