export class listOperation{
    offset:number;
    limit:number;
    order:string;
    sort:string;
    Search:string;
    OrganizationId?:number
}

export class statesDropDown{
    Label:number;
    Value: string;
}
export class organizationDropDown{
    id:number;
    name: string;
}
export class clinicDropDown{
    id:number;
    name: string;
}
export class providerDropDown{
    id:number;
    name: string;
}
export class EthnicityDropDown
{
    ethnicity:Array<organizationDropDown>;
}
export class RaceDropDown
{
    Race:Array<organizationDropDown>;
}
export class OrganizationListDropDown
{
    Organization:Array<organizationDropDown>;
}
export class GenderDropDown
{
    Gender:Array<organizationDropDown>;
}
export class CountriesStates
{
    countries:Array<organizationDropDown>;
    states:Array<organizationDropDown>;
    region:Array<organizationDropDown>;
}

export class multiselectDropDown{
    item_id:number;
    item_text: string;
}
export enum Direction {
    CCDA = "CCD-A",
    OtherCCD = "Other CCD",
    PayerRoster = "Payer Roster",
    LabResults = "Lab Results",
    FHIR="FHIR",
    DirectQuery="Direct Query"
}
    export enum Frequencies
    {
        Daily=1,
        Weekly,
        Monthly
    }
    export enum FrequencyDays
    {
        Monday=1,
        Tuesday,
        Wednesday,
        Thursday,
        Friday,
        Saturday,
        Sunday
    }
    export enum AcquisitionMethods
    {
        SFTP=1,
        NETWORK,
        FHIR,
        HL7ORU ,
        DirectQuery,

    }
    export class AdminOrganizationTabs
    {
        Organization:boolean;
        Clinic:boolean;
        Department:boolean;
        DataSource:boolean;

    }
    export enum TypeSave {
       AutoSave,
       ManualSave
    }
    export class commonDropDown{
        id:number;
        name: string;
    }
    export class associationGoal{
        goal:number;
    }