export class patientSubClaim{
    PatientId:number;
    ServiceName:string;
    ClinicName:string;
    diagnosisDate:Date;
    Unit:string;
    Charges:string;
    modifier:string;
    serviceCode:string;
    contactNo:string;
}