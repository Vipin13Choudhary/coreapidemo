import { patientSubClaim } from "./patientSubClaim";

export class PatientClaim
{
    PatientId:number;
    payerName:string;
    Createddate:Date;
    diagnosisDate:Date;
    patientName:string;
    patientAddress:string;
    patientClaimChildrenDetails:patientSubClaim[];

}