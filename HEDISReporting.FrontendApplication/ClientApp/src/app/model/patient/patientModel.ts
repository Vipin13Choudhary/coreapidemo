//import { PatientClaim } from "./PatientClaim";

export class PatientModel
{
    patientDemographics: PatientDemographics;
    snomedCodeModel:SnomedCodeModel[];
    patientAllergies:PatientAllergies[];
    patientLabTest:PatientLabTest[];
    patientclaimData:PatientClaim[];
}
export class PatientDemographics
    {
        name:string;
        age:string;
        country:string;
        phone:string;
        gender:string;
        ssn:string;
        dob:string;
        activeDiagnosis:string;
        ppName:string;
        ppCountry:string;
        ppPhone:string;
        cpName:string;
        cpCountry:string;
        cpPhone:string;
        email:string;
        lastvisit:string;
        provider:string;
        organizationName:string;

    }
export  class SnomedCodeModel{
    patientID:string;
    snomedCode:string;
    snomedCodeDesc:string;
    snomedIcdMap:SnomedIcdMap[];
    isVisible:boolean;
    patientDiagnosisId:number;
}
export class SnomedIcdMap
{
    snomedCode:string;
    mapTarget:string;
    icdName:string;
}
export class PatientAllergies
{
    allergen:string;
    allergyType:string;
}
export class PatientLabTest
{
    test:string;
    finding:string;
    result:string;
    testSchedule:string;
}
export enum PatientNavigationScreens
{
    MeasureAnalysis,
    DetailMeasureAnalysis,
    MainPatientList,
}
export class PatientClaimFilters
{
    OrganizationId:number;
    PatientId:number;
    FromDate:Date;
    ToDate:Date;
    offset:number;
    limit:number;
    order:string;
    sort:string;
}

export class PatientClaim
{
    PatientId:number;
    payerName:string;
    Createddate:Date;
    diagnosisDate:Date;
    patientName:string;
    patientAddress:string;
    patientClaimChildrenDetails:patientSubClaim[];

}

export class patientSubClaim{
    PatientId:number;
    ServiceName:string;
    ClinicName:string;
    diagnosisDate:Date;
    Unit:string;
    Charges:string;
    modifier:string;
    serviceCode:string;
    contactNo:string;
}