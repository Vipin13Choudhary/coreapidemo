export class PatientClaimFilters
{
    OrganizationId:number;
    PatientId:number;
    FromDate:Date;
    ToDate:Date;
    offset:number;
    limit:number;
    order:string;
    sort:string;
}