import { listOperation } from '../common/common'

export class CommonFilters extends listOperation {

    MeasureId: number;
    VendorId: number;
    ProviderId: number;
    AgeRange: string;
    PeriodStart: string;
    PeriodEnd: string;
    Gendor: number;
    EthnicityId: number;
    RaceId: number;
    Flag: number;
    UserId: number;
    OrganizationId: number;
    ClinicId:number;
    PayerId:number;
    NewCal: number;
    IsFilter: number;
    Status: number;
    Type: number;
    patientId:number;

}

export class PatientDetails {
    patientId: number;
    patientName: string;
    nextVisit: string;
    lastVisit: string;
    raceId: number
    status: number
    organizationId: number
    gender: number;
    ethnicityId: number;
    age: string;
    dob: string
}

export class GoalDetails {
    ABAassociationgoal: number;
    ABAclinicgoal: number;
    ABAcorporategoal: number;
    ABApayergoal: number;
    ABAprovidergoal: number;
    CCassociationgoal: number;
    CCclinicgoal: number;
    CCcorporategoal: number;
    CCpayergoal: number;
    CCprovidergoal: number;
    BCSassociationgoal: number;
    BCSclinicgoal: number;
    BCScorporategoal: number;
    BCSpayergoal: number;
    BCSprovidergoal: number;
    CCSassociationgoal: number;
    CCSclinicgoal: number;
    CCScorporategoal: number;
    CCSpayergoal: number;
    CCSprovidergoal: number;
    CHBPassociationgoal: number;
    CHBPclinicgoal: number;
    CHBPcorporategoal: number;
    CHBPpayergoal: number;
    CHBPprovidergoal: number;
    OrganizationId:number;
    IsEdit:boolean;
}