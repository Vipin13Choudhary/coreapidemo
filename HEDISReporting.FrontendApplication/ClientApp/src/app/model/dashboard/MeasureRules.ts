import { List } from "underscore";
import { observable } from "rxjs";

// public class MasterRule
//     {
//         public string Name 
//         public string Abbreviation 
//         public int MeasureRuleId 
//         public int MeasureId 
//         public int EntityId 
//         public int MeasureYear 
//         public DateTime TrackingEnds 
//         public string Description 
//         public string ReferenceCodes 
//         public string DenominatorRules 
//         public string DenominatorSQL 
//         public string ExclusionRules 
//         public string NumeratorRules 
//         public string NumeratorSQL 
//     } 
export class MasterRule
{
    name?:string;
    abbreviation?:string ;
    measureRuleId?:number;
    measureId?:number;
    entityId?:number;
    measureYear?:number;
    trackingEnds?:Date;
    description?:string;
    referenceCodes?:string;
    denominatorRules?:string;
    denominatorSQL?:string;
    exclusionRules?:string;
    numeratorRules?:string;
    numeratorSQL?:string;
    CPTCodeHeader?: string;
    CPTCodeBody?: string;
    ICDCodeHeader?: string;
    ICDCodeBody?: string;
    HCPCSHeader?: string;
    HCPCSBody?: string;
    MeasureName?: string;
    MeasureType?:number;

}
export class MasterGoal
{
    // ABAgoal:number;
    // CCgoal:number;
    // BCSgoal:number;
    // CCSgoal:number;
    // CHBPgoal:number;
    EntityId:number;
    EntityTypeId:number;
    Goal:number;
    MeasureId:number;
    Id:number;
    UserId:number;
}
export class AddCopyMeasure
{
    EntityId:number;
    EntityTypeId:number;
}
export class OrganizationGoalModal {
    OrganizationId: number;
    OrganizationName: string;
    OrganizationType: number
    ClinicGoalModal: Array<ClinicGoalModal>;
}
export class ClinicGoalModal {
    ClinicModal: ClinicModal;
    ProviderModal: Array<ProviderModal>;
}
export class ClinicModal {
    ClinicId: number;
    ClinicName: string
    Type: string;
}
export class ProviderModal {
    ProviderId: number
    ProviderName: string;
    OrganisationId: number;
    ClinicId: number
}
