export class department
{
    DepartmentId:number;
    DepartmentName:string;
    DepartmentAddress:string;
    DepartmentPhoneNumber:string;
    DepartmentFax:string;
    CountryId:number;
    StateId:number;
    City:string;
    PostalCode:string;
    ClinicId:number;
    DepartmentEmail:string;
    ContactPersonEmail:string;
    ContactPersonName:string;
    ContactPersonPhonenumber:string;
    DepartmentAddress2:string;
    UserId:number;

}
