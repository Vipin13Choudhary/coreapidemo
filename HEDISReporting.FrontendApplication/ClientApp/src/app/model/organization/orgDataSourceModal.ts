import { Time } from "@angular/common";





export class orgDataSourceModal{
    DataSourceId:Number;
    DataType : Number;
    SourceOfData:Number;
    AcquisitionMethod:Number;
    GetDataSource:GetDataSource;
    GetDataSourceSftp:GetDataSourceSftp;
    ReceiveDataSource:ReceiveDataSource;
    CreatedBy:Number;
    UpdatedBy:Number;
    IsDeleted:Boolean=false;
    OrganizationId:Number;
    IsEdit:boolean;
}
export class GetDataSource
{
    GetDataSourceId :number;
    Frequency :number;
    FrequencyDays:string; 
    FrequencyWeek :string;
    FrequencyTime :string;
    FileType :number;
    DataSourceId:number;
}
export class GetDataSourceSftp
{
    DataSourceSftpId:number;
    Host:string;
    Port:string;
    UserName:string;
    Password:string;
    GetDataSourceId:Number;
}
export class ReceiveDataSource
{
    ReceiveDataSourceId:Number;
    Frequency:Number;
    FrequencyDays:string;
    FrequencyWeek:string;
    FrequencyTime:string;
    FileType :number;
    DataSourceId:number;
    Directory:string;
    DatasourceName:string;
    Username:string;
    Password:string;
}