export class organizationListFilter
{
    ELEMENT_DATA:any;
    dataSource:any;
    length:any;
    pageSize:any;
    pageSizeOptions:number[];
    displayedColumns:any[];
    pageEvent:any;

}


// ELEMENT_DATA: Element[];
//   dataSource: any;
//   length = 5;
//   pageSize = 10;
//   pageSizeOptions: number[] = [1, 2, 3, 5];
//   displayedColumns = ['OrganizationName','GlobalType', 'OrganizationAddress', 'OrganizationPhoneNumber', 'OrganizationFax','Action'];
//   pageEvent: PageEvent