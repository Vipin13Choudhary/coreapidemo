import {baseEntity} from "./baseEntity";
export class Provider extends baseEntity
{
    ProviderId:  number;
    FirstName:string;
    ModdleName:string;
    LastName:string;
    NPI:string;
    DisplayName:string;
    LicenseType:string;
    ProviderType:string;
    Status:string;
    InitialCredentialDate:string;
    CredentialExpirationDate:string;
  LastCredentialDate: string;


}
