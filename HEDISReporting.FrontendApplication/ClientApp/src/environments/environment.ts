// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  //Local
     baseUrl : 'http://localhost:52174/api/',
     clientBaseUrl:'http://localhost:4200/#/'

  //Client Prod
    //baseUrl : 'http://75.126.168.31:7017/api/',
    //clientBaseUrl: "http://75.126.168.31:7016/#/"

    //Client Demo
    //baseUrl: 'http://75.126.168.31:7019/api/',
    //clientBaseUrl:"http://75.126.168.31:7018/#/"

    //Our Staging
    //baseUrl: 'http://75.126.168.31:7042/api/',
    //clientBaseUrl:"http://75.126.168.31:7026/#/"



  //Our Staging
  //baseUrl: 'https://75.126.168.31:7042/api/',
  //clientBaseUrl: "https://stagingwin.com:7026/#"
  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
