﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using HEDISReporting.Common.Enum;
using HEDISReporting.Common.Response;
using HEDISReporting.DataContract.ApiResponse;
using HEDISReporting.DataContract.Authentication;
using HEDISReporting.DataContract.Common;
using HEDISReporting.DataContract.DataSource;
using HEDISReporting.DataContract.Organization;
using HEDISReporting.Service.User;
using HEDISReporting.ServiceContract.Organization;
using HEDISReporting.ServiceContract.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;

namespace HEDISReporting.Web.API.Controllers.Organization
{

    [AllowAnonymous]
    [Route("api/[controller]")]
    [ApiController]
    public class OrganizationController  :BaseController
    {
     
      
       private APIResponse response;
        //private string Commonresponse;
        private readonly IOrganizationService _organizationService;

        public OrganizationController(
                             IOrganizationService organizationService
                            ) : base(organizationService)
        {

            _organizationService = organizationService;

        }

        #region get Organization List
        [HttpPost]
        [Route("GetOrganizationList")]
        public async Task<dynamic> GetOrganizationList([FromBody]OrganizationListingOperands data)
        {
           
            var list= await _organizationService.GetOrganizationList(data);
                return list;

        }

        #endregion


        #region get All Dropdown
        [HttpPost]
        [Route("GetAllDropDown")]
        public async Task<OrganizationDropdown> GetAllDropDown([FromBody] RegionDropDown data)

        {
                var list = await _organizationService.GetAllDropDown(data.OrganizationId, data.UserId);
                return list;
        }

        #endregion
        #region OrganizationConfiguration
        [HttpPost]
        [Route("SaveOrganizationConfiguration")]
        public async Task<IActionResult> SaveOrganizationConfiguration([FromBody]OrganizationDataSource organizationSftp)
        {

            //Directory.GetCurrentDirectory() + "\\wwwroot\\CDA\\" + String.Format("{0}_{1:yyyyMMddhhmmss}", "cda", DateTime.Now) + ".xml";
            DataSourceResponseModel result =  await _organizationService.SaveOrganizationConfiguration(organizationSftp);
            if(result.DataSourceId > 0 &&  !string.IsNullOrEmpty(result.MethodName))
            {
                string baseDirectory = string.Concat( Directory.GetCurrentDirectory(),@"\","wwwroot",@"\");
                string organizationPath = baseDirectory + result.OrganizationName + @"\";
                string dataSoruceTypePath = organizationPath + result.MethodName;
                if(!Directory.Exists(dataSoruceTypePath))//create the directory for organization if does not exist that will be used to store the files fatched from sftp
                {
                    Directory.CreateDirectory(dataSoruceTypePath);
                }
            }
            
            return Ok(new { Status = result.DataSourceId > 0? HttpStatusCode.OK:HttpStatusCode.BadRequest, result = result });
        }
        [HttpPost]
        [Route("GetOrganizationConfigurationList")]
        public async Task<IActionResult> GetOrganizationConfigurationList([FromBody]JObject filterItems)
        {
            OrganizationConfigurationListModel filters = filterItems.ToObject<OrganizationConfigurationListModel>();
            dynamic result= await _organizationService.GetOrganizationConfigurationList(filters);
            return Ok(result);
        }
        #endregion
        #region Organization Clinic Setup
        [HttpPost]
        [Route("SaveOrgnizationClinic")]
        public async Task<IActionResult> SaveOrgnizationClinic([FromBody] OrganizationClinic organizationClinic)
        {
          
                var result = await _organizationService.SaveOrgnizationClinic(organizationClinic);
                return Ok(new { Status = result > 0 ? HttpStatusCode.OK : HttpStatusCode.BadRequest, result = result });
     
        }
        #endregion

        #region save Organization  
        [HttpPost]
        [Route("SaveOrganization")]
        public async Task<IActionResult> SaveOrganization([FromBody] OrganizationModel data)
        {

                var result = await _organizationService.SaveOrganization(data);
                return Ok(new { Status = result > 0 ? HttpStatusCode.OK : HttpStatusCode.BadRequest, result = result });

        }

        #endregion

        #region save Organization Department  
        [HttpPost]
        [Route("SaveOrganizationDepartment")]
        public async Task<IActionResult> SaveOrganizationDepartment([FromBody] OrganizationDepartment data)
        {

                var result = await _organizationService.SaveOrganizationDepartment(data);
                return Ok(new { Status = result > 0 ? HttpStatusCode.OK : HttpStatusCode.BadRequest, result = result });

        }

        #endregion

        #region get Organization Department  List
        [HttpPost]
        [Route("GetDepartmentList")]
        public async Task<dynamic> GetDepartmentList([FromBody]DepartmentListOperands data)
        {

                var list = await _organizationService.GetDepartmentList(data);
                return list;

   
        }

        #endregion
        #region Organizations Clinic List
        [HttpPost]
        [Route("GetOrganizationsClinicsList")]
        public async Task<IActionResult> GetOrganizationsClinicsList([FromBody]JObject filterItems)
        {
            OrganizationConfigurationListModel filters = filterItems.ToObject<OrganizationConfigurationListModel>();
            dynamic result = await _organizationService.GetOrganizationsClinicsList(filters);
            return Ok(result);
        }
        #endregion
        #region
        [HttpGet]
        [Route("GetDataSourceDropDowns")]
        public async Task<IActionResult> GetDataSourceDropDowns(int configurationTypeId)
        {
            dynamic result=null;

               result = await _organizationService.GetDataSourceDropDowns(configurationTypeId);
      
            return Ok(result);
        }
        #endregion

        #region Countries and States
        [HttpGet]
        [Route("GetCountriesStates")]
        public async Task<IActionResult> GetCountriesStates(int countryId)
        {
            //GetCountriesStates
            var response = await _organizationService.GetCountriesStates(countryId);
            return Ok(new { Status = response==null ? HttpStatusCode.BadRequest : HttpStatusCode.OK, result = response });
        }


        #endregion

        #region Organization region list

        [HttpPost]
        [Route("GetOrganizationRegionList")]
        public async Task<dynamic> GetOrganizationRegionList([FromBody]OrgRegionListingOperands data)
        {


                var list = await _organizationService.GetOrganizationRegionList(data);
                return list;

        }

        #endregion

        #region save organization region
        
        [HttpPost]
        [Route("SaveOrganizationRegion")]
        public async Task<dynamic> SaveOrganizationRegion([FromBody] OrganizationRegion data)
        {

            var response = await _organizationService.SaveOrganizationRegion(data);
            return Ok(new { Status = (response <=0) ? HttpStatusCode.BadRequest : HttpStatusCode.OK, result = response });

        }
        [HttpGet]
        [Route("GetDataSourceFrequencyAndDays")]
        public dynamic GetDataSourceFrequencyAndDays()
        {

           List<DropDownListItem> frequencies= Enum.GetNames(typeof(Frquencies)).Select(x => new DropDownListItem
           {
                Id =Convert.ToInt32(Enum.Parse(typeof(Frquencies), x)),
                Name= x
            }).ToList();

            List<DropDownListItem> frequencyDays = Enum.GetNames(typeof(FrequencyDays)).Select(x => new DropDownListItem
            {
                Id = Convert.ToInt32(Enum.Parse(typeof(FrequencyDays), x)),
                Name = x
            }).ToList();
            List<DropDownListItem> weeks = Enum.GetNames(typeof(WeekNumber)).Select(x => new DropDownListItem
            {
                Id = Convert.ToInt32(Enum.Parse(typeof(WeekNumber), x)),
                Name = x
            }).ToList();

            return Ok(new { frequencies, frequencyDays, weeks });
        }
        #endregion

        #region delete organization 

        [HttpPost]
        [Route("DeleteOrganizationData")]
        public async Task<dynamic> DeleteOrganizationData([FromBody] DeleteData data)
        {

            var response = await _organizationService.DeleteOrganizationData(data);
            return Ok(new { Status = response == null ? HttpStatusCode.BadRequest : HttpStatusCode.OK, result = response });

        }

        #endregion

        #region

        [HttpGet]
        [Route("GetOrganizationDetailById")]
        public async Task<dynamic> GetOrganizationDetailById(int organizationId)
        {

                var response = await _organizationService.GetOrganizationDetailById(organizationId);
                return Ok(new { Status = response == null ? HttpStatusCode.BadRequest : HttpStatusCode.OK, result = response });
      

        }

        #endregion

        #region get clinic detail


        [HttpGet]
        [Route("GetOrganizationClinicDetailById")]
        public async Task<dynamic> GetOrganizationClinicDetailById(int clinicId)
        {

                var response = await _organizationService.GetOrganizationClinicDetailById(clinicId);
                return Ok(new { Status = response == null ? HttpStatusCode.BadRequest : HttpStatusCode.OK, result = response });

        }

        #endregion


        #region get clinic detail


        [HttpGet]
        [Route("GetOrganizationDepartmentDetailById")]
        public async Task<dynamic> GetOrganizationDepartmentDetailById(int departmentId)
        {

                var response = await _organizationService.GetOrganizationDepartmentDetailById(departmentId);
                return Ok(new { Status = response == null ? HttpStatusCode.BadRequest : HttpStatusCode.OK, result = response });


        }

        #endregion

        #region
        [HttpGet]
        [Route("GetOrganizationsDataSourceDetail")]
        public async Task<dynamic> GetOrganizationsDataSourceDetail(int organizationId,int DataSourceId)
        {
                var response = await _organizationService.GetOrganizationsDataSourceDetail(organizationId, DataSourceId);
                return Ok(new { Status = response == null ? HttpStatusCode.BadRequest : HttpStatusCode.OK, result = response });
        }

        #endregion

        #region Get Entity data
        [HttpGet]
        [Route("GetAllEntity")]
        public async Task<OrganizationDropdown> GetAllEntity(int TypeId,int OrganizationId)
        {
            var list = await _organizationService.GetAllEntity(TypeId, OrganizationId);
            return list;
        }
        #endregion

        #region
        [HttpGet]
        [Route("GetCountryCodeByIP")]
        public async Task<dynamic> GetCountryCodeByIP(string IPAddress)
        {
            var response = await _organizationService.GetCountryTypeByIP(string.IsNullOrEmpty(IPAddress)?string.Empty: IPAddress);
            return Ok(new { Status = response == null ? HttpStatusCode.BadRequest : HttpStatusCode.OK, result = response });
        }

        #endregion

        #region get Organization List for Windows Service
        [HttpGet]
        [Route("GetOrganizationList_WinService")]
        public async Task<dynamic> GetOrganizationList_WinService()
        {

            var list = await _organizationService.GetOrganizationList_WinService();
            return list;

        }

        #endregion

    }
}