﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HEDISReporting.DataContract.Dashboard;
using HEDISReporting.ServiceContract.CareGap;
using HEDISReporting.ServiceContract.Patient;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;


namespace HEDISReporting.Web.API.Controllers.CareGap
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class CareGapController : BaseController
    {
        private readonly ICareGapService _careGapService;
        #region Construtor of the class
        public CareGapController(ICareGapService careGapService) : base(careGapService)
        {
            _careGapService = careGapService;
        }
        #endregion

        #region get all care gap patients for ABA 2018 
        [HttpPost]
        [Route("GetABACareGap2018")]
        public async Task<dynamic> GetABACareGap2018(DashBoardFilters data)
        {

            var list = await _careGapService.GetABACareGap2018(data);
            return list;

        }
        #endregion

        #region get all care gap patients
        [HttpPost]
        [Route("GetCareGapPatientList")]
        public async Task<dynamic> GetCareGapPatientList(DashBoardFilters data)
        {

            var list = await _careGapService.GetCareGapPatientList(data);
            return list;

        }
        #endregion

        #region get Imunization data for Particular Patient
        [HttpPost]
        [Route("GetImunizationData")]
        public async Task<dynamic> GetImunizationData(DashBoardFilters data)
        {
            var list = await _careGapService.GetImunizationPatientList(data);
            return list;

        }
        #endregion
    }
}