﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HEDISReporting.ServiceContract;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HEDISReporting.Web.API.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class BaseController : ControllerBase
    {
        IBaseService _baseService;

        public IBaseService BaseService
        {
            get { return _baseService; }
        }

        public BaseController(IBaseService baseService)
        {
            _baseService = baseService;
        }

     

    }
}