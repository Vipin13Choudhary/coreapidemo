﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HEDISReporting.DataContract.Provider;
using HEDISReporting.ServiceContract.ProviderS;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HEDISReporting.Web.API.Controllers.Provider
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProviderController : BaseController
    {
        private readonly IProviderService _providerService;

        public ProviderController(
                             IProviderService providerService
                            ) : base(providerService)
        {

            _providerService = providerService;

        }
        #region get Organization List
        [HttpPost]
        [Route("GetProviderList")]
        public async Task<dynamic> GetProviderList([FromBody]ProviderListingOperands data)
        {

            var list = await _providerService.GetProviderList(data);
            return list;

        }

        #endregion

        #region get Organization List
        [HttpGet]
        [Route("GetProviderById")]
        public async Task<dynamic> GetProviderById(int Id)
        {

            var list = await _providerService.GetProviderById(Id);
            return list;

        }

        #endregion
    }
}