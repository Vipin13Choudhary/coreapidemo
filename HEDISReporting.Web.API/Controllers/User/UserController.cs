﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HEDISReporting.DataContract.Authentication;
using HEDISReporting.ServiceContract.User;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using HEDISReporting.DataContract.ApiResponse;
using HEDISReporting.Common.Response;
using HEDISReporting.Common.EncrytDecrypt;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Cors;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;

namespace HEDISReporting.Web.API.Controllers.User
{


    [Route("api/[controller]")]
    [ApiController]
    public class UserController :BaseController
    {
        private IUserService _userService;
        private readonly UserManager<ApplicationUser> _userManager;
        private APIResponse response;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IConfiguration _configuration;
     
        public UserController(IUserService userService, UserManager<ApplicationUser> userManager,
                              SignInManager<ApplicationUser> signInManager, IConfiguration configuration)
            : base(userService)
        {
            _userService = userService;
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = configuration;
           

        }
        /// <summary>
        /// This API is used to do a healthcheck on the User Controller.
        /// </summary>
        /// <returns>HealthCheck Information</returns>
        // GET: api/User/HealthCheck/
        
        [HttpGet]
        [Route("HealthCheckAsync")]
        public async Task<IActionResult> HealthCheckAsync()
        {
            var result = await _userService.HealthCheckAsync();

            var response = Ok(result);

            return response;
        }
        /// <summary>
        /// This API is used to get the User data based on userId.
        /// </summary>
        /// <param name="chaseId"></param>
        /// <returns>User Details</returns>
        // GET: api/User?userId=1234

        [HttpGet]
        [Route("User")]
        public async Task<IActionResult> GetUserAsync(int userId)
        {
            IActionResult response = null;

            var user = await _userService.GetUserAsync(userId, 0);
            response = Ok(user);

            return response;
        }


        [HttpPost]
        [Route("Login")]
        public async Task<object> Login([FromBody]Login model)
        {
      
                response = new APIResponse();

                if (!ModelState.IsValid)
                {
                    response.Message = CommonResponse.Res_InvalidCredential;
                    
                    return response;
                }
                ApplicationUser user = await _userManager.FindByNameAsync(model.UserName);
                if (user == null)
                {
                    response.Message = CommonResponse.Res_InvalidCredential;
                    return response;
                }
                 //var decryptedPassword = EncrptDecryptPassword.Encrypt("hedis2018");

                var encryptedPassword = EncrptDecryptPassword.Encrypt(model.Password);

                //var result = await _signInManager.CheckPasswordSignInAsync(user, model.Password, false);
                //  var result= await _userManager.
                if (user.PasswordHash != encryptedPassword)
                {
                    response.Message = CommonResponse.Res_InvalidCredential;
                    return response;
                }
                var claims = new[]
                    {
                        new Claim(ClaimTypes.Name, user.UserName)
                    };
                
                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JwtKey"]));
                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                var expires = DateTime.Now.AddDays(Convert.ToDouble(_configuration["JwtExpireDays"]));
               
                var token = new JwtSecurityToken(
                            issuer: _configuration["JwtIssuer"],
                            audience: _configuration["JwtAudience"],
                            claims: claims,
                            expires: expires,
                            signingCredentials: creds);
                var userData = await _userService.getloginUserId(user.Id.ToString());
            //response.data.Role = Roles.;
            //_userService.LoginUser = new DataContract.Authentication.LoginUser();
            //_userService.LoginUser.UserId = userData.UserId;
            //_userService.LoginUser.UserName = userData.FirstName + " " + userData.LastName;
            response.data.UserId = userData.UserId.ToString();
            var Roles = await _userService.GetUserRoles(userData.UserId);
            response.data.OrganizationId = userData.OrganizationId;
            if(Roles!=null)
            response.data.Role = Roles.Role;
            response.data.UserName = string.Concat(user.LastName," ",user.FirstName);
            response.data.Token = new JwtSecurityTokenHandler().WriteToken(token);
                response.Message = CommonResponse.Res_StatusSuccess;
                
                //response.data.Token = new JwtSecurityTokenHandler().WriteToken(token);

                return new OkObjectResult(response);
            
         
           
        }
        /// <summary>
        /// To get menus specific to users
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetUserMenus")]
        public async Task<IActionResult> GetUserMenus(int userId, string baseUrl)
        {
            var userMenus = await _userService.GetUserMenus(userId,baseUrl);
            
            return Ok(userMenus); 
        }

    }
}