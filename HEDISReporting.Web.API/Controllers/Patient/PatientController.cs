﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using HEDISReporting.DataContract.Common;
using HEDISReporting.ServiceContract.Patient;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using HEDISReporting.DataContract.ApiResponse;
using HEDISReporting.DataContract.DataSource;
using HEDISReporting.SftpSync.Models;
using HEDISReporting.SftpSync;
using HEDISReporting.DataContract.SyncModels;
using HEDISReporting.Common;
using Microsoft.AspNetCore.Authorization;
using HEDISReporting.DataContract.Patients;
using System.Net.Mime;
using Microsoft.Extensions.Configuration;
using HEDISReporting.DataContract.PatientChart;
using HEDISReporting.Common.SnomedApiModels;
using Newtonsoft.Json.Linq;
using HEDISReporting.Common.ExtensionMethods;

namespace HEDISReporting.Web.API.Controllers.Patient
{
    [ApiController]
    // [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("api/[controller]")]

    public class PatientController : BaseController
    {
        private readonly IPatientService _patientService;
        private static HttpClient Client { get; } = new HttpClient();
        IConfiguration _configuration;
        #region Construtor of the class
        public PatientController(IPatientService patientService, IConfiguration configuration) : base(patientService)
        {
            _patientService = patientService;
            _configuration = configuration;
        }
        #endregion

        #region Class Methods
        [HttpPost]
        [DisableRequestSizeLimit]
        [Route("ImportPatientCCDA")]
        public async Task<IActionResult> ImportPatientCCDA()
        {
            //[FromBody] JObject formData
            Response response = null;
            HttpRequest httpRequest = HttpContext.Request;
            IFormFileCollection formFiles = httpRequest.Form.Files;
            int organizationId = Convert.ToInt32(httpRequest.Form["OrganizationId"].FirstOrDefault());
            int userId = Convert.ToInt32(httpRequest.Form["UserId"].FirstOrDefault());
            if (formFiles.Count > 0)
            {
                IFormFile postedFile = formFiles[0];

                var result = string.Empty;
                using (var reader = new StreamReader(postedFile.OpenReadStream()))
                {
                    result = reader.ReadToEnd();
                }

                response = await _patientService.ImportPatientCCDA(result, organizationId, userId, "");

                //List<FileSyncs> SuccessFiles = new List<FileSyncs>();
                //SuccessFiles.Add(new FileSyncs {
                //    FileName= postedFile.FileName,
                //    FilePostedDate=DateTime.Now,
                //    FileSize=postedFile.size
                //});


                //postedFile.FileName
            }

            return Ok(response);
        }


        #endregion

        #region Helping Methods
        /////////////////////////
        //helping methods
        #endregion

        #region get patient List
        [HttpPost]
        [Route("GetAllPatientList")]
        public async Task<dynamic> GetAllPatientList([FromBody]OrganizationListModel data)
        {
            //hard coded values must be removed before deployment.


            var list = await _patientService.GetAllPatientList(data);
            return list;

        }
        #endregion

        #region Ethnicity
        [HttpGet]
        [Route("GetEthnicity")]
        public async Task<IActionResult> GetEthnicity(int EthnicityId)
        {
            //GetCountriesStates
            var response = await _patientService.GetEthnicity(EthnicityId);
            return Ok(new { Status = response == null ? HttpStatusCode.BadRequest : HttpStatusCode.OK, result = response });
        }
        #endregion

        #region Race
        [HttpGet]
        [Route("GetRace")]
        public async Task<IActionResult> GetRace(int RaceId)
        {
            //GetCountriesStates
            var response = await _patientService.GetRace(RaceId);
            return Ok(new { Status = response == null ? HttpStatusCode.BadRequest : HttpStatusCode.OK, result = response });
        }
        #endregion


        #region Gender

        [HttpGet]
        [Route("GetGender")]
        public async Task<IActionResult> GetGender(int GenderId)
        {
            //GetCountriesStates
            //var test = Context.HttpContext.Request;
            var t = Request.Headers["Autheraization"];
            var response = await _patientService.GetGender(GenderId);
            return Ok(new { Status = response == null ? HttpStatusCode.BadRequest : HttpStatusCode.OK, result = response });
        }
        #endregion

        #region Organization
        [HttpGet]
        [Route("GetOrganization")]
        public async Task<IActionResult> GetOrganization(int OrganizationId)
        {
            //GetCountriesStates
            var response = await _patientService.GetOrganization(OrganizationId);
            return Ok(new { Status = response == null ? HttpStatusCode.BadRequest : HttpStatusCode.OK, result = response });
        }
        #endregion


        #region Clinics
        [HttpGet]
        [Route("GetClinics")]
        public async Task<IActionResult> GetClinics(int OrganizationId)
        {
            //GetCountriesStates
            var response = await _patientService.GetClinics(OrganizationId);
            return Ok(new { Status = response == null ? HttpStatusCode.BadRequest : HttpStatusCode.OK, result = response });
        }
        #endregion
        #region Providers
        [HttpGet]
        [Route("GetProviders")]
        public async Task<IActionResult> GetProviders(int ClinicId)
        {
            //GetCountriesStates
            var response = await _patientService.GetProviders(ClinicId);
            return Ok(new { Status = response == null ? HttpStatusCode.BadRequest : HttpStatusCode.OK, result = response });
        }
        #endregion

        #region Payers
        [HttpGet]
        [Route("GetPayers")]
        public async Task<IActionResult> GetPayers()
        {
            //GetCountriesStates
            var response = await _patientService.GetPayers();
            return Ok(new { Status = response == null ? HttpStatusCode.BadRequest : HttpStatusCode.OK, result = response });
        }
        #endregion
        #region Get Filtered Patient List
        [HttpGet]
        [Route("GetFilteredPatientList")]
        public async Task<dynamic> GetFilteredPatientList(int EthnicityId, int OrganizationId, int RaceId, int GenderId)
        {
            //GetCountriesStates
            try
            {

                var list = await _patientService.GetFilteredPatientList(EthnicityId, OrganizationId, RaceId, GenderId);
                return list;

            }
            catch (Exception ex)
            {
                //  response.Message = CommonResponse.Res_StatusFailure;
                return null;
            }
        }
        #endregion
        #region Patient Chart
        [HttpGet]
        [Route("GetPatientChart")]
        public async Task<dynamic> GetPatientChart(int paientId, int tabId)
        {
            dynamic patientDetail = await _patientService.GetPatientChart(paientId, 1, tabId);
            return patientDetail;
        }
        [HttpGet]
        [Route("GetPatientChartTabs")]
        public async Task<dynamic> GetPatientChartTabs(int paientId, int tabId)
        {
            dynamic patientDetail = await _patientService.GetPatientChartTabs(paientId, 2, tabId);

            //List<DiagnosisModel> diagnosisModels = new List<DiagnosisModel>();
            //using (HttpClient client = new HttpClient())
            //{

            //    HttpResponseMessage response =   await client.GetAsync("https://www.snochillies.com/Snomed/ICD10Detial?ConceptID=10725009&Map=All");
            //    if(response.IsSuccessStatusCode)
            //    {
            //        string data = await response.Content.ReadAsStringAsync();
            //        diagnosisModels =ExtensionMethods.DeserializeFromJson<List<DiagnosisModel>>(data);
            //        //_AveaPracticeModel = ExtensionMethod.DeserializeFromJson<List<AveaPracticeRepositoryModel>>(_result);

            //    }


            //}
            return patientDetail;
        }
        #endregion
        #region Get Patient dataSource configuration detail
        [HttpGet]
        [Route("GetOrganizationFolderDetail")]
        public async Task<DataSourceResponseModel> GetOrganizationFolderDetail(int organizationId)
        {
            return await _patientService.GetOrganizationFolderDetail(organizationId);
        }
        #endregion
        #region sync Files ,parse then and save to db
        [HttpGet]
        [Route("SyncSFTPClient")]
        public async Task<IActionResult> SyncSFTPClient(int organizationId, int userId)
        {
            try
            {
                if (organizationId == 0 || userId == 0)
                    return BadRequest();
                bool IsAllDone = false;
                //below details will be replaced with dynamic values as it's going to be connected to sync button
                SftpRequestModel sftpRequestModel = new SftpRequestModel();
                sftpRequestModel.OrganizationId = organizationId;
                sftpRequestModel.UserId = userId;
                sftpRequestModel.SftpFileModels = new List<SftpFileModel>();
                sftpRequestModel.SftpInfoModel = new SftpInfoModel();
                sftpRequestModel.SyncLimit = 100;
                List<FileSyncs> SuccessFiles = new List<FileSyncs>();
                List<FileSyncs> FailedFiles = new List<FileSyncs>();
                //Get the organization datasource detail(get and receive monitor share)
                SftpHedisRequestModel sftpHedisRequestModel = await _patientService.GetDataSourceDetailForSync(sftpRequestModel.OrganizationId);
                //Prepare the model to call sftp sync
                sftpRequestModel.SftpFileModels = sftpHedisRequestModel.fileSyncs.Select(x => new SftpFileModel
                {
                    FileName = x.FileName,
                    FilePostedDate = x.FilePostedDate,
                    FileSize = x.FileSize
                }).ToList();
                if (string.IsNullOrEmpty(sftpHedisRequestModel.getSource.Host) || string.IsNullOrEmpty(sftpHedisRequestModel.getSource.Port) || string.IsNullOrEmpty(sftpHedisRequestModel.getSource.UserName) || string.IsNullOrEmpty(sftpHedisRequestModel.getSource.Password) || string.IsNullOrEmpty(sftpHedisRequestModel.receiveSource.DirectoryPath))
                    return Ok(-1);

                sftpRequestModel.SftpInfoModel.Domain = sftpHedisRequestModel.getSource.Host;
                sftpRequestModel.SftpInfoModel.Port = sftpHedisRequestModel.getSource.Port;
                sftpRequestModel.SftpInfoModel.UserName = sftpHedisRequestModel.getSource.UserName;
                sftpRequestModel.SftpInfoModel.Password = sftpHedisRequestModel.getSource.Password;
                sftpRequestModel.SftpInfoModel.DirectoryPath = sftpHedisRequestModel.receiveSource.DirectoryPath;

                SftpSyncing sftpSyncing = new SftpSyncing(sftpRequestModel.SftpInfoModel, sftpRequestModel.SftpFileModels, sftpRequestModel.SyncLimit);
                List<SftpFileModel> sftpFileModels = await sftpSyncing.GetAllSftpFiles() as List<SftpFileModel>;
                foreach (SftpFileModel loadedFiles in sftpFileModels)
                {
                    FileSyncs files = new FileSyncs
                    {
                        FileName = loadedFiles.FileName,
                        FilePostedDate = loadedFiles.FilePostedDate,
                        FilePostedDateString = loadedFiles.FilePostedDate.ToString("yyyy-MM-dd HH:mm:ss"),
                        FileSize = loadedFiles.FileSize
                    };
                    var response = await _patientService.ImportPatientCCDA(loadedFiles.FileContent, sftpRequestModel.OrganizationId, sftpRequestModel.UserId, files.FileName);
                    if (response != null && response.Data != "0")
                    {
                        SuccessFiles.Add(files);
                        string syncedFilesXml = XmlHelper.GetAsXml(files);
                        IsAllDone = await _patientService.InsertSyncedFiles(syncedFilesXml, sftpRequestModel.OrganizationId, sftpRequestModel.UserId);

                    }
                    else
                    {
                        FailedFiles.Add(files);
                    }

                }
                //if (SuccessFiles.Count > 0)
                //{
                //    string syncedFilesXml = XmlHelper.GetAsXml(SuccessFiles);
                //    IsAllDone = await _patientService.InsertSyncedFiles(syncedFilesXml, sftpRequestModel.OrganizationId, sftpRequestModel.UserId);
                //}

                //save succeeded files in db for history and avoid loading already synced files

                //return Ok(IsAllDone==true ?1 :0);
                return Ok(1);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        [HttpGet]
        [Route("GetPatientCda")]
        public async Task<string> GetPatientCda(int organizationId, string fileName, bool isSuccessFiles, int patientId)
        {
            if (organizationId == 0 || string.IsNullOrEmpty(fileName))
                return null;
            DataSourceResponseModel folderdetail = await GetOrganizationFolderDetail(organizationId);
            if (folderdetail != null && !string.IsNullOrEmpty(folderdetail.OrganizationName) && !string.IsNullOrEmpty(folderdetail.MethodName))
            {
                string baseDirectory = _configuration["Domain:DomainUrl"];
                //string organizationPath = Path.Combine(baseDirectory, folderdetail.OrganizationName);//Comment only for demo
                string organizationPath = Path.Combine(baseDirectory, "WhitehouseClinics");
                string dataSoruceTypePath = Path.Combine(organizationPath, folderdetail.MethodName);
                string successFilePath = Path.Combine(dataSoruceTypePath, "SuccessFiles");
                string exceptionFilePath = Path.Combine(dataSoruceTypePath, "ExceptionalFiles");
                string finalFilePath = isSuccessFiles == true ? Path.Combine(successFilePath, fileName) : Path.Combine(exceptionFilePath, fileName);
                //string locaFilePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", folderdetail.OrganizationName, "SFTP", "SuccessFiles", fileName);//Comment only for demo
                string locaFilePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "WhitehouseClinics", "SFTP", "SuccessFiles", fileName);
                bool IsDemo = Convert.ToBoolean(_configuration.GetSection("AppEnvironment").Value);
                if (IsDemo)
                {
                    bool result = await _patientService.GetDeIdentifiedInfo(patientId, locaFilePath);
                }


                return finalFilePath.Replace(@"\", @"/");

            }


            return null;
        }
        #endregion


        #region To get lab details
        [HttpPost]
        [Route("GetLabDetailsPatient")]
        public async Task<IActionResult> GetLabDetailsPatient([FromBody]LabListingOperands data)
        {
            var response = await _patientService.GetLabFromPatients(data);
            return Ok(new { Status = response == null ? HttpStatusCode.BadRequest : HttpStatusCode.OK, result = response });
        }


        [HttpGet]
        [Route("GetPatientDetailsLab")]
        public async Task<IActionResult> GetPatientDetailsLab(int patientId)
        {
            //GetCountriesStates
            var response = await _patientService.GetPatientsFromLab(patientId);
            return Ok(new { Status = response == null ? HttpStatusCode.BadRequest : HttpStatusCode.OK, result = response });
        }
        #endregion


        #region To get pdf details
        //[HttpPost]
        //[Route("GetLabDetailsPatient")]
        //public async Task<IActionResult> GetLabDetailsPatient([FromBody]LabListingOperands data)
        //{
        //    var response = await _patientService.GetLabFromPatients(data);
        //    return Ok(new { Status = response == null ? HttpStatusCode.BadRequest : HttpStatusCode.OK, result = response });
        //}

        [HttpPost]
        [Route("OnPost")]
        public async Task<string> OnPost(PatientClaimDetails parentClaimDetail)

        {
            //FileResult response = await _patientService.GetPDF(parentClaimDetail);
            var response = await _patientService.GetPDF(parentClaimDetail);
            return response;
            
        }
        #endregion

        #region To Get Patient Status And Reason Code
        [HttpGet]
        [Route("GetStatusReasonCode")]
        public async Task<IActionResult> GetStatusReasonCode()
        {
            var response = await _patientService.GetStatusReasonCode();
            return Ok(new { Status = response == null ? HttpStatusCode.BadRequest : HttpStatusCode.OK, result = response });
        }
        #endregion

    }
}
