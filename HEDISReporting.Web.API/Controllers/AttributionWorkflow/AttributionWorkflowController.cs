﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using HEDISReporting.Common.ExtensionMethods;
using HEDISReporting.DataContract.AttributeExcel;
using HEDISReporting.DataContract.Dashboard;
using HEDISReporting.ServiceContract.AttributionWorkflow;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Clients.ActiveDirectory;

namespace HEDISReporting.Web.API.Controllers.AttributionWorkflow    
{
    [Route("api/[controller]")]
    [ApiController]
    //[EnableCors("CORS")]
    public class AttributionWorkflowController : BaseController
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IAttributionWorkflowService _attributionWorkflowService;
        public AttributionWorkflowController(IHostingEnvironment hostingEnvironment,
            IAttributionWorkflowService attributionWorkflowService) : base(attributionWorkflowService)
        {
            _hostingEnvironment = hostingEnvironment;
            _attributionWorkflowService = attributionWorkflowService;
        }
        #region ImportExcelData
        [HttpPost]
        [Route("ImportExcelData")]
        public dynamic ImportExcelData()
        {
           dynamic list = _attributionWorkflowService.ReadExcelData(Request.Form.Files[0], Request.Form.Files[1].FileName);
            return list;
        }
        #endregion

        #region GetAllOrganizationPayer
        [HttpGet]
        [Route("GetAllOrganizationPayer")]
        public async Task<dynamic> Get()
        {
            dynamic list = await _attributionWorkflowService.OrganizationPayerList();
            return list;
        }
        #endregion

        #region NeedAttentionata
        [HttpPost]
        [Route("NeedAttentionata")]
        public async Task<dynamic> GetNeedAttribution(NeedAttentionAttribute needAttention)
        {
            dynamic list = await _attributionWorkflowService.GetNeedAttentionList(needAttention);
            return list;
        }
        #endregion

        #region PatientAttributionStatus
        [HttpPost]
        [Route("PatientAttributionStatus")]
        public async Task<dynamic> PatientAttributionStatus(DashBoardFilters data)
        {
            var response = await _attributionWorkflowService.PatientAttributionStatus(data);
            return response;
        }
        #endregion

        #region Update Need Attention data
        [HttpPost]
        [Route("UpdateNeedAttentionata")]
        public async Task<dynamic> UpdateNeedAttentionata([FromBody]List<NeedAttention> needAttention)
        {
            var response = await _attributionWorkflowService.UpdateNeedAttentionata(needAttention);
            return response;
        }
        #endregion

        #region Update CareGap  data
        [HttpPost]
        [Route("UpdateCareGapdata")]
        public async Task<dynamic> UpdateCareGapdata([FromBody]List<NeedAttention> needAttention)
        {
            var response = await _attributionWorkflowService.UpdateCareGapdata(needAttention);
            return response;
        }
        #endregion

        #region Update NeedAttention   data
        [HttpPost]
        [Route("UpdateNeedattentiondata")]
        public async Task<dynamic> UpdateNeedattentiondata([FromBody]List<NeedAttention> needAttention)
        {
            var response = await _attributionWorkflowService.UpdateNeedattentiondata(needAttention);
            return response;
        }
        #endregion
    }


}
