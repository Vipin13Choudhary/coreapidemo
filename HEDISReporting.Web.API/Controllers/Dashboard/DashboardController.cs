﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using HEDISReporting.DataContract.Common;
using HEDISReporting.DataContract.Dashboard;
using HEDISReporting.DataContract.DataSource;
using HEDISReporting.DataContract.PatientChart;
using HEDISReporting.ServiceContract.Dashboard;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HEDISReporting.Web.API.Controllers.Dashboard
{
    [Route("api/[controller]")]
    [ApiController]
    public class DashboardController : BaseController
    {
        private readonly IDashboardService _dashboardService;


        #region Construtor of the class
        public DashboardController(IDashboardService dashboardService) : base(dashboardService)
        {
            _dashboardService = dashboardService;
        }
        #endregion

        #region get dashboard Measure details
        [HttpPost]
        [Route("GetAllABADetails")]
        public async Task<MeasureData> GetAllABADetails(DashBoardFilters data)
        {

            var list = await _dashboardService.GetAllABADetails(data);
            //Thread.Sleep(10000);
            return list;
        }

        #endregion

        #region get All patient under the measure
        [HttpPost]
        [Route("GetAllMeasurePatientList")]
        public async Task<dynamic> GetAllMeasurePatientList(DashBoardFilters data)
        {

            var list = await _dashboardService.GetAllMeasurePatientList(data);
            return list;

        }

        #endregion

        #region
        [HttpPost]
        [Route("GetAllDropDowm")]
        public async Task<FilterDropdown> GetAllDropDowm(int UserId)
        {

            return await _dashboardService.GetAllDropDowm(UserId);


        }
        #endregion

        #region GetMeasureAnalysis
        [HttpPost]
        [Route("GetMeasureAnalysis")]
        public async Task<MeasureAnalysisDetail> GetMeasureAnalysis(DashBoardFilters data)
        {
            var list = await _dashboardService.GetMeasureAnalysis(data);
            return list;

        }


        #endregion

        #region get  default goals
        [HttpPost]
        [Route("GetDefaultGoalsDetails")]
        //public async Task<GoalDashboard> GetDefaultGoalsDetails(RegionDropDown data)
        //{

        //    var list = await _dashboardService.GetDefaultGoalsDetails(int.Parse(data.UserId));
        //    return list;

        //}
        public async Task<GoalDashboard> GetDefaultGoalsDetails(DashBoardFilters data)
        {

             var list=  await _dashboardService.GetDefaultGoalsDetails(data);

            return list;
        }

        #endregion

        #region Get Organization Clinic & payer
        [HttpGet]
        [Route("GetClinicPayerForOrganizaton")]
        public async Task<dynamic> GetClinicPayerForOrganizaton(int Type,int Id,int LoginOrganizationId=0)
        {
            var list = await _dashboardService.GetClinicPayerForOrganizaton(Type,Id,LoginOrganizationId);
            return list;
        }

        #endregion

        #region save dashboard details

        [HttpPost]
        [Route("SaveDashboardGoalDetails")]
        public async Task<int> SaveDashboardGoalDetails(SavedDashboardGoalDetails data)
        {

            return await _dashboardService.SaveDashboardGoalDetails(data);
        }

        #endregion

        #region get dashboard COL Measure details
        [HttpPost]
        [Route("GetCOLAllDetails")]
        public async Task<MeasureData> GetCOLAllDetails(DashBoardFilters data)

        {

            var list = await _dashboardService.GetCOLAllDetails(data);
            return list;

        }

        #endregion

        #region -- Get BCS Details--
        [HttpPost]
        [Route("GetBCSAllDetails")]
        public async Task<MeasureData> GetBSCAllDetails(DashBoardFilters data)
        {

            var list = await _dashboardService.GetBCSAllDetails(data);
            return list;

        }
        #endregion

        #region get dashboard COL Measure details
        [HttpPost]
        [Route("GetCOLPatientsList")]
        public async Task<dynamic> GetCOLPatientsList(DashBoardFilters data)
        {

            var list = await _dashboardService.GetCOLPatientsList(data);
            return list;

        }

        [HttpPost]
        [Route("GetBCSPatientsList")]
        public async Task<dynamic> GetBCSPatientsList(DashBoardFilters data)
        {

            var list = await _dashboardService.GetBCSPatientsList(data);
            return list;

        }

        #endregion

        #region get dashboard CCS Measure details
        [HttpPost]
        [Route("GetCCSAllDetails")]
        public async Task<MeasureData> GetCCSAllDetails(DashBoardFilters data)

        {

            var list = await _dashboardService.GetCCSAllDetails(data);
            return list;

        }


        [HttpPost]
        [Route("GetCCSPatientsList")]
        public async Task<dynamic> GetCCSPatientList(DashBoardFilters data)
        {

            var list = await _dashboardService.GetCCSPatientsList(data);
            return list;

        }

        #endregion

        #region get all the measure type master
        /// <summary>
        /// get all the measure type master (Hedis,wellcare etc..)
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAllMeasureTypeMaster")]
        public async Task<List<MeasureTypeMaster>> GetAllMeasureTypeMaster()
        {
            return await _dashboardService.GetAllMeasureTypeMaster();
        }

        #endregion

        #region ----- Get CBP Details ------
        [HttpPost]
        [Route("GetCBPAllDetails")]
        public async Task<MeasureData> GetCBPAllDetails(DashBoardFilters data)
        {
            try
            {
                var list = await _dashboardService.GetCBPAllDetails(data);
                //Thread.Sleep(10000);
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion

        #region get dashboard Measure details
        #region get all the measure type master
        /// <summary>
        /// get all the measure type master (Hedis,wellcare etc..)
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("GetABAWellCareDetails")]
        public async Task<dynamic> GetABAWellCareDetails(DashBoardFilters data)
        {
            return await _dashboardService.GetABAWellCareDetails(data);
        }


        [HttpPost]
        [Route("GetBCSWellCareDetails")]
        public async Task<dynamic> GetBCSWellCareDetails(DashBoardFilters data)
        {
            return await _dashboardService.GetBCSWellCareDetails(data);
        }


        [HttpPost]
        [Route("GetMeasureWellCarePatientList")]
        public async Task<dynamic> GetMeasureWellCarePatientList(DashBoardFilters data)
        {

            return await _dashboardService.GetMeasureWellCarePatientList(data);

        }



        [HttpPost]
        [Route("GetCBPPatientsList")]
        public async Task<dynamic> GetCBPPatientsList(DashBoardFilters data)
        {
            try
            {
                var list = await _dashboardService.GetCBPPatientsList(data);
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #endregion




        [HttpPost]
        [Route("GetMeasureAnalysisWellCare")]
        public async Task<MeasureAnalysisDetail> GetMeasureAnalysisWellCare(DashBoardFilters data)
        {
            var list = await _dashboardService.GetMeasureAnalysisWellCare(data);
            return list;

        }
        #endregion

        #region get details for the COL Wellcare
        [HttpPost]
        [Route("GetCOLWellCareDetails")]
        public async Task<dynamic> GetCOLWellCareDetails(DashBoardFilters data)
        {

            return await _dashboardService.GetCOLWellCareDetails(data);

        }

        [HttpPost]
        [Route("GetColWellCarePatientList")]
        public async Task<dynamic> GetColWellCarePatientList(DashBoardFilters data)
        {

            return await _dashboardService.GetCOLMeasureWellCarePatientList(data);

        }


        #endregion

        #region Get Measures List
        [HttpPost]
        [Route("GetMeasures")]
        public async Task<dynamic> GetMeasures(DashBoardFilters data)
        {
            dynamic var = await _dashboardService.GetMeasures(data);
            return var;
        }
        #endregion

        #region Save SetMeasuredetails

        [HttpPost]
        [Route("SetMeasuredetails")]
        public async Task<dynamic> SetMeasuredetails([FromBody] MasterRule data)
        {

            var response = await _dashboardService.SetMeasuredetails(data);
            return Ok(new { Status = (response <= 0) ? HttpStatusCode.BadRequest : HttpStatusCode.OK, result = response });

        }
       
        #endregion

        #region Get Measures Name
        [HttpGet]
        [Route("GetMeasuresname")]
        public async Task<FilterNames> GetMeasuresName(int userId)
        {
            return  await _dashboardService.GetMeasuresName(userId);
           
        }
        #endregion
        #region Get Measure Details
        [HttpGet]
        [Route("GetMeasureDetails")]
        public async Task<dynamic> GetMeasureDetails(string Id, string MeasureTypeId)
        {
            dynamic var = await _dashboardService.GetMeasureDetails(Convert.ToInt32(Id), MeasureTypeId);
            return var;
        }
        #endregion

        #region get details for the CCS  Wellcare
        [HttpPost]
        [Route("GetCCSWellCareDetails")]
        public async Task<dynamic> GetCCSWellCareDetails(DashBoardFilters data)
        {

            return await _dashboardService.GetCCSWellCareDetails(data);

        }

        [HttpPost]
        [Route("GetCCSWellCarePatientList")]
        public async Task<dynamic> GetCCSWellCarePatientList(DashBoardFilters data)
        {

            return await _dashboardService.GetCCSMeasureWellCarePatientList(data);

        }
        [HttpPost]
        [Route("GetBCSWellCarePatientList")]
        public async Task<dynamic> GetBCSWellCarePatientList(DashBoardFilters data)
        {

            return await _dashboardService.GetBCSMeasureWellCarePatientList(data);

        }
        #endregion

        #region get details for the CBP  Wellcare
        [HttpPost]
        [Route("GetCBPWellCareDetails")]
        public async Task<dynamic> GetCBPWellCareDetails(DashBoardFilters data)
        {

            return await _dashboardService.GetCBPMeasureWellcareDetail(data);

        }

        [HttpPost]
        [Route("GetCBPWellCarePatientList")]
        public async Task<dynamic> GetCBPWellCarePatientList(DashBoardFilters data)
        {

            return await _dashboardService.GetCBPWellcarePatientList(data);

        }
        #endregion

        #region GetMeasuregoals
        [HttpPost]
        [Route("GetMeasureGoals")]
        public async Task<dynamic> GetMeasureGoals(RegionDropDown data)
        {
            return await _dashboardService.GetMeasureGoals(Convert.ToInt32(data.UserId), data.OrganizationId);
        }
        #endregion

        #region SubmitGoalsDetails
        [HttpPost]
        [Route("SubmitGoalsDetails")]
        public async Task<dynamic> SubmitGoalsDetails(GoalDetails details)
        {
            return await _dashboardService.IUGoals(details);
        }
        #endregion
        #region SubmitMasterGoalsDetails
        [HttpPost]
        [Route("SubmitMasterGoalsDetails")]
        public async Task<dynamic> SubmitMasterGoalsDetails(MasterGoal details)
        {
            return await _dashboardService.SubmitMasterGoalsDetails(details);
        }
        #endregion

        #region MasterSelectedMeasure
        [HttpGet]
        [Route("MasterSelectedMeasure")]
        public async Task<dynamic> MasterSelectedMeasure(int OrganizationId, int Type)
        {
            return await _dashboardService.MasterSelectedMeasure(OrganizationId,Type);
        }
        #endregion

        #region MasterMeasureGoals
        [HttpGet]
        [Route("MasterMeasureGoals")]
        public async Task<dynamic> MasterMeasureGoals(int OrganizationId, int Type, string Search)
        {
            return await _dashboardService.MasterMeasureGoals(OrganizationId,Type, Search);
        }
        #endregion

        #region

        [HttpGet]
        [Route("GetEntityGoal")]
        public async Task<dynamic> GetEntityGoal(int Id)
        {
            return await _dashboardService.GetEntityGoal(Id);
        }
        #endregion

        #region Get Entity Measure
        [HttpGet]
        [Route("GetEntityMeasure")]
        public async Task<dynamic> GetEntityMeasure(int EntityType, int EntityId)
        {
            return await _dashboardService.GetEntityMeasure(EntityType,EntityId);
        }
        #endregion

        #region Copy measure
        [HttpGet]
        [Route("CopyMersure")]
        public async Task<dynamic> CopyMersure(int CEntityType, int CEntityId, int userId, int EntityId, int EntityType)
        {
            return await _dashboardService.CopyMersure(CEntityType, CEntityId,userId,EntityId,EntityType);
        }
        #endregion
     
        #region Delete Measure
        [HttpGet]
        [Route("DeleteMeasure")]
        public async Task<dynamic> DeleteMeasure(int Id)
        {
            return await _dashboardService.DeleteMeasure(Id);
        }
        #endregion
        #region Delete All Measure
        [HttpGet]
        [Route("DeleteAllMeasure")]
        public async Task<dynamic> DeleteAllMeasure(int Id,int Type)
        {
            return await _dashboardService.DeleteAllMeasure(Id,Type);
        }
        #endregion
        [HttpGet]
        [Route("GetMeasureDescription")]
        public async Task<string> GetMeasureDescription(int measureId, int entityId)
        {
            return await _dashboardService.GetMeasureDescription(measureId, entityId);
        }

        #region -------Get Patient Claim Details-----------
        [HttpPost]
        [Route("GetAllPatientClaimDetails")]
        public async Task<List<PatientClaimDetails>> GetAllPatientClaimDetails(ClaimFilters data)
        {
            try
            {
                var list = await _dashboardService.GetClaimResult(data);
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion
    }
}