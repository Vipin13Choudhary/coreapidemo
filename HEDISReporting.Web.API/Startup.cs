﻿using HEDISReporting.DataContract.Authentication;
using HEDISReporting.Repository.Organization;
using HEDISReporting.Repository.User;
using HEDISReporting.RepositoryContract.Organization;
using HEDISReporting.RepositoryContract.User;
using HEDISReporting.Service;
using HEDISReporting.Service.Organization;
using HEDISReporting.Service.User;
using HEDISReporting.ServiceContract;
using HEDISReporting.ServiceContract.Organization;
using HEDISReporting.ServiceContract.Patient;
using HEDISReporting.Service.Patient;
using HEDISReporting.ServiceContract.User;
using HEDISReporting.Web.API.Dapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using HEDISReporting.RepositoryContract.Patient;
using HEDISReporting.Repository.Patient;
using Microsoft.AspNetCore.Http.Features;
using System;
using HEDISReporting.Web.API.Logger;
using HEDISReporting.ServiceContract.Exception;
using HEDISReporting.RepositoryContract.Exception;
using HEDISReporting.Repository;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using HEDISReporting.RepositoryContract.Dashboard;
using HEDISReporting.Repository.Dashboard;
using HEDISReporting.ServiceContract.Dashboard;
using HEDISReporting.Service.Dashboard;
using HEDISReporting.ServiceContract.ProviderS;
using HEDISReporting.Service.Provider;
using HEDISReporting.RepositoryContract.Provider;
using HEDISReporting.Repository.Provider;
using HEDISReporting.ServiceContract.CareGap;
using HEDISReporting.Service.CareGap;
using HEDISReporting.RepositoryContract.CareGap;
using HEDISReporting.Repository.CareGap;
using HEDISReporting.RepositoryContract.AttributionWorkflow;
using HEDISReporting.Repository.AttributionWorkflow;
using HEDISReporting.ServiceContract.AttributionWorkflow;
using HEDISReporting.Service.AttributionWorkflow;

namespace HEDISReporting.Web.API
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

       

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = Configuration["JwtIssuer"],
                    ValidAudience = Configuration["JwtAudience"],
                    IssuerSigningKey = new SymmetricSecurityKey(
                        Encoding.UTF8.GetBytes(Configuration["JwtKey"]))
                };
            });

            services.AddCors(options =>
            {
                options.AddPolicy("CORS", corsPolicyBuilder => corsPolicyBuilder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials()
                    );
            });
            //CorsPolicy.ConfigureServices(services,"");


            
            services.Configure<FormOptions>(options =>
            {
                options.MemoryBufferThreshold = Int32.MaxValue;
            });
           
            // additional configuration

            services.AddMvc(
                config => {config.Filters.Add(typeof(CustomExceptionFilter));}
                ).AddJsonOptions(a => a.SerializerSettings.NullValueHandling = NullValueHandling.Ignore);
           
            ConfigureDependencyInjection(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();
            //app.UseHttpsRedirection();
            //enable Cors 
            app.UseCors("CORS");
            app.UseStaticFiles();
            app.UseMvc();

           
            

        }
        private void ConfigureDependencyInjection(IServiceCollection services)
        {
            // Identity Services
            services.AddTransient<IUserStore<ApplicationUser>, UserStore>();
            services.AddTransient<IRoleStore<ApplicationRole>, RoleStore>();

            services.AddIdentity<ApplicationUser, ApplicationRole>()
                .AddDefaultTokenProviders();

            // Add application services.                        
            //services.AddTransient<IAuthenticationService, AuthenticationService>();
            services.AddTransient<IBaseService, BaseService>();
            //services.AddTransient<IEmailService, EmailService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IOrganizationService, OrganizationService>();
            services.AddTransient<IPatientService, PatientService>();
            services.AddTransient<IDashboardService, DashboardService>();
            services.AddTransient<IExceptionService, ExceptionService>();
            services.AddTransient<IProviderService, ProviderService>();
            services.AddTransient<ICareGapService, CareGapService>();
            services.AddTransient<IAttributionWorkflowService, AttributionWorkflowService>();
            // Add repos as scoped dependency so they are shared per request.            
            //services.AddScoped<IAuthenticationRepository, AuthenticationRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IOrganizationRepository, OrganizationRepository>();
            services.AddScoped<IPatientRepository, PatientRepository>();
            services.AddScoped<IDashboardRepository, DashboardRepository>();
            services.AddScoped<IExceptionRepository, ExceptionRepository>();
            services.AddScoped<IProviderRepository, ProviderRepository>();
            services.AddScoped<ICareGapRepository, CareGapRepository>();
            services.AddScoped<IAttributionWorkflowRepository, AttributionWorkflowRepository>();
            services.AddSingleton<IConfiguration>(Configuration);
        }
    }
}
