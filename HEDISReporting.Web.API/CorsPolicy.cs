﻿using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HEDISReporting.Web.API
{
    public static class CorsPolicy
    {
        public const string PRODUCTION = "Production";
        public const string ALLOW_ALL = "AllowAll";
        private static string baseOriginUrl = "";
        public static void ConfigureServices(IServiceCollection services, string baseOrigin)
        {
            //services.AddCors(AllowProduction);
            baseOriginUrl = baseOrigin;
            services.AddCors(AllowAll);
        }
        private static void AllowProduction(CorsOptions options)
        {
            throw new Exception("Not Implemented");
            //will be updated as per domain URL

            options.AddPolicy(PRODUCTION, builder =>
            {
                builder
                    .WithOrigins("")
                    .WithMethods("")
                    .WithHeaders("");
            });
        }
        private static void AllowAll(CorsOptions options)
        {

            options.AddPolicy(ALLOW_ALL, builder =>
            {
                builder.WithOrigins(baseOriginUrl)
                    .AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            });
        }
    }
}
