﻿using HEDISReporting.Common.CustomExceptions;
using HEDISReporting.Common.Enum;
using HEDISReporting.DataContract.Exceptions;
using HEDISReporting.ServiceContract.Exception;
using HEDISReporting.Web.API.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Net;

namespace HEDISReporting.Web.API.Logger
{
    public class CustomExceptionFilter : BaseController, IExceptionFilter
    {

        IExceptionService _exceptionService;
        public CustomExceptionFilter(IExceptionService baseRepository) : base(baseRepository)
        {
            _exceptionService = baseRepository;
        }
        public void OnException(ExceptionContext context)
        {
           
            ExceptionModal exceptionModal = new ExceptionModal();
            String message = String.Empty;

            var exceptionType = context.Exception.GetType();
            if (exceptionType == typeof(UnauthorizedAccessException))
            {
                exceptionModal.Message = "Unauthorized Access";
                exceptionModal.Status = HttpStatusCode.Unauthorized;
                exceptionModal.Section =  AppSections.GeneralException;
            }
            else if (exceptionType == typeof(NotImplementedException))
            {
                exceptionModal.Message = "A server error occurred.";
                exceptionModal.Status = HttpStatusCode.NotImplemented;
                exceptionModal.Section =AppSections.GeneralException;
            }
            else if (exceptionType == typeof(HEDISCustomException))
            {
                exceptionModal.Message = context.Exception.ToString();
                exceptionModal.Status = HttpStatusCode.InternalServerError;
                exceptionModal.Section = AppSections.CustomException;
            }
            else if(exceptionType==typeof(HEDISCDAParsingException))
            {
                exceptionModal.Message = context.Exception.Message;
                exceptionModal.Status = HttpStatusCode.InternalServerError;
                exceptionModal.Section = AppSections.CDAParsing;
            }
            else
            {
                exceptionModal.Message = context.Exception.Message;
                exceptionModal.Status = HttpStatusCode.InternalServerError;
                exceptionModal.Section = AppSections.GeneralException;
            }
            context.ExceptionHandled = true;

            HttpResponse response = context.HttpContext.Response;
            response.StatusCode = (int)exceptionModal.Status;
            response.ContentType = "application/json";
            var err = exceptionModal.Message + " " + context.Exception.StackTrace;
            exceptionModal.Message = err;
            HttpRequest request = context.HttpContext.Request;

            exceptionModal.UserName = request.Headers["UserName"];
            exceptionModal.UserId = request.Headers["UserId"];
            if(exceptionModal!=null && exceptionModal.UserId != null)
            _exceptionService.SaveExceptionLogs((int)exceptionModal.Status, exceptionModal.Message, exceptionModal.UserName, exceptionModal.UserId,(int)exceptionModal.Section,"","");

            response.WriteAsync(err);
        }
    }
   
    
}
