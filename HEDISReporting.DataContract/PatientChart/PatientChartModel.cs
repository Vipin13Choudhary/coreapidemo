﻿using HEDISReporting.DataContract.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.DataContract.PatientChart
{
    public class PatientDemographicsModel
    {
        public string Name { get; set; }
        public string Age { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
        public string Gender { get; set; }
        public string ssn { get; set; }
        public string DOB { get; set; }
        public string ActiveDiagnosis { get; set; }
        public string PPName { get; set; }
        public string PPCountry { get; set; }
        public string PPPhone { get; set; }
        public string CPName { get; set; }
        public string CPCountry { get; set; }
        public string CPPhone { get; set; }
        public string Provider { get; set; }
        public string Email { get; set; }
        public string OrganizationName { get; set; }
    }
    public class PatientDiagnosis
    {
        public string ProblemName { get; set; }
        public string Code { get; set; }
        public string DiagnosisImpression { get; set; }
        public string Primary { get; set; }
        public string DiagnosisDate { get; set; }
    }
    public class PatientAllergies
    {
        public string Allergen { get; set; }
        public string AllergyType { get; set; }

    }
    public class PatientLabTest
    {
        public string Test { get; set; }
        public string Finding { get; set; }
        public string Result { get; set; }
        public string TestSchedule { get; set; }

    }
    public class SnomedCodeModel
    {
        public int PatientID { get; set; }
        public string SnomedCode { get; set; }
        public string SnomedCodeDesc { get; set; }
        public List<SnomedIcdMap> SnomedIcdMap { get; set; }
        public bool IsVisible { get; set; } = false;
        public int PatientDiagnosisId { get; set; }
    }
    public class SnomedIcdMap
    {
        public string SnomedCode { get; set; }
        public string MapTarget { get; set; }
        public string IcdName { get; set; }
       
       
    }
    public class PatientChartModel
    {
        public PatientDemographicsModel patientDemographicsModel { get; set; }
        public List<SnomedCodeModel> snomedCodeModels { get; set; }
        public List<PatientLabTest> patientLabTest { get; set; }
        public List<PatientAllergies> patientAllergies { get; set; }
        public List<PatientClaimChildrenDetails> PatientClaim { get; set; }
    }
    public class ClaimDetails {
        public int PatientId { get; set; }
        public string ServiceName { get; set; }
        public string ClinicName { get; set; }
        public Nullable<DateTime> DiagnosisDate { get; set; }
        public string Unit { get; set; }
        public string Charges { get; set; }
        public string PatientName { get; set; }

        public string PatientAddress { get; set; }
        public string Email { get; set; }

        public string MRN { get; set; }
        public Nullable<DateTime> DOB { get; set; }
        public string SSN { get; set; }
        public string ContactNo { get; set; }
        public string DiagnosisCode { get; set; }
        public string icdName { get; set; }
        public string ClinicAddress1 { get; set; }
        public string ClinicPhoneNumber { get; set; }
        public string PayerName { get; set; }
        public string State { get; set; }
        public string City { get; set; }
    }

    public class PatientClaimDetails {
        public int PatientId { get; set; }
        public string PayerName { get; set; }
        public string  PatientName { get; set; }
        public string PatientAddress { get; set; }
        public Nullable<DateTime> Createddate { get; set; }
        public Nullable<DateTime> DiagnosisDate { get; set; }
        public List<PatientClaimChildrenDetails> PatientClaimChildrenDetails { get; set; }
    }
    public class PatientClaimChildrenDetails {
        public int PatientId { get; set; }
        public string ServiceName { get; set; }
        public string ClinicName { get; set; }
        public Nullable<DateTime> DiagnosisDate { get; set; }
        public string Unit { get; set; }
        public string Charges { get; set; }

        public string Modifier { get; set; }
        public string ServiceCode { get; set; }

        public string ContactNo { get; set; }
    }

    public class ClaimFilters: ListingOperands
    {
        public int OrganizationId { get; set; }
        public int PatientId { get; set; }
        public Nullable<DateTime> FromDate { get; set; }
        public Nullable<DateTime> ToDate { get; set; }
    }
}