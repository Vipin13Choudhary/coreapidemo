﻿namespace HEDISReporting.DataContract.PageStatistics
{
    public class PageStatisticsItem
    {
        public string Key { get; set; }

        public decimal Value { get; set; }
    }
}
