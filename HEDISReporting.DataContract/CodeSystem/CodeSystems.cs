﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.DataContract.CodeSystem
{
    /// <summary>
    /// code systems with thier code system OID
    /// <createdBy>Rakesh</createdBy>
    /// <createdDate>24'th Jan 2019</createdDate>
    /// </summary>
    public struct CodeSystems
    {
        public string ICD10CM { get { return "ICD10CM"; } }
        public string ICD10CMCodesystemOID { get { return "2.16.840.1.113883.6.90"; } }

        public string ICD9PCS { get { return "ICD9PCS"; } }
        public string ICD9PCSCodesystemOID { get { return "2.16.840.1.113883.6.104"; } }

        public string ICD9CM { get { return "ICD9CM"; } }
        public string ICD9CMCodesystemOID { get { return "2.16.840.1.113883.6.103"; } }

        public string POS { get { return "POS"; } }
        public string POSCodesystemOID { get { return "2.16.840.1.113883.6.50"; } }


        public string ICD10PCS { get { return "ICD10PCS"; } }
        public string ICD10PCSCodesystemOID { get { return "2.16.840.1.113883.6.4"; } }

        public string CPT { get { return "CPT"; } }
        public string CPTCodesystemOID { get { return "2.16.840.1.113883.6.12"; } }

        public string CPTCATII { get { return "CPT-CAT-II"; } }
        public string CPTCATIICodesystemOID { get { return "2.16.840.1.113883.6.12"; } }

        public string UBREV { get { return "UBREV"; } }
        public string UBREVCodesystemOID { get { return "2.16.840.1.113883.6.301.3"; } }

        public string LOINC { get { return "LOINC"; } }
        public string LOINCCodesystemOID { get { return "2.16.840.1.113883.6.1"; } }

        public string HCPCS { get { return "HCPCS"; } }
        public string HCPCSCodesystemOID { get { return "2.16.840.1.113883.6.285"; } }

        public string CVX { get { return "CVX"; } }
        public string CVXCodesystemOID { get { return "2.16.840.1.113883.12.292"; } }

        public string UBTOB { get { return "UBTOB"; } }
        public string UBTOBCodesystemOID { get { return "2.16.840.1.113883.6.301.1"; } }
        public string SNOMEDCT { get { return "SNOMEDCT"; } }
        public string SNOMEDCTCodesystemOID { get { return "2.16.840.1.113883.6.96"; } }
    }

}
