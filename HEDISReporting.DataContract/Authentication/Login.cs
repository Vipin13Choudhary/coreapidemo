﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.DataContract.Authentication
{
    /// <summary>
    /// Class for login information.
    /// </summary>
    public class Login
    {
        /// <summary>
        /// User name 
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Password
        /// </summary>
        public string Password { get; set; }
    }
}
