﻿namespace HEDISReporting.DataContract.Authentication
{
    /// <summary>
    /// Login user information including the token
    /// </summary>
    public class LoginUser
    {
        /// <summary>
        /// User Id
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// User name
        /// </summary>
        public string UserName { get; set; }        
    }
}
