﻿using HEDISReporting.DataContract.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.DataContract.Provider
{
    public class ProviderModel:BaseEntity
    {
        public int ProviderId { get; set; }
        public string FirstName { get; set; }
        public string ModdleName { get; set; }
        public string LastName { get; set; }
        public string NPI { get; set; }
        public string DisplayName { get; set; }
        public string LicenseName { get; set; }
        public string ProviderType { get; set; }
        public bool Status { get; set; }
        public DateTime InitialCredentialDate { get; set; }
        public DateTime CredentialExpirationDate { get; set; }
        public DateTime LastCredentialDate { get; set; }
        public string Clinic { get; set; }
        public string Phone { get; set; }
        public string Cell { get; set; }
        public string Pager { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string EHR { get; set; }
        public string LicenseNo { get; set; }
        public string UPIN { get; set; }
        public string GroupName { get; set; }
        public string Sufix { get; set; }
    }
    public class ProviderListingOperands : ListingOperands
    {

        public string ProviderId { get; set; }
    }
}
