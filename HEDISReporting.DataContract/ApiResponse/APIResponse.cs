﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.DataContract.ApiResponse
{
   public class APIResponse
    {
        public APIResponse()
        {
            data = new data();
        }
        public int StatusCode { get; set; }
        public string Message { get; set; }
        public data data { get; set; }
    }

    public class data
    {
        public string UserId { get; set; }
        public string Token { get; set; }
        public IList<string> Roles { get; set; }

        public int UserType { get; set; }

        public string UserName { get; set; }
        public int OrganizationId { get; set; }
        public string Role { get; set; }

    }
    public class Response
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
        public string Data { get; set; }
    }
}
