﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.DataContract.User
{
   public class UserMenus
    {
        public int MenuId { get; set; }
        public string MainMenuName { get; set; }
        public string MainMenuController { get; set; }
        public string MainMenuAction { get; set; }
        public string Parameters { get; set; }
        public string MainMenuUrl { get; set; }
        public string MenuIcon { get; set; }
        public List<UserSubMenus> SubMenus { get; set; }
    }
    public class UserSubMenus
    {
        public int MenuId { get; set; }
        public string SubMenusName { get; set; }
        public string SubMenusController { get; set; }
        public string SubMenusAction { get; set; }
        public string Parameters { get; set; }
        public string SubMenuUrl { get; set; }
        public string MenuIcon { get; set; }
    }

    public class UserRolesData
    {
        public int Id { get; set; }
        public string Role { get; set; }
        public int UserId { get; set; }
        public int OrganizationId { get; set; }

    }
}
