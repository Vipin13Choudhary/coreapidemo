﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.DataContract.User
{
    public class UserModel
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Status { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string LoginName { get; set; }
        public Byte[] LoginPassword { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int OrganizationId { get; set; }
        public string PostalCode { get; set; }
        /// <summary>
        /// Password salt
        /// This will be used to generate the password hash
        /// </summary>
        public byte[] PasswordSalt { get; set; }
    }
}
