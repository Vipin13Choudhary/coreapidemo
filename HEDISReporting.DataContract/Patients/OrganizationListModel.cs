﻿using HEDISReporting.DataContract.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.DataContract.Patients
{
    public class OrganizationListModel: ListingOperands
    {
        public int OrganizationId { get; set; }
    }
}
