﻿using System;
using System.Collections.Generic;
using System.Text;
using HEDISReporting.DataContract.Common;

namespace HEDISReporting.DataContract.Patients
{
    public class Patients
    {
        public int PatientID { get; set; }
        public int DeletedBy { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string EmergencyContactFirstName { get; set; }
        public string EmergencyContactLastName { get; set; }
        public string EmergencyContactPhone { get; set; }
        public int Ethnicity { get; set; }
        public int MaritalStatus { get; set; }
        public int Gender { get; set; }
        public bool IsActive { get; set; }
        public bool IsVerified { get; set; }
        public string PhotoPath { get; set; }
        public string PhotoThumbnailPath { get; set; }
        public int Race { get; set; }
        public int SecondaryRaceID { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int EmergencyContactRelationship { get; set; }
        public bool IsDeleted { get; set; }
        public int Citizenship { get; set; }
        public string PrimaryProvider { get; set; }
        public DateTime DeletedDate { get; set; }
        public int OrganizationId { get; set; }
        public int LocationId { get; set; }  
        public int RenderingProviderID { get; set; }
        public bool OptOut { get; set; }
        public string Note { get; set; }
        public int UserId { get; set; }
        public bool IsPortalActivate { get; set; }
        public string EmergencyContactOthers { get; set; }
        public int EmploymentID { get; set; }
        public int StaffId { get; set; }
        public DateTime DOB { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MRN { get; set; }
        public string SSN { get; set; }
        public bool IsPortalRequired { get; set; }
        public string ClientID { get; set; }
      
    }

    public class LabListingOperands : ListingOperands
    {
        public string LabName { get; set; }
        public string OrderNo { get; set; }
        public string TestName { get; set; }
        public Nullable<DateTime> FromDate { get; set; }
        public Nullable<DateTime> ToDate { get; set; }
        public long PatientId { get; set; }
    }
    public class PatientCCDAModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Nullable<DateTime> DOB { get; set; }
        public string Email { get; set; }
        public string ContactNo { get; set; }
        public string Phone { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string ApartmentNumber { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string PCPFName { get; set; }
        public string PCPLName { get; set; }
        public string PCPMName { get; set; }
        public string PCPNPI { get; set; }
        public string PayerPCPFName { get; set; }
        public string PayerPCPLName { get; set; }
        public string PayerPCPMName { get; set; }
        public string PayerPCPNPI { get; set; }
    }

}
