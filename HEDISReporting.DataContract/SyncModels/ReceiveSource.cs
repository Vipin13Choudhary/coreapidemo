﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.DataContract.SyncModels
{
    public class ReceiveSource
    {
        public int DataSourceId { get; set; }
        public int Frequency { get; set; }
        public string FrequencyDays { get; set; }
        public TimeSpan FrequencyTime { get; set; }
        public string DirectoryPath { get; set; }
    }
}
