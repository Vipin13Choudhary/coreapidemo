﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.DataContract.SyncModels
{
    public class SftpHedisRequestModel
    {
        public ReceiveSource receiveSource { get; set; }
        public GetSource getSource { get; set; }
        public List<FileSyncs> fileSyncs { get; set; }
    }
}
