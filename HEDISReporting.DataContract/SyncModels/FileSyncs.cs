﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.DataContract.SyncModels
{
    public class FileSyncs
    {
        public int FileSyncId { get; set; }
        public string FileName { get; set; }
        public long FileSize { get; set; }
        public DateTime FilePostedDate { get; set; }
        public string FilePostedDateString { get; set; }
        public int FileType { get; set; }
    }
}
