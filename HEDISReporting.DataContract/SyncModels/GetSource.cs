﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.DataContract.SyncModels
{
    public class GetSource
    {
        public int DataSourceId {get;set;} 
        public int Frequency { get; set; }
        public string FrequencyDays { get; set; }
        public string FrequencyWeek { get; set; }
        public TimeSpan FrequencyTime { get; set; }
        public string Host { get; set; }
        public string Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
