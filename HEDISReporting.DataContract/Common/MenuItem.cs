﻿namespace HEDISReporting.DataContract.Common
{
    /// <summary>
    /// Object representing the Menu Item for various details page
    /// </summary>
    public class MenuItem
    {
        /// <summary>
        /// Text to be displayed as each Menu Item
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Redirect Path
        /// </summary>
        public string Path { get; set; }
    }
}
