﻿namespace HEDISReporting.DataContract.Common
{
    public class States
    {
        /// <summary>
        /// State key
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// state value
        /// </summary>
        public string Value { get; set; }
    }
}
