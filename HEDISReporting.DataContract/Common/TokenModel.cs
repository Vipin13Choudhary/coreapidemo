﻿namespace HEDISReporting.ServiceContract.Patient
{
    public class TokenModel
    {
        public int OrganizationID { get; set; }
        public int UserID { get; set; }
    }
}