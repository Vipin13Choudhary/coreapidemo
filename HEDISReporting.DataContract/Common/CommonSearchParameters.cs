﻿namespace HEDISReporting.DataContract.Common
{
    public class CommonSearchParameters
    {
        public string SortDirection { get; set; }
        public string SortField { get; set; }

        public int? StartRecord { get; set; }

        public int? EndRecord { get; set; }

        public int CallerUserId { get; set; }

        public bool Debug { get; set; }
    }

    public class RegionDropDown
    {
        public string UserId { get;set;}
        public int OrganizationId { get;set;}
    }

    public class DeleteData
    {
        public int Id { get; set; }
        public string TableName { get; set; }

        public int UserId { get; set; }
    }
}
