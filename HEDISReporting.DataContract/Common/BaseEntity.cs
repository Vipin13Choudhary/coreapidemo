﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.DataContract.Common
{
   public class BaseEntity
    {
        public DateTime CreatedDate { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public bool IsDeleted { get; set; }
        public int DeletedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public int CreatedBy { get; set; }

        public string AspNetUserId { get; set; }
    }

}
