﻿using System.Collections.Generic;

namespace HEDISReporting.DataContract.Common
{
    public class DropDownListItem
    {
        /// <summary>
        /// Id for drop down list.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Name for drop down list.
        /// </summary>
        public string Name { get; set; }
    }
    public class OrganizationDropdown
    {
        public List<DropDownListItem> OrganizationsList { get; set; }
        public List<States> StatesList { get; set; }
        public List<DropDownListItem> OrganizationRegion { get; set; }
        public List<DropDownListItem> VendorType { get; set; }

        public List<DropDownListItem> ClinicList { get; set; }
        public List<DropDownListItem> SearchClinicList { get; set; }

    }
    public class CountriesSates
    {
        public List<DropDownListItem> Countries { get; set; }
        public List<DropDownListItem> States { get; set; }
    }

    public class RegionDropDowns
    {
        public int item_id { get; set; }
        public string item_text { get; set; }
    }
}
