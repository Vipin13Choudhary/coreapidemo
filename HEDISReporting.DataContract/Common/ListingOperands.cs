﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.DataContract.Common
{
   public class ListingOperands
    {
        public int offset { get; set; }
        public int limit { get; set; }
        public string order { get; set; }
        public string sort { get; set; }
        public string Search { get; set; }
    }
}
