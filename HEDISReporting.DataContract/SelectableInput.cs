﻿namespace HEDISReporting.DataContract
{
    public class SelectableInput<T> where T : class
    {
        public string Text { get; set; }
        public string Value { get; set; }
        public T Extra { get; set; }
    }
}
