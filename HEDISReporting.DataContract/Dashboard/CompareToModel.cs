﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.DataContract.Dashboard
{
    public class CompareToModel
    {
        public int AssId { get; set; }
        public string AssName { get; set; }
        public int CorpId { get; set; }
        public string  CorpName { get; set; }
        public int PayerId { get; set; }
        public string PayerName { get; set; }
        public int ProviderId { get; set; }
        public string ProviderName { get; set; }
        public int ClinicId { get; set; }
        public string ClinicName { get; set; }
    }
}
