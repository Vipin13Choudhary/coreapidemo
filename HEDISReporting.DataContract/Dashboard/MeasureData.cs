﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.DataContract.Dashboard
{
    public class MeasureData
    {

        public List<decimal> Result { get; set; }

        //public List<PatientUnderMeasurement> AllEligiblePatient { get; set; }
        //public List<PatientUnderMeasurement> QualifiedPatient { get; set; }
        //public List<PatientUnderMeasurement> NonQualifiedPatient { get; set; }

        public List<NumericalMeasureData> NumericalData { get; set; }

    }

    public class PatientUnderMeasurement
    {

        public int PatientId { get; set; }
        public string PatientName { get; set; }
        public int OrganizationId { get; set; }

        public int ProviderId { get; set; }
        public int RaceId { get; set; }
        public int EthinicityId { get; set; }
        public int Gender { get; set; }
        public int Age { get; set; }
        public int Vendor { get; set; }
        public DateTime DOB { get; set; }
        public int Status { get; set; }
        public DateTime LastVisit { get; set; }
        public DateTime NextVisit { get; set; }

        public int Gap { get; set; }
        public string CaregapFlag { get; set; }

    }
    public class NumericalMeasureData
    {
        public int Numerator { get; set; }
        public int Denominator { get; set; }
    }

    public class MeasureAnalysisModel
    {
        public string Month { get; set; }
        public string PatientCount { get; set; }
        public string Year { get; set; }
        public int MonthNumber { get; set; }

    }

    public class MeasureAnalysisDetail
    {
        public List<MeasureAnalysisModel> CurrentData { get; set; }
        public List<MeasureAnalysisModel> PreviousData { get; set; }
    }

    public class GoalData
    {
        public decimal OrganizationGoal { get; set; }
        public decimal NationalGoal { get; set; }
        public decimal AssociationGoal { get; set; }
        public decimal ClinicGoal { get; set; }
        public decimal ProviderGoal { get; set; }
    }
    public class MasterMeasureCategory
    {
        public int Id { get; set; }
        public string CategoryName { get; set; }
        public string Code { get; set; }
    }
    public class MasterMeasures
    {
        public int MasterMeasureId { get; set; }
        public string Name { get; set; }
        public int MeasureType { get; set; }
        public int MeasureCategory { get; set; }
        public string Abbreviation { get; set; }
        public decimal MET { get; set; }
        public string AssociationGoal { get; set; }
        public string CorporateGoal { get; set; }
        public string ClinicGoal { get; set; }
        public string PayerGoal { get; set; }
        public string ProviderGoal { get; set; }
        public int AssPatientNeeded { get; set; }
        public int CoPatientNeeded { get; set; }
        public int PayPatientNeeded { get; set; }
        public int ProPatientNeeded { get; set; }
        public int ClinicPatientNeeded { get; set; }
        public int Numerator { get; set; }
        public int Denominator { get; set; }
        public string BackAbbr { get; set; }


    }
    public class MeasureList
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string CategoryCode { get; set; }
        public List<MasterMeasures> MasterMeasureList { get; set; }
        public List<SavedDashboardGoalDetails> UserDashboardSetting { get; set; }
    }
    public class MasterRule
    {
        public string Name { get; set; }
        public string Abbreviation { get; set; }
        public int MeasureRuleId { get; set; }
        public int MeasureId { get; set; }
        public int EntityId { get; set; }
        public int MeasureYear { get; set; }
        public DateTime TrackingEnds { get; set; }
        public string Description { get; set; }
        public string ReferenceCodes { get; set; }
        public string DenominatorRules { get; set; }
        public string DenominatorSQL { get; set; }
        public string ExclusionRules { get; set; }
        public string NumeratorRules { get; set; }
        public string NumeratorSQL { get; set; }
        public string CPTCodeHeader { get; set; }
        public string CPTCodeBody { get; set; }
        public string ICDCodeHeader { get; set; }
        public string ICDCodeBody { get; set; }
        public string HCPCSHeader { get; set; }
        public string HCPCSBody { get; set; }
        public string MeasureName { get; set; }
        public int MeasureType { get; set; }
    }
    public class OrganizationGoalModal
    {
        public int OrganizationId { get; set; }
        public string OrganizationName { get; set; }
        public int OrganizationType { get; set; }
        public string OrganizationTypeName { get; set; }
        public int Count { get; set; }
        public List<ClinicGoalModal> ClinicGoalModal { get; set; }
    }
    

    public class ClinicGoalModal
    {
        public int ClinicId { get; set; }
        public string ClinicName { get; set; }
        public string Type { get; set; }
        public int Count { get; set; }
        public int TypeId { get; set; }
        public List<ProviderModal> ProviderModal { get; set; }
    }


    public class OrganizationModal
    {
        public int OrganizationId { get; set; }
        public string OrganizationName { get; set; }
        public int OrganizationType { get; set; }
        public string OrganizationTypeName { get; set; }
    }
    public class ClinicModal
    {
        public int ClinicId { get; set; }
        public string ClinicName { get; set; }
        public string Type { get; set; }
        public int OrganizationId { get; set; }
    }
    public class ProviderModal
    {
        public int ProviderId { get; set; }
        public string ProviderName { get; set; }
        public int OrganisationId { get; set; }
        public int ClinicId { get; set; }
        public int Count { get; set; }
    }
    public class MeasureCount
    {
        public int Count { get; set; }
        public int OrganizationId { get; set; }
        public int Type { get; set; }
    }

}
