﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.DataContract.Dashboard
{
   public class MeasurePatient
    {
      public int EligiblePatientListWithTimeID { get;set;}
      public int PatientId{get;set;}
      public string PatientName{get;set;}
      public string Age{get;set;}
      public string DOB{get;set;}
      public string Status{get;set;}
      public string TimeStamp{get;set;}
      public string MeasureId{get;set;}
      public string OrganizationId{get;set;}
      public string LastVisit{get;set;}
      public string NextVisit{get;set;}
      public string RaceName{get;set;}
      public string EthnicityName{get;set;}
      public string CreatedDate{get;set;}

    }
}
