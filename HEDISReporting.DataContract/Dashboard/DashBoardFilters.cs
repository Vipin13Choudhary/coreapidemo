﻿using HEDISReporting.DataContract.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.DataContract.Dashboard
{
    public class DashBoardFilters : ListingOperands
    {

        public int MeasureId { get; set; }
        public int VendorId { get; set; }
        public int ProviderId { get; set; }
        public string AgeRange { get; set; }
        public DateTime PeriodStart { get; set; }
        public DateTime PeriodEnd { get; set; }
        public int Gendor { get; set; }
        public int EthnicityId { get; set; }
        public int RaceId { get; set; }
        public int OrganizationId { get; set; }

        public int Flag { get; set; }

        public int UserId { get; set; }
        public int NewCal { get; set; }
        public int IsFilter { get; set; }
        public int Status { get; set; }
        public int Type { get; set; }


        public int ClinicId { get; set; }
         public int PayerId { get; set;  }
         public int patientId { get; set; }

    }
    public class MasterGoal
    {
        //public decimal ABAgoal { get; set; }
        //public decimal CCgoal { get; set; }
        //public decimal BCSgoal { get; set; }
        //public decimal CCSgoal { get; set; }
        //public decimal CHBPgoal { get; set; }
        public decimal Goal { get; set; }
        public int EntityId { get; set; }
        public int Id { get; set; }
        public int MeasureId { get; set; }
        public int EntityTypeId { get; set; }
        public int UserId { get; set; }
    }
    public class List
    {
        public List<string> Name { get; set; }
        public List<OrgMeasureGoal> list { get; set; }
    }
    public class OrgMeasureGoal
    {
        public int MasterGoalId { get; set; }
        public string Name { get; set; }
        public decimal Goal { get; set; }
        public int Measure { get; set; }
    }
    public class GoalDetails
    {
        public int aBAassociationgoal { get; set; }
        public int aBAclinicgoal { get; set; }
        public int aBAcorporategoal { get; set; }
        public int aBApayergoal { get; set; }
        public int aBAprovidergoal { get; set; }
        public int cCassociationgoal { get; set; }
        public int cCclinicgoal { get; set; }
        public int cCcorporategoal { get; set; }
        public int cCpayergoal { get; set; }
        public int cCprovidergoal { get; set; }
        public int bCSassociationgoal { get; set; }
        public int bCSclinicgoal { get; set; }
        public int bCScorporategoal { get; set; }
        public int bCSpayergoal { get; set; }
        public int bCSprovidergoal { get; set; }
        public int cCSassociationgoal { get; set; }
        public int cCSclinicgoal { get; set; }
        public int cCScorporategoal { get; set; }
        public int cCSpayergoal { get; set; }
        public int cCSprovidergoal { get; set; }
        public int cHBPassociationgoal { get; set; }
        public int cHBPclinicgoal { get; set; }
        public int cHBPcorporategoal { get; set; }
        public int cHBPpayergoal { get; set; }
        public int cHBPprovidergoal { get; set; }
        public int organizationId { get; set; }
        public bool isEdit { get; set; }
    }
    public class MeasureTypeMaster
    {
        public int Id { get; set; }
        public string Name { get; set; }

    }
    public class IndividualGoalDetail
    {
        public int Id { get; set; }
        public string Goal { get; set; }

        public int MeasureId { get; set; }

    }
    public  class GoalDashboard
    {
        public List<IndividualGoalDetail> AssociationGoal { get; set; }

        public List<IndividualGoalDetail> CorporateGoal { get; set; }
        public List<IndividualGoalDetail> ClinicGoal { get; set; }
        public List<IndividualGoalDetail> PayerGoal { get; set; }
        public List<IndividualGoalDetail> ProviderGoal { get; set; }

        public List<SavedDashboardGoalDetails> UserDashboardSetting { get; set; }
    }

    public class SavedDashboardGoalDetails
    {
        public int UserId { get; set; }
        public int  SelectedGoal { get; set; }
        public string Case { get; set; }
        public int OrganizationId { get; set; }
        public int AssociationGoal { get; set; }
        public int CorporateGoal { get; set; }
        public int ProviderGoal { get; set; }
        public int PayerGoal { get; set; }
        public int ClinicGoal { get; set; }
        public int BaseLine { get; set; }


    }

}
