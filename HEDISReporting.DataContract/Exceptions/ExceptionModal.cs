﻿using HEDISReporting.Common.Enum;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace HEDISReporting.DataContract.Exceptions
{
    public class ExceptionModal
    {
        public HttpStatusCode Status {get;set;}
        public string Message { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string AuthToken { get; set; }
        public AppSections Section { get; set; }

    }
}
