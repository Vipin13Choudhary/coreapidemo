﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HEDISReporting.DataContract.Navigation
{
    /// <summary>
    /// Represents the object used for Navigation Items.
    /// </summary>
    public class NavigationItem
    {
        /// <summary>
        /// Name of the image
        /// </summary>
        public string Icon { get; set; }

        /// <summary>
        /// Redirect Url
        /// </summary>
        public string RouterLink { get; set; }

        /// <summary>
        /// Image title
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// Display menus in expanded mode
        /// </summary>
        public bool Expanded { get; set; }

        /// <summary>
        /// Child menu items
        /// </summary>
        public List<NavigationItem> Items { get; set; }
    }
}
