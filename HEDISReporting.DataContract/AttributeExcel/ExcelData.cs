﻿using HEDISReporting.DataContract.Organization;
using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.DataContract.AttributeExcel
{
    public class ExcelData
    {
        public string LOB { get; set; }
        public string MASTER_IPA { get; set; }
        public string IPA_ID { get; set; }
        public string SEQ_MEMB_ID { get; set; }
        public string SUBSCRIBER_ID { get; set; }
        public string FUNDING_COUNTY { get; set; }
        public string PLAN_CODE { get; set; }
        public string SEQ_PCP_ID { get; set; }
        public string PCP_FIRST_NAME { get; set; }
        public string PCP_LAST_NAME { get; set; }
        public string COHORT { get; set; }
        public string ELIG_CATEGORY { get; set; }
        public Nullable<DateTime> DATE_OF_BIRTH { get; set; }
        public string GENDER { get; set; }
        public string MEDICAID_NO { get; set; }
        public string MEDICARE_NO { get; set; }
        public string LAST_NAME { get; set; }
        public string FIRST_NAME { get; set; }
        public string ADDRESS_LINE_1 { get; set; }
        public string ADDRESS_LINE_2 { get; set; }
        public string CITY { get; set; }
        public string STATE { get; set; }
        public string ZIP { get; set; }
        public string RISK_SCORE_AB { get; set; }
        public string RISK_SCORE_D { get; set; }
        public string HOSPICE { get; set; }
        public string ESRD { get; set; }
        public string INSTITUTIONAL { get; set; }
        public string NURSING_HOME_CERTIFIABLE { get; set; }
        public string MEDICAID { get; set; }
        public string MEDICAID_ADD_ON { get; set; }
        public string PREVIOUS_DISABLE { get; set; }
        public string HOME_PHONE_NUMBER { get; set; }
        public string DUAL_ELIG { get; set; }
        public string RECEIVEDMONTH { get; set; }
        public int PayerId { get; set; }
        public int OrganizationId { get; set; }
        public string KMPI { get; set; }
    }
    public class NeedAttentionAttribute
    {
        public int patientId { get; set; }
        public string sort { get; set; }
        public string order { get; set; }
        public int limit { get; set; }
        public int offset { get; set; }
    }
    public class NeedAttention
    {
        public string PatientComment { get; set; }
        public int PatientId { get; set; }
        public int PatientStatusCode { get; set; }
        public int PatientWorkItemsId { get; set; }
        public int ReasonCode { get; set; }
        public int ReasonCodeId { get; set; }
        public int StatusCodeId { get; set; }
    }

}
