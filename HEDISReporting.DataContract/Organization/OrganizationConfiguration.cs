﻿using HEDISReporting.DataContract.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.DataContract.Organization
{

    public class OrganizationDataSource : BaseEntity
    {
        
        public int DataSourceId { get; set; }
        public int DataType { get; set; }
        public int SourceOfData { get; set; }
        public int AcquisitionMethod { get; set; }
        public GetDataSource GetDataSource { get; set; }
        public GetDataSourceSftp GetDataSourceSftp { get; set; }
        public ReceiveDataSource ReceiveDataSource { get; set; }
        public int OrganizationId { get; set; }

        public bool IsEdit { get; set; }

    }
    public class GetDataSource
    {
        public int GetDataSourceId { get; set; }
        public int Frequency { get; set; }
        public string FrequencyDays { get; set; }
        public string FrequencyWeek { get; set; }
        public string FrequencyTime { get; set; }
        public int FileType { get; set; }
        public int DataSourceId { get; set; }
    }
    public class GetDataSourceSftp
    {
        public int DataSourceSftpId { get; set; }
        public string Host { get; set; }
        public string Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int GetDataSourceId { get; set; }
    }
    public class ReceiveDataSource
    {
        public int ReceiveDataSourceId { get; set; }
        public int Frequency { get; set; }
        public string FrequencyDays { get; set; }
        public string FrequencyWeek { get; set; }
        public string FrequencyTime { get; set; }
        public int FileType { get; set; }
        public int DataSourceId { get; set; }
        public string Directory { get; set; }
        public string DatasourceName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }

    public class OrganizationDataSourced
    {

        public int DataSourceId { get; set; }
        public int DataType { get; set; }
        public int SourceOfData { get; set; }
        public int AcquisitionMethod { get; set; }
        public int OrganizationId { get; set; }

    }
}
