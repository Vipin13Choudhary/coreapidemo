﻿using HEDISReporting.DataContract.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.DataContract.Organization
{
    public class OrganizationConfigurationListModel : ListingOperands
    {
        public int OrganizationId  {get;set;}
        public int DataSourceId { get; set; }
    }
}
