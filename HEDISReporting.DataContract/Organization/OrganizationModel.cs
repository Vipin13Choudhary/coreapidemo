﻿using HEDISReporting.DataContract.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.DataContract.Organization
{
   public class OrganizationModel:BaseEntity
    {
        public int OrganizationId { get; set; }
        public string OrganizationName { get; set; }
        public string OrganizationAddress { get; set; }
        public string OrganizationPhoneNumber { get; set; }
        public string OrganizationFax { get; set; }
        public string OrganizationWebSite { get; set; }
        public int OrganizationType { get; set; }
        public string OrganizationLogo { get; set; }
        public int CountryId { get; set; }
        public int StateId { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }

        public string OrganizationEmail { get; set; }
        public string OrganizationDisplayName { get; set; }
        public string OrganizationTaxID { get; set; }

        //  public int Vendor { get; set; }

        public int UserId { get; set; }
        public string ContactPersonName { get; set; }
        public string ContactPersonEmail { get; set; }
        public string ContactPersonPhonenumber { get; set; }
        public string Address2 { get; set; }

        public List<VendorList> Vendor { get; set; }

        public List<CountryCodeDetails> CountryCodeDetails { get; set; }

        public int RegionId { get; set; }


    }

    public class OrganizationListingOperands: ListingOperands
    {
       
        public string OrganizationId { get; set; }
    }

    public class VendorList
    {
        public int item_id { get; set; }
        public string item_text { get; set; }
    }

    public class IPToCountry
    {
        public double BeginningIP { get; set; }
        public double EndingIP { get; set; }
        public string TwoCountryCode { get; set; }
        public string CountryName { get; set; }
    }


    public class CountryCodeDetails
    {
        public string CountryCode { get; set; }
        public string InternationalDialing { get; set; }

    }


   
}
