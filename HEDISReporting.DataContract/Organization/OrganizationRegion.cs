﻿using HEDISReporting.DataContract.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.DataContract.Organization
{
    public class OrganizationRegion :BaseEntity
    {
        public int OrganizationRegionId{ get; set; }
        public string RegionName { get; set; }
        public bool IsActivate { get; set; }
        public int OrganizationId { get; set; }
        public int RegionId { get; set; }
       
    }

    public class OrgRegionListingOperands : ListingOperands
    {

        public int OrganizationId { get; set; }
        public string IsActive { get; set; }
    }
}
