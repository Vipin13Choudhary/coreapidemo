﻿using HEDISReporting.DataContract.Common;

namespace HEDISReporting.DataContract.Organization
{
   public class OrganizationClinic:BaseEntity
    {
        public int ClinicId { get; set; }
        public string ClinicName { get; set; }
        public string ClinicAddress1 { get; set; }
        public string ClinicAddress2 { get; set; }
        public string ClinicPhoneNumber { get; set; }
        public string ClinicFax { get; set; }
        public int CountryId { get; set; }
        public int StateId { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public int OrganizationId { get; set; }
        public string ContactPerson { get; set; }
        public string ContactPersonEmail { get; set; }
        public string ContactPersonPhone { get; set; }
        public string ClinicWebsite { get; set; }
        public string ClinicDisplayName { get; set; }
        public string ClinicLocationId { get; set; }
        public string RegionId { get; set; }
    }

  
}

   


















