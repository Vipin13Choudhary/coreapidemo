﻿using HEDISReporting.DataContract.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.DataContract.Organization
{
   public class OrganizationDepartment :BaseEntity
    {
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public string DepartmentAddress { get; set; }
        public string DepartmentPhoneNumber { get; set; }
        public string DepartmentFax { get; set; }
        public int CountryId { get; set; }
        public int StateId { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public int ClinicId { get; set; }
        public string DepartmentEmail { get; set; }
        public string ContactPersonEmail { get; set; }
        public string ContactPersonName { get; set; }
        public string ContactPersonPhonenumber { get; set; }
        public string DepartmentAddress2 { get; set; }
        public int UserId { get; set; }

        public int RegionId { get; set; }
    }

    public class DepartmentListOperands : ListingOperands
    {
            public string ClinicId { get; set; }
        public int organizationId { get; set; }
        
    }
}
