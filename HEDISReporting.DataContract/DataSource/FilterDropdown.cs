﻿using HEDISReporting.DataContract.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.DataContract.DataSource
{
   public class FilterDropdown
    {

        public List<DropDownListItem> MeasureList{get;set;}
        public List<DropDownListItem> VendorList{get;set;}
        public List<DropDownListItem> GendorList{get;set;}
        public List<DropDownListItem> RaceList{get;set;}
        public List<DropDownListItem> EthinicityList{get;set;}


    }
}
