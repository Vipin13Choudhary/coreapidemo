﻿using HEDISReporting.DataContract.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.DataContract.DataSource
{
    public class DataSourceModal
    {
        public List<DropDownListItem> Configuration { get; set; }
        public List<DropDownListItem> Vendor { get; set; }
        public List<DropDownListItem> Payers { get; set; }
    }
    public class DataSourceResponseModel
    {
        public int DataSourceId { get; set; }
        public int OrganizationId { get; set; }
        public string DatatType { get; set; }
        public int SourceOfData { get; set; }
        public string MethodName { get; set; }
        public string OrganizationName { get; set; }
    }
}
