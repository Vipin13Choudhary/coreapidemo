﻿using HEDISReporting.DataContract.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.DataContract.DataSource
{
  
        public class FilterNames
        {

            public DropDownListItem ProviderItem { get; set; }
            public DropDownListItem OrganizationItem { get; set; }
            public DropDownListItem ClinicItem { get; set; }
            public DropDownListItem PayerItem { get; set; }
            public DropDownListItem AssociationItem { get; set; }


        }
    }


