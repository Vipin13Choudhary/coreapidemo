﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.DataContract.Enums.MeasureTypeMaster
{
    /// <summary>
    /// values here should be same has in database table MeasureTypeMaster
    /// </summary>
    public enum MeasureTypeMasterEnum
    {
        HEDIS2018=1,
        WellCare2018,
        HEDIS2019,
        WellCare2019
    }
    public enum MasterMeasure
    {
        ABA=1,
        COL,
        BCS,
        CCS,
        CHBP
    }
}
