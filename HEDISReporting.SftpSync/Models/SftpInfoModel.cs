﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.SftpSync.Models
{
   public class SftpInfoModel
    {
        public string Domain { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Port { get; set; }
        public string DirectoryPath { get; set; }

    }
}
