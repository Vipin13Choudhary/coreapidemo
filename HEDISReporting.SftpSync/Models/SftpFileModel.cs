﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.SftpSync.Models
{
    public class SftpFileModel
    {
        public string FileName { get; set; }
        public long FileSize { get; set; }
        public DateTime FilePostedDate { get; set; }
        public string FileContent { get; set; }
    }
}
