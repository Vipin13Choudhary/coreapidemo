﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.SftpSync.Models
{
    public class SftpRequestModel
    {
        public int OrganizationId { get; set; }
        public int UserId { get; set; }
        public int FileSyncCount { get; set; }
        public List<SftpFileModel> SftpFileModels { get; set; } 
        public SftpInfoModel SftpInfoModel { get; set; }
        public int SyncLimit { get; set; }
    }
}
