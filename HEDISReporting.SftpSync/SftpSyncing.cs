﻿using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Renci.SshNet.Sftp;
using HEDISReporting.SftpSync.Models;

namespace HEDISReporting.SftpSync
{
    public class SftpSyncing
    {
        private readonly SftpInfoModel _sftpInfo;
        private readonly List<SftpFileModel> _fileModel;
        private readonly List<SftpFileModel> _syncedFiles;
        private readonly int _readfileCount;
        public SftpSyncing(SftpInfoModel sftpInfo, List<SftpFileModel> syncedFiles,int readfileCount)
        {
            this._sftpInfo = sftpInfo;
            this._fileModel = new List<SftpFileModel>();
            this._syncedFiles = syncedFiles;
            this._readfileCount = readfileCount;
        }
        public async Task<IList<SftpFileModel>> GetAllSftpFiles()
        {
         
            ConnectionInfo ConnNfo = new ConnectionInfo(_sftpInfo.Domain, Convert.ToInt32(_sftpInfo.Port), _sftpInfo.UserName,
                 new AuthenticationMethod[]{
                    new PasswordAuthenticationMethod(_sftpInfo.UserName,_sftpInfo.Password)
                 }
             );
            using (var client = new SftpClient(ConnNfo))
            { 
                client.Connect();
                IList<SftpFileModel> files = new List<SftpFileModel>();
                if (client.IsConnected)
                {
                    string[] directoryTree = _sftpInfo.DirectoryPath.Split('/');
                    if (directoryTree.Length > 0)
                    {
                        files = await TraverseDirecotry(client, client.WorkingDirectory.ToString(), _sftpInfo.DirectoryPath.Split('/'), string.Concat("/", _sftpInfo.DirectoryPath));

                    }
                }
                client.Disconnect();
                return files;
            }
        }
        public async Task<SftpClient> RetryToConnect()
        {
            ConnectionInfo ConnNfo = new ConnectionInfo(_sftpInfo.Domain, Convert.ToInt32(_sftpInfo.Port), _sftpInfo.UserName,
                 new AuthenticationMethod[]{
                    new PasswordAuthenticationMethod(_sftpInfo.UserName,_sftpInfo.Password)
                 }
             );
            using (var client = new SftpClient(ConnNfo))
            {
                client.Connect();
                return await Task.Run(() => client);
            }
        }
        private async Task<IList<SftpFileModel>> TraverseDirecotry(SftpClient client, String dirName, string[] diretoryTree, string directtoryPath)
        {

            IEnumerable<SftpFile> directory = await Task.Run(() => client.ListDirectory(dirName));
            SftpFile currentDirectory = directory.Where(x => x.IsDirectory == true && diretoryTree.Contains(x.Name)).FirstOrDefault();
            if (currentDirectory != null && (currentDirectory.FullName != directtoryPath))
            {
                await TraverseDirecotry(client, currentDirectory.FullName, diretoryTree, directtoryPath);
            }
            return await DownloadFiles(client, currentDirectory.FullName);
        }

        private async Task<IList<SftpFileModel>> DownloadFiles(SftpClient client, string dirToRead)
        {
            IEnumerable<SftpFile> directoryFiles = await Task.Run(() => client.ListDirectory(dirToRead).Where(x => x.IsRegularFile));
            List<SftpFile> filesToRead = new List<SftpFile>();
            int i = 0;
            foreach (SftpFile item in directoryFiles)
            {
                    SftpFileModel fileModel = new SftpFileModel
                    {
                        FileName = item.Name,
                        FilePostedDate = item.LastWriteTime,
                        FileSize = item.Attributes.Size,
                    };
                    // either we receive same file with different modified date or the completely new file               
                    bool oldfileCount = _syncedFiles.Any(x =>
                      (x.FileName == fileModel.FileName && x.FilePostedDate != fileModel.FilePostedDate)
                     );
                    bool newfileCount = _syncedFiles.All(x => (x.FileName != fileModel.FileName)
                     );
                     if (oldfileCount || newfileCount)
                     {
                         filesToRead.Add(item);
                     }
               
              
            }
            foreach (var file in filesToRead)
            {
                SftpFileModel fileModel = new SftpFileModel
                {
                    FileName = file.Name,
                    FilePostedDate = file.LastWriteTime,
                    FileSize = file.Attributes.Size,
                };
                string fileContent = await Task.Run(() => client.ReadAllText(file.FullName));
                fileModel.FileContent = fileContent;
                _fileModel.Add(fileModel);
                i++;
                if (i == _readfileCount)//limiting files to 300 only in one shot
                    break;
            }
            return _fileModel;
        }
    }
}
