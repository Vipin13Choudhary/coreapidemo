﻿using HEDISReporting.DataContract.Provider;
using HEDISReporting.RepositoryContact;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HEDISReporting.RepositoryContract.Provider
{
    public interface IProviderRepository: IBaseRepository
    {
        Task<dynamic> GetProviderList(ProviderListingOperands data);
        Task<dynamic> GetProviderById(int Id);
    }
}
