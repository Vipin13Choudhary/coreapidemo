﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HEDISReporting.DataContract.User;
using HEDISReporting.RepositoryContact;

namespace HEDISReporting.RepositoryContract.User
{
   public interface IUserRepository:IBaseRepository
    {
        /// <summary>
        /// IUserRepository is hiding(using keyword "new") the IBaseRepository's Healthcheck to enable call to IUserRepository's HealthCheck.
        /// </summary>
        /// <returns>HealthCheck Information</returns>
        new Task<object> HealthCheckAsync();

        /// <summary>
        /// Returns UserModel for specified userId
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<UserModel> GetUserAsync(int userId);

        Task<dynamic> GetUserMenus(int userId);
        Task<dynamic> GetUserRoles(int userId);
        Task<UserModel> getloginUserId(string aspNetUserId);
    }
}
