﻿using HEDISReporting.DataContract.Common;
using HEDISReporting.DataContract.DataSource;
using HEDISReporting.DataContract.Patients;
using HEDISReporting.DataContract.SyncModels;
using HEDISReporting.RepositoryContact;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HEDISReporting.RepositoryContract.Patient
{
    public interface IPatientRepository : IBaseRepository
    {
        Task<dynamic> GetAllPatientList(OrganizationListModel data);
        Task<dynamic> GetEthnicity(int EthnicityId);
        Task<dynamic> GetRace(int RaceId);
        Task<dynamic> GetOrganization(int OrganizationId);
        Task<dynamic> GetFilteredPatientList(int EthnicityId, int OrganizationId, int RaceId, int GenderId);
        Task<dynamic> GetGender(int GenderId);
        Task<dynamic> GetPatientChart(int paientId, int sectionId, int tabId);
        Task<dynamic> GetPatientChartTabs(int paientId, int sectionId, int tabId);
        Task<DataSourceResponseModel> GetOrganizationFolderDetail(int organizationId);
        Task<SftpHedisRequestModel> GetDataSourceDetailForSync(int organizationId);
        Task<int> ImportPatientCCDA(string base64File, int organizationID, int userID, string receivedFile);
        Task<bool> InsertSyncedFiles(string syncedFiles, int organizationId, int userId);
        Task<dynamic> GetClinics(int OrganizationId);
        Task<dynamic> GetProviders(int ClinicId);

        Task<dynamic> GetPayers();

        Task<dynamic> GetLabResultForPatient(LabListingOperands data);

        Task<dynamic> GetPatientResultForLab(int patientId);
        Task<PatientCCDAModel> GetDeIdentifiedInfo(int patipenId);
        Task<dynamic> GetStatusReasonCode();
    }
}
