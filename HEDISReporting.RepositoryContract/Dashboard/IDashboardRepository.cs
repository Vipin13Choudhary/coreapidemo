﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HEDISReporting.DataContract.Dashboard;
using HEDISReporting.DataContract.PatientChart;
using HEDISReporting.RepositoryContact;

namespace HEDISReporting.RepositoryContract.Dashboard
{
    public interface IDashboardRepository : IBaseRepository
    {
        Task<dynamic> GetAllABADetails(DashBoardFilters data);
        Task<List<dynamic>> GetAllMeasurePatientList(DashBoardFilters data);
        Task<dynamic> GetAllDropDowm(object userId);
        Task<dynamic> GetMeasureAnalysis(DashBoardFilters data);
        Task<dynamic> GetDefaultGoalsDetails(DashBoardFilters data);
        Task<dynamic> SaveDashboardGoalDetails(string xmlData);
        Task<dynamic> GetDefaultGoalsSettings(int userId);

        Task<dynamic> GetCOLAllDetails(DashBoardFilters data);
        Task<List<dynamic>> GetCOLPatientsList(DashBoardFilters data);
        Task<dynamic> GetBCSAllDetails(DashBoardFilters data);
        Task<List<dynamic>> GetBCSPatientsList(DashBoardFilters data);

        Task<dynamic> GetCBPAllDetails(DashBoardFilters data);

        Task<dynamic> GetCBPPatientsList(DashBoardFilters data);
        Task<dynamic> GetCCSAllDetails(DashBoardFilters data);
        Task<List<dynamic>> GetCCSPatientsList(DashBoardFilters data);

        Task<List<MeasureTypeMaster>> GetAllMeasureTypeMaster();
        Task<dynamic> GetABAWellCareDetails(DashBoardFilters data);
        Task<dynamic> GetBCSWellCareDetails(DashBoardFilters data);
        Task<List<dynamic>> GetMeasureWellCarePatientList(DashBoardFilters data);
        Task<dynamic> GetMeasureAnalysisWellCare(DashBoardFilters data);
        Task<dynamic> GetCOLWellCareDetails(DashBoardFilters data);

        //tes
        Task<List<dynamic>> GetCOLMeasureWellCarePatientList(DashBoardFilters data);
        Task<dynamic> GetMeasures();
        Task<dynamic> GetMeasureDetails(int Id, string MeasureTypeId);

        Task<dynamic> GetCCSWellCareDetails(DashBoardFilters data);
        Task<List<dynamic>> GetCCSMeasureWellCarePatientList(DashBoardFilters data);
        
               Task<List<dynamic>> GetBCSMeasureWellCarePatientList(DashBoardFilters data);
        Task<List<dynamic>> GetCBPMeasureWellCarePatientList(DashBoardFilters data);
        Task<dynamic> GetCBPWellCareDetails(DashBoardFilters data);
        Task<dynamic> GetMeasureGoals(int UserId, int OrganizationId);
        Task<dynamic> IUGoal(GoalDetails details);
        Task<dynamic> GetClinicPayerForOrganizaton(int LoginOrganizationId);
        Task<dynamic> SubmitMasterGoalsDetails(MasterGoal details);
        Task<dynamic> MasterSelectedMeasure(int OrganizationId, int Type);
        Task<dynamic> MasterMeasureGoals(int OrganizationId, int Type);
        Task<dynamic> MasterMeasureGoals1(int CEntityType, int CEntityId, int EntityType, int EntityId);
        Task<dynamic> GetMeasuresName(int userId);
        Task<dynamic> GetEntityGoal(int Id);
        Task<dynamic> GetEntityMeasure(int EntityType, int EntityId);
        Task<dynamic> DeleteMeasure(int Id);
        Task<dynamic> DeleteAllMeasure(int Id, int Type);

        Task<int> SetMeasuredetails(string data);

        Task<string> GetMeasureDescription(int measureId, int entityId);

        Task<List<ClaimDetails>> GetClaimPatientList(ClaimFilters data);
    }
}
