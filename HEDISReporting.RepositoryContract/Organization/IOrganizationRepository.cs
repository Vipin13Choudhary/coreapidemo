﻿using HEDISReporting.DataContract.Common;
using HEDISReporting.DataContract.DataSource;
using HEDISReporting.DataContract.Organization;
using HEDISReporting.RepositoryContact;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HEDISReporting.RepositoryContract.Organization
{
   public interface IOrganizationRepository : IBaseRepository
    {
        Task<dynamic> GetOrganizationList(OrganizationListingOperands data);
        Task<dynamic> GetAllDropDown(int OrganizationId, string  userId) ;
        Task<DataSourceResponseModel> SaveOrganizationConfiguration(string xmlData,bool IsEdit);
        Task<int> SaveOrganization(string xmlData, int OrganizationId);
        Task<int> SaveOrganizationDepartment(string xmlData,int DepartmentId);
        Task<dynamic> GetDepartmentList(DepartmentListOperands data);
        Task<int> SaveOrgnizationClinic(string xmlData,int clinicId);
        Task<dynamic> GetCountriesStates(int countryId);
        Task<dynamic> GetOrganizationConfigurationList(OrganizationConfigurationListModel filterItems);
        Task<dynamic> GetOrganizationsClinicsList(OrganizationConfigurationListModel filterItems);
        Task<dynamic> GetOrganizationRegionList(OrgRegionListingOperands data);
        Task<int> SaveOrganizationRegion(string data,int regionId);
        Task<dynamic> GetDataSourceDropDowns(int ConfigurationTypeId);
        Task<int> DeleteOrganizationData(DeleteData data);
        Task<dynamic> GetOrganizationDetailById(int organizationId);
        Task<dynamic> GetOrganizationClinicDetailById(int clinicId);
        Task<dynamic> GetOrganizationDepartmentDetailById(int departmentId);
        Task<dynamic> GetOrganizationsDataSourceDetail(int organizationId,int DataSourceId);
        Task<dynamic> GetCountryISDFromIP(double longIP);
        Task<dynamic> GetAllEntity(int TypeId,int OrganizationId);
        Task<dynamic> GetOrganizationList_WinService();
    }
}
