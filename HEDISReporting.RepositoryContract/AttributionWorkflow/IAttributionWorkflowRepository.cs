﻿using HEDISReporting.DataContract.AttributeExcel;
using HEDISReporting.DataContract.Dashboard;
using HEDISReporting.DataContract.DataSource;
using HEDISReporting.RepositoryContact;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace HEDISReporting.RepositoryContract.AttributionWorkflow
{
    public interface IAttributionWorkflowRepository : IBaseRepository
    {
        dynamic ReadExcelData(FileInfo file, int OrganizationId, int PayerId,int UserId);
        Task<dynamic> OrganizationPayerList();
        Task<dynamic> GetAttentionList(NeedAttentionAttribute needAttention);
        Task<dynamic> PatientAttributionStatus(DashBoardFilters data);
        Task<dynamic> UpdateNeedAttentionata(List<NeedAttention> needAttention);
        Task<dynamic> UpdateCareGapdata(List<NeedAttention> needAttention);
        Task<dynamic> UpdateNeedattentiondata(List<NeedAttention> needAttention);


    }
}
