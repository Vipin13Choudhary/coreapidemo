﻿using HEDISReporting.DataContract;
using HEDISReporting.DataContract.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace HEDISReporting.RepositoryContact
{
    /// <summary>
    ///  Interafce to work with Repository.
    /// </summary>
    public interface IBaseRepository
    {
        /// <summary>
        /// Gets HealthCheck for Base Repository.
        /// </summary>
        /// <returns>UTC Datetime</returns>
        Task<object> HealthCheckAsync();

        /// <summary>
        /// Returns one (first) element of the result set
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        T GetFirstOrDefault<T>(string sql, object parameters = null, int? commandTimeout = null, CommandType? commandType = null);

        /// <summary>
        /// Returns one (first) element of the result set
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        Task<T> GetFirstOrDefaultAsync<T>(string sql, object parameters = null, int? commandTimeout = null, CommandType? commandType = null);

        /// <summary>
        /// Returns list of elements
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        IEnumerable<T> Get<T>(string sql, object parameters = null, int? commandTimeout = null, CommandType? commandType = null);

        /// <summary>
        /// Returns list of elements
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        Task<IEnumerable<T>> GetAsync<T>(string sql, object parameters = null, int? commandTimeout = null, CommandType? commandType = null);

        /// <summary>
        /// Inserts an entity and returns the id of the newly inserted entity
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        int Add(string sql, object parameters = null, int? commandTimeout = null, CommandType? commandType = null);

        /// <summary>
        /// Inserts an entity and returns the id of the newly inserted entity
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        Task<int> AddAsync(string sql, object parameters = null, int? commandTimeout = null, CommandType? commandType = null);

        /// <summary>
        /// Returns multiple results based on the types specified.
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        Task<dynamic> GetMultipleAsync<T1, T2>(string sql, object parameters = null, int? commandTimeout = null, CommandType? commandType = null);

        /// <summary>
        /// Registers the list of properties which you would like to map to it's corresponsing sql column name.
        /// This is usually used where your object's property name is different from the underline sql column name.
        /// </summary>
        /// <typeparam name="T">The type of the object</typeparam>
        /// <param name="columns">list fo columns</param>
        void RegisterColumnMappings<T>(IEnumerable<SqlColumnMapping> columns);
        /// <summary>
        /// Returns multiple with 4 arguments
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        Task<dynamic> GetMultipleAsync<T1, T2, T3, T4>(string sql, object parameters = null, int? commandTimeout = null, CommandType? commandType = null);
        /// <summary>
        /// Returns multiple with 3 arguments
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        Task<dynamic> GetMultipleAsync<T1, T2, T3>(string sql, object parameters = null, int? commandTimeout = null, CommandType? commandType = null);
        Task SaveExceptionLogs(int status, string stackTrace, string userName, string userId, int section, string fileName, string dataXML);


    }
}
