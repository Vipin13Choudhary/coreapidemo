﻿using HEDISReporting.DataContract.Dashboard;
using HEDISReporting.RepositoryContact;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HEDISReporting.RepositoryContract.CareGap
{
   public interface ICareGapRepository : IBaseRepository
    {
        Task<List<dynamic>> GetABACareGap2018(DashBoardFilters data);
        Task<List<dynamic>> GetAllCareGapPatientList(DashBoardFilters data);
        Task<List<dynamic>> GetImunizationPatientList(DashBoardFilters data);
        
    }
}
