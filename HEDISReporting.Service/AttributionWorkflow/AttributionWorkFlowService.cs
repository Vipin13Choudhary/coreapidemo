﻿using HEDISReporting.Common.ExtensionMethods;
using HEDISReporting.DataContract.AttributeExcel;
using HEDISReporting.DataContract.Dashboard;
using HEDISReporting.RepositoryContract.AttributionWorkflow;
using HEDISReporting.RepositoryContract.CareGap;
using HEDISReporting.ServiceContract.AttributionWorkflow;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace HEDISReporting.Service.AttributionWorkflow
{
    public class AttributionWorkflowService : BaseService, IAttributionWorkflowService
    {
        private readonly IAttributionWorkflowRepository _attributionWorkflowRepository;
        private readonly IHostingEnvironment _hostingEnvironment;
        public AttributionWorkflowService(IAttributionWorkflowRepository attributionWorkflowRepository, IConfiguration configuration, IHostingEnvironment hostingEnvironment) :base(attributionWorkflowRepository,configuration)
        {
            _attributionWorkflowRepository = attributionWorkflowRepository;
            _hostingEnvironment = hostingEnvironment;
        }

        public async Task<dynamic> OrganizationPayerList()
        {
            dynamic organizationPayerList = await _attributionWorkflowRepository.OrganizationPayerList();
            return organizationPayerList;
        }

        public dynamic ReadExcelData(IFormFile file,string filename)
        {
            string Url = SaveFile.storeAnyFile(file);
            string FileName = file.FileName.Split("#")[0];
            string sWebRootFolder = _hostingEnvironment.WebRootPath;
            List<ExcelData> list = new List<ExcelData>();
            FileInfo files = new FileInfo(Path.Combine(sWebRootFolder, @"ExcelFile\"+FileName));
            //If File verison is less the 2004 or file have .xls extension then we will convert this file .xlsx
            //if (FileName.Split('.')[1] == "xls")
            //    files = SaveFile.ConvertxlsToxlsx(files.ToString(), FileName);
            //End
            dynamic excelDatas=_attributionWorkflowRepository.ReadExcelData(files, Convert.ToInt32(filename.Split("#")[1]), Convert.ToInt32(filename.Split("#")[2]), Convert.ToInt32(filename.Split("#")[3]));
            return excelDatas;
        }
        public async Task<dynamic> GetNeedAttentionList(NeedAttentionAttribute needAttention)
        {
            dynamic AttentionList = await _attributionWorkflowRepository.GetAttentionList(needAttention);
            return AttentionList;
        }
        public async Task<dynamic> PatientAttributionStatus(DashBoardFilters data)
        {
            dynamic AttentionList = await _attributionWorkflowRepository.PatientAttributionStatus(data);
            return AttentionList;
        }

        public async Task<dynamic> UpdateNeedAttentionata(List<NeedAttention> needAttention)
        {
            dynamic AttentionList = await _attributionWorkflowRepository.UpdateNeedAttentionata(needAttention);
            return AttentionList;
        }
        public async Task<dynamic> UpdateCareGapdata(List<NeedAttention> needAttention)
        {
            dynamic AttentionList = await _attributionWorkflowRepository.UpdateCareGapdata(needAttention);
            return AttentionList;
        }

        public async Task<dynamic> UpdateNeedattentiondata(List<NeedAttention> needAttention)
        {
            dynamic AttentionList = await _attributionWorkflowRepository.UpdateNeedattentiondata(needAttention);
            return AttentionList;
        }

    }
}
