﻿using HEDISReporting.Common.CustomExceptions;
using HEDISReporting.Common.Response;
using HEDISReporting.DataContract.ApiResponse;
using HEDISReporting.DataContract.Common;
using HEDISReporting.DataContract.DataSource;


using HEDISReporting.DataContract.PatientChart;
using HEDISReporting.DataContract.Patients;
using HEDISReporting.DataContract.SyncModels;
using HEDISReporting.RepositoryContract.Patient;
using HEDISReporting.ServiceContract.Patient;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using SelectPdf;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace HEDISReporting.Service.Patient
{
    public class PatientService : BaseService, IPatientService
    {
        private readonly IPatientRepository _patientRepository;
        private readonly IHostingEnvironment _hostingEnvironment;
        readonly IConfiguration _configuration;

        public PatientService(IHostingEnvironment hostingEnvironment,IPatientRepository patientRepository, IConfiguration configuration) : base(patientRepository, configuration)
        {
            _patientRepository = patientRepository;
            _hostingEnvironment = hostingEnvironment;
            _configuration = configuration;
        }

        public async Task<Response> ImportPatientCCDA(string file, int OrganizationId, int UserId, string receivedFile)//, TokenModel token
        {
            Response response = new Response();
            //try
            //{

            //    // string base64File = file;

                if (!string.IsNullOrEmpty(file))
                {
                    var recordId = await _patientRepository.ImportPatientCCDA(file, OrganizationId, UserId, receivedFile);

                    if (recordId >= 0)
                    {
                        response.Data = recordId.ToString();
                        response.Message = CommonResponse.Res_CCDASuccess;
                        response.StatusCode = (int)HttpStatusCode.OK;
                    }
                    else
                    {
                        response.Data = recordId.ToString();
                        response.Message = CommonResponse.Res_CCDAError;
                        response.StatusCode = (int)HttpStatusCode.InternalServerError;

                    }
                }
                else
                {
                    response.Data = "";
                    response.Message = CommonResponse.Res_InvalidCredential;
                    response.StatusCode = (int)HttpStatusCode.NotAcceptable;
                }
            //}
            //catch (Exception Ex)
            //{
               
            //    //throw new HEDISCustomException(Ex.Message);
            //    response.Data = "0";
            //    response.Message = CommonResponse.Res_ServerError;
            //    response.StatusCode = (int)HttpStatusCode.InternalServerError;
            //}
            return response;
        }

        public async Task<dynamic> GetAllPatientList(OrganizationListModel data)
        {
            return await _patientRepository.GetAllPatientList(data);
        }
        public async Task<dynamic> GetEthnicity(int EthnicityId)
        {
            dynamic Ethnicity = await _patientRepository.GetEthnicity(EthnicityId);
            List<DropDownListItem> ethnicity = Ethnicity.Item1;
            return new { ethnicity };
        }

        public async Task<dynamic> GetRace(int RaceId)
        {
            dynamic race = await _patientRepository.GetRace(RaceId);
            List<DropDownListItem> Race = race.Item1;
            return new { Race };
        }
        public async Task<dynamic> GetGender(int GenderId)
        {
            dynamic Gender = await _patientRepository.GetGender(GenderId);
            List<DropDownListItem> gender = Gender.Item1;
            return new { gender };
        }

        public async Task<dynamic> GetOrganization(int OrganizationId)
        {
            dynamic organization = await _patientRepository.GetOrganization(OrganizationId);
            List<DropDownListItem> Organization = organization.Item1;
            return new { Organization };
        }
        public async Task<dynamic> GetFilteredPatientList(int EthnicityId, int OrganizationId, int RaceId, int GenderId)
        {
            return await _patientRepository.GetFilteredPatientList(EthnicityId, OrganizationId, RaceId, GenderId);
        }
        public async Task<dynamic> GetPatientChart(int paientId, int sectionId, int tabId)
        {
            dynamic patientDetail= await _patientRepository.GetPatientChart(paientId, sectionId, tabId);
            PatientChartModel patientChartModel = new PatientChartModel();
            patientChartModel.patientDemographicsModel= ((List<PatientDemographicsModel>)patientDetail.Item1).Count!=0? patientDetail.Item1[0]:new PatientDemographicsModel();
            //managedPatientdetail.Item1 = patientDetail.Item1;
            List<SnomedCodeModel> snomedCodeModel = ((List<SnomedCodeModel>)patientDetail.Item2).Count != 0 ? patientDetail.Item2 : new List<SnomedCodeModel>();
            List<SnomedIcdMap> snomedIcdMap = ((List<SnomedIcdMap>)patientDetail.Item3).Count != 0 ? patientDetail.Item3 : new List<SnomedIcdMap>();
            foreach (var snomedCode in snomedCodeModel)
            {
                snomedCode.SnomedIcdMap = new List<SnomedIcdMap>();
                snomedCode.SnomedIcdMap=snomedIcdMap.Where(x => x.SnomedCode == snomedCode.SnomedCode).ToList();
            }
            patientChartModel.snomedCodeModels = snomedCodeModel;
            patientChartModel.patientLabTest = patientDetail.Item4??new List<PatientLabTest>();
            patientChartModel.patientAllergies = patientDetail.Item5??new List<PatientAllergies>();
            return patientChartModel;
            //sftpRequestModel.getSource = ((List<GetSource>)dataSourceDetail.Item1).Count != 0 ? dataSourceDetail.Item1[0] : new GetSource();
        }
        public async Task<dynamic> GetPatientChartTabs(int paientId, int sectionId, int tabId)
        {
            dynamic patientDetail= await _patientRepository.GetPatientChartTabs(paientId, sectionId, tabId);
            PatientChartModel patientChartModel = new PatientChartModel();
            List<SnomedCodeModel> snomedCodeModel = ((List<SnomedCodeModel>)patientDetail.Item1).Count != 0 ? patientDetail.Item1 : new List<SnomedCodeModel>();
            List<SnomedIcdMap> snomedIcdMap = ((List<SnomedIcdMap>)patientDetail.Item2).Count != 0 ? patientDetail.Item2 : new List<SnomedIcdMap>();
            foreach (var snomedCode in snomedCodeModel)
            {
                snomedCode.SnomedIcdMap = new List<SnomedIcdMap>();
                snomedCode.SnomedIcdMap = snomedIcdMap.Where(x => x.SnomedCode == snomedCode.SnomedCode).ToList();
            }
            patientChartModel.snomedCodeModels = snomedCodeModel;
            patientChartModel.patientLabTest = patientDetail.Item3?? new List<PatientLabTest>();
            patientChartModel.patientAllergies = patientDetail.Item4?? new List<PatientLabTest>();
            //List<PatientClaimDetails> model = new List<PatientClaimDetails>();
            //var models = await _patientRepository.GetClaimPatientList(data);
            //patientChartModel.PatientClaim= _dashboardRepository.GetClaimPatientList(data);
            //foreach (var item in model)
            //{
            //    List<PatienClaimChildrenDetails> obj = new List<PatienClaimChildrenDetails>();
            //    obj.Add(new PatienClaimChildrenDetails()
            //    {
            //        Charges = item.Charges,
            //        ClinicName = item.ClinicName,
            //        Unit = item.Unit,
            //        ServiceName = item.ServiceName,
            //        DiagnosisDate = item.DiagnosisDate,
            //        PatientId = item.PatientId
            //    });
            //    model.Add(new PatientClaimDetails()
            //    {
            //        PatientId = item.PatientId,
            //        PayerName = item.PayerName,
            //        Createddate = item.DiagnosisDate,
            //        DiagnosisDate = item.DiagnosisDate,
            //        PatienClaimChildrenDetails = obj
            //    });
            //}
            return patientChartModel;
        }
        public async Task<DataSourceResponseModel> GetOrganizationFolderDetail(int organizationId)
        {
            return await _patientRepository.GetOrganizationFolderDetail(organizationId);
        }
        public async Task<SftpHedisRequestModel> GetDataSourceDetailForSync(int organizationId)
        {
            return await _patientRepository.GetDataSourceDetailForSync(organizationId);
        }
        public  async Task<bool> InsertSyncedFiles(string syncedFiles, int organizationId, int userId)
        {
            return await _patientRepository.InsertSyncedFiles(syncedFiles, organizationId, userId);
        }

        public async Task<dynamic> GetClinics(int OrganizationId)
        {
            //dynamic organization = await _patientRepository.GetClinics(OrganizationId);
            //List<DropDownListItem> Organization = organization.Item1;
            //return new { Organization };

            dynamic organization = await _patientRepository.GetClinics(OrganizationId);
            //List<DropDownListItem> Organization = organization.Item1;
            FilterDropdown Fdata = new FilterDropdown();
            Fdata.RaceList = organization.Item1;
            Fdata.EthinicityList = organization.Item2;
            return Fdata;
        }
        public async Task<dynamic> GetProviders(int ClinicId)
        {
            dynamic organization = await _patientRepository.GetProviders(ClinicId);
            List<DropDownListItem> Organization = organization.Item1;
            return new { Organization };
                  }
    
    public async Task<dynamic> GetPayers()
    {
        dynamic payers = await _patientRepository.GetPayers();
        List<DropDownListItem> Payers = payers.Item1;
        return new { Payers };
    }


        public async Task<dynamic> GetLabFromPatients(LabListingOperands data)
        {
            return await _patientRepository.GetLabResultForPatient(data);
        }
        #region Get Patient All Status and Region
        public async Task<dynamic> GetStatusReasonCode()
        {
            return await _patientRepository.GetStatusReasonCode();
        }
        #endregion
        public async Task<dynamic> GetPatientsFromLab(int patientId)
        {
            return await _patientRepository.GetPatientResultForLab(patientId);
        }
        
        public async Task<bool> GetDeIdentifiedInfo(int patientid, string filePath)
        {
            string plainXML = File.ReadAllText(filePath);
            PatientCCDAModel deIdentifiedInfo = await _patientRepository.GetDeIdentifiedInfo(patientid);
            // Compile a standard XPath expression
            var doc1 = new XmlDocument();
            doc1.LoadXml(plainXML);
            XmlNodeList patientinfo = doc1.GetElementsByTagName("patient");
            //This Below code is used to replace the url in CCDA file 
            XmlProcessingInstruction xmlstylesheetNode =(XmlProcessingInstruction)doc1.SelectSingleNode("/processing-instruction('xml-stylesheet')");
            Regex regex = new Regex("href=[\"']?((?:.(?![\"']?\\s+(?:\\S+)=|[>\"']))+.)[\"']?",RegexOptions.IgnoreCase | RegexOptions.Singleline);
            string href = "";
            var DomainUrl = _configuration.GetSection("Domain:DomainUrl").Value;
            if (xmlstylesheetNode != null)
            {
                XmlElement piEl = (XmlElement)doc1.ReadNode(XmlReader.Create(new StringReader("<pi " + xmlstylesheetNode.Value + "/>")));
                 href = piEl.GetAttribute("href");
            }
            xmlstylesheetNode.Data = regex.Replace("type='text/xsl' href='"+ href + "'", "href='"+ DomainUrl + "/xlssheet/cda_hedisxsl.xsl'");
            //End
            XmlNodeList patientdob = doc1.GetElementsByTagName("birthTime");
            XmlNodeList patientaddr = doc1.GetElementsByTagName("addr");
            XmlNodeList patientids = doc1.GetElementsByTagName("id");
            XmlNodeList docdate = doc1.GetElementsByTagName("effectiveTime");
            XmlNodeList telecomnodelist = doc1.GetElementsByTagName("telecom");
            XmlNode namenode = patientinfo[0].FirstChild;
            XmlNode noden = patientaddr[0];
            XmlNode dob = patientdob[0];
            string dobs = dob.Attributes["value"].Value;
            int year = Convert.ToInt32(dobs.Substring(0, 4));
            int month = Convert.ToInt32(dobs.Substring(4, 2));
            int day = Convert.ToInt32(dobs.Substring(6, 2));
            XmlNode ptaddr = patientaddr[0];
            int i = 0;
            DateTime datedob = new DateTime(year, month, day).AddDays(5);
            DateTime ptdob = datedob;
            string modifieddate = String.Format("{0:yyyy/MM/dd}", ptdob).Replace("/", "").Replace("-", "");
            dob.Attributes["value"].Value = String.Format("{0:yyyy/MM/dd}", deIdentifiedInfo.DOB).Replace("/", "").Replace("-", "");// modifieddate;

            foreach (XmlNode telecomnode in telecomnodelist)
            {
                if (telecomnode.ParentNode.Name == "patientRole")
                {
                    telecomnode.Attributes["value"].Value = "tel:" + deIdentifiedInfo.Phone;
                }
            }

            foreach (XmlNode docdt in docdate)
            {
                if (docdt.ParentNode.Name == "ClinicalDocument")
                {
                    string documentdate = docdt.Attributes["value"].Value;
                    int docyear = Convert.ToInt32(documentdate.Substring(0, 4));
                    int docmonth = Convert.ToInt32(documentdate.Substring(4, 2));
                    int docday = Convert.ToInt32(documentdate.Substring(6, 2));
                    DateTime datedoc = new DateTime(docyear, docmonth, docday).AddDays(5);
                    DateTime ptdoc = datedoc;
                    string modifieddatedoc = String.Format("{0:yyyy/MM/dd}", ptdoc).Replace("/", "").Replace("-", "");
                    docdt.Attributes["value"].Value = modifieddatedoc;
                    break;
                }
            }
            foreach (XmlNode ptid in patientids)
            {
                foreach (XmlAttribute attribute in ptid.Attributes)
                {
                    if (attribute.Name == "root" && (attribute.Value == "2.16.840.1.113883.3.109.3.4452.4.1.1.80210.2.2.1" || attribute.Value == "2.16.840.1.113883.19.5.99999.1") && ptid.ParentNode.Name == "ClinicalDocument")
                    {
                        ptid.Attributes["extension"].Value = "123456789";
                        break;
                    }
                    if (attribute.Name == "root" && (attribute.Value == "2.16.840.1.113883.3.109.3.4452.4.1.1.80210.2.1" || attribute.Value == "2.16.840.1.113883.4.1") && ptid.ParentNode.Name == "patientRole")
                    {
                        ptid.Attributes["extension"].Value = "123456789";
                        break;
                    }
                }
            }
            foreach (XmlNode givenname in namenode)
            {
                if (givenname.Name == "given")
                {
                    if (i == 0)
                    {
                        givenname.InnerText = deIdentifiedInfo.FirstName;
                    }
                    i++;
                }
                if (givenname.Name == "family")
                {
                    givenname.InnerText = deIdentifiedInfo.LastName;
                }
            }
            foreach (XmlNode addrname in ptaddr)
            {
                if (addrname.Name == "streetAddressLine")
                {
                    addrname.InnerText = deIdentifiedInfo.Address1;

                }
                if (addrname.Name == "city")
                {
                    addrname.InnerText = deIdentifiedInfo.City;
                }
                if (addrname.Name == "postalCode")
                {
                    addrname.InnerText = deIdentifiedInfo.Zip;
                }
            }
            doc1.Save(filePath);
            return true;
        }
        [BindProperty]
        public string TxtUrl { get; set; }

        [BindProperty]
        public bool showHeaderOnFirstPage { get; set; }

        [BindProperty]
        public bool showHeaderOnOddPages { get; set; }

        [BindProperty]
        public bool showHeaderOnEvenPages { get; set; }

        [BindProperty]
        public int headerHeight { get; set; }

        [BindProperty]
        public bool showFooterOnFirstPage { get; set; }

        [BindProperty]
        public bool showFooterOnOddPages { get; set; }

        [BindProperty]
        public bool showFooterOnEvenPages { get; set; }

        [BindProperty]
        public int footerHeight { get; set; }

        [BindProperty]
        public bool showPageNumbering { get; set; }
        [BindProperty]
        public string TxtHtmlCode { get; set; }

        [BindProperty]
        public string TxtBaseUrl { get; set; }

        [BindProperty]
        public string DdlPageSize { get; set; }
        public List<SelectListItem> PageSizes { get; } = new List<SelectListItem>
        {
            new SelectListItem { Value = "A1", Text = "A1" },
            new SelectListItem { Value = "A2", Text = "A2" },
            new SelectListItem { Value = "A3", Text = "A3" },
            new SelectListItem { Value = "A4", Text = "A4" },
            new SelectListItem { Value = "A5", Text = "A5" },
            new SelectListItem { Value = "Letter", Text = "Letter" },
            new SelectListItem { Value = "HalfLetter", Text = "HalfLetter" },
            new SelectListItem { Value = "Ledger", Text = "Ledger" },
            new SelectListItem { Value = "Legal", Text = "Legal" },
        };

        [BindProperty]
        public string DdlPageOrientation { get; set; }

        public List<SelectListItem> PageOrientations { get; } = new List<SelectListItem>
        {
            new SelectListItem { Value = "Portrait", Text = "Portrait" },
            new SelectListItem { Value = "Landscape", Text = "Landscape" },
        };

        [BindProperty]
        public string TxtWidth { get; set; }

        [BindProperty]
        public string TxtHeight { get; set; }

        public async Task<string> GetPDF(PatientClaimDetails parentClaimDetail)
        {
            PdfPageSize pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize),
               "A4", true);
            PdfPageOrientation pdfOrientation =
              (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation),
              "Portrait", true);
            int webPageWidth = 1024;
            try
            {
                webPageWidth = Convert.ToInt32(webPageWidth);
            }
            catch { }

            int webPageHeight = 0;
            try
            {
                webPageHeight = Convert.ToInt32(webPageHeight);
            }
            catch { }
            Guid guid = Guid.NewGuid();
            
            string FileName = guid + ".pdf";//parentClaimDetail.PatientName.Trim(' ')+parentClaimDetail.PatientClaimChildrenDetails[0].DiagnosisDate;
            string DirectoryName = Path.Combine(_hostingEnvironment.WebRootPath, "pdfstorage");
            string ImagePath = Path.Combine(_hostingEnvironment.WebRootPath, "PDFTemplate\\qrcode.jpg");
            string pdffilepath=  Path.Combine(_hostingEnvironment.WebRootPath, "pdfstorage" + '\\' + FileName);
            bool exist = System.IO.Directory.Exists(DirectoryName);
            if (!exist)
                System.IO.Directory.CreateDirectory(DirectoryName);
           
            FileInfo files = new FileInfo(Path.Combine(DirectoryName, FileName));
            HtmlToPdf converter = new HtmlToPdf();
            //converter.Options.DisplayHeader = true;
            //  converter.Options.DisplayFooter = true;
            converter.Options.AutoFitHeight = HtmlToPdfPageFitMode.AutoFit;
            //converter.Options.AutoFitWidth = HtmlToPdfPageFitMode.AutoFit;
            converter.Options.PdfPageSize = PdfPageSize.A4;
            converter.Options.MarginLeft = 10;
            converter.Options.MarginTop = 3;
            converter.Options.MarginBottom = 3;
            //converter.Options.MarginRight = 1;
            converter.Options.MarginRight = 10;
            converter.Options.MinPageLoadTime = 1;
            converter.Options.MaxPageLoadTime = 30;
            converter.Options.PdfPageOrientation = PdfPageOrientation.Portrait;
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(Path.Combine(_hostingEnvironment.WebRootPath, "PDFTemplate" + '\\' + @"pdftemplate.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{PatientName}", parentClaimDetail.PatientName);
            //string pdftemplate = Path.Combine(_hostingEnvironment.WebRootPath, "PDFTemplate" + '\\' + @"pdftemplate.html");
            //var htmlString = File.ReadAllText(pdftemplate);
            //htmlString.Replace("{PatientName}", parentClaimDetail.PatientName);
            PdfDocument doc = converter.ConvertHtmlString(body.ToString(), pdffilepath);
            //PdfFont font1 = doc.AddFont(PdfStandardFont.Helvetica);

            //font1.Size = 20;
            // save pdf document
            byte[] pdf = doc.Save();
        

            // close pdf document

            File.WriteAllBytes(pdffilepath, pdf);
            doc.Close();

            byte[] pdfBytes = File.ReadAllBytes(pdffilepath);
             var pdfBase64 = Convert.ToBase64String(pdfBytes);
            // return resulted pdf document
            //FileResult fileResult = new FileContentResult(pdf, "application/pdf");
            //fileResult.FileDownloadName = "NewPDF.pdf";
             return pdfBase64;

        }





    }

}
    

