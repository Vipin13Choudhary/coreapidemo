﻿using HEDISReporting.Common;
using HEDISReporting.Common.Enum;
using HEDISReporting.DataContract.Dashboard;
using HEDISReporting.DataContract.DataSource;
using HEDISReporting.RepositoryContract.Dashboard;
using HEDISReporting.ServiceContract.Dashboard;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using HEDISReporting.DataContract.Common;
using HEDISReporting.DataContract.Enums.MeasureTypeMaster;
using HEDISReporting.DataContract.PatientChart;

namespace HEDISReporting.Service.Dashboard
{
    public class DashboardService : BaseService, IDashboardService
    {
        private readonly IDashboardRepository _dashboardRepository;

        public DashboardService(IDashboardRepository dashboardRepository, IConfiguration configuration) : base(dashboardRepository, configuration)
        {
            _dashboardRepository = dashboardRepository;
        }


        public async Task<MeasureData> GetAllABADetails(DashBoardFilters data)
        {
            dynamic result = await _dashboardRepository.GetAllABADetails(data);
            MeasureData Mdata = new MeasureData();
            //Mdata.AllEligiblePatient = result.Item1;
            //Mdata.QualifiedPatient = result.Item2;
            //Mdata.NonQualifiedPatient = result.Item3;
            Mdata.NumericalData = result.Item1;
            Mdata.Result = result.Item2;

            return Mdata;
        }

        #region get all measures patient list
        public async Task<dynamic> GetAllMeasurePatientList(DashBoardFilters data)
        {
            //TimeSpan ts = new TimeSpan(24, 0, 0);
            //data.PeriodStart = data.PeriodStart.Add(ts);
            //data.PeriodEnd = data.PeriodEnd.Add(ts);
            //data.PeriodEnd=data.PeriodEnd.AddDays(-1);
            //data.PeriodStart=data.PeriodStart.AddDays(-1);
            // return null;
            return await _dashboardRepository.GetAllMeasurePatientList(data);
        }



        #endregion

        #region get all drop down for the filter dashboard
        public async Task<FilterDropdown> GetAllDropDowm(object userId)
        {
            dynamic result = await _dashboardRepository.GetAllDropDowm(userId);
            FilterDropdown Fdata = new FilterDropdown();
            Fdata.RaceList = result.Item1;
            Fdata.EthinicityList = result.Item2;
            Fdata.GendorList = result.Item3;
            Fdata.VendorList = result.Item4;
            Fdata.MeasureList = result.Item5;

            return Fdata;
        }

        #endregion

        #region get data for measure graph
        public async Task<MeasureAnalysisDetail> GetMeasureAnalysis(DashBoardFilters data)
        {
            dynamic result = await _dashboardRepository.GetMeasureAnalysis(data);
            MeasureAnalysisDetail Mdata = new MeasureAnalysisDetail();
            Mdata.CurrentData = result.Item1;
            Mdata.PreviousData = result.Item2;

            return Mdata;
        }
        #endregion

        #region get default goals details

        public async Task<GoalDashboard> GetDefaultGoalsDetails(DashBoardFilters data)
        {
            GoalDashboard goal = new GoalDashboard();
            dynamic result = await _dashboardRepository.GetDefaultGoalsDetails(data);
            goal.AssociationGoal = result.Item1;
            goal.CorporateGoal = result.Item2;
            goal.ClinicGoal = result.Item3;
            goal.PayerGoal = result.Item4;
            goal.ProviderGoal = result.Item5;
            goal.UserDashboardSetting = result.DefaultSetting;

            return goal;
        }

        #endregion

        #region save goal details

        public async Task<int> SaveDashboardGoalDetails(SavedDashboardGoalDetails data)
        {


            dynamic result = await _dashboardRepository.GetDefaultGoalsSettings(data.UserId);
            SavedDashboardGoalDetails settingDetails = result[0];

            //if (settingDetails.Count > 0)
            //{
            //    data.ClinicGoal = settingDetails[0].ClinicGoal;
            //    data.AssociationGoal = settingDetails[0].AssociationGoal;
            //    data.CorporateGoal = settingDetails[0].CorporateGoal;
            //    data.PayerGoal = settingDetails[0].PayerGoal;
            //    data.ProviderGoal = settingDetails[0].ProviderGoal;
            //}
            //else
            //{
            //    data.ClinicGoal = 0;
            //    data.AssociationGoal = 1;
            //    data.CorporateGoal = 1;
            //    data.PayerGoal = 0;
            //    data.ProviderGoal = 0;
            //}

            if (data.Case == "Add")
                AddDashboardGoals(data, settingDetails);
            if (data.Case == "Remove")
            {
                data.ClinicGoal = data.ClinicGoal == 0 ? settingDetails.ClinicGoal : data.ClinicGoal;
                data.CorporateGoal = data.CorporateGoal == 0 ? settingDetails.CorporateGoal : data.CorporateGoal;
                data.PayerGoal = data.PayerGoal == 0 ? settingDetails.PayerGoal : data.PayerGoal;
                data.ProviderGoal = data.ProviderGoal == 0 ? settingDetails.ProviderGoal : data.ProviderGoal;

                RemoveDashboardGolas(data, settingDetails);
            }



            string xmlData = XmlHelper.GetAsXml<SavedDashboardGoalDetails>(data);
            int resultdata = await _dashboardRepository.SaveDashboardGoalDetails(xmlData);
            return 1;
        }

        private static void RemoveDashboardGolas(SavedDashboardGoalDetails data, SavedDashboardGoalDetails oldSettings)
        {

            if (data.CorporateGoal == Convert.ToInt32(DashboardDetails.CopGoal) && data.Case.Contains("Remove"))
            {
                data.CorporateGoal = 0;
            }
            if (data.ProviderGoal == Convert.ToInt32(DashboardDetails.ProGoal) && data.Case.Contains("Remove"))
            {
                data.ProviderGoal = 0;
            }
            if (data.PayerGoal == Convert.ToInt32(DashboardDetails.PayGoal) && data.Case.Contains("Remove"))
            {
                data.PayerGoal = 0;
            }
            if (data.ClinicGoal == Convert.ToInt32(DashboardDetails.ClinicGoal) && data.Case.Contains("Remove"))
            {
                data.ClinicGoal = 0;
            }
            //if (data.SelectedGoal.Contains(Convert.ToInt32(DashboardDetails.AssGoal)) && (data.Case.Contains("Remove")))
            //{
            //    data.AssociationGoal = 0;
            //}
            //if (data.SelectedGoal.Contains(Convert.ToInt32(DashboardDetails.CopGoal)) && (data.Case.Contains("Remove")))
            //{
            //    data.CorporateGoal = 0;
            //}
            //if (data.SelectedGoal.Contains(Convert.ToInt32(DashboardDetails.PayGoal)) && (data.Case.Contains("Remove")))
            //{
            //    data.PayerGoal = 0;
            //}
            //if (data.SelectedGoal.Contains(Convert.ToInt32(DashboardDetails.ProGoal)) && (data.Case.Contains("Remove")))
            //{
            //    data.ProviderGoal = 0;
            //}
            //if (data.SelectedGoal.Contains(Convert.ToInt32(DashboardDetails.ClinicGoal)) && (data.Case.Contains("Remove")))
            //{
            //    data.ClinicGoal = 0;
            //}
        }

        private static void AddDashboardGoals(SavedDashboardGoalDetails data, SavedDashboardGoalDetails oldSettings)
        {
            if (data.CorporateGoal == oldSettings.CorporateGoal && data.Case.Contains("Add"))
            {
                data.CorporateGoal = oldSettings.CorporateGoal;
            }
            if (data.ProviderGoal == oldSettings.ProviderGoal && data.Case.Contains("Add"))
            {
                data.ProviderGoal = oldSettings.ProviderGoal;
            }
            if (data.PayerGoal == oldSettings.PayerGoal && data.Case.Contains("Add"))
            {
                data.PayerGoal = oldSettings.PayerGoal;
            }
            if (data.ClinicGoal == oldSettings.ClinicGoal && data.Case.Contains("Add"))
            {
                data.ClinicGoal = oldSettings.ClinicGoal;
            }
            //if (data.SelectedGoal.Contains(Convert.ToInt32(DashboardDetails.ClinicGoal)) && (data.Case.Contains("Add")))
            //{
            //    data.ClinicGoal = 1;
            //}
            //if (data.SelectedGoal.Contains(Convert.ToInt32(DashboardDetails.AssGoal)) && (data.Case.Contains("Add")))
            //{
            //    data.AssociationGoal = 1;
            //}
            //if (data.SelectedGoal.Contains(Convert.ToInt32(DashboardDetails.CopGoal)) && (data.Case.Contains("Add")))
            //{
            //    data.CorporateGoal = 1;
            //}
            //if (data.SelectedGoal.Contains(Convert.ToInt32(DashboardDetails.PayGoal)) && (data.Case.Contains("Add")))
            //{
            //    data.PayerGoal = 1;
            //}
            //if (data.SelectedGoal.Contains(Convert.ToInt32(DashboardDetails.ProGoal)) && (data.Case.Contains("Add")))
            //{
            //    data.ProviderGoal = 1;
            //}
        }
        #endregion

        #region to get inital col details
        public async Task<MeasureData> GetCOLAllDetails(DashBoardFilters data)
        {

            dynamic result = await _dashboardRepository.GetCOLAllDetails(data);
            MeasureData Mdata = new MeasureData();

            Mdata.NumericalData = result.Item1;
            Mdata.Result = result.Item2;

            return Mdata;
        }

        #endregion


        #region ---- To get all BCS details---- 
        public async Task<MeasureData> GetBCSAllDetails(DashBoardFilters data)
        {
            dynamic result = await _dashboardRepository.GetBCSAllDetails(data);
            MeasureData Mdata = new MeasureData();
            Mdata.NumericalData = result.Item1;
            Mdata.Result = result.Item2;
            return Mdata;
        }
        #endregion


        #region ---To Get all CBP details------
        #region ---To Get all CBP details------
        public async Task<MeasureData> GetCBPAllDetails(DashBoardFilters data)
        {
            dynamic result = await _dashboardRepository.GetCBPAllDetails(data);
            MeasureData Mdata = new MeasureData();
            Mdata.NumericalData = result.Item1;
            Mdata.Result = result.Item2;
            return Mdata;
        }

        public async Task<dynamic> GetCBPPatientsList(DashBoardFilters data)
        {
            return await _dashboardRepository.GetCBPPatientsList(data);

        }
        #endregion
        #endregion
        public async Task<List<PatientClaimDetails>> GetClaimResult(ClaimFilters data)
        {
            List<PatientClaimDetails> model = new List<PatientClaimDetails>();
            var models = await _dashboardRepository.GetClaimPatientList(data);
            foreach (var item in models)
            {
                List<PatientClaimChildrenDetails> obj = new List<PatientClaimChildrenDetails>();
                obj.Add(new PatientClaimChildrenDetails()
                {
                    //Charges = item.Charges,
                    //ClinicName = item.ClinicName,
                    //Unit = item.Unit,
                    //ServiceName = item.ServiceName,
                    //DiagnosisDate = item.DiagnosisDate,
                    //PatientId = item.PatientId

                    Charges = "200",
                    ClinicName = "Juniper Health",
                    Unit = "1",
                    ServiceName = "DETOX",
                    DiagnosisDate = DateTime.UtcNow,
                    PatientId = item.PatientId,
                    ServiceCode = "34535",
                    Modifier = "QZ",
                    ContactNo = item.ContactNo
                    
                });
                model.Add(new PatientClaimDetails()
                {
                    PatientId = item.PatientId,
                    PayerName = "Aetna",
                    Createddate = item.DiagnosisDate,
                    DiagnosisDate = item.DiagnosisDate,
                    PatientName=item.PatientName,
                    PatientAddress=item.PatientAddress,
                    PatientClaimChildrenDetails = obj
                });
            }
            return model;
        }

        #region get all col patient list

        public async Task<dynamic> GetCOLPatientsList(DashBoardFilters data)
        {
            return await _dashboardRepository.GetCOLPatientsList(data);
        }

        public async Task<dynamic> GetBCSPatientsList(DashBoardFilters data)
        {
            return await _dashboardRepository.GetBCSPatientsList(data);
        }


        #endregion

        #region  get all CCS patient list
        public async Task<MeasureData> GetCCSAllDetails(DashBoardFilters data)
        {

            dynamic result = await _dashboardRepository.GetCCSAllDetails(data);
            MeasureData Mdata = new MeasureData();

            Mdata.NumericalData = result.Item1;
            Mdata.Result = result.Item2;

            return Mdata;
        }


        public async Task<dynamic> GetCCSPatientsList(DashBoardFilters data)
        {
            return await _dashboardRepository.GetCCSPatientsList(data);

        }
        #endregion

        public async Task<List<MeasureTypeMaster>> GetAllMeasureTypeMaster()
        {
            return await _dashboardRepository.GetAllMeasureTypeMaster();
        }


        public async Task<MeasureData> GetABAWellCareDetails(DashBoardFilters data)
        {

            dynamic result = await _dashboardRepository.GetABAWellCareDetails(data);
            MeasureData Mdata = new MeasureData();

            Mdata.NumericalData = result.Item1;
            Mdata.Result = result.Item2;

            return Mdata;
        }


        public async Task<MeasureData> GetBCSWellCareDetails(DashBoardFilters data)
        {

            dynamic result = await _dashboardRepository.GetBCSWellCareDetails(data);
            MeasureData Mdata = new MeasureData();
            Mdata.NumericalData = result.Item1;
            Mdata.Result = result.Item2;

            return Mdata;
        }



        public async Task<dynamic> GetMeasureWellCarePatientList(DashBoardFilters data)
        {
            return await _dashboardRepository.GetMeasureWellCarePatientList(data);

        }
        public async Task<MeasureAnalysisDetail> GetMeasureAnalysisWellCare(DashBoardFilters data)
        {
            dynamic result = await _dashboardRepository.GetMeasureAnalysisWellCare(data);
            MeasureAnalysisDetail Mdata = new MeasureAnalysisDetail();
            Mdata.CurrentData = result.Item1;
            Mdata.PreviousData = result.Item2;

            return Mdata;
        }

        //tet
        public async Task<MeasureData> GetCOLWellCareDetails(DashBoardFilters data)



        {

            dynamic result = await _dashboardRepository.GetCOLWellCareDetails(data);
            MeasureData Mdata = new MeasureData();

            Mdata.NumericalData = result.Item1;
            Mdata.Result = result.Item2;

            return Mdata;
        }

        public async Task<dynamic> GetCOLMeasureWellCarePatientList(DashBoardFilters data)
        {
            return await _dashboardRepository.GetCOLMeasureWellCarePatientList(data);

        }



        #region GetMeasures
        public async Task<dynamic> GetMeasures(DashBoardFilters data)
        {
            List<MeasureList> measureLists = new List<MeasureList>();

            dynamic result = null, result1 = null, result2 = null, result5 = null, result4 = null;

            dynamic MeasuresList = await _dashboardRepository.GetMeasures();

            dynamic result3 = await _dashboardRepository.GetDefaultGoalsDetails(data);
       

            if (data.Type == Convert.ToInt32(MeasureTypeMasterEnum.HEDIS2018))
            {
                result = await _dashboardRepository.GetAllABADetails(data);

                result1 = await _dashboardRepository.GetCOLAllDetails(data);

                result2 = await _dashboardRepository.GetBCSAllDetails(data);

                result5 = await _dashboardRepository.GetCBPAllDetails(data);

                result4 = await _dashboardRepository.GetCCSAllDetails(data);
            }
            else if (data.Type == Convert.ToInt32(MeasureTypeMasterEnum.WellCare2018))
            {
                result = await _dashboardRepository.GetABAWellCareDetails(data);

                result1 = await _dashboardRepository.GetCOLWellCareDetails(data);

                result2 = await _dashboardRepository.GetBCSWellCareDetails(data);
                //this line is used to calculate CBP Welcare  ----------Suraj chandra
                result5 = await _dashboardRepository.GetCBPWellCareDetails(data);
                //-------End
                result4 = await _dashboardRepository.GetCCSWellCareDetails(data);
            }
            else if (data.Type == Convert.ToInt32(MeasureTypeMasterEnum.HEDIS2019))
            {
                result = await _dashboardRepository.GetAllABADetails(data);

                result1 = await _dashboardRepository.GetCOLAllDetails(data);

                result2 = await _dashboardRepository.GetBCSAllDetails(data);

                result5 = await _dashboardRepository.GetCBPAllDetails(data);

                result4 = await _dashboardRepository.GetCCSAllDetails(data);
            }
            else if (data.Type == Convert.ToInt32(MeasureTypeMasterEnum.WellCare2019))
            {
                result = await _dashboardRepository.GetABAWellCareDetails(data);

                result1 = await _dashboardRepository.GetCOLWellCareDetails(data);

                result2 = await _dashboardRepository.GetBCSWellCareDetails(data);
                //this line is used to calculate CBP Welcare  ----------Suraj chandra
                result5 = await _dashboardRepository.GetCBPWellCareDetails(data);
                //-------End
                result4 = await _dashboardRepository.GetCCSWellCareDetails(data);
            }

            MeasureData Mdata5 = new MeasureData();
            if (result5 != null)
            {
                Mdata5.NumericalData = result5.Item1;
                Mdata5.Result = result5.Item2;
            }
               
            MeasureData Mdata4 = new MeasureData();
            if (result4 != null)
            {
                Mdata4.NumericalData = result4.Item1;
                Mdata4.Result = result4.Item2;
            }

            MeasureData Mdata2 = new MeasureData();
            if (result2 != null)
            {
                Mdata2.NumericalData = result2.Item1;
                Mdata2.Result = result2.Item2;
            }

            GoalDashboard goal = new GoalDashboard();
            goal.AssociationGoal = result3.Item1;
            goal.CorporateGoal = result3.Item2;
            goal.ClinicGoal = result3.Item3;
            goal.PayerGoal = result3.Item4;
            goal.ProviderGoal = result3.Item5;
            goal.UserDashboardSetting = result3.DefaultSetting;

            MeasureData Mdata = new MeasureData();
            if (result != null)
            {
                Mdata.NumericalData = result.Item1;
                Mdata.Result = result.Item2;
            }

            MeasureData Mdata1 = new MeasureData();
            if (result1 != null)
            {
                Mdata1.NumericalData = result1.Item1;
                Mdata1.Result = result1.Item2;
            }

            List<MasterMeasureCategory> list1 = MeasuresList.Item1;
            List<MasterMeasures> list2 = MeasuresList.Item2;
            foreach (var item in list1)
            {
                MeasureList obj = new MeasureList();
                obj.CategoryCode = item.Code;
                obj.CategoryId = item.Id;
                obj.CategoryName = item.CategoryName;
                obj.MasterMeasureList = new List<MasterMeasures>();
                obj.UserDashboardSetting = goal.UserDashboardSetting;
                foreach (MasterMeasures item1 in list2.Where(x => x.MeasureCategory == item.Id).ToList())
                {
                    try
                    {
                        var AssociationGoal = goal.AssociationGoal.Where(x => x.MeasureId == item1.MasterMeasureId).FirstOrDefault();

                        item1.AssociationGoal = AssociationGoal==null? "0" : AssociationGoal.Goal;

                        var CorporateGoal = goal.CorporateGoal.Where(x => x.MeasureId == item1.MasterMeasureId).FirstOrDefault();

                        item1.CorporateGoal = CorporateGoal==null?"0": CorporateGoal.Goal;

                        var ClinicGoal = goal.ClinicGoal.Where(x => x.MeasureId == item1.MasterMeasureId).FirstOrDefault();

                        item1.ClinicGoal = ClinicGoal==null?"0": ClinicGoal.Goal;

                        var PayerGoal = goal.PayerGoal.Where(x => x.MeasureId == item1.MasterMeasureId).FirstOrDefault();

                        item1.PayerGoal = PayerGoal==null?"0": PayerGoal.Goal;

                        var ProviderGoal = goal.ProviderGoal.Where(x => x.MeasureId == item1.MasterMeasureId).FirstOrDefault();

                        item1.ProviderGoal = ProviderGoal==null?"0": ProviderGoal.Goal;

                        if (item1.MasterMeasureId == 1)
                        {
                            item1.Numerator = Mdata.NumericalData[0].Numerator;
                            item1.Denominator = Mdata.NumericalData[0].Denominator;
                            item1.MET = Mdata.Result[0];
                            item1.AssPatientNeeded = PatientNeeded(Mdata, item1.AssociationGoal);
                            item1.CoPatientNeeded = PatientNeeded(Mdata, item1.CorporateGoal);
                            item1.PayPatientNeeded = PatientNeeded(Mdata, item1.PayerGoal);
                            item1.ProPatientNeeded = PatientNeeded(Mdata, item1.ProviderGoal);
                            item1.ClinicPatientNeeded = PatientNeeded(Mdata, item1.ClinicGoal);
                        }
                        else if (item1.MasterMeasureId == 2)
                        {
                            item1.Numerator = Mdata1.NumericalData[0].Numerator;
                            item1.Denominator = Mdata1.NumericalData[0].Denominator;
                            item1.MET = Mdata1.Result[0];
                            item1.AssPatientNeeded = PatientNeeded(Mdata1, item1.AssociationGoal);
                            item1.CoPatientNeeded = PatientNeeded(Mdata1, item1.CorporateGoal);
                            item1.PayPatientNeeded = PatientNeeded(Mdata1, item1.PayerGoal);
                            item1.ProPatientNeeded = PatientNeeded(Mdata1, item1.ProviderGoal);
                            item1.ClinicPatientNeeded = PatientNeeded(Mdata1, item1.ClinicGoal);
                        }
                        else if (item1.MasterMeasureId == 3)
                        {
                            item1.Numerator = Mdata2.NumericalData[0].Numerator;
                            item1.Denominator = Mdata2.NumericalData[0].Denominator;
                            item1.MET = Mdata2.Result[0];
                            item1.AssPatientNeeded = PatientNeeded(Mdata2, item1.AssociationGoal);
                            item1.CoPatientNeeded = PatientNeeded(Mdata2, item1.CorporateGoal);
                            item1.PayPatientNeeded = PatientNeeded(Mdata2, item1.PayerGoal);
                            item1.ProPatientNeeded = PatientNeeded(Mdata2, item1.ProviderGoal);
                            item1.ClinicPatientNeeded = PatientNeeded(Mdata2, item1.ClinicGoal);
                        }
                        else if (item1.MasterMeasureId == 4)
                        {
                            item1.Numerator = Mdata4.NumericalData[0].Numerator;
                            item1.Denominator = Mdata4.NumericalData[0].Denominator;
                            item1.MET = Mdata4.Result[0];
                            item1.AssPatientNeeded = PatientNeeded(Mdata4, item1.AssociationGoal);
                            item1.CoPatientNeeded = PatientNeeded(Mdata4, item1.CorporateGoal);
                            item1.PayPatientNeeded = PatientNeeded(Mdata4, item1.PayerGoal);
                            item1.ProPatientNeeded = PatientNeeded(Mdata4, item1.ProviderGoal);
                            item1.ClinicPatientNeeded = PatientNeeded(Mdata4, item1.ClinicGoal);
                        }
                        else if (item1.MasterMeasureId == 5)
                        {
                            item1.Numerator = Mdata5.NumericalData[0].Numerator;
                            item1.Denominator = Mdata5.NumericalData[0].Denominator;
                            item1.MET = Mdata5.Result[0];
                            item1.AssPatientNeeded = PatientNeeded(Mdata5, item1.AssociationGoal);
                            item1.CoPatientNeeded = PatientNeeded(Mdata5, item1.CorporateGoal);
                            item1.PayPatientNeeded = PatientNeeded(Mdata5, item1.PayerGoal);
                            item1.ProPatientNeeded = PatientNeeded(Mdata5, item1.ProviderGoal);
                            item1.ClinicPatientNeeded = PatientNeeded(Mdata5, item1.ClinicGoal);
                        }
                    }
                    catch(Exception ex)
                    {
                        var message=ex.Message;
                    }
                   
                    obj.MasterMeasureList.Add(item1);
                }

                //foreach (GoalDashboard goalDashboard in result3.)
                //{

                //}
                measureLists.Add(obj);
            }
            return measureLists;
        }

        private static int PatientNeeded(MeasureData Mdata, string Goal)
        {
            int PatientNeeded = 0;
            int AssTotalPatient = Convert.ToInt32(Math.Round(Convert.ToDecimal((Convert.ToDouble(Goal) * Mdata.NumericalData[0].Denominator) / 100)));
            if (Mdata.NumericalData[0].Numerator < AssTotalPatient)
            {
                PatientNeeded = AssTotalPatient - Mdata.NumericalData[0].Numerator;
            }
            else
            {
                PatientNeeded = 0;
            }

            return PatientNeeded;
        }
        #endregion
        public async Task<dynamic> GetMeasureDetails(int Id, string MeasureTypeId)
        {
            dynamic MeasuresDetails = await _dashboardRepository.GetMeasureDetails(Id, MeasureTypeId);
            return MeasuresDetails;
        }


        public async Task<dynamic> GetMeasuresName(int userId)
        {
            //dynamic measurenames = await _dashboardRepository.GetMeasuresName(data);
            //List<DropDownListItem> Measurename = measurenames.Item1;
            //return new { Measurename };
             dynamic result = await _dashboardRepository.GetMeasuresName(userId);
            CompareToModel model = (CompareToModel)result[0];
            FilterNames Fdata = new FilterNames();
            Fdata.ProviderItem = new DropDownListItem();
            Fdata.OrganizationItem = new DropDownListItem();
            Fdata.ClinicItem = new DropDownListItem();
            Fdata.PayerItem = new DropDownListItem();
            if (model != null)
            {

                Fdata.ProviderItem.Id = model.ProviderId;
                Fdata.ProviderItem.Name = model.ProviderName;

                //Fdata.ProviderItem.

                Fdata.OrganizationItem.Id = model.CorpId;
                Fdata.OrganizationItem.Name = model.CorpName;


                Fdata.ClinicItem.Id = model.ClinicId;
                Fdata.ClinicItem.Name = model.ClinicName;


                Fdata.PayerItem.Id = model.PayerId;
                Fdata.PayerItem.Name = model.PayerName;
            }

            //Fdata.AssociationItem = result.Item5;
            return Fdata;
        }


        #region   get CCS measure and patientlist 
        public async Task<MeasureData> GetCCSWellCareDetails(DashBoardFilters data)
        {

            dynamic result = await _dashboardRepository.GetCCSWellCareDetails(data);
            MeasureData Mdata = new MeasureData();

            Mdata.NumericalData = result.Item1;
            Mdata.Result = result.Item2;

            return Mdata;

        }
        public async Task<dynamic> GetCCSMeasureWellCarePatientList(DashBoardFilters data)
        {
            return await _dashboardRepository.GetCCSMeasureWellCarePatientList(data);

        }


        public async Task<dynamic> GetBCSMeasureWellCarePatientList(DashBoardFilters data)
        {
            return await _dashboardRepository.GetBCSMeasureWellCarePatientList(data);

        }
        

        #endregion

        #region GetMeasureGoals
        public async Task<dynamic> GetMeasureGoals(int UserId, int OrganizationId)
        {
            GoalDashboard goal = new GoalDashboard();
            dynamic result = await _dashboardRepository.GetMeasureGoals(UserId, OrganizationId);
            goal.AssociationGoal = result.Item1;
            goal.CorporateGoal = result.Item2;
            goal.ClinicGoal = result.Item3;
            goal.PayerGoal = result.Item4;
            goal.ProviderGoal = result.Item5;
            // goal.UserDashboardSetting = result.DefaultSetting;

            return goal;
        }
        #endregion

        #region IUGoals
        public async Task<dynamic> IUGoals(GoalDetails details)
        {
            dynamic result = await _dashboardRepository.IUGoal(details);
            return result;
        }
        #endregion

        #region GetClinicPayerForOrganizaton
        public async Task<dynamic> GetClinicPayerForOrganizaton(int Type, int Id,int LoginOrganizationId)
        {
            List<OrganizationGoalModal> goalModallist = new List<OrganizationGoalModal>();
            List<OrganizationGoalModal> goalModallist1 = new List<OrganizationGoalModal>();
            try
            {
                dynamic result = await _dashboardRepository.GetClinicPayerForOrganizaton(LoginOrganizationId);
                List<OrganizationModal> organizationModals = result.Item1;
                List<ClinicModal> clinicModals = result.Item2;
                List<ProviderModal> providerModals = result.Item3;
                List<MeasureCount> counts = result.Item4;
                //var data = result.Item1;

                if (Id != 0)
                {
                    if (Type == 7)
                    {
                        providerModals = providerModals.Where(p => p.ProviderId == Id).ToList();
                        clinicModals = clinicModals.Where(c => c.ClinicId == providerModals[0].ClinicId).ToList();
                        if (clinicModals.Count>0)
                        {
                            organizationModals = organizationModals.Where(o => o.OrganizationId == clinicModals[0].OrganizationId & o.OrganizationType == 2).ToList();
                        }
                        else
                        {
                            organizationModals = organizationModals.Where(o => o.OrganizationId == providerModals[0].OrganisationId & o.OrganizationType == 2).ToList();
                        }
                        
                    }
                    else if (Type == 6)
                    {
                        clinicModals = clinicModals.Where(c => c.ClinicId == Id).ToList();
                        organizationModals = organizationModals.Where(o => o.OrganizationId == clinicModals[0].OrganizationId & o.OrganizationType == 2).ToList();
                    }
                    else if (Type == 1 || Type == 2 || Type == 3 || Type == 4 || Type == 5)
                    {
                        organizationModals = organizationModals.Where(o => o.OrganizationId == Id && o.OrganizationType == Type).ToList();
                    }
                }

                foreach (OrganizationModal organization in organizationModals)
                {
                    OrganizationGoalModal goalModal = new OrganizationGoalModal();
                    goalModal.ClinicGoalModal = new List<ClinicGoalModal>();
                    goalModal.OrganizationId = organization.OrganizationId;
                    goalModal.OrganizationName = organization.OrganizationName;
                    goalModal.OrganizationType = organization.OrganizationType;
                    goalModal.OrganizationTypeName = organization.OrganizationTypeName;
                    goalModal.Count = 0;
                    foreach (MeasureCount item in counts.Where(x => x.OrganizationId == organization.OrganizationId && x.Type == organization.OrganizationType))
                    {
                        goalModal.Count = item.Count;
                    }
                    if (goalModal.OrganizationType==2)
                    {
                        foreach (ClinicModal item in clinicModals.Where(x => x.OrganizationId == organization.OrganizationId))
                        {
                            ClinicGoalModal obj = new ClinicGoalModal();
                            obj.ClinicId = item.ClinicId;
                            obj.ClinicName = item.ClinicName;
                            obj.Type = "Clinic";
                            obj.ProviderModal = new List<ProviderModal>();
                            obj.Count = 0;
                            obj.TypeId = 6;
                            foreach (MeasureCount measureCount in counts.Where(x => x.OrganizationId == item.ClinicId && x.Type == 6))
                            {
                                obj.Count = measureCount.Count;
                            }
                            foreach (ProviderModal item1 in providerModals.Where(x => x.ClinicId == item.ClinicId && x.OrganisationId == organization.OrganizationId))
                            {
                                foreach (MeasureCount measureCount in counts.Where(x => x.OrganizationId == item1.ProviderId && x.Type == 7))
                                {
                                    item1.Count = measureCount.Count;
                                }
                                obj.ProviderModal.Add(item1);
                            }

                            goalModal.ClinicGoalModal.Add(obj);
                        }
                        foreach (ProviderModal item in providerModals.Where(x => x.ClinicId == 0 && x.OrganisationId == organization.OrganizationId))
                        {
                            ClinicGoalModal obj = new ClinicGoalModal();
                            obj.ClinicId = item.ProviderId;
                            obj.ClinicName = item.ProviderName;
                            obj.Type = "Provider";
                            obj.Count = 0;
                            obj.TypeId = 7;
                            foreach (MeasureCount measureCount in counts.Where(x => x.OrganizationId == item.ProviderId && x.Type == 7))
                            {
                                obj.Count = measureCount.Count;
                            }
                            goalModal.ClinicGoalModal.Add(obj);
                        }
                    }
                    goalModallist.Add(goalModal);

                }
                if (Id == 0)
                {
                    if (Type == 7)
                    {
                        goalModallist1 = goalModallist;
                        //try
                        //{
                        //    goalModallist = goalModallist.Where(o => o.ClinicGoalModal.Any(c => c.ProviderModal.Count>0)).ToList();
                        //}
                        //catch (Exception ex)
                        //{
                        //    goalModallist = goalModallist.Where(o => o.ClinicGoalModal.Any(c => c.ProviderModal != null)).ToList();
                        //}
                        //if (goalModallist.Count==0)
                        //{
                        //    goalModallist = goalModallist1.Where(o => o.ClinicGoalModal.Count>0).ToList();
                        //}
                        //else
                        //{
                            goalModallist = goalModallist.Where(o => o.ClinicGoalModal.Any(c => c.Type == "Clinic" && c.ProviderModal?.Count > 0)).ToList();
                        //goalModallist = goalModallist.Where(o => o.ClinicGoalModal.Any(c => c.Type =="Clinic" && c.ProviderModal.Any(p => p?.Count > 0))).ToList();
                        //}
                        //if (goalModallist.Any(o=>o.ClinicGoalModal.Count>0))
                        //{
                        //    if (goalModallist.Any(o => o.ClinicGoalModal.Any(c=> c.ProviderModal != null || c.ProviderModal.Count > 0)))
                        //    {

                        //    }
                        //}
                    }
                    else if (Type == 6)
                    {
                            goalModallist = goalModallist.Where(o => o.ClinicGoalModal?.Count > 0).ToList();
                        if (goalModallist.Count>0)
                        {
                            goalModallist = goalModallist.Where(o => o.ClinicGoalModal.Any(c => c.Type == "Clinic")).ToList();
                        }
                    }
                    else if (Type == 1 || Type == 2 || Type == 3 || Type == 4 || Type == 5)
                    {
                        goalModallist = goalModallist.Where(o => o.OrganizationType == Type).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
            }
            
            return goalModallist;

        }
        #endregion

        public async Task<dynamic> SubmitMasterGoalsDetails(MasterGoal details)
        {
            return await _dashboardRepository.SubmitMasterGoalsDetails(details);
        }

        public async Task<dynamic> MasterSelectedMeasure(int OrganizationId, int Type)
        {
            return await _dashboardRepository.MasterSelectedMeasure(OrganizationId, Type);
        }

        public async Task<dynamic> MasterMeasureGoals(int OrganizationId, int Type, string Search)
        {
            dynamic res = await _dashboardRepository.MasterMeasureGoals(OrganizationId, Type);
            List result = new List();
            result.Name = res.Item1;
            result.list = res.Item2;
            //res.Where(x => x.Name.Contains(Search));
            if (!string.IsNullOrEmpty(Search))
                result.list = result.list.Where(x => x.Name.ToLower().Contains(Search.ToLower())).ToList();

            return result;

        }

        public async Task<dynamic> GetCBPMeasureWellcareDetail(DashBoardFilters data)
        {
            return await _dashboardRepository.GetCBPWellCareDetails(data);

        }
        public async Task<dynamic> GetCBPWellcarePatientList(DashBoardFilters data)
        {
            return await _dashboardRepository.GetCBPMeasureWellCarePatientList(data);

        }

        public async Task<dynamic> GetEntityGoal(int Id)
        {
            return await _dashboardRepository.GetEntityGoal(Id);
        }

        public async Task<dynamic> GetEntityMeasure(int EntityType, int EntityId)
        {
            return await _dashboardRepository.GetEntityMeasure(EntityType, EntityId);
        }
        public async Task<dynamic> CopyMersure(int CEntityType, int CEntityId, int userId, int EntityId, int EntityType)
        {
            dynamic result = await _dashboardRepository.MasterMeasureGoals1(CEntityType, CEntityId, EntityId, EntityType);
            dynamic res = 0;
            List<MasterGoal> list = new List<MasterGoal>();
            list = result;
            if(list.Count>0)
            {
                foreach (MasterGoal item in list)
                {
                    item.Id = 0;
                    item.UserId = userId;
                    item.EntityId = EntityId;
                    item.EntityTypeId = EntityType;
                    res = await _dashboardRepository.SubmitMasterGoalsDetails(item);
                }
            }
            else
            {
                res = -1;
            }
            return res;
        }

        public async Task<dynamic> DeleteMeasure(int Id)
        {
            return await _dashboardRepository.DeleteMeasure(Id);
        }

        #region  save SetMeasuredetails region

        public async Task<int> SetMeasuredetails(MasterRule data)
        {
            string xmlData = XmlHelper.GetAsXml<MasterRule>(data);
            int result = await _dashboardRepository.SetMeasuredetails(xmlData);
            return result;

        }

        #endregion

        public async Task<string> GetMeasureDescription(int measureId, int entityId)
        {
            return await _dashboardRepository.GetMeasureDescription(measureId, entityId);
        }

        public async Task<dynamic> DeleteAllMeasure(int Id, int Type)
        {
            return await _dashboardRepository.DeleteAllMeasure(Id, Type);
        }
    }
}
