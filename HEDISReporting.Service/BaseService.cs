﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HEDISReporting.DataContract.Authentication;
using HEDISReporting.RepositoryContact;
using HEDISReporting.RepositoryContract.CareGap;
using HEDISReporting.ServiceContract;
using Microsoft.Extensions.Configuration;

namespace HEDISReporting.Service
{
    public class BaseService : IBaseService
    {
        IBaseRepository _baseRepository;
        IBaseService _baseService;
        public IConfiguration Configuration;

        private LoginUser _loginUser;
        private ICareGapRepository careGapRepository;

        public BaseService(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public BaseService(IBaseRepository baseRepository, IConfiguration configuration)
        {
            _baseRepository = baseRepository;
            Configuration = configuration;
        }
        public BaseService(IBaseRepository baseRepository, IBaseService baseService, IConfiguration configuration)
        {
            _baseRepository = baseRepository;
            _baseService = baseService;
            Configuration = configuration;
        }

        public BaseService(ICareGapRepository careGapRepository)
        {
            this.careGapRepository = careGapRepository;
        }

        public LoginUser LoginUser
        {
            get
            {
                if (_loginUser == null)
                {
                    throw new UnauthorizedAccessException();
                }
                else
                {
                    return _loginUser;
                }
               
            }

            set { _loginUser = value; }
        }

        public async Task<object> HealthCheckAsync()
        {
           return await _baseRepository.HealthCheckAsync();
        }
    }
}
