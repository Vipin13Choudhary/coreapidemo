﻿using HEDISReporting.RepositoryContract.Exception;
using HEDISReporting.ServiceContract.Exception;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HEDISReporting.Service
{
    public class ExceptionService: BaseService, IExceptionService
    {
        private readonly IExceptionRepository _exceptionReporsitory;
        public ExceptionService(IExceptionRepository exceptionService, IConfiguration configuration) : base(exceptionService, configuration)
        {
            _exceptionReporsitory = exceptionService;
        }

        public async Task SaveExceptionLogs(int status, string stackTrace, string userName, string userId, int section, string fileName, string dataXML)
        {
           await _exceptionReporsitory.SaveExceptionLogs(status, stackTrace, userName, userId, section, fileName, dataXML);
        }
    }
}
