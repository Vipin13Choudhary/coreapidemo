﻿using HEDISReporting.Common;
using HEDISReporting.Common.Enum;
using HEDISReporting.DataContract.Common;
using HEDISReporting.DataContract.DataSource;
using HEDISReporting.DataContract.Organization;
using HEDISReporting.RepositoryContract.Organization;
using HEDISReporting.ServiceContract.Organization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HEDISReporting.Service.Organization
{
    public class OrganizationService : BaseService, IOrganizationService
    {


        private readonly IOrganizationRepository _iOrganizationRepository;

        public OrganizationService(IOrganizationRepository iOrganizationRepository, IConfiguration configuration, IHostingEnvironment env) : base(iOrganizationRepository, configuration)
        {
            _iOrganizationRepository = iOrganizationRepository;
            // _env = env;
        }

        #region get organization list

        public async Task<dynamic> GetOrganizationList(OrganizationListingOperands data)
        {


            return await _iOrganizationRepository.GetOrganizationList(data);

        }
        #endregion

        #region get all organization dropdown
        public async Task<OrganizationDropdown> GetAllDropDown(int OrganizationId, string userId)
        {
            dynamic data = await _iOrganizationRepository.GetAllDropDown(OrganizationId, userId);
            OrganizationDropdown dropDown = new OrganizationDropdown();
            List<States> states = data.Item2;
            List<DropDownListItem> organizations = data.Item1;
            List<DropDownListItem> region = data.Item3;
            List<DropDownListItem> vendorType = data.Item4;
            List<DropDownListItem> clinicList = data.Item5;

            dropDown.OrganizationsList = organizations;
            dropDown.StatesList = states;
            dropDown.OrganizationRegion = region;
            dropDown.VendorType = vendorType;
            dropDown.ClinicList = clinicList;

            return dropDown;

        }
        #endregion

        #region save organization configuration
        public async Task<DataSourceResponseModel> SaveOrganizationConfiguration(OrganizationDataSource organizationSftp)
        {
            string xmlData = XmlHelper.GetAsXml<OrganizationDataSource>(organizationSftp);
            DataSourceResponseModel result = await _iOrganizationRepository.SaveOrganizationConfiguration(xmlData, organizationSftp.IsEdit);
            return result;

        }
        #endregion

        #region save organization

        public async Task<int> SaveOrganization(OrganizationModel data)
        {
            data.CreatedBy = data.UserId;
            // data.CountryId = 1;
            string xmlData = XmlHelper.GetAsXml<OrganizationModel>(data);
            int result = await _iOrganizationRepository.SaveOrganization(xmlData, data.OrganizationId);
            return result;


        }
        public async Task<int> SaveOrgnizationClinic(OrganizationClinic organizationClinic)
        {
            //client app should pass user Id as created by and Organization Id
            //  LoginUser = new DataContract.Authentication.LoginUser();
            organizationClinic.CreatedBy = organizationClinic.CreatedBy;
            string xmlData = XmlHelper.GetAsXml<OrganizationClinic>(organizationClinic);
            int result = await _iOrganizationRepository.SaveOrgnizationClinic(xmlData, organizationClinic.ClinicId);
            return result;
        }

        #endregion

        #region save department details
        public async Task<int> SaveOrganizationDepartment(OrganizationDepartment data)
        {
            data.CreatedBy = data.UserId;
            //data.CountryId = 1;
            string xmlData = XmlHelper.GetAsXml<OrganizationDepartment>(data);
            int result = await _iOrganizationRepository.SaveOrganizationDepartment(xmlData, data.DepartmentId);
            return result;


        }
        #endregion

        #region Countries and  States
        public async Task<dynamic> GetCountriesStates(int countryId)
        {
            dynamic countriesStates = await _iOrganizationRepository.GetCountriesStates(countryId);
            List<DropDownListItem> countries = countriesStates.Item1;
            List<DropDownListItem> states = countriesStates.Item2;
            List<DropDownListItem> regions = countriesStates.Item3;
            List<DropDownListItem> organizationDropDown = countriesStates.Item4;
            return new { countries, states, regions, organizationDropDown };
        }
        #endregion

        #region get organization department list

        public async Task<dynamic> GetDepartmentList(DepartmentListOperands data)
        {
            return await _iOrganizationRepository.GetDepartmentList(data);
        }

        #endregion
        public async Task<dynamic> GetOrganizationConfigurationList(OrganizationConfigurationListModel filterItems)
        {
            return await _iOrganizationRepository.GetOrganizationConfigurationList(filterItems);
        }
        public async Task<dynamic> GetOrganizationsClinicsList(OrganizationConfigurationListModel filterItems)
        {
            return await _iOrganizationRepository.GetOrganizationsClinicsList(filterItems);
        }

        #region get organization region  list

        public async Task<dynamic> GetOrganizationRegionList(OrgRegionListingOperands data)
        {


            return await _iOrganizationRepository.GetOrganizationRegionList(data);

        }
        #endregion

        #region  save organization region

        public async Task<int> SaveOrganizationRegion(OrganizationRegion data)
        {

            string xmlData = XmlHelper.GetAsXml<OrganizationRegion>(data);
            int result = await _iOrganizationRepository.SaveOrganizationRegion(xmlData, data.RegionId);
            return result;

        }

        #endregion


        public async Task<dynamic> GetDataSourceDropDowns(int ConfigurationTypeId)
        {
            return await _iOrganizationRepository.GetDataSourceDropDowns(ConfigurationTypeId);
        }

        public async Task<int> DeleteOrganizationData(DeleteData data)
        {
            return await _iOrganizationRepository.DeleteOrganizationData(data);
        }

        public async Task<dynamic> GetOrganizationDetailById(int organizationId)
        {
            dynamic data = await _iOrganizationRepository.GetOrganizationDetailById(organizationId);
            dynamic model = data.Item1;
            List<RegionDropDowns> vendor = data.Item2;
            return new { model, vendor };

        }

        public async Task<dynamic> GetOrganizationClinicDetailById(int clinicId)
        {
            dynamic data = await _iOrganizationRepository.GetOrganizationClinicDetailById(clinicId);

            return data;

        }
        public async Task<dynamic> GetOrganizationDepartmentDetailById(int departmentId)
        {
            dynamic data = await _iOrganizationRepository.GetOrganizationDepartmentDetailById(departmentId);
            return data;

        }

        public async Task<dynamic> GetOrganizationsDataSourceDetail(int organizationId, int DataSourceId)
        {
            dynamic DataSourcedata = new OrganizationDataSource();
            dynamic data = await _iOrganizationRepository.GetOrganizationsDataSourceDetail(organizationId, DataSourceId);
            //if(data.item4.length>0)
            dynamic ReceiveDataSource = data.Item4;
            dynamic GetDataSourceSftp = data.Item3;
            dynamic GetDataSource = data.Item2;
            dynamic OrganizationDataSource = data.Item1;
            return new { ReceiveDataSource, GetDataSourceSftp, GetDataSource, OrganizationDataSource };
            //return data;

        }

        /// <summary>
        /// To get country ISD code using IP address
        /// Created By : Himanshu Joshi
        /// </summary>
        /// <param name="IPAddress"></param>
        /// <returns></returns>
        public async Task<dynamic> GetCountryTypeByIP(string IPAddress)
        {
            string nowip = IPAddress;
            string dottedip;
            double Dot2LongIP=0.00;
            dottedip = nowip;
            //  IP Address = 202.186.13.4
            //  So, w = 202, x = 186, y = 13 and z = 4
            // IP Number = 16777216 * 202 + 65536 * 186 + 256 * 13 + 4
            //= 3388997632 + 12189696 + 3328 + 4
            //= 3401190660
            if (!string.IsNullOrEmpty(nowip))
            {
                string[] IPArray = nowip.Split(".");
                Dot2LongIP = (16777216 * Convert.ToDouble(IPArray[0])) + (65536 * Convert.ToDouble(IPArray[1])) + (256 * Convert.ToDouble(IPArray[2])) + Convert.ToDouble(IPArray[3]);
            }
            dynamic data = await _iOrganizationRepository.GetCountryISDFromIP(Dot2LongIP);
            var code = data.InternationalDialing;
            return new { code };
        }

        #region get organization list for Windows Service

        public async Task<dynamic> GetOrganizationList_WinService()
        {


            return await _iOrganizationRepository.GetOrganizationList_WinService();

        }
        #endregion
        public async Task<OrganizationDropdown> GetAllEntity(int TypeId,int OrganizationId)
        {
            dynamic data = await _iOrganizationRepository.GetAllEntity(TypeId, OrganizationId);
            
            OrganizationDropdown dropDown = new OrganizationDropdown();
            if (TypeId!=0)
            {
                if (data != null)
                {
                    List<DropDownListItem> organizations = data;
                    dropDown.OrganizationsList = organizations;
                }
            }

            return dropDown;
        }
    }
}
