﻿using HEDISReporting.Common.CustomExceptions;
using HEDISReporting.Common.Response;
using HEDISReporting.DataContract.ApiResponse;
using HEDISReporting.DataContract.Common;
using HEDISReporting.DataContract.Dashboard;
using HEDISReporting.DataContract.DataSource;
using HEDISReporting.DataContract.PatientChart;
using HEDISReporting.DataContract.Patients;
using HEDISReporting.DataContract.SyncModels;
using HEDISReporting.RepositoryContract.CareGap;
using HEDISReporting.RepositoryContract.Patient;
using HEDISReporting.ServiceContract.CareGap;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace HEDISReporting.Service.CareGap
{
    public class CareGapService : BaseService, ICareGapService
    {
        private readonly ICareGapRepository _careGapRepository;
        public CareGapService(ICareGapRepository careGapRepository) : base(careGapRepository)
        {
            _careGapRepository = careGapRepository;
        }
        #region get all measures patient list
        public async Task<dynamic> GetABACareGap2018(DashBoardFilters data)
        {
            return await _careGapRepository.GetABACareGap2018(data);
        }
        #endregion

        #region get all Care Gap measures patient list
        public async Task<dynamic> GetCareGapPatientList(DashBoardFilters data)
        {
            return await _careGapRepository.GetAllCareGapPatientList(data);
        }
        #endregion

        #region get all Imunization  patient list
        public async Task<dynamic> GetImunizationPatientList(DashBoardFilters data)
        {
            return await _careGapRepository.GetImunizationPatientList(data);
        }
        #endregion

    }
}
