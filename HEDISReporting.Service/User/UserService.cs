﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HEDISReporting.DataContract.User;
using HEDISReporting.RepositoryContract.User;
using HEDISReporting.ServiceContract.User;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace HEDISReporting.Service.User
{
    public class UserService : BaseService, IUserService
    {

        private readonly IUserRepository _userRepository;
        // private readonly IAuthenticationService _authenticationService;
        private readonly IHostingEnvironment _env;
        public UserService(IUserRepository userRepository, IConfiguration configuration, IHostingEnvironment env) : base(userRepository, configuration)
        {
            _userRepository = userRepository;
            _env = env;
        }
        public async Task<UserModel> GetUserAsync(int userId, int callerUserId)
        {
            UserModel userModel = null;
            //callerUserId = LoginUser.UserId;

            userModel = await _userRepository.GetUserAsync(userId);
            if (userModel != null)
            { }

            return userModel;
        }
        /// <summary>
        /// Return dynamic list of user menus
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<List<UserMenus>> GetUserMenus(int userId,string baseUrl)
        {
            dynamic menus = await _userRepository.GetUserMenus(userId);
           
            List<UserMenus> userMenus = menus.Item1;
            List<UserSubMenus> subMenus = menus.Item2;
            List<UserMenus> menuCollection = new List<UserMenus>();
                foreach (UserMenus userMenu in userMenus)
                {
                    UserMenus userMenuItem = new UserMenus();
                    userMenuItem = userMenu;
                    userMenuItem.SubMenus = new List<UserSubMenus>();
                    userMenuItem.SubMenus.AddRange(subMenus.Where(x => x.MenuId == userMenu.MenuId).ToList());
                userMenuItem.MainMenuUrl = userMenuItem.SubMenus.Count != 0 ? "" : string.Concat(baseUrl, userMenuItem.MainMenuController, '/', userMenuItem.MainMenuAction);
                //userMenuItem.MainMenuUrl = string.Concat(baseUrl, userMenuItem.MainMenuController, '/', userMenuItem.MainMenuAction);
                foreach (var subMenuItems in userMenuItem.SubMenus)
                    {
                    subMenuItems.SubMenuUrl= string.Concat(baseUrl, subMenuItems.SubMenusController, '/', subMenuItems.SubMenusAction);
                    }
                    menuCollection.Add(userMenuItem);
                }
            return menuCollection;
        }


        /// <summary>
        /// Return user Roles
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<UserRolesData> GetUserRoles(int userId)
        {
          return await _userRepository.GetUserRoles(userId);
            
        }

        #region for getting the login userId
        public Task<UserModel> getloginUserId(string aspNetUserId) {

            return _userRepository.getloginUserId(aspNetUserId);
        }
       #endregion



    }
}
