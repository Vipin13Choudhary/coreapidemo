﻿using HEDISReporting.DataContract.ApiResponse;
using HEDISReporting.DataContract.Provider;
using HEDISReporting.RepositoryContract.Provider;
using HEDISReporting.ServiceContract.Organization;
using HEDISReporting.ServiceContract.ProviderS;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HEDISReporting.Service.Provider
{
    public class ProviderService : BaseService, IProviderService
    {
        private readonly IProviderRepository _iProviderRepository;

        public ProviderService(IProviderRepository iOrganizationRepository, IConfiguration configuration, IHostingEnvironment env) : base(iOrganizationRepository, configuration)
        {
            _iProviderRepository = iOrganizationRepository;
            // _env = env;
        }

        #region get organization list

        public async Task<dynamic> GetProviderList(ProviderListingOperands data)
        {


            return await _iProviderRepository.GetProviderList(data);

        }

        public async Task<dynamic> GetProviderById(int Id)
        {
            return await _iProviderRepository.GetProviderById(Id);

        }

        #endregion
    }
}
