﻿using HEDISReporting.DataContract.User;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HEDISReporting.ServiceContract.User
{
    public interface IUserService:IBaseService
    {
       
        /// <summary>
        /// Returns UserModel for specified userId
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<UserModel> GetUserAsync(int userId, int callerUserId);
        /// <summary>
        /// Return dynamic list of user menus
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<List<UserMenus>> GetUserMenus(int userId, string baseUrl);
        Task<UserRolesData> GetUserRoles(int userId);
        Task<UserModel> getloginUserId(string aspNetUserId);
    }
}
