﻿using HEDISReporting.DataContract.Provider;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HEDISReporting.ServiceContract.ProviderS
{
    public interface IProviderService:IBaseService
    {
        Task<dynamic> GetProviderList(ProviderListingOperands data);
        Task<dynamic> GetProviderById(int Id);
    }
}
