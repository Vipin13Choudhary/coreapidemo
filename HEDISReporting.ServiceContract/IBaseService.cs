﻿using HEDISReporting.DataContract.Authentication;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HEDISReporting.ServiceContract
{
   public interface IBaseService
    {
        // <summary>
        /// Declares HealthCheck to be implemeted by the deriving services .
        /// </summary>
        /// <returns>HealthCheck Information</returns>
        Task<object> HealthCheckAsync();

        /// <summary>
        /// Login user information
        /// </summary>
        LoginUser LoginUser { get; set; }

    }
}
