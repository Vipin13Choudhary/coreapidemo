﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HEDISReporting.DataContract.ApiResponse;
using HEDISReporting.DataContract.Authentication;
using HEDISReporting.DataContract.Common;
using HEDISReporting.DataContract.DataSource;
//using HEDISReporting.DataContract.Labs;//Comitted by Amita
using HEDISReporting.DataContract.PatientChart;
using HEDISReporting.DataContract.Patients;
using HEDISReporting.DataContract.SyncModels;
using Newtonsoft.Json.Linq;

namespace HEDISReporting.ServiceContract.Patient
{
    public interface IPatientService : IBaseService
    {
        /// <summary>
        ///  IPatientService is hiding(using keyword "new") the IPatientService's Healthcheck to enable call to IPatientService's HealthCheck.
        /// </summary>
        /// <returns>HealthCheck Information</returns>
        new Task<object> HealthCheckAsync();
        Task<Response> ImportPatientCCDA(string file, int OrganizationId, int UserId, string receivedFile);//, TokenModel token
        Task<dynamic> GetAllPatientList(OrganizationListModel data);
        Task<dynamic> GetEthnicity(int EthnicityId);
        Task<dynamic> GetRace(int RaceId);
        Task<dynamic> GetOrganization(int OrganizationId);
        Task<dynamic> GetFilteredPatientList(int EthnicityId, int OrganizationId, int RaceId,int GenderId);
        Task<dynamic> GetGender(int GenderId);
        Task<dynamic> GetPatientChart(int paientId, int sectionId, int tabId);
        Task<dynamic> GetPatientChartTabs(int paientId, int sectionId, int tabId);
        Task<DataSourceResponseModel> GetOrganizationFolderDetail(int organizationId);
        Task<SftpHedisRequestModel> GetDataSourceDetailForSync(int organizationId);
        Task<bool> InsertSyncedFiles(string syncedFiles, int organizationId, int userId);
        Task<dynamic> GetClinics(int OrganizationId);
        Task<dynamic> GetProviders(int ClinicId);
        Task<dynamic> GetPayers();
        Task<dynamic> GetLabFromPatients(LabListingOperands data);
        Task<dynamic> GetPatientsFromLab(int patientId);

        Task<bool> GetDeIdentifiedInfo(int patientid, string filePath);
        Task<string> GetPDF(PatientClaimDetails parentClaimDetail);
        Task<dynamic> GetStatusReasonCode();
    }
}





