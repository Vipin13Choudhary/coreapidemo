﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HEDISReporting.ServiceContract.Exception
{
   public interface IExceptionService: IBaseService
    {
        new Task<object> HealthCheckAsync();
        Task SaveExceptionLogs(int status, string stackTrace, string userName, string userId, int section, string fileName, string dataXML);
    }
}
