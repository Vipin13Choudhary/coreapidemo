﻿using HEDISReporting.DataContract.Common;
using HEDISReporting.DataContract.DataSource;
using HEDISReporting.DataContract.Organization;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HEDISReporting.ServiceContract.Organization
{
    public interface IOrganizationService : IBaseService
    {
        Task<dynamic> GetOrganizationList(OrganizationListingOperands data);
        Task<OrganizationDropdown> GetAllDropDown(int OrganizationId, string userId);
        Task<DataSourceResponseModel> SaveOrganizationConfiguration(OrganizationDataSource organizationSftp);
        Task<int> SaveOrganization(OrganizationModel data);
        Task<int> SaveOrganizationDepartment(OrganizationDepartment data);
        Task<dynamic> GetDepartmentList(DepartmentListOperands data);
        Task<int> SaveOrgnizationClinic(OrganizationClinic organizationClinic);
        Task<dynamic> GetCountriesStates(int countryId);
        Task<dynamic> GetOrganizationConfigurationList(OrganizationConfigurationListModel filterItems);
        Task<dynamic> GetOrganizationRegionList(OrgRegionListingOperands data);
        Task<int> SaveOrganizationRegion(OrganizationRegion data);
        Task<dynamic> GetOrganizationsClinicsList(OrganizationConfigurationListModel filterItems);
        Task<dynamic> GetDataSourceDropDowns(int ConfigurationTypeId);
        Task<int> DeleteOrganizationData(DeleteData data);
        Task<dynamic> GetOrganizationDetailById(int organizationId);
        Task<dynamic> GetOrganizationClinicDetailById(int clinicId);
        Task<dynamic> GetOrganizationDepartmentDetailById(int organizationId);
        Task<dynamic> GetOrganizationsDataSourceDetail(int organizationId,int DataSourceId);
        Task<dynamic> GetCountryTypeByIP(string IPAddress);
        Task<OrganizationDropdown> GetAllEntity(int TypeId,int OrganizationId);
        Task<dynamic> GetOrganizationList_WinService();
    }
}
