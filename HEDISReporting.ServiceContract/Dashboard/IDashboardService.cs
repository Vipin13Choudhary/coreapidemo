﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HEDISReporting.DataContract.Dashboard;
using HEDISReporting.DataContract.DataSource;
using HEDISReporting.DataContract.PatientChart;

namespace HEDISReporting.ServiceContract.Dashboard
{
    public interface IDashboardService : IBaseService
    {
        Task<MeasureData> GetAllABADetails(DashBoardFilters data);
        Task<dynamic> GetAllMeasurePatientList(DashBoardFilters data);
        Task<FilterDropdown> GetAllDropDowm(object userId);
        Task<MeasureAnalysisDetail> GetMeasureAnalysis(DashBoardFilters data);
        Task<GoalDashboard> GetDefaultGoalsDetails(DashBoardFilters data);
        Task<int> SaveDashboardGoalDetails(SavedDashboardGoalDetails data);
        Task<MeasureData> GetCOLAllDetails(DashBoardFilters data);
        Task<dynamic> GetCOLPatientsList(DashBoardFilters data);
        Task<MeasureData> GetCCSAllDetails(DashBoardFilters data);
        Task<MeasureData> GetBCSAllDetails(DashBoardFilters data);
        Task<dynamic> GetBCSPatientsList(DashBoardFilters data);
        Task<MeasureData> GetCBPAllDetails(DashBoardFilters data);
        Task<dynamic> GetCBPPatientsList(DashBoardFilters data);

        Task<dynamic> GetCCSPatientsList(DashBoardFilters data);
        Task<List<MeasureTypeMaster>> GetAllMeasureTypeMaster();
        Task<MeasureData> GetABAWellCareDetails(DashBoardFilters data);
        Task<MeasureData> GetBCSWellCareDetails(DashBoardFilters data);
        Task<dynamic> GetMeasureWellCarePatientList(DashBoardFilters data);
        Task<MeasureAnalysisDetail> GetMeasureAnalysisWellCare(DashBoardFilters data);
        Task<MeasureData> GetCOLWellCareDetails(DashBoardFilters data);
        Task<dynamic> GetCOLMeasureWellCarePatientList(DashBoardFilters data);
        Task<dynamic> GetMeasures(DashBoardFilters data);
        Task<dynamic> GetMeasureDetails(int Id,string MeasureTypeId);
        Task<dynamic> GetClinicPayerForOrganizaton(int Type, int Id,int LoginOrganizationId);
        Task<MeasureData> GetCCSWellCareDetails(DashBoardFilters data);
        Task<dynamic> GetCCSMeasureWellCarePatientList(DashBoardFilters data);
        Task<dynamic> GetCBPMeasureWellcareDetail(DashBoardFilters data);
        Task<dynamic> GetCBPWellcarePatientList(DashBoardFilters data);
        Task<dynamic> GetMeasureGoals(int UserId, int OrganizationId);
        Task<dynamic> IUGoals(GoalDetails details);
        Task<dynamic> SubmitMasterGoalsDetails(MasterGoal details);
        Task<dynamic> MasterSelectedMeasure(int OrganizationId, int Type);
        Task<dynamic> MasterMeasureGoals(int OrganizationId, int Type, string Search);
       
        Task<dynamic> GetMeasuresName(int userId);
        Task<dynamic> GetEntityGoal(int Id);
        Task<dynamic> GetEntityMeasure(int EntityType, int EntityId);
        Task<dynamic> CopyMersure(int CEntityType, int CEntityId, int userId, int EntityId, int EntityType);
        Task<dynamic> DeleteMeasure(int Id);
        Task<dynamic> DeleteAllMeasure(int Id, int Type);
        Task<int> SetMeasuredetails(MasterRule data);
        Task<string> GetMeasureDescription(int measureId, int entityId);
        Task<List<PatientClaimDetails>> GetClaimResult(ClaimFilters data);

        Task<dynamic> GetBCSMeasureWellCarePatientList(DashBoardFilters data);


    }
}
