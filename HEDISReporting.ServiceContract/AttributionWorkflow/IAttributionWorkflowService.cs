﻿using HEDISReporting.DataContract.AttributeExcel;
using HEDISReporting.DataContract.Dashboard;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HEDISReporting.ServiceContract.AttributionWorkflow
{
    public interface IAttributionWorkflowService : IBaseService
    {
        dynamic ReadExcelData(IFormFile file,string filename);
        Task<dynamic> OrganizationPayerList();
        Task<dynamic> GetNeedAttentionList(NeedAttentionAttribute needAttention);
        Task<dynamic> PatientAttributionStatus(DashBoardFilters data);
        Task<dynamic> UpdateNeedAttentionata(List<NeedAttention> needAttention);
        Task<dynamic> UpdateCareGapdata(List<NeedAttention> needAttention);
        Task<dynamic> UpdateNeedattentiondata(List<NeedAttention> needAttention);
    }
}
