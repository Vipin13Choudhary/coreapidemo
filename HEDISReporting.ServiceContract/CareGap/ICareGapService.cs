﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HEDISReporting.DataContract.ApiResponse;
using HEDISReporting.DataContract.Authentication;
using HEDISReporting.DataContract.Common;
using HEDISReporting.DataContract.Dashboard;
using HEDISReporting.DataContract.DataSource;
using HEDISReporting.DataContract.Patients;
using HEDISReporting.DataContract.SyncModels;
using Newtonsoft.Json.Linq;

namespace HEDISReporting.ServiceContract.CareGap
{
   public interface ICareGapService : IBaseService
    {
        Task<dynamic> GetABACareGap2018(DashBoardFilters data);
        Task<dynamic> GetCareGapPatientList(DashBoardFilters data);
        Task<dynamic> GetImunizationPatientList(DashBoardFilters data);
    }
}
