﻿using System;

namespace HEDISReporting.Common.CustomExceptions
{
    public class HEDISCustomException : Exception
    {
        public HEDISCustomException()
        { }

        public HEDISCustomException(string message)
            : base(message)
        { }

        public HEDISCustomException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
    public class HEDISCDAParsingException:Exception
    {
        public HEDISCDAParsingException()
        { }

        public HEDISCDAParsingException(string message)
            : base(message)
        { }

        public HEDISCDAParsingException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}