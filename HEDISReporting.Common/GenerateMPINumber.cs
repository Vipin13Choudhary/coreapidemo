﻿using HEDISReporting.Common.EncrytDecrypt;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace HEDISReporting.Common
{
   public static class GenerateMPINumber
    {
        public static string GetPatientMPINumber(string patientSsnNumber)
        {
           return EncrptDecryptPassword.Encrypt(patientSsnNumber);
        }
        public static string GetPatientSSNFromMPI(string patientMPI)
        {
            return EncrptDecryptPassword.Decrypt(patientMPI);
        }
        //This fncction is used to genrate MPI number with 4 Parameter and 4th parameter is patientAccId
        public static Guid GetPatientMPI(string firstName,string lastName,string dob,string patientAccId)
        {
            string comContent = string.Concat(firstName.ToLower(), lastName, dob, patientAccId);
           
            using (MD5 md5 = MD5.Create())
            {
                byte[] hash = md5.ComputeHash(Encoding.Default.GetBytes(comContent));
                Guid result = new Guid(hash);
                return result;
            }
           
        }
        //This fncction is used to genrate MPI number with 4 Parameter and 4th parameter is Gender
        public static string GenratePatientKMPINumber(string firstName, string lastName, string dob, string gender)
        {
            string comContent = string.Concat(firstName.ToLower(), lastName.ToLower(), dob, gender.ToLower().Substring(0, 1));

            using (MD5 md5 = MD5.Create())
            {
                byte[] hash = md5.ComputeHash(Encoding.Default.GetBytes(comContent));
                var result = GenerateMPINumber.ToHex(hash, true);
                return result;
            }

        }
        public static string ToHex(this byte[] bytes, bool upperCase)
        {
            StringBuilder result = new StringBuilder(bytes.Length * 2);

            for (int i = 0; i < bytes.Length; i++)
                result.Append(bytes[i].ToString(upperCase ? "X2" : "x2"));

            return result.ToString();
        }
    }
}
