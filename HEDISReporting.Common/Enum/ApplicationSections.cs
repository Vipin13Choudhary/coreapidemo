﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.Common.Enum
{
    public enum AppSections
    {
        CDAParsing = 1,
        GeneralException = 2,
        CustomException=3,
        ProceduralException=4
    }
}
