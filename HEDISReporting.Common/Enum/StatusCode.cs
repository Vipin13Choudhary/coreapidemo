﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.Common.Enum
{
  

      public enum LoginStatus
        {
            Success = 1,
            Invalid = 2,
            Error = 3
        }


    public enum DashboardDetails
    {
        AssGoal = 1,
        CopGoal = 2,
        OrgGoal = 3,
        PayGoal = 4,
        ProGoal = 5,
        ClinicGoal=6

    }

}
