﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.Common.Enum
{
    public enum Frquencies
    {
        Daily=1,
        Weekly,
        Monthly
    }
    public enum FrequencyDays
    {
        Monday=1,
        Tuesday,
        Wednesday,
        Thursday,
        Friday,
        Saturday,
        Sunday
    }
    public enum WeekNumber
    {
        First=1,
        Second,
        Third,
        Fourth,
        Fifth,
        Last
    }
}
