﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.Common
{
   public static class BMI
    {
        public static double BMICalculator(string Height, float Weightlbs)
        {
            float BMI = 0f;
            try
            {
                string[] HeightsplitFeetInches = Height.Split('.');
                int _height = Convert.ToInt32(Convert.ToInt32(HeightsplitFeetInches[0]) * 12) + (HeightsplitFeetInches.Length > 1 ? Convert.ToInt32(HeightsplitFeetInches[1]) : 0);
                BMI = ((Weightlbs) / (_height * _height)) * 703;
                return Math.Round(BMI, 2);
            }
            catch (Exception)
            {
                return 0;
            }
        }
    }
}
