﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.Common.Response
{
   public static class CommonResponse
    {
        #region User specific Response
        public static readonly string Res_InvalidCredential="Username and Password are invalid !";
        public static readonly string Res_StatusSuccess = "Success";
        public static readonly string Res_StatusFailure = "Failure";
        #endregion
        #region CCDA Response
        public static readonly string Res_CCDASuccess = "CCDAImportedSuccessfully";
        public static readonly string Res_CCDAError = "CCDAError";
        public static readonly string Res_ServerError = "ServerError";
        //public sta
        #endregion
    }
}
