﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.Common.SnomedApiModels
{
    public class DiagnosisModel
    {
        public string ICDCode { get; set; }
        public string DiagnosisDescription { get; set; }
        public string MapRule { get; set; }
        public string MapAdvice { get; set; }
    }
}
