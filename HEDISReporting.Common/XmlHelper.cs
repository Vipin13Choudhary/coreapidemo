﻿using Newtonsoft.Json;
using System;
using System.Dynamic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace HEDISReporting.Common
{
    public static class XmlHelper
    {
        /// <summary>
        /// Returns the input data as xml
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string GetAsXml<T>(T data)
        {
            //MemoryStream stream = null;
            //TextWriter writer = null;
            //try
            //{
            //    stream = new MemoryStream(); // read xml in memory
            //    writer = new StreamWriter(stream, Encoding.Unicode);
            //    var serializer = new XmlSerializer(typeof(T));
            //    serializer.Serialize(writer, data); // read object
            //    var count = (int)stream.Length; // saves object in memory stream
            //    var arr = new byte[count];
            //    stream.Seek(0, SeekOrigin.Begin);
            //    // copy stream contents in byte array
            //    stream.Read(arr, 0, count);
            //    var utf = new UnicodeEncoding(); // convert byte array to string
            //    //return ReplaceFirstOccurrance(utf.GetString(arr).Trim(), "utf-16", "utf-8");
            //    return utf.GetString(arr).Trim().Remove(0, 40);
            //}
            //catch (Exception ex)
            //{
            //    return string.Empty;
            //}
            //finally
            //{
            //    if (stream != null) stream.Close();
            //    if (writer != null) writer.Close();
            //}
            var result = string.Empty;
            var xmlData = string.Empty;

            using (StringWriter sw = new StringWriter())
            {
                // Exclude namespace from the xml
                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                ns.Add("", "");

                var serializer = new XmlSerializer(typeof(T));

                serializer.Serialize(sw, data, ns);

                result = sw.ToString();


            }

            if (!string.IsNullOrEmpty(result))
            {
                xmlData = XElement.Parse(result).ToString();
            }

            return xmlData;
        }
      

        /// <summary>
        /// Retruns the Xml data as an object of type T
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xmlData"></param>
        /// <returns></returns>
        public static T GetFromXml<T>(string xmlData)
        {
            var result = default(T);

            // Create Xml serializer
            XmlSerializer serializer = new XmlSerializer(typeof(T));

            // Deserialize 
            using (StringReader sr = new StringReader(xmlData))
            {
                result = (T)serializer.Deserialize(sr);
            }

            return result;
        }


        public static ExpandoObject ToExpandoObject(string xmlString)
        {
            var xmlNode = new XmlDocument();
            xmlNode.LoadXml(xmlString);            
            var json = JsonConvert.SerializeXmlNode(xmlNode.DocumentElement);
            var obj = JsonConvert.DeserializeObject<ExpandoObject>(json);
            return obj;
        }
    }
}
