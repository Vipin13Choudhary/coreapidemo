﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.Common.ExtensionMethods
{
    public static class PatientMPINumber
    {
        public static string GetPaitnetMPINumber(this string patientSeting)
        {
            //byte[] bytes = Encoding.ASCII.GetBytes(patientSeting);
            //int MPI = BitConverter.ToInt32(bytes, 0);
            //return MPI.ToString();
            char[] array = patientSeting.ToCharArray();
            string MPI = "";
            foreach (var item in array)
            {
                MPI = MPI + (int)item;
            }
            return MPI;
        }
        public static string GetPatientFromMPI(this string mpi)
        {
            //int _mpi = Convert.ToInt32(mpi);
            //byte[] bytes2 = BitConverter.GetBytes(_mpi);
            //string patientString = Encoding.ASCII.GetString(bytes2);
            //return patientString;
            char[] array = mpi.ToCharArray();
            string patientString = "";
            foreach (int item in array)
            {
                patientString = patientString + (char)item;
            }

            return patientString;
        }
    }
}
