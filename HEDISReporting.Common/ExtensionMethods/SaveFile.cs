﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http.Headers;
using System.Text;

namespace HEDISReporting.Common.ExtensionMethods
{
    public static class SaveFile
    {
        public static string storeAnyFile(IFormFile file) {
            string Url="";
            try
            {
                var folderName = Path.Combine("wwwroot","ExcelFile");
                var name=file.ContentDisposition;
                bool exist=System.IO.Directory.Exists(folderName);
                if(!exist)
                    System.IO.Directory.CreateDirectory(folderName);
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
                if (file.Length > 0)
                {
                    var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    var fullPath = Path.Combine(pathToSave, fileName);
                    var dbPath = Path.Combine(folderName, fileName);

                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }
                    Url=fullPath;
                }
            }
            catch (Exception ex) {
                Url = ex.Message;
            }
            return Url;
        }

        public static FileInfo ConvertxlsToxlsx(String filesFolder, string filename)
        {
            string sWebRootFolder =  Path.Combine(Directory.GetCurrentDirectory(), "wwwroot");
            var app = new Microsoft.Office.Interop.Excel.Application();
            //var Foldername = filesFolder.Replace("Copy of KPCA_Wellcare_Health_Help_Inc-Password Protected.xls", "");
            //bool exist = System.IO.Directory.Exists(Foldername);
            //if (!exist)
            //    System.IO.Directory.CreateDirectory(Foldername);
            var wb = app.Workbooks.Open(filesFolder);
            filesFolder = filesFolder.Split("xls")[0];
            wb.SaveAs(Filename: filesFolder + "xlsx", FileFormat: Microsoft.Office.Interop.Excel.XlFileFormat.xlOpenXMLWorkbook);
            wb.Close();
            app.Quit();
            FileInfo file = new FileInfo(Path.Combine(sWebRootFolder,"ExcelFile\\", filename.Split('.')[0] + ".xlsx"));
            return file;
        }
    }
}
