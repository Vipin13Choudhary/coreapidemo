﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.Common.ExtensionMethods
{
    public static class ExtensionMethods
    {
        public static T DeserializeFromJson<T>(this string json)
        {
            T result = default(T);

            if (json != null)
            {
                result = JsonConvert.DeserializeObject<T>(json);
                
            }
            return result;
        }
    }
}
