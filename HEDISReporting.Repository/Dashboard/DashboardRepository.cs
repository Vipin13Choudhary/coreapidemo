﻿using HEDISReporting.DataContract.Common;
using HEDISReporting.DataContract.Dashboard;
using HEDISReporting.DataContract.Enums.MeasureTypeMaster;
using HEDISReporting.DataContract.PatientChart;
using HEDISReporting.RepositoryContract.Dashboard;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;


namespace HEDISReporting.Repository.Dashboard
{
    public class DashboardRepository : BaseRepository, IDashboardRepository
    {
        public DashboardRepository(IConfiguration configuration) : base(configuration)
        {

        }
        
        public async Task<dynamic> GetAllABADetails(DashBoardFilters data)
        {


            object parameter = new
            {
                @TimePeriodStart = data.PeriodStart,
                @TimePeriodEnd = data.PeriodEnd,
                @OrganizationId = data.OrganizationId,
                @VendorId = data.PayerId,
                @AgeFrom = data.AgeRange.Split('-')[0],
                @AgeTo = data.AgeRange.Split('-')[1],
                @Gender = data.Gendor,
                @Ethinicity = data.EthnicityId,
                @Race = data.RaceId,
                @UserId = data.UserId,
                @NewCal = data.NewCal,
                @IsFilter = data.IsFilter 

            };
       
            dynamic MeasureData = await GetMultipleAsync<NumericalMeasureData, decimal>("usp_CalculateABAMeasure_HEDIS2018", parameter, null, CommandType.StoredProcedure);
            return MeasureData;
        }

        #region get all measure patient list
        public async Task<List<dynamic>> GetAllMeasurePatientList(DashBoardFilters data)

        {
            object parameters = new
            {
                @offset = data.offset,
                @limit = data.limit,
                @order = data.order,
                @sort = data.sort,
                @search = data.Search,
                @TimePeriodStart = data.PeriodStart,
                @TimePeriodEnd = data.PeriodEnd,
                @OrganizationId = data.OrganizationId,
                @VendorId = data.VendorId,
                @AgeFrom = data.AgeRange.Split('-')[0],
                @AgeTo = data.AgeRange.Split('-')[1],
                @Gender = data.Gendor,
                @Ethinicity = data.EthnicityId,
                @Race = data.RaceId,
                @Status = data.Status,
                @UserId = data.UserId,
                @Filter = data.IsFilter,
                @Flag = data.Flag
            };
            string measureProcedure = GetMeasureDataSetProcedure((MasterMeasure)data.MeasureId, MeasureTypeMasterEnum.HEDIS2018);
            dynamic measurePatientList = await GetAsync<dynamic>(measureProcedure, parameters, null, CommandType.StoredProcedure);
            return measurePatientList;
        }
        public static string GetMeasureDataSetProcedure(MasterMeasure measuerId, MeasureTypeMasterEnum measureType)
        {
            if (measureType== MeasureTypeMasterEnum.HEDIS2018)
            {
                switch (measuerId)
                {

                    case MasterMeasure.ABA:
                        return "usp_ABAMeasurePatientlist";
                    case MasterMeasure.BCS:
                        return "usp_BCSMeasurePatientlist";
                    case MasterMeasure.COL:
                        return "usp_COLMeasurePatientlist";
                    case MasterMeasure.CCS:
                        return "usp_CCLMeasurePatientlist";
                    case MasterMeasure.CHBP:
                        return "usp_CBPMeasurePatientlist";

                }
            }
            else
            {
                switch (measuerId)
                {

                    case MasterMeasure.ABA:
                        return "usp_ABAMeasurePatientlist_WellCare";
                    case MasterMeasure.BCS:
                        return "usp_BCSMeasurePatientlist_WellCare";
                    case MasterMeasure.COL:
                        return "usp_COLMeasurePatientlist_WellCare";
                    case MasterMeasure.CCS:
                        return "usp_CCSMeasurePatientlist_WellCare";
                    case MasterMeasure.CHBP:
                        return "usp_CBPMeasurePatientlist_WellCare";

                }
            }
            return string.Empty;
        }
        #endregion

        #region get all drop downs
        public async Task<dynamic> GetAllDropDowm(object userId)
        {

            object parameter = new
            {
                @UserId = userId

            };
            dynamic MeasureData = await GetMultipleAsyncOrg<DropDownListItem, DropDownListItem, DropDownListItem, DropDownListItem, DropDownListItem>("usp_GetAllFilterDropDown", parameter, null, CommandType.StoredProcedure);
            return MeasureData;
        }
        #endregion

        #region data for measure analysis
        public async Task<dynamic> GetMeasureAnalysis(DashBoardFilters data)
        {
            object parameters = new
            {

                @TimePeriodStart = data.PeriodStart,
                @TimePeriodEnd = data.PeriodEnd,
                @OrganizationId = data.OrganizationId,
                @VendorId = data.VendorId,
                @AgeFrom = data.AgeRange.Split('-')[0],
                @AgeTo = data.AgeRange.Split('-')[1],
                @Gender = data.Gendor,
                @Ethinicity = data.EthnicityId,
                @Race = data.RaceId,
                @UserId = data.UserId,
                @Filter = data.IsFilter,
                @MeasureId = data.MeasureId

            };
            dynamic measurePatientList = await GetMultipleAsync<MeasureAnalysisModel, MeasureAnalysisModel>("usp_MeasureAnalysisCount", parameters, null, CommandType.StoredProcedure);
            return measurePatientList;
        }
        #endregion

        #region get default goals
        public async Task<dynamic> GetDefaultGoalsDetails(DashBoardFilters data)
        {
            object getMeasureParameters = new
            {

                @OrganizationId = data.OrganizationId,
                @ClinicId = data.ClinicId,
                @ProviderId = data.ProviderId,
                @PayerId = data.PayerId


            };
            object getSettingParams = new
            {
                @UserId = data.UserId
            };
            dynamic GoalData = await GetMultipleAsyncOrg<IndividualGoalDetail, IndividualGoalDetail, IndividualGoalDetail, IndividualGoalDetail, IndividualGoalDetail>("usp_GetDefaultMeasureGoals", getMeasureParameters, null, CommandType.StoredProcedure);
            GoalData.DefaultSetting = await GetAsync<SavedDashboardGoalDetails>("usp_DefaultGoalsSetting", getSettingParams, null, CommandType.StoredProcedure);

            return GoalData;

        }

        #endregion

        #region save the dashboard details
        public async Task<dynamic> SaveDashboardGoalDetails(string xmlData)
        {
            object parameter = new
            {
                @xmlData = xmlData
            };
            dynamic result = await GetFirstOrDefaultAsync<int>("usp_SaveAndUpdateDashboardDetails", parameter, null, CommandType.StoredProcedure);
            return result;
        }

        #endregion
           
        #region get default goals settings
        public async Task<dynamic> GetDefaultGoalsSettings(int userId)
        {
            object parameter = new
            {
                @UserId = userId
            };
            dynamic DefaultSetting = await GetAsync<SavedDashboardGoalDetails>("usp_DefaultGoalsSetting", parameter, null, CommandType.StoredProcedure);

            return DefaultSetting;

        }

        #endregion

        #region get all the col details for the dashboard
        public async Task<dynamic> GetCOLAllDetails(DashBoardFilters data)
        {

            object parameter = new
            {
                @TimePeriodStart = data.PeriodStart,
                @TimePeriodEnd = data.PeriodEnd,
                @OrganizationId = data.OrganizationId,
                @VendorId = data.VendorId,
                @AgeFrom = data.AgeRange.Split('-')[0],
                @AgeTo = data.AgeRange.Split('-')[1],
                @Gender = data.Gendor,
                @Ethinicity = data.EthnicityId,
                @Race = data.RaceId,
                @UserId = data.UserId,
                @NewCal = data.NewCal,
                @IsFilter = data.IsFilter

            };
            dynamic MeasureData = await GetMultipleAsync<NumericalMeasureData, decimal>("usp_CalculateCOLMeasure_HEDIS2018", parameter, null, CommandType.StoredProcedure);
            return MeasureData;
        }


        #endregion

        #region get all the COL patient list
        public async Task<List<dynamic>> GetCOLPatientsList(DashBoardFilters data)
        {
            object parameters = new
            {
                @offset = data.offset,
                @limit = data.limit,
                @order = data.order,
                @sort = data.sort,
                @search = data.Search,
                @Status = data.Status,
                @TimePeriodStart = data.PeriodStart,
                @TimePeriodEnd = data.PeriodEnd,                                                              
                @OrganizationId = data.OrganizationId,
                @VendorId = data.VendorId,
                @AgeFrom = data.AgeRange.Split('-')[0],
                @AgeTo = data.AgeRange.Split('-')[1],
                @Gender = data.Gendor,
                @Ethinicity = data.EthnicityId,
                @Race = data.RaceId,
                @UserId = data.UserId,
                @Filter = data.IsFilter,
                @Flag = data.Flag
            };
            dynamic measurePatientList = await GetAsync<dynamic>("usp_COLMeasurePatientlist", parameters, null, CommandType.StoredProcedure);
            return measurePatientList;
        }


        #endregion   

        #region
        public async Task<dynamic> GetMeasures()
        {
           dynamic MeasureList = await GetMultipleAsync<MasterMeasureCategory, MasterMeasures>("GetMeasures", null, null, CommandType.StoredProcedure);
            return MeasureList;
        }
        #endregion

        #region get all CCS dashboard details
        public async Task<dynamic> GetCCSAllDetails(DashBoardFilters data)
        {
            object parameter = new
            {
                @TimePeriodStart = data.PeriodStart,
                @TimePeriodEnd = data.PeriodEnd,
                @OrganizationId = data.OrganizationId,
                @VendorId = data.VendorId,
                @AgeFrom = data.AgeRange.Split('-')[0],
                @AgeTo = data.AgeRange.Split('-')[1],
                @Gender = data.Gendor,
                @Ethinicity = data.EthnicityId,
                @Race = data.RaceId,
                @UserId = data.UserId,
                //@NewCal = data.NewCal==0?1:data.NewCal,
                @NewCal = data.NewCal,
                @IsFilter = data.IsFilter

            };
            dynamic MeasureData = await GetMultipleAsync<NumericalMeasureData, decimal>("usp_CalculateCCSMeasure_HEDIS2018", parameter, null, CommandType.StoredProcedure);
            return MeasureData;
        }


        public async Task<List<dynamic>> GetCCSPatientsList(DashBoardFilters data)
        {
            object parameters = new
            {
                @offset = data.offset,
                @limit = data.limit,
                @order = data.order,
                @sort = data.sort,
                @search = data.Search,
                @Status = data.Status,
                @TimePeriodStart = data.PeriodStart,
                @TimePeriodEnd = data.PeriodEnd,
                @OrganizationId = data.OrganizationId,
                @VendorId = data.VendorId,
                @AgeFrom = data.AgeRange.Split('-')[0],
                @AgeTo = data.AgeRange.Split('-')[1],
                @Gender = data.Gendor,
                @Ethinicity = data.EthnicityId,
                @Race = data.RaceId,
                @UserId = data.UserId,
                @Filter = data.IsFilter,
                @Flag = data.Flag
            };
            dynamic measurePatientList = await GetAsync<dynamic>("usp_CCLMeasurePatientlist", parameters, null, CommandType.StoredProcedure);
            return measurePatientList;
        }
        #endregion



        #region ----- To get all BCS details------
        public async Task<dynamic> GetBCSAllDetails(DashBoardFilters data)
        {
            try
            {
                object parameter = new
                {
                    @TimePeriodStart = data.PeriodStart,
                    @TimePeriodEnd = data.PeriodEnd,
                    @OrganizationId = data.OrganizationId,
                    @VendorId = data.VendorId,
                    @AgeFrom = data.AgeRange.Split('-')[0],
                    @AgeTo = data.AgeRange.Split('-')[1],
                    @Gender = data.Gendor,
                    @Ethinicity = data.EthnicityId,
                    @Race = data.RaceId,
                    @UserId = data.UserId,
                    @NewCal = data.NewCal,
                    @IsFilter = data.IsFilter

                };
                dynamic MeasureData = await GetMultipleAsync<NumericalMeasureData, decimal>("usp_CalculateBCSMeasure_HEDIS2018", parameter, null, CommandType.StoredProcedure);
                return MeasureData;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public async Task<List<dynamic>> GetBCSPatientsList(DashBoardFilters data)
        {
            object parameters = new
            {
                @offset = data.offset,
                @limit = data.limit,
                @order = data.order,
                @sort = string.IsNullOrEmpty(data.sort) ? null : data.sort,
                @search = data.Search,
                @Status = data.Status,
                @TimePeriodStart = data.PeriodStart,
                @TimePeriodEnd = data.PeriodEnd,
                @OrganizationId = data.OrganizationId,
                @VendorId = data.VendorId,
                @AgeFrom = data.AgeRange.Split('-')[0],
                @AgeTo = data.AgeRange.Split('-')[1],
                @Gender = data.Gendor,
                @Ethinicity = data.EthnicityId,
                @Race = data.RaceId,
                @UserId = data.UserId,
                @Filter = data.IsFilter,
                @Flag = data.Flag
            };
            dynamic measurePatientList = await GetAsync<dynamic>("usp_BCSMeasurePatientlist", parameters, null, CommandType.StoredProcedure);
            return measurePatientList;
        }

        #endregion


        #region -- To get CBP details -----

        public async Task<dynamic> GetCBPAllDetails(DashBoardFilters data)
        {
           
                object parameter = new
                {
                    @TimePeriodStart = data.PeriodStart,
                    @TimePeriodEnd = data.PeriodEnd,
                    @OrganizationId = data.OrganizationId,
                    @VendorId = data.VendorId,
                    @AgeFrom = data.AgeRange.Split('-')[0],
                    @AgeTo = data.AgeRange.Split('-')[1],
                    @Gender = data.Gendor,
                    @Ethinicity = data.EthnicityId,
                    @Race = data.RaceId,
                    @UserId = data.UserId,
                    @NewCal = data.NewCal,
                    @IsFilter = data.IsFilter

                };
                dynamic MeasureData = await GetMultipleAsync<NumericalMeasureData, decimal>("usp_CalculateCBPMeasure_HEDIS2018", parameter, null, CommandType.StoredProcedure);
                return MeasureData;
           
            
        }

        #endregion

        #region get the list of the measures (Hedis,Wellcare etc..)
        public async Task<List<MeasureTypeMaster>> GetAllMeasureTypeMaster()
        {
            dynamic MasterMeasureList = await GetAsync<MeasureTypeMaster>("usp_GetMeasureTypeMaster", null, null, CommandType.StoredProcedure);
            return MasterMeasureList;

        }

        public async Task<dynamic> GetMeasureDetails(int Id, string MeasureTypeId)
        {
            object parameter = new
            {
                @Id = Id,
                @MeasureType = Convert.ToInt32(MeasureTypeId)
            };
            dynamic MeasureList = await GetAsync<MasterRule>("GetMeasureDetails", parameter, null, CommandType.StoredProcedure);
            return MeasureList;
        }

        #endregion

        #region getABA result for well care

        public async Task<dynamic> GetABAWellCareDetails(DashBoardFilters data)
        {
            object parameter = new
            {
                @TimePeriodStart = data.PeriodStart,
                @TimePeriodEnd = data.PeriodEnd,
                @OrganizationId = data.OrganizationId,
                @VendorId = data.VendorId,
                @AgeFrom = data.AgeRange.Split('-')[0],
                @AgeTo = data.AgeRange.Split('-')[1],
                @Gender = data.Gendor,
                @Ethinicity = data.EthnicityId,
                @Race = data.RaceId,
                @UserId = data.UserId,
                @NewCal = data.NewCal,
                @IsFilter = data.IsFilter

            };
            dynamic MeasureData = await GetMultipleAsync<NumericalMeasureData, decimal>("usp_CalculateABAMeasure_WellCare2018", parameter, null, CommandType.StoredProcedure);
            return MeasureData;
        }



        public async Task<List<dynamic>> GetMeasureWellCarePatientList(DashBoardFilters data)
        {
          
            object parameters = new
            {
                @offset = data.offset,
                @limit = data.limit,
                @order = data.order,
                @sort = data.sort,
                @search = data.Search,
                @Status = data.Status,
                @TimePeriodStart = data.PeriodStart,
                @TimePeriodEnd = data.PeriodEnd,
                @OrganizationId = data.OrganizationId,
                @VendorId = data.VendorId,
                @AgeFrom = data.AgeRange.Split('-')[0],
                @AgeTo = data.AgeRange.Split('-')[1],
                @Gender = data.Gendor,
                @Ethinicity = data.EthnicityId,
                @Race = data.RaceId,
                @UserId = data.UserId,
                @Filter = data.IsFilter,
                @Flag = data.Flag
            };
            string measureProcedure = GetMeasureDataSetProcedure((MasterMeasure)data.MeasureId, MeasureTypeMasterEnum.WellCare2018);
            dynamic measurePatientList = await GetAsync<dynamic>(measureProcedure, parameters, null, CommandType.StoredProcedure);
            return measurePatientList;
        }
        #endregion



        #region
        public async Task<dynamic> GetBCSWellCareDetails(DashBoardFilters data)
        {
               object parameter = new
            {
                @TimePeriodStart = data.PeriodStart,
                @TimePeriodEnd = data.PeriodEnd,
                @OrganizationId = data.OrganizationId,
                @VendorId = data.VendorId,
                @AgeFrom = data.AgeRange.Split('-')[0],
                @AgeTo = data.AgeRange.Split('-')[1],
                @Gender = data.Gendor,
                @Ethinicity = data.EthnicityId,
                @Race = data.RaceId,
                @UserId = data.UserId,
                @NewCal = data.NewCal,
                @IsFilter = data.IsFilter

            };
                 dynamic MeasureData = await GetMultipleAsync<NumericalMeasureData, decimal>("usp_CalculateBCSMeasure_WellCare2018", parameter, null, CommandType.StoredProcedure);
            return MeasureData;
        }


        public async Task<List<dynamic>> GetBCSMeasureWellCarePatientList(DashBoardFilters data)
        {
            object parameters = new
            {
                @offset = data.offset,
                @limit = data.limit,
                @order = data.order,
                @sort = data.sort,
                @search = data.Search,
                @Status = data.Status,
                @TimePeriodStart = data.PeriodStart,
                @TimePeriodEnd = data.PeriodEnd,
                @OrganizationId = data.OrganizationId,
                @VendorId = data.VendorId,
                @AgeFrom = data.AgeRange.Split('-')[0],
                @AgeTo = data.AgeRange.Split('-')[1],
                @Gender = data.Gendor,
                @Ethinicity = data.EthnicityId,
                @Race = data.RaceId,
                @UserId = data.UserId,
                @Filter = data.IsFilter,
                @Flag = data.Flag
            };
            dynamic measurePatientList = await GetAsync<dynamic>("usp_BCSMeasurePatientlist_WellCare", parameters, null, CommandType.StoredProcedure);
            return measurePatientList;
        }
        #endregion

        //tet

        public async Task<dynamic> GetMeasureAnalysisWellCare(DashBoardFilters data)
        {
            object parameters = new
            {

                @TimePeriodStart = data.PeriodStart,
                @TimePeriodEnd = data.PeriodEnd,
                @OrganizationId = data.OrganizationId,
                @VendorId = data.VendorId,
                @AgeFrom = data.AgeRange.Split('-')[0],
                @AgeTo = data.AgeRange.Split('-')[1],
                @Gender = data.Gendor,
                @Ethinicity = data.EthnicityId,
                @Race = data.RaceId,
                @UserId = data.UserId,
                @Filter = data.IsFilter,
                @MeasureId = data.MeasureId

            };
            dynamic measurePatientList = await GetMultipleAsync<MeasureAnalysisModel, MeasureAnalysisModel>("usp_MeasureAnalysisCount_WellCare", parameters, null, CommandType.StoredProcedure);
            return measurePatientList;
        }

        #region get COL result for well care 



        public async Task<dynamic> GetCOLWellCareDetails(DashBoardFilters data)
        {

            object parameter = new
            {
                @TimePeriodStart = data.PeriodStart,
                @TimePeriodEnd = data.PeriodEnd,
                @OrganizationId = data.OrganizationId,
                @VendorId = data.VendorId,
                @AgeFrom = data.AgeRange.Split('-')[0],
                @AgeTo = data.AgeRange.Split('-')[1],
                @Gender = data.Gendor,
                @Ethinicity = data.EthnicityId,
                @Race = data.RaceId,
                @UserId = data.UserId,
                @NewCal = data.NewCal,
                @IsFilter = data.IsFilter

            };
            dynamic MeasureData = await GetMultipleAsync<NumericalMeasureData, decimal>("usp_CalculateCOLMeasure_WellCare2018", parameter, null, CommandType.StoredProcedure);
            return MeasureData;

        }



        public async Task<List<dynamic>> GetCOLMeasureWellCarePatientList(DashBoardFilters data)
        {
            object parameters = new
            {
                @offset = data.offset,
                @limit = data.limit,
                @order = data.order,
                @sort = data.sort,
                @search = data.Search,
                @Status = data.Status,
                @TimePeriodStart = data.PeriodStart,
                @TimePeriodEnd = data.PeriodEnd,
                @OrganizationId = data.OrganizationId,
                @VendorId = data.VendorId,
                @AgeFrom = data.AgeRange.Split('-')[0],
                @AgeTo = data.AgeRange.Split('-')[1],
                @Gender = data.Gendor,
                @Ethinicity = data.EthnicityId,
                @Race = data.RaceId,
                @UserId = data.UserId,
                @Filter = data.IsFilter,
                @Flag = data.Flag
            };
            dynamic measurePatientList = await GetAsync<dynamic>("usp_COLMeasurePatientlist_WellCare", parameters, null, CommandType.StoredProcedure);
            return measurePatientList;
        }
        #endregion
        public async Task<dynamic> GetCBPPatientsList(DashBoardFilters data)
        {
            object parameters = new
            {
                @offset = data.offset,
                @limit = data.limit,
                @order = data.order,
                @sort = data.sort,
                @search = data.Search,
                @Status = data.Status,
                @TimePeriodStart = data.PeriodStart,
                @TimePeriodEnd = data.PeriodEnd,
                @OrganizationId = data.OrganizationId,
                @VendorId = data.VendorId,
                @AgeFrom = data.AgeRange.Split('-')[0],
                @AgeTo = data.AgeRange.Split('-')[1],
                @Gender = data.Gendor,
                @Ethinicity = data.EthnicityId,
                @Race = data.RaceId,
                @UserId = data.UserId,
                @Filter = data.IsFilter,
                @Flag = data.Flag
            };
            dynamic measurePatientList = await GetAsync<dynamic>("usp_CBPMeasurePatientlist", parameters, null, CommandType.StoredProcedure);
            return measurePatientList;
        }


        #region  get CCS result for well care 
        public async Task<dynamic> GetCCSWellCareDetails(DashBoardFilters data)
        {
            object parameter = new
            {
                @TimePeriodStart = data.PeriodStart,
                @TimePeriodEnd = data.PeriodEnd,
                @OrganizationId = data.OrganizationId,
                @VendorId = data.VendorId,
                @AgeFrom = data.AgeRange.Split('-')[0],
                @AgeTo = data.AgeRange.Split('-')[1],
                @Gender = data.Gendor,
                @Ethinicity = data.EthnicityId,
                @Race = data.RaceId,
                @UserId = data.UserId,
                @NewCal = data.NewCal,
                @IsFilter = data.IsFilter

            };
            dynamic MeasureData = await GetMultipleAsync<NumericalMeasureData, decimal>("usp_CalculateCCSMeasure_WellCare2018", parameter, null, CommandType.StoredProcedure);
            return MeasureData;
        }



        public async Task<List<dynamic>> GetCCSMeasureWellCarePatientList(DashBoardFilters data)
        {
            object parameters = new
            {
                @offset = data.offset,
                @limit = data.limit,
                @order = data.order,
                @sort = data.sort,
                @search = data.Search,
                @Status = data.Status,
                @TimePeriodStart = data.PeriodStart,
                @TimePeriodEnd = data.PeriodEnd,
                @OrganizationId = data.OrganizationId,
                @VendorId = data.VendorId,
                @AgeFrom = data.AgeRange.Split('-')[0],
                @AgeTo = data.AgeRange.Split('-')[1],
                @Gender = data.Gendor,
                @Ethinicity = data.EthnicityId,
                @Race = data.RaceId,
                @UserId = data.UserId,
                @Filter = data.IsFilter,
                @Flag = data.Flag
            };
            dynamic measurePatientList = await GetAsync<dynamic>("usp_CCSMeasurePatientlist_WellCare", parameters, null, CommandType.StoredProcedure);
            return measurePatientList;
        }


        


        #endregion

        public async Task<dynamic> GetMeasureGoals(int userId, int organizationId)
        {
            object parameters = new
            {
                @UserId = userId,
                @OrganizationId = organizationId

            };
            dynamic measurePatientList = await GetMultipleAsyncOrg<IndividualGoalDetail, IndividualGoalDetail, IndividualGoalDetail, IndividualGoalDetail, IndividualGoalDetail>("usp_GetMeasureGoals", parameters, null, CommandType.StoredProcedure);
            return measurePatientList;
        }

        public async Task<dynamic> IUGoal(GoalDetails details)
        {
            object parameters = new
            {
                @ABAassociationgoal = details.aBAassociationgoal,
                @ABAclinicgoal = details.aBAclinicgoal,
                @ABAcorporategoal = details.aBAcorporategoal,
                @ABApayergoal = details.aBApayergoal,
                @ABAprovidergoal = details.aBAprovidergoal,
                @CCassociationgoal = details.cCassociationgoal,
                @CCclinicgoal = details.cCclinicgoal,
                @CCcorporategoal = details.cCcorporategoal,
                @CCpayergoal = details.cCpayergoal,
                @CCprovidergoal = details.cCprovidergoal,
                @BCSassociationgoal = details.bCSassociationgoal,
                @BCSclinicgoal = details.bCSclinicgoal,
                @BCScorporategoal = details.bCScorporategoal,
                @BCSpayergoal = details.bCSpayergoal,
                @BCSprovidergoal = details.bCSprovidergoal,
                @CCSassociationgoal = details.cCSassociationgoal,
                @CCSclinicgoal = details.cCSclinicgoal,
                @CCScorporategoal = details.cCScorporategoal,
                @CCSpayergoal = details.cCSpayergoal,
                @CCSprovidergoal = details.cCSprovidergoal,
                @CHBPassociationgoal = details.cHBPassociationgoal,
                @CHBPclinicgoal = details.cHBPclinicgoal,
                @CHBPcorporategoal = details.cHBPcorporategoal,
                @CHBPpayergoal = details.cHBPpayergoal,
                @CHBPprovidergoal = details.cHBPprovidergoal,
                @OrganizationId = details.organizationId,
                @IsEdit = details.isEdit

            };
            dynamic measurePatientList = await GetAsync<dynamic>("usp_IUGoal", parameters, null, CommandType.StoredProcedure);
            return measurePatientList;
        }

        #region  get CBP result for well care 
        public async Task<dynamic> GetCBPWellCareDetails(DashBoardFilters data)
        {


            object parameter = new
            {
                @TimePeriodStart = data.PeriodStart,
                    @TimePeriodEnd = data.PeriodEnd,
                    @OrganizationId = data.OrganizationId,
                    @VendorId = data.VendorId,
                    @AgeFrom = data.AgeRange.Split('-')[0],
                    @AgeTo = data.AgeRange.Split('-')[1],
                    @Gender = data.Gendor,
                    @Ethinicity = data.EthnicityId,
                    @Race = data.RaceId,
                    @UserId = data.UserId,
                    @NewCal = 1,
                    @IsFilter = data.IsFilter

                };
            dynamic MeasureData = await GetMultipleAsync<NumericalMeasureData, decimal>("usp_CalculateCBPWellcare_WellCare2018", parameter, null, CommandType.StoredProcedure);
            return MeasureData;
            //MeasureData = await GetMultipleAsync<NumericalMeasureData, decimal>("usp_CalculateCBPWellcare_WellCare2018", parameter, null, CommandType.StoredProcedure);



        }
        public async Task<List<dynamic>> GetCBPMeasureWellCarePatientList(DashBoardFilters data)
        {
            object parameters = new
            {
                @offset = data.offset,
                @limit = data.limit,
                @order = data.order,
                @sort = data.sort,
                @search = data.Search,
                @Status = data.Status,
                @TimePeriodStart = data.PeriodStart,
                @TimePeriodEnd = data.PeriodEnd,
                @OrganizationId = data.OrganizationId,
                @VendorId = data.VendorId,
                @AgeFrom = data.AgeRange.Split('-')[0],
                @AgeTo = data.AgeRange.Split('-')[1],
                @Gender = data.Gendor,
                @Ethinicity = data.EthnicityId,
                @Race = data.RaceId,
                @UserId = data.UserId,
                @Filter = data.IsFilter,
                @Flag = data.Flag
            };
            dynamic measurePatientList = await GetAsync<dynamic>("usp_CBPMeasurePatientlist_WellCare", parameters, null, CommandType.StoredProcedure);
            return measurePatientList;
        }

        public async Task<dynamic> GetClinicPayerForOrganizaton(int Id)
        {
            object parameters = new
            {
                @id = Id

            };
            dynamic measurePatientList = await GetMultipleAsync<OrganizationModal, ClinicModal, ProviderModal,MeasureCount> ("usp_GetClinicOrganization", parameters, null, CommandType.StoredProcedure);
            return measurePatientList;
        }

        public async Task<dynamic> SubmitMasterGoalsDetails(MasterGoal details)
        {
            object parameter = new
            {
                //@enitityid = details.EntityId,
                //@entityTypeId = details.EntityTypeId,
                //@aBAgoal = details.ABAgoal,
                //@cCgoal = details.CCgoal,
                //@bCSgoal = details.BCSgoal,
                //@cCSgoal = details.CCSgoal,
                //@cHBPgoal = details.CHBPgoal
                @enitityid = details.EntityId,
                @entityTypeId = details.EntityTypeId,
                @Goal = details.Goal,
                @Id = details.Id,
                @MeasureId = details.MeasureId,
                @UserId = details.UserId,
            };
            dynamic goalresullt = await GetAsync<int>("usp_InsertUpdateMasterGoal", parameter, null, CommandType.StoredProcedure);
            return goalresullt;
        }

        public async Task<dynamic> MasterSelectedMeasure(int OrganizationId, int Type)
        {
            object parameter = new
            {
                @Id = OrganizationId,
                @Type = Type
            };
            dynamic goalresullt = await GetAsync<dynamic>("usp_GetOrgSelectedMeasure", parameter, null, CommandType.StoredProcedure);
            return goalresullt;
        }

        public async Task<dynamic> MasterMeasureGoals(int OrganizationId, int Type)
        {
            object parameter = new
            {
                @EntityId = Type,
                @EntityType = OrganizationId

            };
            dynamic goalresullt = await GetMultipleAsync<string,OrgMeasureGoal>("usp_GetOrgMeasureGoals", parameter, null, CommandType.StoredProcedure);
            return goalresullt;
        }
        #endregion

        #region Get Measurenames
        public async Task<dynamic> GetMeasuresName(int userId)
        {
            object parameter = new
            {
                @userId = userId
            };
            dynamic Measurenames = await GetAsync<CompareToModel>("usp_GetMeasureNames", parameter, null, CommandType.StoredProcedure);
            return Measurenames;




        }


        #endregion
        #region save organization  region

        public async Task<int> SetMeasuredetails(string xmlData)
        {
            object parameters = new { @xmlData = xmlData };
                dynamic updatedId = await GetFirstOrDefaultAsync<int>("usp_SetMeasuredetails", parameters, null, CommandType.StoredProcedure);
                return updatedId;
        }
  

        #endregion


        #region Get Measure Description only by Measure Id and Type
        public async Task<string> GetMeasureDescription(int measureId,int entityId)
        {
            string query = "select isnull(Description,'') from MeasureRule where MeasureId=@measureId and MeasureType=@entityId and IsActive=1";
            object parameter = new
            {
                @measureId = measureId,
                @entityId = entityId
            };
           string measuerDescription= await GetFirstOrDefaultAsync<string>(query, parameter, null, CommandType.Text);
            return measuerDescription;
        }

        public async Task<dynamic> GetDefaultGoalsDetails(int userId)
        {
            object parameter = new
            {
                @id = userId
            };
            dynamic Measurenames = await GetAsync<dynamic>("usp_GetGoalDetails", parameter, null, CommandType.StoredProcedure);
            return Measurenames;
        }

        public async Task<dynamic> GetClinicPayerForOrganizaton()
        {
            object parameter = new
            {
                @id = 0
            };
            dynamic Measurenames = await GetMultipleAsync<OrganizationModal, ClinicModal, ProviderModal,MeasureCount>("usp_GetClinicOrganization", parameter, null, CommandType.StoredProcedure);
            return Measurenames;
        }

        public async Task<dynamic> MasterMeasureGoals1(int CEntityType, int CEntityId, int EntityType, int EntityId)
        {
            object parameter = new
            {
                @CEntityType = CEntityType,
                @CEntityId = CEntityId,
                @EntityType = EntityType,
                @EntityId = EntityId
            };
            //dynamic Measurenames = await GetMultipleAsync<string,MasterGoal>("usp_GetOrgMeasureGoals", parameter, null, CommandType.StoredProcedure);
            dynamic Measurenames = await GetAsync<MasterGoal>("usp_GetFilterGoals", parameter, null, CommandType.StoredProcedure);
            return Measurenames;
        }

        public async Task<dynamic> GetEntityGoal(int Id)
        {
            object parameter = new
            {
                @id = Id
            };
            dynamic Measurenames = await GetAsync<dynamic>("usp_GetGoalDetails", parameter, null, CommandType.StoredProcedure);
            return Measurenames;
        }

        public async Task<dynamic> GetEntityMeasure(int EntityType, int EntityId)
        {
            object parameter = new
            {
                @EntityType = EntityType,
                @EntityId = EntityId
            };
            dynamic Measurenames = await GetAsync<dynamic>("usp_GetEntityMeasure", parameter, null, CommandType.StoredProcedure);
            return Measurenames;
        }

        public async Task<dynamic> DeleteMeasure(int Id)
        {
            object parameter = new
            {
                @id = Id
            };
            dynamic Measurenames = await GetAsync<dynamic>("usp_DeleteMeasureGoal", parameter, null, CommandType.StoredProcedure);
            return Measurenames;
        }

        public async Task<dynamic> DeleteAllMeasure(int Id, int Type)
        {
            object parameter = new
            {
                @id = Id,
                @type=Type
            };
            dynamic Measurenames = await GetAsync<dynamic>("usp_DeleteAllMeasureGoal", parameter, null, CommandType.StoredProcedure);
            return Measurenames;
        }
        #endregion

        public async Task<List<ClaimDetails>> GetClaimPatientList(ClaimFilters data) {
            object parameters = new
            {
                @offset = data.offset,
                @limit = data.limit,
                @order = data.order,
                @sort = data.sort,
                //@search = data.Search,
                @OrganizationId=data.OrganizationId,
                @PatientId=data.PatientId,//9218
                @FromDate=data.FromDate,
                @ToDate=data.ToDate
            };
            dynamic MasterMeasureList = await GetAsync<ClaimDetails>("usp_GetPatientClaimDetails", parameters, null, CommandType.StoredProcedure);
            return MasterMeasureList;
        }
    }
}
