﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using HEDISReporting.DataContract.User;
using HEDISReporting.RepositoryContract.User;
using Microsoft.Extensions.Configuration;

namespace HEDISReporting.Repository.User
{
    public class UserRepository : BaseRepository, IUserRepository
    {
        public UserRepository(IConfiguration configuration):base(configuration)
        {
        }
        public async Task<UserModel> GetUserAsync(int userId)
        {
            //UserModel userModel = null;
            //var query = "";//Stored Procedure name.

            //// use dynamic parameter
            //DynamicParameters parameter = new DynamicParameters();
            //parameter.Add("@UserId", userId, DbType.Int32, ParameterDirection.Input);

            //userModel = new UserModel(){FirstName="Rakesh"};// await GetFirstOrDefaultAsync<UserModel>(query, parameter, commandType: CommandType.StoredProcedure);

            return  new UserModel();
        }
        public async Task<dynamic> GetUserMenus(int userId)
        {
            object parameters = new { @UserId = userId };
             dynamic userMenus = await GetMultipleAsync<UserMenus, UserSubMenus> ("usp_GetUserMenus", parameters, null, CommandType.StoredProcedure);
            return userMenus;
        }


        public async Task<dynamic> GetUserRoles(int userId)
        {
            object parameters = new { @UserId = userId };
            dynamic userRoles = await GetFirstOrDefaultAsync<UserRolesData>("usp_GetRoles", parameters, null, CommandType.StoredProcedure);
            return userRoles;
        }

        #region for getting the login userId
        public async Task<UserModel> getloginUserId(string aspNetUserId)
        {
            // conn.QueryFirstOrDefaultAsync<T>(sql, parameters, null, commandTimeout, commandType);
            string sql = "SELECT * FROM users WHERE AspNetUserId = @aspNetUserId;";
            return  await GetFirstOrDefaultAsync<UserModel>(sql, new { aspNetUserId =aspNetUserId }, null, null);
        }
        #endregion


    }
}
