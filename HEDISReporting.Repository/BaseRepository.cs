﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Dapper.FluentColumnMapping;
using HEDISReporting.DataContract;
using HEDISReporting.DataContract.Common;
using HEDISReporting.RepositoryContact;
using Microsoft.Extensions.Configuration;

namespace HEDISReporting.Repository
{
    /// <summary>
    ///  Base Repository to work with database.
    /// </summary>    
    public class BaseRepository : IBaseRepository
    {
        public IConfiguration Configuration;
        
        private string _connectionString= string.Empty;
        // column mapping collection
        private ColumnMappingCollection _mappings = null;
        public IConfiguration LabConnection;

        private string _labConnection = string.Empty;

        public BaseRepository(IConfiguration configuration)
        {
            _mappings = new ColumnMappingCollection();

            Configuration = configuration;

            _connectionString = configuration.GetSection("Data:ConnectionString").Value;


            LabConnection = configuration;

            _labConnection = configuration.GetSection("Data:LabConnectionString").Value;
        }

        /// <summary>
        /// Gets HealthCheck for Base Repository. Declared virtual for other Repositories to override.
        /// </summary>
        /// <returns>UTC datetime</returns>
        public virtual async Task<object> HealthCheckAsync()
        {
            DateTime currentUtcDateTime = DateTime.MinValue;

            var sql = "Select GetUtcDate()";

            currentUtcDateTime = await GetFirstOrDefaultAsync<DateTime>(sql, null, commandType: CommandType.Text);

            return currentUtcDateTime;
        }

        /// <summary>
        /// Returns the first result set based on the type of the requested entity.
        /// Example : To get details of an address based on address id.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        public T GetFirstOrDefault<T>(string sql, object parameters = null, int? commandTimeout = null, CommandType? commandType = null)
        {
            T result = default(T);

            using (IDbConnection conn = GetConnection())
            {
                result = conn.QueryFirstOrDefault<T>(sql, parameters, null, commandTimeout, commandType);
            }

            return result;
        }

        /// <summary>
        /// Returns the first result set based on the type of the requested entity.
        /// Example : To get details of an address based on address id.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        public async Task<T> GetFirstOrDefaultAsync<T>(string sql, object parameters = null, int? commandTimeout = null, CommandType? commandType = null)
        {
            T result = default(T);

            using (IDbConnection conn = GetConnection())
            {
                result = await conn.QueryFirstOrDefaultAsync<T>(sql, parameters, null, commandTimeout, commandType);
            }

            return result;
        }

        /// <summary>
        /// Returns an collection of the type of requested entity.
        /// Example: To list of all address.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        public IEnumerable<T> Get<T>(string sql, object parameters = null, int? commandTimeout = null, CommandType? commandType = null)
        {
            IEnumerable<T> result = default(IEnumerable<T>);

            using (IDbConnection conn = GetConnection())
            {
                result = conn.Query<T>(sql, parameters, null, true, commandTimeout, commandType);
            }

            return result;
        }

        /// <summary>
        /// Returns an collection of the type of requested entity.
        /// Example: To list of all address.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        public async Task<IEnumerable<T>> GetAsync<T>(string sql, object parameters = null, int? commandTimeout = null, CommandType? commandType = null)
        {
            IEnumerable<T> result = default(IEnumerable<T>);

            using (IDbConnection conn = GetConnection())
            {
                result = await conn.QueryAsync<T>(sql, parameters, null, commandTimeout, commandType);
            }
        
            return result;
        }

        /// <summary>
        /// Adds an entity. 
        /// The onus of getting the Id of the newly added entity is on each class that implements this API.
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        public int Add(string sql, object parameters = null, int? commandTimeout = null, CommandType? commandType = null)
        {
            var result = 0;

            using (IDbConnection conn = GetConnection())
            {
                result = conn.Execute(sql, parameters, null, commandTimeout, commandType);
            }

            return result;
        }

        /// <summary>
        /// Adds an entity. 
        /// The onus of getting the Id of the newly added entity is on each class that implements this API.
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        public async Task<int> AddAsync(string sql, object parameters = null, int? commandTimeout = null, CommandType? commandType = null)
        {
            var result = 0;

            using (IDbConnection conn = GetConnection())
            {
                result = await conn.ExecuteAsync(sql, parameters, null, commandTimeout, commandType);
            }

            return result;
        }

        /// <summary>
        /// Updates an entity. 
        /// The onus of getting the Id of the newly added entity is on each class that implements this API.
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        public async Task<int> UpdateAsync(string sql, object parameters = null, int? commandTimeout = null, CommandType? commandType = null)
        {
            return await this.AddAsync(sql, parameters, commandTimeout, commandType);
        }

        /// <summary>
        /// Deletes an entity. 
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        protected async Task DeleteAsync(string sql, object parameters = null, int? commandTimeout = null, CommandType? commandType = null)
        {
            using (IDbConnection conn = GetConnection())
            {
                await conn.ExecuteAsync(sql, parameters, null, commandTimeout, commandType);
            }
        }

        /// <summary>
        /// Returns multiple results based on the types specified.
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        public async Task<dynamic> GetMultipleAsync<T1, T2>(string sql, object parameters = null, int? commandTimeout = null, CommandType? commandType = null)
        {
            try
            {
                dynamic result = new ExpandoObject();

                using (IDbConnection conn = GetConnection())
                {
                    using (var multi = await conn.QueryMultipleAsync(sql, parameters, null, commandTimeout, commandType))
                    {

                        var t1 = multi.Read<T1>();
                        var t2 = multi.Read<T2>();

                        result.Item1 = t1;
                        result.Item2 = t2;
                    }
                }

                return result;
            }
            catch(Exception EX)
            {
                throw null;
            }
        }

        /// <summary>
        /// Registers the list of properties which you would like to map to it's corresponsing sql column name.
        /// This is usually used where your object's property name is different from the underline sql column name.
        /// </summary>
        /// <typeparam name="T">The type of the object</typeparam>
        /// <param name="columns">list fo columns</param>
        public void RegisterColumnMappings<T>(IEnumerable<SqlColumnMapping> columns)
        {
            // register only if the type is not already added to the mapping collection
            if (!_mappings.Mappings.Keys.Contains(typeof(T)))
            {
                var mappedType = _mappings.RegisterType<T>();

                foreach (var item in columns)
                {
                    mappedType
                    .MapProperty(item.Source).ToColumn(item.Target);
                }

                // register with dapper
                _mappings.RegisterWithDapper();
            }
        }


        /// <summary>
        /// Returns multiple (more then two currently static ) results based on the types specified.
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        
        public async Task<dynamic> GetMultipleAsyncOrg<T1, T2,T3,T4,T5>(string sql, object parameters = null, int? commandTimeout = null, CommandType? commandType = null)
        {
            dynamic result = new ExpandoObject();
            //try
            //{
                using (IDbConnection conn = GetConnection())
                {
                    using (var multi = await conn.QueryMultipleAsync(sql, parameters, null, commandTimeout, commandType))
                    {
                        var t1 = multi.IsConsumed==false? multi.Read<T1>():null;
                        var t2 = multi.IsConsumed == false ? multi.Read<T2>():null;
                        var t3 = multi.IsConsumed == false ? multi.Read<T3>():null;
                        var t4 = multi.IsConsumed == false ? multi.Read<T4>():null;
                        var t5 = multi.IsConsumed == false ? multi.Read<T5>():null;
                        result.Item1 = t1;
                        result.Item2 = t2;
                        result.Item3 = t3;
                        result.Item4 = t4;
                        result.Item5 = t5;
                    }
                }
            //}
            //catch(Exception ex)
            //{
            //    var resulttest = ex;
            //}
           

            return result;
        }
        /// <summary>
        /// Returns multiple with 4 arguments
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        public async Task<dynamic> GetMultipleAsync<T1, T2, T3, T4>(string sql, object parameters = null, int? commandTimeout = null, CommandType? commandType = null)
        {
            dynamic result = new ExpandoObject();
            try
            {
                using (IDbConnection conn = GetConnection())
                {
                    using (var multi = await conn.QueryMultipleAsync(sql, parameters, null, commandTimeout, commandType))
                    {
                        var t1 = multi.IsConsumed == false ? multi.Read<T1>() : null;
                        var t2 = multi.IsConsumed == false ? multi.Read<T2>() : null;
                        var t3 = multi.IsConsumed == false ? multi.Read<T3>() : null;
                        var t4 = multi.IsConsumed == false ? multi.Read<T4>() : null;
                        result.Item1 = t1;
                        result.Item2 = t2;
                        result.Item3 = t3;
                        result.Item4 = t4;
                    }
                }
            }
            catch (Exception ex)
            {
                var resulttest = ex;
            }


            return result;
        }
        /// <summary>
        /// Returns multiple with 3 arguments
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        public async Task<dynamic> GetMultipleAsync<T1, T2, T3>(string sql, object parameters = null, int? commandTimeout = null, CommandType? commandType = null)
        {
            dynamic result = new ExpandoObject();
            try
            {
                using (IDbConnection conn = GetConnection())
                {
                    using (var multi = await conn.QueryMultipleAsync(sql, parameters, null, commandTimeout, commandType))
                    {
                        var t1 = multi.IsConsumed == false ? multi.Read<T1>() : null;
                        var t2 = multi.IsConsumed == false ? multi.Read<T2>() : null;
                        var t3 = multi.IsConsumed == false ? multi.Read<T3>() : null;
                        result.Item1 = t1;
                        result.Item2 = t2;
                        result.Item3 = t3;

                    }
                }
            }
            catch (Exception ex)
            {
                var resulttest = ex;
            }


            return result;
        }
        public async Task SaveExceptionLogs(int status, string stackTrace, string userName, string userId, int section,string fileName,string dataXML)
        {
            object parameters = new { status, stackTrace, moduleName = "", userName, userId, section, fileName, dataXML };
            await AddAsync("usp_LogExceptions", parameters, null, System.Data.CommandType.StoredProcedure);
        }
        /// <summary>
        /// Returns the sql connection based on the database against which the comnection is requested for.
        /// The default is against DatabaseOne database.
        /// </summary>
        /// <param name="databaseName"></param>
        /// <returns></returns>

        private IDbConnection GetConnection()
        {
            string connectionString = _connectionString;
            return new SqlConnection(connectionString);
        }


        private IDbConnection GetLabConnection()
        {
            string labConnectionString = _labConnection;
            return new SqlConnection(labConnectionString);
        }


        /// <summary>
        /// Get Patient Details for the patient
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        public async Task<dynamic> GetPatientDetailsForLab<T>(string sql, object parameters = null, int? commandTimeout = null, CommandType? commandType = null)
        {

            dynamic result = new ExpandoObject();
            try
            {
                using (IDbConnection conn = GetConnection())
                {
                    using (var multi = await conn.QueryMultipleAsync(sql, parameters, null, commandTimeout, commandType))
                    {
                        var t1 = multi.IsConsumed == false ? multi.Read<T>() : null;
                        result.Item1 = t1;

                    }
                }
            }
            catch (Exception ex)
            {
                var resulttest = ex;
            }


            return result;

        }



        /// <summary>
        /// To get lab details for patient
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        public async Task<dynamic> GetLabDetailsForPatient<T>(string sql, object parameters = null, int? commandTimeout = null, CommandType? commandType = null)
        {

            dynamic result = new ExpandoObject();
            try
            {
                using (IDbConnection conn = GetConnection())
                {
                    using (var multi = await conn.QueryMultipleAsync(sql, parameters, null, commandTimeout, commandType))
                    {
                        var t1 = multi.IsConsumed == false ? multi.Read<T>() : null;
                        result.Item1 = t1;

                    }
                }
            }
            catch (Exception ex)
            {
                var resulttest = ex;
            }


            return result;

        }

        /// <summary>
        /// To get lab details for patient
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        public async Task<dynamic> GetNeedAttentionPatient<T>(string sql, object parameters = null, int? commandTimeout = null, CommandType? commandType = null)
        {

            dynamic result = new ExpandoObject();
            try
            {
                using (IDbConnection conn = GetConnection())
                {
                    using (var multi = await conn.QueryMultipleAsync(sql, parameters, null, commandTimeout, commandType))
                    {
                        var t1 = multi.IsConsumed == false ? multi.Read<T>() : null;
                        result.Item1 = t1;

                    }
                }
            }
            catch (Exception ex)
            {
                var resulttest = ex;
            }


            return result;

        }

    }
}
