﻿using HEDISReporting.RepositoryContract.Exception;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HEDISReporting.Repository
{
    public class ExceptionRepository : BaseRepository, IExceptionRepository
    {
        public ExceptionRepository(IConfiguration configuration):base(configuration)
        {
        }
       
    }
}
