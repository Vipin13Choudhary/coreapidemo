﻿using HEDISReporting.CCDAParsing.Model;
using HEDISReporting.CCDAParsing.RecordParser;
using HEDISReporting.DataContract.Common;
using HEDISReporting.Common;
using HEDISReporting.RepositoryContract.Patient;
using Microsoft.Extensions.Configuration;
using System;
using System.Data;
using System.IO;
using System.Threading.Tasks;
using HEDISReporting.DataContract.PatientChart;
using PatientAllergies = HEDISReporting.DataContract.PatientChart.PatientAllergies;
using HEDISReporting.DataContract.DataSource;
using HEDISReporting.DataContract.SyncModels;
using HEDISReporting.DataContract.Exceptions;
using System.Net;
using HEDISReporting.Common.Enum;
using System.Xml;
using HEDISReporting.DataContract.Patients;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Linq;

namespace HEDISReporting.Repository.Patient
{
    public class PatientRepository : BaseRepository,IPatientRepository
    {
        private readonly IConfiguration _configuration;
        public PatientRepository(IConfiguration configuration) : base(configuration)
        {
            this._configuration = configuration;
        }

        /// <summary>
        /// import patient info from ccda(Manual Uploaded files)
        /// <Updates>
        /// <date>1/3/2019</date>
        /// <description>
        /// file naming convention changes to MPI from ssn for security reason.
        /// Introduced MPI Number which will unique for any paticular patient based on his/her ssn although this will be an encrypted string
        /// </description>
        /// </Updates>
        /// </summary>
        /// <param name="base64File"></param>
        /// <param name="organizationID"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        public async Task<int> ImportPatientCCDA(string base64File, int organizationID, int userID,string receivedFile)
        {
            #region Add xsl sheet to xml
            base64File = base64File.Replace("  ", " ");
            XmlDocument doc = new XmlDocument();
            doc.PreserveWhitespace = true;
            doc.LoadXml(base64File);
            
            string xlsSection =  _configuration["Domain:DomainUrl"] + @"/xlssheet/cda_hedisxsl.xsl";
            if(doc.FirstChild.Name== "xml-stylesheet")
            {
                base64File=base64File.Replace(doc.FirstChild.OuterXml, $@"<?xml-stylesheet type='text/xsl' href='{xlsSection}'?>");
            }
            else if(doc.FirstChild.NextSibling.Name== "xml-stylesheet")
            {
                base64File = base64File.Replace(doc.FirstChild.NextSibling.OuterXml, $@"<?xml-stylesheet type='text/xsl' href='{xlsSection}'?>");
                //doc.FirstChild.NextSibling.OuterXml = doc.FirstChild.NextSibling.OuterXml.Replace(doc.FirstChild.NextSibling.OuterXml, $@"<?xml-stylesheet type='text/xsl' href='{xlsSection}'?>");
            }
            #endregion
            string filePathRead = Directory.GetCurrentDirectory() + "\\wwwroot\\CDA\\" + String.Format("{0}_{1:yyyyMMddhhmmss}", "cda", DateTime.Now) + ".xml";
            File.WriteAllText(filePathRead, base64File);
            RecordParser recordParser = new RecordParser();
            PatientClinicalInformation patientClinicalInformation = new PatientClinicalInformation();
            var xmlPatientDetail = "";
            try
            {
                patientClinicalInformation = recordParser.ParseCCDAFile(filePathRead, true);
                string PatientSSN = patientClinicalInformation.ptDemographicDetail.SSN;
                string firstName = patientClinicalInformation.ptDemographicDetail.FirstName;
                string lastName = patientClinicalInformation.ptDemographicDetail.LastName;
                string dob = patientClinicalInformation.ptDemographicDetail.DateofBirth;
                string gender = patientClinicalInformation.ptDemographicDetail.gender;
                string patientAccId = patientClinicalInformation.ptDemographicDetail.SSN;
                // Add on 30042019
                string KMPI = GenerateMPINumber.GenratePatientKMPINumber(firstName, lastName, dob, gender);
                //End 
                //--Get The Detail of patient from MPI number;
                string patientMPI = GenerateMPINumber.GetPatientMPINumber(PatientSSN);
                //To be used later
                //Guid testMPI = GenerateMPINumber.GetPatientMPI(firstName, lastName, dob, patientAccId);
                patientClinicalInformation.ptDemographicDetail.MPI = patientMPI;
                patientClinicalInformation.ptDemographicDetail.KMPI = KMPI;
                String fileName = string.Concat(patientClinicalInformation.ptDemographicDetail.LastName.ToLower(),"_", patientClinicalInformation.ptDemographicDetail.FirstName.ToLower());
                patientClinicalInformation.ptDemographicDetail.ReceivedFile = receivedFile.Split('.')[0];
                patientClinicalInformation.ptDemographicDetail.CCDAFileName = String.Format("{0}_{1:yyyyMMddhhmmss}", string.Concat(fileName,'_', patientClinicalInformation.ptDemographicDetail.ReceivedFile), DateTime.UtcNow);
                //string getPatientSSNBack = GenerateMPINumber.GetPatientSSNFromMPI(patientMPI);
                DataSourceResponseModel folderdetail =await GetOrganizationFolderDetail(organizationID);

               
                xmlPatientDetail = XmlHelper.GetAsXml(patientClinicalInformation);
                object parameters = new { @patientDetail = xmlPatientDetail, @organizationId = organizationID, @userId = userID,@fileName= patientClinicalInformation.ptDemographicDetail.CCDAFileName };
                int updatedId = await GetFirstOrDefaultAsync<int>("usp_InsertPatientCDAData", parameters, null, CommandType.StoredProcedure);

                if (folderdetail!=null && !string.IsNullOrEmpty(folderdetail.OrganizationName) && !string.IsNullOrEmpty(folderdetail.MethodName))
                   {
                    string baseDirectory = string.Concat(Directory.GetCurrentDirectory(), @"\", "wwwroot", @"\");
                    string organizationPath = baseDirectory + folderdetail.OrganizationName + @"\";
                    string dataSoruceTypePath = organizationPath + folderdetail.MethodName;
                    string successFilePath = dataSoruceTypePath + @"\" + "SuccessFiles";
                    string failourFilePath = dataSoruceTypePath + @"\" + "ExceptionalFiles";
                     if(updatedId>0)
                    {
                        dataSoruceTypePath = successFilePath;
                    }
                    else
                    {
                        dataSoruceTypePath = failourFilePath;
                    }
                    /*
                     there are two points of directory creation 
                     1) when datasource created.
                     2) when first file  is received.
                     */
                    //create the directory for organization if does not exist that will be used to store the files fatched from sftp
                    if (!Directory.Exists(dataSoruceTypePath))
                    {
                        Directory.CreateDirectory(dataSoruceTypePath);
                    }
                    patientClinicalInformation.ptDemographicDetail.CCDAFileName = string.Concat(patientClinicalInformation.ptDemographicDetail.CCDAFileName,"_", updatedId.ToString(), ".xml");
                    string destinationFilePath = dataSoruceTypePath + @"\" + $@"{ patientClinicalInformation.ptDemographicDetail.CCDAFileName}";
                    File.Copy(filePathRead, destinationFilePath);
                    File.Delete(filePathRead);
                }
              
                return updatedId;
            }
            catch (Exception Ex)
            {
                ExceptionModal exceptionModal = new ExceptionModal();
                var err = Ex.Message + " " + Ex.StackTrace;
                exceptionModal.Message = err;
                exceptionModal.Status = HttpStatusCode.InternalServerError;
                exceptionModal.Section = AppSections.CDAParsing;
                await SaveExceptionLogs((int)exceptionModal.Status, exceptionModal.Message, "", userID.ToString(), 
                    (int)exceptionModal.Section,string .Concat (patientClinicalInformation.ptDemographicDetail.CCDAFileName,'_',"0.xml")
                    , xmlPatientDetail);
                return 0;
            }
        }

        #region
        public async Task<dynamic> GetAllPatientList(OrganizationListModel data)
        {
            object parameters = new
            {
                data.offset,
                data.limit,
                data.order,
                data.sort,
                data.Search,
                data.OrganizationId
            };
            dynamic patientList = await GetAsync<dynamic>("usp_GetPatientList", parameters, null, CommandType.StoredProcedure);
            return patientList;
        }
        #endregion

        #region Get Ethnicity
        public async Task<dynamic> GetEthnicity(int EthnicityId)
        {
            object parameter = new { @EthnicityId = EthnicityId };
            dynamic Ethnicity = await GetMultipleAsyncOrg<DropDownListItem, DropDownListItem, DropDownListItem, DropDownListItem, DropDownListItem>("usp_GetEthnicity", parameter, null, CommandType.StoredProcedure);
            return Ethnicity;
        }
        #endregion

        #region Get Race
        public async Task<dynamic> GetRace(int RaceId)
        {
            object parameter = new { @RaceId = RaceId };
            dynamic race = await GetMultipleAsyncOrg<DropDownListItem, DropDownListItem, DropDownListItem, DropDownListItem, DropDownListItem>("usp_GetRace", parameter, null, CommandType.StoredProcedure);
            return race;
        }
        #endregion

        #region Get Gender
        public async Task<dynamic> GetGender(int GenderId)
        {
            object parameter = new { @GenderId = GenderId };
            dynamic Gender = await GetMultipleAsyncOrg<DropDownListItem, DropDownListItem, DropDownListItem, DropDownListItem, DropDownListItem>("usp_GetGender", parameter, null, CommandType.StoredProcedure);
            return Gender;
        }
        #endregion

        #region Get Organization
        public async Task<dynamic> GetOrganization(int OrganizationId)
        {
            object parameter = new { @OrganizationId = OrganizationId };
            dynamic Organization = await GetMultipleAsyncOrg<DropDownListItem, DropDownListItem, DropDownListItem, DropDownListItem, DropDownListItem>("usp_GetAllOrganization", parameter, null, CommandType.StoredProcedure);
            return Organization;
        }
        #endregion
        #region Patient chart
        public async Task<dynamic> GetPatientChart(int paientId, int sectionId, int tabId)
        {
            object parameters = new { @PatientId = paientId, @Section = sectionId, @TabId = tabId };
            dynamic patientChart = await GetMultipleAsyncOrg<PatientDemographicsModel, SnomedCodeModel, SnomedIcdMap, PatientLabTest, PatientAllergies>("usp_GetPatientChart", parameters, null, CommandType.StoredProcedure);
            return patientChart;
            


        }
        public async Task<dynamic> GetPatientChartTabs(int paientId, int sectionId, int tabId)
        {
            object parameters = new { @PatientId = paientId, @Section = sectionId, @TabId = tabId };
            dynamic patientChart = await GetMultipleAsync<SnomedCodeModel, SnomedIcdMap, PatientLabTest, PatientAllergies>("usp_GetPatientChart", parameters, null, CommandType.StoredProcedure);
            return patientChart;
            
        }

        #endregion
        #region Get Patient sftp Folder Detail
        public async Task<DataSourceResponseModel> GetOrganizationFolderDetail(int organizationId)
        {
            object parameters = new { @orgId = organizationId };
            return await GetFirstOrDefaultAsync<DataSourceResponseModel>("GetOrganizationFolderDetail", parameters, null, CommandType.StoredProcedure);
        }
        #endregion
        #region GetFilteredPatientList
        public async Task<dynamic> GetFilteredPatientList(int EthnicityId, int OrganizationId, int RaceId,int GenderId)
        {
            object parameters = new
            {
                @EthnicityId = EthnicityId,
                @OrganizationId = OrganizationId,
                @RaceId = RaceId,
                @GenderId = GenderId
            };
            dynamic patientfilterList = await GetAsync<dynamic>("usp_GetFilteredPatientList", parameters, null, CommandType.StoredProcedure);
            return patientfilterList;
        }
        #endregion
        public async Task<SftpHedisRequestModel> GetDataSourceDetailForSync(int organizationId)
        {
           
                object parameters = new { @OrgId = organizationId };
                dynamic dataSourceDetail = await GetMultipleAsync<GetSource, ReceiveSource, FileSyncs>("usp_GetDataSourceDetailForSync", parameters, null, CommandType.StoredProcedure);
                SftpHedisRequestModel sftpRequestModel = new SftpHedisRequestModel();
                sftpRequestModel.getSource = ((List<GetSource>)dataSourceDetail.Item1).Count != 0 ? dataSourceDetail.Item1[0] :new  GetSource();
                sftpRequestModel.receiveSource = ((List<ReceiveSource>)dataSourceDetail.Item2).Count != 0 ? dataSourceDetail.Item2[0] : new ReceiveSource();
                sftpRequestModel.fileSyncs = ((List<FileSyncs>)dataSourceDetail.Item3).Count != 0 ? dataSourceDetail.Item3 : new List<FileSyncs>();
            return sftpRequestModel;
             
        }
        public async Task<bool> InsertSyncedFiles(string syncedFiles,int organizationId,int userId)
        {
            object parameters = new { @syncedFileCml= syncedFiles, @OrgId = organizationId, @userId=userId };
            bool IsAllDone = await GetFirstOrDefaultAsync<bool>("usp_InsertSyncedFiles", parameters, null, CommandType.StoredProcedure);
            return IsAllDone;
        }
        #region Get Clinics
        public async Task<dynamic> GetClinics(int OrganizationId)
        {
            object parameter = new { @OrganizationId = OrganizationId };
            dynamic Organization = await GetMultipleAsyncOrg<DropDownListItem, DropDownListItem, DropDownListItem, DropDownListItem, DropDownListItem>("usp_GetClinicsByOrganizationId", parameter, null, CommandType.StoredProcedure);
            return Organization;
        }
        #endregion
        #region Get Providers
        public async Task<dynamic> GetProviders(int ClinicId)
        {
            object parameter = new { @ClinicId = ClinicId };
            dynamic Organization = await GetMultipleAsyncOrg<DropDownListItem, DropDownListItem, DropDownListItem, DropDownListItem, DropDownListItem>("usp_GetProvidersByClinicId", parameter, null, CommandType.StoredProcedure);
            return Organization;
        }
        #endregion
        #region Get Payers
        public async Task<dynamic> GetPayers()
        {
           // object parameter = new { @ClinicId = ClinicId };
            dynamic Payers = await GetMultipleAsyncOrg<DropDownListItem, DropDownListItem, DropDownListItem, DropDownListItem, DropDownListItem>("usp_GetPayers", null, null, CommandType.StoredProcedure);
            return Payers;
        }
        #endregion
        public async Task<dynamic> GetLabResultForPatient(LabListingOperands data)
        {
            object parameter = new
            {
                @offset = data.offset,
                @limit = data.limit,
                @order = data.order,
                @sort = data.sort,
                @LabName = data.LabName,
                @OrderNo = data.OrderNo,
                @TestName = data.TestName,
                @FromDate = data.FromDate,
                @ToDate = data.ToDate,
                @PatientId = data.PatientId
            };
            return await GetLabDetailsForPatient<dynamic>("usp_GetLabPatientList", parameter, null, CommandType.StoredProcedure);
        }
        public async Task<dynamic> GetPatientResultForLab(int patientId)
        {
            object parameters = new { @patientIdParam = 2 };
            return await GetLabDetailsForPatient<dynamic>("select * from v_PatientDataExtract where HL7TransactionId = @patientId", null, null, CommandType.Text);
        }
        public async Task<dynamic> GetPatientStatus(int patientId)
        {
            object parameters = new { @PatientId = patientId };
            return await GetAsync<dynamic>("usp_GetPatientStatus", parameters, null, CommandType.StoredProcedure);
        }
        public async Task<PatientCCDAModel> GetDeIdentifiedInfo(int patipenId)
        {
            object parameters = new { @patientId = patipenId };
            dynamic dataSourceDetail = await GetAsync<PatientCCDAModel>("sp_GetPatientDeIdentifiedInfo", parameters, null, CommandType.StoredProcedure);
            List<PatientCCDAModel> deIdentifiedInfo = new List<PatientCCDAModel>();
            deIdentifiedInfo = (List<PatientCCDAModel>)dataSourceDetail;
            return deIdentifiedInfo.FirstOrDefault();
        }
        #region Get Patient All Status and Reason Code
        public async Task<dynamic> GetStatusReasonCode()
        {
            return await GetMultipleAsync<dynamic, dynamic>("usp_GetStatusReasonCode", null, null, CommandType.StoredProcedure);
        }
        #endregion
    }
}
