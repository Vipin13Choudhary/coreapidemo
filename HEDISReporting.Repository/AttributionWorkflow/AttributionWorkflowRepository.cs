﻿using ExcelDataReader;
using HEDISReporting.Common;
using HEDISReporting.DataContract.AttributeExcel;
using HEDISReporting.DataContract.Dashboard;
using HEDISReporting.DataContract.DataSource;
using HEDISReporting.RepositoryContract.AttributionWorkflow;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;

using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace HEDISReporting.Repository.AttributionWorkflow
{
    public class AttributionWorkflowRepository : BaseRepository, IAttributionWorkflowRepository
    {
        private readonly IConfiguration _configuration;
        public AttributionWorkflowRepository(IConfiguration configuration) : base(configuration)
        {

        }
        public dynamic ReadExcelData(FileInfo files,int OrganizationId, int PayerId,int UserId)
        {
            List<ExcelData> list = new List<ExcelData>();
            try
            {
                //System.Text.Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
                using (var stream = File.Open(files.FullName, FileMode.Open, FileAccess.Read))
                {
                    // Auto-detect format, supports:
                    //  - Binary Excel files (2.0-2003 format; *.xls)
                    //  - OpenXml Excel files (2007 format; *.xlsx)
                    using (var reader = ExcelReaderFactory.CreateReader(stream))
                    {
                        // Choose one of either 1 or 2:
                        // 1. Use the reader methods
                        do
                        {
                            int count = 0;
                            while (reader.Read())
                            {
                                ExcelData excelData = new ExcelData();
                                excelData.LOB = reader.GetValue(0) == null ? "" : reader.GetValue(0).ToString().Trim();
                                excelData.MASTER_IPA = reader.GetValue(1) == null ? "" : reader.GetValue(1).ToString().Trim();
                                excelData.IPA_ID = reader.GetValue(2) == null ? "" : reader.GetValue(2).ToString().Trim();
                                excelData.SEQ_MEMB_ID = reader.GetValue(3)== null ? "" : reader.GetValue(3).ToString().Trim();
                                excelData.SUBSCRIBER_ID = reader.GetValue(4) == null ? "" : reader.GetValue(4).ToString().Trim();
                                excelData.FUNDING_COUNTY = reader.GetValue(5) == null ? "" : reader.GetValue(5).ToString().Trim();
                                excelData.PLAN_CODE = reader.GetValue(6) == null ? "" : reader.GetValue(6).ToString().Trim();
                                excelData.SEQ_PCP_ID = reader.GetValue(7) == null ? "" : reader.GetValue(7).ToString().Trim();
                                excelData.PCP_FIRST_NAME = reader.GetValue(8) == null ? "" : reader.GetValue(8).ToString().Trim();
                                excelData.PCP_LAST_NAME = reader.GetValue(9) == null ? "" : reader.GetValue(9).ToString().Trim();
                                excelData.COHORT = reader.GetValue(10) == null ? "" : reader.GetValue(10).ToString().Trim();
                                excelData.ELIG_CATEGORY = reader.GetValue(11) == null ? "" : reader.GetValue(11).ToString().Trim();
                                var tempdata = reader.GetValue(12) == null ? "" : reader.GetValue(12).ToString().Trim();
                                Nullable<DateTime> dob = new Nullable<DateTime>();
                                if (tempdata != null && count != 0)
                                {
                                    //double tempDob = Convert.ToDouble(tempdata);
                                    dob = Convert.ToDateTime(tempdata);
                                }
                                excelData.DATE_OF_BIRTH = dob==null? dob : dob.Value;
                                excelData.GENDER = reader.GetValue(13) == null ? "" : reader.GetValue(13).ToString().Trim();
                                excelData.MEDICAID_NO = reader.GetValue(14) == null ? "" : reader.GetValue(14).ToString().Trim();
                                excelData.MEDICARE_NO = reader.GetValue(15) == null ? "" : reader.GetValue(15).ToString().Trim();
                                excelData.LAST_NAME = reader.GetValue(16) == null ? "" : reader.GetValue(16).ToString().Trim();
                                excelData.FIRST_NAME = reader.GetValue(17) == null ? "" : reader.GetValue(17).ToString().Trim();
                                excelData.ADDRESS_LINE_1 = reader.GetValue(18) == null ? "" : reader.GetValue(18).ToString().Trim();
                                excelData.ADDRESS_LINE_2 = reader.GetValue(19)== null ? "" : reader.GetValue(19).ToString().Trim();
                                excelData.CITY = reader.GetValue(20) == null ? "" : reader.GetValue(20).ToString().Trim();
                                excelData.STATE = reader.GetValue(21) == null ? "" : reader.GetValue(21).ToString().Trim();
                                excelData.ZIP = reader.GetValue(22) == null ? "" : reader.GetValue(22).ToString().Trim();
                                excelData.RISK_SCORE_AB = reader.GetValue(23) == null ? "" : reader.GetValue(23).ToString().Trim();
                                excelData.RISK_SCORE_D = reader.GetValue(24) == null ? "" : reader.GetValue(24).ToString().Trim();
                                excelData.HOSPICE = reader.GetValue(25) == null ? "" : reader.GetValue(25).ToString().Trim();
                                excelData.ESRD = reader.GetValue(26) == null ? "" : reader.GetValue(26).ToString().Trim();
                                excelData.INSTITUTIONAL = reader.GetValue(27) == null ? "" : reader.GetValue(27).ToString().Trim();
                                excelData.NURSING_HOME_CERTIFIABLE = reader.GetValue(28) == null ? "" : reader.GetValue(28).ToString().Trim();
                                excelData.MEDICAID = reader.GetValue(29) == null ? "" : reader.GetValue(29).ToString().Trim();
                                excelData.MEDICAID_ADD_ON = reader.GetValue(30) == null ? "" : reader.GetValue(30).ToString().Trim();
                                excelData.PREVIOUS_DISABLE = reader.GetValue(31) == null ? "" : reader.GetValue(31).ToString().Trim();
                                excelData.HOME_PHONE_NUMBER = reader.GetValue(32) == null ? "" : reader.GetValue(32).ToString().Trim();
                                excelData.DUAL_ELIG = reader.GetValue(33) == null ? "" : reader.GetValue(33).ToString().Trim();
                                excelData.RECEIVEDMONTH = reader.GetValue(34) == null ? "" : reader.GetValue(34).ToString().Trim();
                                excelData.PayerId = PayerId;
                                excelData.OrganizationId = OrganizationId;
                                excelData.KMPI = Convert.ToString(GenerateMPINumber.GenratePatientKMPINumber(excelData.FIRST_NAME, excelData.LAST_NAME, Convert.ToString(excelData.DATE_OF_BIRTH), excelData.GENDER));
                                list.Add(excelData);
                                count++;
                            }
                        } while (reader.NextResult());

                        // 2. Use the AsDataSet extension method
                        //var result1 = reader.AsDataSet();
                        list.RemoveAt(0);
                        dynamic result = ConvertListToXMLAsync(list, OrganizationId, PayerId, UserId);
                        return result;
                        // The result of each spreadsheet is in result.Tables
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public dynamic ConvertListToXMLAsync(List<ExcelData> exceldata,int OrganizationId,int PayerId,int UserId)
        {
            dynamic dataSourceResponse = "";
            try
            {
                var xmlData = new XElement("ExcelImportdata",
                            from patient in exceldata
                            select new XElement("ExcelImportdata",
                                   new XAttribute("LOB", patient.LOB),
                                   new XElement("MASTER_IPA", patient.MASTER_IPA),
                                   new XElement("IPA_ID", patient.IPA_ID),
                                   new XElement("SEQ_MEMB_ID", patient.SEQ_MEMB_ID),
                                   new XElement("SUBSCRIBER_ID", patient.SUBSCRIBER_ID),
                                   new XElement("FUNDING_COUNTY", patient.FUNDING_COUNTY),
                                   new XElement("PLAN_CODE", patient.PLAN_CODE),
                                   new XElement("SEQ_PCP_ID", patient.SEQ_PCP_ID),
                                   new XElement("PCP_FIRST_NAME", patient.PCP_FIRST_NAME),
                                   new XElement("PCP_LAST_NAME", patient.PCP_LAST_NAME),
                                   new XElement("COHORT", patient.COHORT),
                                   new XElement("ELIG_CATEGORY", patient.ELIG_CATEGORY),
                                   new XElement("DATE_OF_BIRTH", patient.DATE_OF_BIRTH),
                                   new XElement("GENDER", patient.GENDER),
                                   new XElement("MEDICAID_NO", patient.MEDICAID_NO),
                                   new XElement("MEDICARE_NO", patient.MEDICARE_NO),
                                   new XElement("LAST_NAME", patient.LAST_NAME),
                                   new XElement("FIRST_NAME", patient.FIRST_NAME),
                                   new XElement("ADDRESS_LINE_1", patient.ADDRESS_LINE_1),
                                   new XElement("ADDRESS_LINE_2", patient.ADDRESS_LINE_2),
                                   new XElement("CITY", patient.CITY),
                                   new XElement("STATE", patient.STATE),
                                   new XElement("ZIP", patient.ZIP),
                                   new XElement("RISK_SCORE_AB", patient.RISK_SCORE_AB),
                                   new XElement("RISK_SCORE_D", patient.RISK_SCORE_D),
                                   new XElement("HOSPICE", patient.HOSPICE),
                                   new XElement("ESRD", patient.ESRD),
                                   new XElement("INSTITUTIONAL", patient.INSTITUTIONAL),
                                   new XElement("NURSING_HOME_CERTIFIABLE", patient.NURSING_HOME_CERTIFIABLE),
                                   new XElement("MEDICAID", patient.MEDICAID),
                                   new XElement("MEDICAID_ADD_ON", patient.MEDICAID_ADD_ON),
                                   new XElement("PREVIOUS_DISABLE", patient.PREVIOUS_DISABLE),
                                   new XElement("HOME_PHONE_NUMBER", patient.HOME_PHONE_NUMBER),
                                   new XElement("DUAL_ELIG", patient.DUAL_ELIG),
                                   new XElement("RECEIVEDMONTH", patient.RECEIVEDMONTH),
                                   new XElement("PayerId", PayerId),
                                   new XElement("OrganizationId", OrganizationId),
                                   new XElement("KMPI", patient.KMPI)
                                  ));
                object parameters = new { @xmlData = xmlData,@UserId= UserId };
                dataSourceResponse = GetFirstOrDefault<dynamic>("usp_InsertBulkXmldata", parameters, 40000, CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return dataSourceResponse;
        }

        public async Task<dynamic> OrganizationPayerList()
        {
            DataSourceResponseModel dataSourceResponse = new DataSourceResponseModel();
            dynamic SourceResponse = await GetMultipleAsync<dynamic, dynamic>("usp_GetAllOrganizationAndPayer", null, null, CommandType.StoredProcedure);
            return SourceResponse;

        }
        public async Task<dynamic> GetAttentionList(NeedAttentionAttribute needAttention)
        {
            object parameters = new { @PatientId = needAttention.patientId };
            dynamic SourceResponse = await GetNeedAttentionPatient<dynamic>("usp_NewPatients", parameters, null, CommandType.StoredProcedure);
            //needAttention.patientId = 10045;
            //object parameters = new { @sort = needAttention.sort, @order = needAttention.order, @limit = needAttention.limit, @offset = needAttention.offset, @PatientId = needAttention.patientId };
            //dynamic SourceResponse = await GetNeedAttentionPatient<dynamic>("usp_NeedsAttention", parameters, null, CommandType.StoredProcedure);
            return SourceResponse;

        }
        public async Task<dynamic> PatientAttributionStatus(DashBoardFilters data)
        {
            object parameters = new { @offset = data.offset, @limit =data.limit, @order = data.order, @sort = data.sort, @search = data.Search, @organizationid = data.OrganizationId, };
            dynamic SourceResponse = await GetMultipleAsync<dynamic,dynamic>("usp_GetAttributionPatientStatus", parameters, null, CommandType.StoredProcedure);
            return SourceResponse;

        }

        public async Task<dynamic> UpdateNeedAttentionata(List<NeedAttention> needAttention)
        {
            dynamic SourceResponse=null;
            foreach (var item in needAttention)
            {
                object parameters = new
                {
                    @PatientWorkItemsId = item.PatientWorkItemsId,
                    @PatientId = item.PatientId,
                    @PatientStatusCode = item.PatientStatusCode,
                    @ReasonCode = item.ReasonCode,
                    @PatientStatusCodeNew = item.StatusCodeId,
                    @ReasonCodeNew = item.ReasonCodeId,
                    @Comment = item.PatientComment
                };
                 SourceResponse = await GetFirstOrDefaultAsync<dynamic>("usp_UpdateNeedAttentionStatus", parameters, null, CommandType.StoredProcedure);
            }
            return SourceResponse;

        }

        public async Task<dynamic> UpdateCareGapdata(List<NeedAttention> needAttention)
        {
            dynamic SourceResponse = null;
            foreach (var item in needAttention)
            {
                object parameters = new
                {
                    @PatientWorkItemsId = item.PatientWorkItemsId,
                    @PatientId = item.PatientId,
                    @PatientStatusCode = item.PatientStatusCode,
                    @ReasonCode = item.ReasonCode,
                    @PatientStatusCodeNew = item.StatusCodeId,
                    @ReasonCodeNew = item.ReasonCodeId,
                    @Comment = item.PatientComment
                };
                SourceResponse = await GetFirstOrDefaultAsync<dynamic>("usp_UpdateCareGapStatus ", parameters, null, CommandType.StoredProcedure);
            }
            return SourceResponse;
        }

        public async Task<dynamic> UpdateNeedattentiondata(List<NeedAttention> needAttention)
        {
            dynamic SourceResponse = null;
            foreach (var item in needAttention)
            {
                object parameters = new
                {
                    @PatientWorkItemsId = item.PatientWorkItemsId,
                    @PatientId = item.PatientId,
                    @PatientStatusCode = item.PatientStatusCode,
                    @ReasonCode = item.ReasonCode,
                    @Comment = item.PatientComment
                };
                SourceResponse = await GetFirstOrDefaultAsync<dynamic>("usp_UpdateNeedAttention ", parameters, null, CommandType.StoredProcedure);
            }
            return SourceResponse;
        }
    }
}
