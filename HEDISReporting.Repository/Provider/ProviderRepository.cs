﻿using HEDISReporting.DataContract.Provider;
using HEDISReporting.RepositoryContract.Provider;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace HEDISReporting.Repository.Provider
{
    public class ProviderRepository:BaseRepository, IProviderRepository
    {
        public ProviderRepository(IConfiguration configuration) : base(configuration)
        {

        }

        public async Task<dynamic> GetProviderById(int Id)
        {
            object parameters = new { @Id = Id };
            dynamic provider = await GetAsync<ProviderModel>("usp_ProviderDetailById", parameters, null, CommandType.StoredProcedure);
            return provider;
        }

        #region get organization list
        public async Task<dynamic> GetProviderList(ProviderListingOperands data)
        {
            object parameters = new
            {
                @offset = data.offset,
                @limit = data.limit,
                @order = data.order,
                @sort = data.sort,
                @search = data.Search,
                @ProviderId = data.ProviderId
            };
            dynamic organizationList = await GetAsync<dynamic>("usp_GetProviderList", parameters, null, CommandType.StoredProcedure);
            return organizationList;
        }
        #endregion
    }
}
