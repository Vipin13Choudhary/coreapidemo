﻿using HEDISReporting.DataContract.Common;
using HEDISReporting.DataContract.DataSource;
using HEDISReporting.DataContract.Organization;
using HEDISReporting.RepositoryContract.Organization;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace HEDISReporting.Repository.Organization
{
    public class OrganizationRepository : BaseRepository, IOrganizationRepository
    {

        public OrganizationRepository(IConfiguration configuration) : base(configuration)
        {

        }

        #region get organization list
        public async Task<dynamic> GetOrganizationList(OrganizationListingOperands data)
        {
            object parameters = new
            {
                @offset = data.offset,
                @limit = data.limit,
                @order = data.order,
                @sort = data.sort,
                @search = data.Search,
                @OrganizationId = data.OrganizationId
            };
            dynamic organizationList = await GetAsync<dynamic>("usp_GetOrganizationList", parameters, null, CommandType.StoredProcedure);
            return organizationList;
        }
        #endregion

        #region get all organizatoin dropdown
        public async Task<dynamic> GetAllDropDown(int OrganizationId, string UserId)
        {
            try
            {
                object parameters = new { @AspNetUserId = UserId, @OrganizationId = OrganizationId };

                dynamic organizationList = await GetMultipleAsyncOrg<DropDownListItem, States, DropDownListItem, DropDownListItem, DropDownListItem>("usp_GetOrganizationDropdown", parameters, null, CommandType.StoredProcedure);
                return organizationList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #endregion
        #region Organization Configuration
        public async Task<DataSourceResponseModel> SaveOrganizationConfiguration(string xmlData, bool IsEdit)
        {
            object parameters = new { @xmlData = xmlData };
            DataSourceResponseModel dataSourceResponse = new DataSourceResponseModel();

            if (IsEdit == false)
            {
                dataSourceResponse = await GetFirstOrDefaultAsync<DataSourceResponseModel>("usp_InsertOrganizationDataSource", parameters, null, CommandType.StoredProcedure);
                return dataSourceResponse;
            }
            else
            {
                dataSourceResponse = await GetFirstOrDefaultAsync<DataSourceResponseModel>("usp_UpdateOrganizationDataSource", parameters, null, CommandType.StoredProcedure);
                return dataSourceResponse;
            }
        }
        public async Task<int> SaveOrgnizationClinic(string xmlData,int clinicId)
        {
            
            object parameters = new { @xmlData = xmlData };
            if (clinicId > 0)
            {
                int updatedId = await GetFirstOrDefaultAsync<int>("usp_UpdateOrganizationClinicInfo", parameters, null, CommandType.StoredProcedure);
                return updatedId;
            }
            else
            {
                int savedId = await GetFirstOrDefaultAsync<int>("usp_InsertOrganizationClinicInfo", parameters, null, CommandType.StoredProcedure);
                return savedId;
            }
        
        }
        public async Task<dynamic> GetOrganizationConfigurationList(OrganizationConfigurationListModel filterItems)
        {
            object parameters = new
            {
                filterItems.offset,
                filterItems.limit,
                filterItems.order,
                filterItems.sort,
                filterItems.Search,
                filterItems.OrganizationId,
                filterItems.DataSourceId

            };
            dynamic sftpList = await GetAsync<dynamic>("usp_GetOrganizationConfigList", parameters, null, CommandType.StoredProcedure);
            return sftpList;
        }
        #endregion

        #region save organization

        public async Task<int> SaveOrganization(string xmlData,int OrganizationId)
        {
            object parameters = new { @xmlData = xmlData };
            if (OrganizationId > 0) {
                dynamic updatedId = await GetFirstOrDefaultAsync<int>("usp_UpdateOrganization", parameters, null, CommandType.StoredProcedure);
                return updatedId;
            }
            else
            {
                dynamic savedId = await GetFirstOrDefaultAsync<int>("usp_InsertOrganization", parameters, null, CommandType.StoredProcedure);
                return savedId;
            }
         
        }

        #endregion


        #region  save organization department


        public async Task<int> SaveOrganizationDepartment(string xmlData,int departmentId)
        {
            object parameters = new { @xmlData = xmlData };
            if (departmentId > 0)
            {
                dynamic savedId = await GetFirstOrDefaultAsync<int>("usp_UpdateOrganizationDepartment", parameters, null, CommandType.StoredProcedure);
                return savedId;

            }
            else
            {
                dynamic savedId = await GetFirstOrDefaultAsync<int>("usp_InsertOrganizationDepartment", parameters, null, CommandType.StoredProcedure);
                return savedId;
            }
        }

        #endregion

        #region  save organization department list


        public async Task<dynamic> GetDepartmentList(DepartmentListOperands data)
        {
            object parameters = new
            {
                @offset = data.offset,
                @limit = data.limit,
                @order = data.order,
                @sort = data.sort,
                @search = data.Search,
                @ClinicId = data.ClinicId,
                @OrganizationId = data.organizationId
            };
            dynamic organizationList = await GetAsync<dynamic>("usp_GetOrganizationDepartmentList", parameters, null, CommandType.StoredProcedure);
            return organizationList;
        }

        #endregion
        #region Organizations Clinics
        public async Task<dynamic> GetOrganizationsClinicsList(OrganizationConfigurationListModel filterItems)
        {
            object parameters = new
            {
                filterItems.offset,
                filterItems.limit,
                filterItems.order,
                filterItems.sort,
                filterItems.Search,
                filterItems.OrganizationId
            };
            dynamic sftpList = await GetAsync<dynamic>("usp_GetOrganizationsClinics", parameters, null, CommandType.StoredProcedure);
            return sftpList;
        }
        #endregion
        #region Get Country and State
        public async Task<dynamic> GetCountriesStates(int countryId)
        {
            object parameter = new { @CountryId = countryId };
            dynamic countriesSates = await GetMultipleAsyncOrg<DropDownListItem, DropDownListItem, DropDownListItem, DropDownListItem, DropDownListItem>("usp_GetCountryStateDrop", parameter, null, CommandType.StoredProcedure);
            return countriesSates;
        }
        #endregion

        #region get organization region list
        public async Task<dynamic> GetOrganizationRegionList(OrgRegionListingOperands data)
        {


            object parameters = new
            {
                data.offset,
                data.limit,
                data.order,
                data.sort,
                data.Search,
                data.IsActive,
                data.OrganizationId
            };
            dynamic regionList = await GetAsync<dynamic>("usp_GetOrganizationRegionList", parameters, null, CommandType.StoredProcedure);
            return regionList;

        }
        #endregion

        #region save organization  region

        public async Task<int> SaveOrganizationRegion(string xmlData,int regionId)
        {
            object parameters = new { @xmlData = xmlData };
            if (regionId > 0)
            {
                dynamic updatedId = await GetFirstOrDefaultAsync<int>("usp_UpdateOrganizationRegion", parameters, null, CommandType.StoredProcedure);
                return updatedId;
            }
            else
            {
                dynamic savedId = await GetFirstOrDefaultAsync<int>("usp_InsertOrganizationRegion", parameters, null, CommandType.StoredProcedure);
                return savedId;
            }

        }


        #endregion
        #region Get Data Source Drop Downs
        public async Task<dynamic> GetDataSourceDropDowns(int ConfigurationTypeId)
        {
            object parameters = new { @ConfigurationTypeId = ConfigurationTypeId };
            dynamic dataSourcesDropDowns = await GetMultipleAsyncOrg<DropDownListItem, DropDownListItem, DropDownListItem, DropDownListItem, DropDownListItem>
                ("usp_GetOrganizationDataSourceDropDowns", parameters, null, CommandType.StoredProcedure);
            return dataSourcesDropDowns;
        }
        #endregion

        #region delete organization
        public async Task<int> DeleteOrganizationData(DeleteData data)
        {
            object parameters = new { @Id = data.Id , @TableName=data.TableName, @UserId=data.UserId };
            dynamic deletedId = await GetFirstOrDefaultAsync<int>("usp_DeletedSelectedItem", parameters, null, CommandType.StoredProcedure);
            return deletedId;


        }
        #endregion

        #region get organization detail by id
        public async Task<dynamic> GetOrganizationDetailById(int organizationId)
        {
            object parameters = new { @Id = organizationId};
            dynamic data = await GetMultipleAsync<OrganizationModel, RegionDropDowns>("usp_OrganizationDetailById", parameters, null, CommandType.StoredProcedure);
            return data;
        }
        #endregion

        #region
        public async Task<dynamic> GetOrganizationClinicDetailById(int clinicId)
        {
            object parameters = new { @Id = clinicId };
            dynamic data = await GetFirstOrDefaultAsync<OrganizationClinic>("usp_ClinicDetailById", parameters, null, CommandType.StoredProcedure);
            return data;

        }
        #endregion

        #region get organization department detail
        public async Task<dynamic> GetOrganizationDepartmentDetailById(int departmentId)
        {
            object parameters = new { @Id = departmentId };
            dynamic data = await GetFirstOrDefaultAsync<OrganizationDepartment>("usp_DepartmentDetailById", parameters, null, CommandType.StoredProcedure);
            return data;

        }
        #endregion

        #region get datasource detail

        public async Task<dynamic> GetOrganizationsDataSourceDetail(int organizationId,int DataSourceId)
        {
            object parameter = new { @Id = organizationId, @DataSourceId = DataSourceId };
            dynamic data = await GetMultipleAsyncOrg<OrganizationDataSource, GetDataSource, GetDataSourceSftp, ReceiveDataSource, DropDownListItem>("usp_OrganizationDataSourceDetail", parameter, null, CommandType.StoredProcedure);

            return data;
        }


        public async Task<dynamic> GetCountryISDFromIP(double longIP)
        {
            object parameters = new { @IP = longIP };
            dynamic data = await GetFirstOrDefaultAsync<CountryCodeDetails>("usp_GetCountryCodeFromIP", parameters, null, CommandType.StoredProcedure);
            return data;
        }

        public async Task<dynamic> GetAllEntity(int TypeId,int OrganizationId)
        {
            object parameters = new { @type = TypeId, @OrganizationId= OrganizationId };

            dynamic organizationList = await GetAsync<DropDownListItem>("usp_GetFilterEntity", parameters, null, CommandType.StoredProcedure);
            return organizationList;
        }


        #endregion
        #region get organization list
        public async Task<dynamic> GetOrganizationList_WinService()
        {
           
            dynamic organizationList = await GetAsync<dynamic>("usp_GetOrganizationList_WinService", null, null, CommandType.StoredProcedure);
            return organizationList;
        }
        #endregion
    }
}
