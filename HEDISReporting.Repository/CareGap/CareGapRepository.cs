﻿using HEDISReporting.DataContract.Dashboard;
using HEDISReporting.RepositoryContract.CareGap;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace HEDISReporting.Repository.CareGap
{
   public class CareGapRepository : BaseRepository, ICareGapRepository
    {
        private readonly IConfiguration _configuration;
        public CareGapRepository(IConfiguration configuration) : base(configuration)
        {
           // this._configuration = configuration;
        }
        #region get all measure
        public async Task<List<dynamic>> GetABACareGap2018(DashBoardFilters data)
        {
            object parameters = new
            {
                @PatientId = data.patientId,
               // @OrganizationId = data.OrganizationId,            
                @UserId = data.UserId,
        
            };
            dynamic careGaoPatientList = await GetAsync<dynamic>("Usp_careGap", parameters, null,CommandType.StoredProcedure);
            return careGaoPatientList;
        }
        #endregion

        #region get all CareGap Patient List
        public async Task<List<dynamic>> GetAllCareGapPatientList(DashBoardFilters data)
        {
            object parameters = new
            {
                @OrganizationId = data.OrganizationId,
                @UserId = data.UserId,
                @Search=data.Search,
                @sort=data.sort,
                @order=data.order,
                @limit =data.limit,
                @offset=data.offset
            };
            dynamic careGaoPatientList = await GetAsync<dynamic>("Usp_calculateGapeManagemtPatientList", parameters, 30000, CommandType.StoredProcedure);
            return careGaoPatientList;
        }
        #endregion

        #region get particular Immunization Patient List
        public async Task<List<dynamic>> GetImunizationPatientList(DashBoardFilters data)
        {
            object parameters = new
            {
                @PatientId = data.patientId,
                @UserId = data.UserId
            };
            dynamic immunizationPatientList = await GetAsync<dynamic>("usp_GetImmunizationPatientList", parameters, null, CommandType.StoredProcedure);
            return immunizationPatientList;
        }
        #endregion
    }
}
