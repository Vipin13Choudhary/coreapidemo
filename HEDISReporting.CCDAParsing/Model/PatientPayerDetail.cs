﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.CCDAParsing.Model
{
    public class PatientPayerDetail
    {
        public string ServiceType { get; set; }
        public string ServiceName { get; set; }
        public string Status { get; set; }
        public string PayerName { get; set; }
        public string Telecom { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PosttalCode { get; set; }
    }
}
