﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.CCDAParsing.Model
{
    public class NameModel
    {
        public string Createengiven { get; set; }
        public string Createenfamily { get; set; }
        public string CreateenSuffix { get; set; }
    }
}
