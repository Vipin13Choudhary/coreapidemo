﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.CCDAParsing.Model
{
    public class PhNoModel
    {
        public string telcomUse { get; set; }
        public string telcomValue { get; set; }
        public string nullFlavor { get; set; }= "UNK";
    }
}
