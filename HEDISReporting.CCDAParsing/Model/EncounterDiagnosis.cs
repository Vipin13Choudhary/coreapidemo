﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.CCDAParsing.Model
{
    public class EncounterDiagnosis
    {
        public Guid EncounterDiagnosisMap { get; set; }
        public int EncId { get; set; }
        //      EncId
        public string IcdCode { get; set; }
        //IcdCode 

        public string  IcdCodeDesc { get; set; }
                                           //IcdCodeDesc 
    }
}
