﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HEDISReporting.CCDAParsing.Model
{
    public class PrimaryCareProvider
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
