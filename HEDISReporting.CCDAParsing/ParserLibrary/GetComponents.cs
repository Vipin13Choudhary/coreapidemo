﻿using HEDISReporting.CCDAParsing.Model;
using HL7SDK.Cda;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;
//using HEDISReporting.DataContract.CodeSystem;

namespace HEDISReporting.CCDAParsing.ParserLibrary
{
    public class GetComponents
    {

        IIVL_TS datetime;
        IPIVL_TS timeduration;
        ICD meterialCode;
        IPQ valueCode;
        ST valueCode2;
        //CodeSystems codeSystems = new CodeSystems();

        /// <summary>
        /// Get Patient Alleries
        /// </summary>
        /// <param name="dataArr"></param>
        /// <returns></returns>
        public List<PatientAllergies> GetAllergies(Dictionary<string, ArrayList> dataArr)
        {

            List<PatientAllergies> alleryies = new List<PatientAllergies>();
            if (dataArr.Count > 0)
            {
                for (int i = 0; i < dataArr.Count; i++)
                {
                    PatientAllergies ptall = new PatientAllergies();
                    ArrayList itemalleryies = dataArr[i.ToString()];
                    ptall.substance = itemalleryies[1].ToString();
                    ptall.reaction = itemalleryies[5].ToString();
                    ptall.status = itemalleryies[3].ToString();
                    ptall.allergyDate = itemalleryies[0].ToString();

                    alleryies.Add(ptall);
                }
            }

            return alleryies;
        }
        public List<PatientAllergies> FillAllergies(IEntryCollection entryCollection)
        {
            List<PatientAllergies> alleryies = new List<PatientAllergies>();
            foreach (IEntry entryitem in entryCollection)
            {
                IAct entryact = entryitem.AsAct;
                IEntryRelationship entryRelationship = entryact.EntryRelationship[0];
                IObservation entryobservation = entryRelationship.AsObservation;
                IIVL_TS effectivetime = entryact.EffectiveTime;
                IParticipant2 allergyParticipant = entryobservation.Participant[0];
                IParticipantRole participantRole = allergyParticipant.ParticipantRole;
                IPlayingEntity playingEntity = participantRole.AsPlayingEntity;
                
                ICE code = playingEntity.Code; 
                 IPNCollection name = playingEntity.Name;
                //*Added By Rakesh* Name is a collection if this not present then it will be null or there is entry but not code detail then it will be of count 0 collection
                
                string substance = (name != null ? (name.Count > 0 ? name[0].Text : null) : code.DisplayName);
                PatientAllergies ptallergies = new PatientAllergies();
                ptallergies.substance = substance;
                IEntryRelationship entryRelationshipMFST = entryobservation.EntryRelationship.Where(r => r.TypeCode.ToString() == "MFST").FirstOrDefault();
                if (entryRelationshipMFST != null && entryRelationshipMFST.AsObservation.Value != null)
                {
                    IANY Reactionvaluecollection = entryRelationshipMFST.AsObservation.Value.FirstOrDefault();
                    CD valuesReaction = (CD)Reactionvaluecollection;
                    ptallergies.reaction = valuesReaction.DisplayName;
                }
                ptallergies.rxNorm = code.Code;
                IEntryRelationship entryRelationshipSUBJ = entryobservation.EntryRelationship.Where(r => r.TypeCode.ToString() == "SUBJ").FirstOrDefault();
                if (entryRelationshipSUBJ != null && entryRelationshipSUBJ.AsObservation.Value != null)
                {
                    IANY Statusvaluecollection = entryRelationshipSUBJ.AsObservation.Value.FirstOrDefault();
                    ICE values = (ICE)Statusvaluecollection;
                    ptallergies.status = values.DisplayName;
                }
                if (effectivetime != null && effectivetime.Value != null)
                {
                    ptallergies.allergyDate = effectivetime.AsDateTime.ToString();
                }
                alleryies.Add(ptallergies);

            }

            return alleryies;
        }
        /// <summary>
        /// Get Patient Problems
        /// </summary>
        /// <param name="dataArr"></param>
        /// <returns></returns>
        public List<PatientProblemes> GetProblems(Dictionary<string, ArrayList> dataArr)
        {
            List<PatientProblemes> Problemes = new List<PatientProblemes>();
            if (dataArr.Count > 0)
            {
                for (int i = 0; i < dataArr.Count; i++)
                {
                    PatientProblemes ptproblem = new PatientProblemes();
                    ArrayList itemproblem = dataArr[i.ToString()];
                    ptproblem.ProblemCode = itemproblem[1].ToString();
                    ptproblem.Status = itemproblem[4].ToString();
                    ptproblem.DateDiagnosed = itemproblem[0].ToString();
                    ptproblem.Description = itemproblem[2].ToString();
                    Problemes.Add(ptproblem);
                }
            }
            return Problemes;
        }

        public List<PatientProblemes> FillProblems(IEntryCollection entryCollection)
        {
            List<PatientProblemes> Problemes = new List<PatientProblemes>();
            foreach (IEntry singleentry in entryCollection)
            {
                IObservation probObservation = singleentry.AsAct.EntryRelationship.FirstOrDefault().AsObservation;
                IIVL_TS effectivetime = probObservation.EffectiveTime;
                IANY probValue = probObservation.Value[0];
                ICD itemVlues = (ICD)probValue;
                PatientProblemes ptproblem = new PatientProblemes();
                ptproblem.ProblemCode = itemVlues.Code != null ? itemVlues.Code : null;
                IEntryRelationship ObserentryRelation = probObservation.EntryRelationship.Where(e => e.AsObservation.Code.Code.ToString() == "33999-4").FirstOrDefault();
                if (ObserentryRelation != null)
                {
                    IANY probStatusVal = ObserentryRelation.AsObservation.Value.FirstOrDefault();
                    ICD StatusVlues = (ICD)probStatusVal;
                    ptproblem.Status = StatusVlues.DisplayName;
                }
                else
                {
                    ptproblem.Status = null;
                }
                ptproblem.DateDiagnosed = effectivetime.Low != null ? effectivetime.Low.Value != null ? effectivetime.Low.AsDateTime.ToString() : null : null;
                ptproblem.Description = itemVlues.DisplayName != null ? itemVlues.DisplayName : null;
                Problemes.Add(ptproblem);
            }
            return Problemes;
        }
        /// <summary>
        /// Patient Social History Information
        /// </summary>
        /// <param name="dataArr"></param>
        /// <returns></returns>
        public SocialHistoryModel GetSocialHistory(Dictionary<string, ArrayList> dataArr)
        {
            SocialHistoryModel ptSocialHistory = new SocialHistoryModel();
            if (dataArr.Count > 0)
            {
                for (int i = 0; i < dataArr.Count; i++)
                {

                    ArrayList itemSocialHistory = dataArr[i.ToString()];
                    ptSocialHistory.Smoker = itemSocialHistory[1].ToString();
                    ptSocialHistory.EntryDate = null;
                }
            }
            return ptSocialHistory;
        }
        public SocialHistoryModel FillSocialHistory(IEntryCollection entryCollection)
        {
            SocialHistoryModel ptSocialHistory = new SocialHistoryModel();
            foreach (IEntry singleentry in entryCollection)
            {
                IObservation socialObservation = singleentry.AsObservation;
                string templateId = socialObservation.TemplateId.Select(s => s.Root.ToString()).FirstOrDefault();
                if (templateId == "2.16.840.1.113883.10.20.22.4.38" || templateId== "2.16.840.1.113883.10.20.22.4.78")
                {
                    ICD socialCode = socialObservation.Code;
                    IIVL_TS effectiveTime = socialObservation.EffectiveTime;
                    if (socialCode.Code == "230056004" || socialCode.Code== "229819007")
                    {
                        
                        ptSocialHistory.Smoker = socialCode.Code != null ? socialCode.Code : null;
                    }
                    if (socialCode.Code == "160573003")
                    {
                        //ptSocialHistory.EntryDate = effectiveTime != null ? effectiveTime.Low != null ? effectiveTime.Low.Value != null ? effectiveTime.Low.Value.ToString() : null : null : null;
                        ptSocialHistory.Alcohol = socialCode.Code != null ? socialCode.Code : null;
                    }
                    if (socialCode.Code == "363908000")
                    {
                        //ptSocialHistory.EntryDate = effectiveTime != null ? effectiveTime.Low != null ? effectiveTime.Low.Value != null ? effectiveTime.Low.Value.ToString() : null : null : null;
                        ptSocialHistory.Drugs = socialCode.Code != null ? socialCode.Code : null;
                    }
                    if (socialCode.Code == "81703003")
                    {
                        //ptSocialHistory.EntryDate = effectiveTime != null ? effectiveTime.Low != null ? effectiveTime.Low.Value != null ? effectiveTime.Low.Value.ToString() : null : null : null;
                        ptSocialHistory.Tobacoo = socialCode.Code != null ? socialCode.Code : null;
                    }
                    if(effectiveTime!=null)
                    {
                        if(effectiveTime.Low!=null)
                        {
                            ptSocialHistory.EntryDate = effectiveTime.Low.Value;
                        }

                    }
                    //ptSocialHistory.EntryDate = effectiveTime != null ? effectiveTime.Low != null ? effectiveTime.Low.Value != null ? effectiveTime.Low.Value.ToString() : null : null : null;

                }


            }
            return ptSocialHistory;
        }
        /// <summary>
        /// Patient ENCOUNTERS Information
        /// </summary>
        /// <param name="dataArr"></param>
        /// <returns></returns>
        public List<VitalSigns> GetVitalSigns(Dictionary<string, ArrayList> dataArr)
        {
            List<VitalSigns> vitalSigns = new List<VitalSigns>();
            if (dataArr.Count > 0)
            {
                for (int i = 0; i < dataArr.Count; i++)
                {
                    VitalSigns ptvitalSigns = new VitalSigns();
                    ArrayList itemvitalSigns = dataArr[i.ToString()];
                    ptvitalSigns.Height = Convert.ToInt16(itemvitalSigns[1].ToString().Split(" ")[0]);
                    ptvitalSigns.WEIGHT = Convert.ToInt16(itemvitalSigns[5].ToString().Split(" ")[0]);
                    ptvitalSigns.BloodPressure = itemvitalSigns[9].ToString();
                    ptvitalSigns.Entrydate = itemvitalSigns[2].ToString() == "null" ? null : new DateTime?(Convert.ToDateTime(itemvitalSigns[2].ToString()));
                    vitalSigns.Add(ptvitalSigns);
                }
            }
            return vitalSigns;
        }
        public List<VitalSigns> FillVitalSigns(IEntryCollection entryCollection)
        {
            List<VitalSigns> vitalSigns = new List<VitalSigns>();
            foreach (IEntry singleentry in entryCollection)
            {
                IOrganizer organizer = singleentry.AsOrganizer;
                IComponent4Collection component = organizer.Component;
                IIVL_TS effectivetime = organizer.EffectiveTime;
                if (effectivetime == null) throw new InvalidOperationException();
                VitalSigns ptvitalSigns = new VitalSigns();
                try
                {
                    ptvitalSigns.VitalDate = effectivetime.AsDateTime;
                }
                catch (Exception)
                {

                    ptvitalSigns.VitalDate = effectivetime != null ? effectivetime.Low != null ? effectivetime.Low.Value != null ? new DateTime?(effectivetime.Low.AsDateTime) : null : null : effectivetime.Value != null ? new DateTime?(effectivetime.AsDateTime) : new DateTime?(effectivetime.AsDateTime);
                }
                
         
                foreach (IComponent4 orgComponent in component)
                {
                    IObservation orgObservation = orgComponent.AsObservation;
                    ICD itemCode = orgObservation.Code;
                    IANY vitalSignsObservationValue = orgObservation.Value[0];
                    IPQ itemVlues = (IPQ)vitalSignsObservationValue;

                    if (itemCode.Code != null)
                    {

                        if (itemCode.Code.ToString() == "8302-2")
                        {
                            ptvitalSigns.Height = itemVlues.Value;
                            ptvitalSigns.HeightUnit = Convert.ToString(itemVlues.Unit);

                        }
                        if (itemCode.Code.ToString() == "3141-9" || itemCode.Code.ToString()=="29463-7")
                        {
                            ptvitalSigns.WEIGHT = itemVlues.Value;
                            ptvitalSigns.WeightUnit = Convert.ToString(itemVlues.Unit);

                        }
                        if (itemCode.Code.ToString() == "8480-6")
                        {
                            ptvitalSigns.BloodPressure = itemVlues.Value.ToString() + " " + itemVlues.Unit.ToString();
                            ptvitalSigns.BloodPressureSystolic = itemVlues.Value.ToString();

                        }

                        if (itemCode.Code.ToString() == "8462-4")
                        {
                            ptvitalSigns.BloodPressureDiastolic = itemVlues.Value.ToString();

                        }
                    }

                }
                vitalSigns.Add(ptvitalSigns);
            }
            return vitalSigns;
        }
        /// <summary>
        /// Patient Medication Information
        /// </summary>
        /// <param name="dataArr"></param>
        /// <returns></returns>
        public List<PatientMedication> GetMedication(Dictionary<string, ArrayList> dataArr)
        {
            List<PatientMedication> Medication = new List<PatientMedication>();
            if (dataArr.Count > 0)
            {
                for (int i = 0; i < dataArr.Count; i++)
                {
                    PatientMedication ptMedication = new PatientMedication();
                    ArrayList itemMedication = dataArr[i.ToString()];
                    ptMedication.Medication = itemMedication[4].ToString();
                    ptMedication.doseUnit = itemMedication[5].ToString();
                    ptMedication.RxNorm = itemMedication[0].ToString();
                    ptMedication.TakingCurrent = itemMedication[1].ToString() == "Active" ? true : false;
                    ptMedication.StartDate = itemMedication[2].ToString() == "null" ? null : new DateTime?(Convert.ToDateTime(itemMedication[2].ToString())); ;
                    ptMedication.EndDate = itemMedication[3].ToString() == "null" ? null : new DateTime?(Convert.ToDateTime(itemMedication[3].ToString()));
                    Medication.Add(ptMedication);
                }
            }
            return Medication;
        }

        public List<PatientMedication> FillMedication(IEntryCollection entryCollection)
        {
            List<PatientMedication> Medication = new List<PatientMedication>();
            foreach (IEntry entryitem in entryCollection)
            {
                ISubstanceAdministration entrySubstanceAdministration = entryitem.AsSubstanceAdministration;
                foreach (var timeitem in entrySubstanceAdministration.EffectiveTime)
                {
                    var obj = timeitem.GetType();
                    string objname = obj.Name;
                    switch (objname)
                    {
                        case "IVL_TS":
                            datetime = (IIVL_TS)timeitem;
                            break;
                        case "PIVL_TS":
                            timeduration = (IPIVL_TS)timeitem;
                            break;
                    }
                }
                IIVL_PQ doseQuantity = entrySubstanceAdministration.DoseQuantity;
                IIVL_PQ rateQuantity = entrySubstanceAdministration.RateQuantity;
                ICE meterialCode = entrySubstanceAdministration.Consumable.ManufacturedProduct.AsMaterial.Code;
                IEntryRelationship entryRelationShip = entrySubstanceAdministration.EntryRelationship.Where(e => e.TypeCode.ToString() == "RSON").FirstOrDefault();
                if (entryRelationShip != null)
                {
                    IANY entryvalue = entryRelationShip.AsObservation.Value.FirstOrDefault();
                    ICE valueCollection = (ICE)entryvalue;
                }
                PatientMedication ptMedication = new PatientMedication();
                ptMedication.Medication = meterialCode.Translation.FirstOrDefault() != null ? meterialCode.Translation.FirstOrDefault().DisplayName : meterialCode.DisplayName != null ? meterialCode.DisplayName : null;
                ptMedication.RxNorm = meterialCode.Code != null ? meterialCode.Code : null;
                ptMedication.Frequency = timeduration != null ? timeduration.Value != null ? timeduration.Value.ToString() : null : null;
                ptMedication.doseUnit = doseQuantity.Value.ToString() + " " + doseQuantity.Unit != null ? doseQuantity.Unit.ToString() : "";
                ptMedication.StartDate = datetime.Low != null ? datetime.Low.Value != null ? new DateTime?(datetime.Low.AsDateTime) : null : null;
                ptMedication.EndDate = datetime.High != null ? datetime.High.Value != null ? new DateTime?(datetime.High.AsDateTime) : null : null;
                Medication.Add(ptMedication);
            }

            return Medication;
        }
        /// <summary>
        /// Patient ENCOUNTERS Information
        /// </summary>
        /// <param name="dataArr"></param>
        /// <returns></returns>
        public List<Encounters> GetEncounters(Dictionary<string, ArrayList> dataArr)
        {
            List<Encounters> encounters = new List<Encounters>();
            if (dataArr.Count > 0)
            {
                for (int i = 0; i < dataArr.Count; i++)
                {
                    Encounters ptEncounters = new Encounters();
                    ArrayList itemencounter = dataArr[i.ToString()];
                    ptEncounters.EncounterDescription = itemencounter[0].ToString();
                    ptEncounters.PerformerName = "";
                    ptEncounters.Location = itemencounter[1].ToString();
                    ptEncounters.EncounterDate = itemencounter[2] == null ? null : new DateTime?(Convert.ToDateTime(itemencounter[2].ToString()));
                    encounters.Add(ptEncounters);
                }
            }
            return encounters;
        }

        public List<Encounters> FillEncounters(IEntryCollection entryCollection, string name)
        {
           
            List<Encounters> Encounters = new List<Encounters>();
            foreach (IEntry entryItem in entryCollection)
            {
                ////--Procedural Encounter
                //IProcedure proceduresEncounter = entryItem.AsProcedure;
                //if(proceduresEncounter!=null)
                //{
                //    IEntryRelationship entryRelationship = proceduresEncounter.EntryRelationship.FirstOrDefault();
                //    Encounters ProceduralEncounters = new Encounters();
                //    if (entryRelationship != null)
                //    {
                //        IEncounter encounter = entryRelationship.AsEncounter;
                //        ProceduralEncounters.Code = encounter.Code.Code;
                //        IIVL_TS effectiveTime = encounter.EffectiveTime;
                //        if (effectiveTime != null)
                //        {
                //            if (effectiveTime.Low.Value != null)
                //            {
                //                ProceduralEncounters.EncounterDate = (Convert.ToDateTime(effectiveTime.Low.AsDateTime));
                //            }
                //        }
                //        ProceduralEncounters.EncounterDescription = encounter.Code.DisplayName;
                //        ProceduralEncounters.PerformerName = "";
                //        ProceduralEncounters.Location = "";

                //    }
                //    if(ProceduralEncounters.Code!=null)
                //        Encounters.Add(ProceduralEncounters);
                //}

                //--Orginal encounter
                IEncounter Encounter = entryItem.AsEncounter;
                if (Encounter!=null)
                {
                    Encounters orginalEncounters = new Encounters();
                    orginalEncounters.EncounterDiagnosisMap = Guid.NewGuid();
                        orginalEncounters.Code = Encounter.Code.Code;
                        IIVL_TS effectiveTime = Encounter.EffectiveTime;
                        if (effectiveTime != null)
                        {
                            if (effectiveTime.Low !=null && effectiveTime.Low.Value != null)
                            {
                                orginalEncounters.EncounterDate = (Convert.ToDateTime(effectiveTime.Low.AsDateTime));
                            }
                        }
                        orginalEncounters.EncounterDescription = Encounter.Code.DisplayName;
                        IPerformer2 performer = Encounter.Performer.FirstOrDefault();
                        if (performer != null)
                        {
                            IPN pNs = performer.AssignedEntity.AssignedPerson.Name.FirstOrDefault();
                            orginalEncounters.PerformerName = pNs != null ? string.Concat(pNs.FindENGiven()," ", pNs.FindENFamily()) : null;


                        }
                    IParticipant2 participant2 = Encounter.Participant.FirstOrDefault();//.ParticipantRole
                      if(participant2!=null)
                    {
                       IPlayingEntity playingEntity =  participant2.ParticipantRole.AsPlayingEntity;
                        if(playingEntity!=null)
                        {
                           IPN pNs= playingEntity.Name.FirstOrDefault();
                            orginalEncounters.Location = pNs?.Text;
                        }
                    }

                    //if (orginalEncounters.Code != null)
                    //    Encounters.Add(orginalEncounters);

                    IEntryRelationshipCollection entryRelationship = Encounter.EntryRelationship;
                    foreach (IEntryRelationship item in entryRelationship)
                    {
                        if(orginalEncounters!=null)
                        {
                            orginalEncounters.encounterDiagnoses = new List<EncounterDiagnosis>();
                            IAct act = item.AsAct;
                            if (act != null)
                            {
                                IEntryRelationship entity = act.EntryRelationship.FirstOrDefault();
                                IObservation Innerobservation = entity.AsObservation;
                                IIVL_TS efftime = Innerobservation.EffectiveTime;

                                //if (efftime != null)
                                //{
                                //    if (efftime.Low.Value != null)
                                //    {
                                //        ObservationEncounters.EncounterDate = (Convert.ToDateTime(efftime.Low.AsDateTime));
                                //    }
                                //}

                                IANY observationvalue = Innerobservation.Value.FirstOrDefault();
                                ICD cD = (ICD)observationvalue;
                                if(!string.IsNullOrEmpty(cD.Code))
                                {
                                    EncounterDiagnosis Observations = new EncounterDiagnosis();
                                    Observations.IcdCode = cD.Code;
                                    Observations.IcdCodeDesc = cD.DisplayName;
                                    Observations.EncounterDiagnosisMap = orginalEncounters.EncounterDiagnosisMap;
                                    orginalEncounters.encounterDiagnoses.Add(Observations);
                                }
                               
                                //ObservationEncounters.PerformerName = orginalEncounters != null ? orginalEncounters.PerformerName : null;
                                //if (ObservationEncounters.Code != null)
                                //    Encounters.Add(ObservationEncounters);
                            }


                        }

                    }
                    if (orginalEncounters.Code != null)
                        Encounters.Add(orginalEncounters);

                }
                
                

                //AsObservation Encounters 
               

              


            }

            return Encounters;
        }
        /// <summary>
        /// Patient Lab Results Information
        /// </summary>
        /// <param name="dataArr"></param>
        /// <returns></returns>
        public List<LabResult> GetLabResults(Dictionary<string, ArrayList> dataArr)
        {
            List<LabResult> labResult = new List<LabResult>();
            if (dataArr.Count > 0)
            {
                for (int i = 0; i < dataArr.Count; i++)
                {
                    LabResult ptLabResult = new LabResult();
                    ArrayList itemlabResult = dataArr[i.ToString()];
                    ptLabResult.LonicCode = itemlabResult[4] == null ? "" : itemlabResult[4].ToString();
                    ptLabResult.TestPerformed = itemlabResult[0].ToString();
                    ptLabResult.ReportDate = itemlabResult[2].ToString() == "null" ? null : new DateTime?(Convert.ToDateTime(itemlabResult[2].ToString()));
                    ptLabResult.TestResultn = itemlabResult[1].ToString().Split(" ")[0].ToString();
                    ptLabResult.Units = itemlabResult[1].ToString().Split(" ")[1].ToString();
                    ptLabResult.NormalFindings = itemlabResult[3].ToString();
                    labResult.Add(ptLabResult);
                }
            }
            return labResult;
        }
        public List<LabResult> FillLabResults(IEntryCollection entryCollection)
        {
            List<LabResult> labResult = new List<LabResult>();
            foreach (IEntry entryitem in entryCollection)
            {
                IOrganizer entryOrganizer = entryitem.AsOrganizer;
                IComponent4Collection entryComponent = entryOrganizer.Component;
                List<LabResult> ptLabResult = new List<LabResult>();
                foreach (IComponent4 obserComponent in entryComponent)
                {
                    LabResult local = new LabResult();
                    IObservation entryObservation = obserComponent.AsObservation;
                    IReferenceRange referenceRange = entryObservation.ReferenceRange.FirstOrDefault();
                    meterialCode = entryObservation.Code;
                    try { valueCode = (IPQ)entryObservation.Value[0]; } catch (Exception) { }
                    local.TestPerformed = meterialCode.DisplayName;
                    if(entryObservation.EffectiveTime!=null && entryObservation.EffectiveTime.Value!=null)
                    local.ReportDate = entryObservation.EffectiveTime == null ? null : new DateTime?(Convert.ToDateTime(entryObservation.EffectiveTime.AsDateTime));
                    local.LonicCode = meterialCode.Code;
                    local.Units = valueCode != null ? valueCode.Unit.ToString() : string.Empty;
                    local.TestResultn = valueCode != null ? valueCode.Value.ToString() : string.Empty;
                    local.NormalFindings = referenceRange != null ? referenceRange.ObservationRange.Text != null ? referenceRange.ObservationRange.Text.Text : null : null;
                    ptLabResult.Add(local);
                }
                labResult.AddRange(ptLabResult);
            }

            return labResult;
        }


        /// <summary>
        /// Patient Reason For Visit Information
        /// </summary>
        /// <param name="dataArr"></param>
        /// <returns></returns>
        public ReasonForVisit GetReason(Dictionary<string, ArrayList> dataArr)
        {
            ReasonForVisit ptReasonForVisit = new ReasonForVisit();
            if (dataArr.Count > 0)
            {
                for (int i = 0; i < dataArr.Count; i++)
                {

                    ArrayList itemReasonForVisit = dataArr[i.ToString()];
                    ptReasonForVisit.Description = itemReasonForVisit[0].ToString();
                    ptReasonForVisit.VisitDate = null;//Convert.ToDateTime(item[2].ToString());
                }
            }
            return ptReasonForVisit;
        }
        public ReasonForVisit FillReason(ISection section)
        {
            ReasonForVisit ptReasonForVisit = new ReasonForVisit();
            IStrucDocText text = section.Text;
            foreach (var item in text.Items)
            {
                var test = item.GetType();
                ptReasonForVisit.Description = item.ToString();
            }

            return ptReasonForVisit;
        }
        /// <summary>
        /// Patient Immunizations Information
        /// </summary>
        /// <param name="dataArr"></param>
        /// <returns></returns>
        public List<Immunization> GetImmunization(Dictionary<string, ArrayList> dataArr)
        {
            List<Immunization> immunization = new List<Immunization>();
            if (dataArr.Count > 0)
            {
                for (int i = 0; i < dataArr.Count; i++)
                {
                    Immunization ptImmunization = new Immunization();
                    ArrayList itemimmunization = dataArr[i.ToString()];
                    ptImmunization.Vaccine = itemimmunization[3].ToString();
                    ptImmunization.ApproximateDate = itemimmunization[2].ToString() == "null" ? null : new DateTime?(Convert.ToDateTime(itemimmunization[2].ToString()));
                    ptImmunization.CVX = Convert.ToInt32(itemimmunization[0].ToString());//Convert.ToDateTime(item[2].ToString());
                    ptImmunization.Manufacturer = itemimmunization[4].ToString();
                    immunization.Add(ptImmunization);
                }
            }
            return immunization;
        }
        public List<Immunization> FillImmunization(IEntryCollection entryCollection)
        {
            List<Immunization> immunization = new List<Immunization>();
            foreach (IEntry singleRecord in entryCollection)
            {
                ISubstanceAdministration entrySubstanceAdministration = singleRecord.AsSubstanceAdministration;
                ISXCM_TS efftime = entrySubstanceAdministration.EffectiveTime[0];
                IConsumable substanseConsumable = entrySubstanceAdministration.Consumable;
                ICE manufacturedProduct = entrySubstanceAdministration.Consumable.ManufacturedProduct.AsMaterial.Code;
                if (manufacturedProduct != null)
                {
                    Immunization ptImmunization = new Immunization();
                    ptImmunization.ApproximateDate = efftime != null ? efftime.Value != null ? new DateTime?(efftime.AsDateTime) : null : null;
                    ptImmunization.CVX = Convert.ToInt32(manufacturedProduct.Code);
                    ptImmunization.Vaccine = manufacturedProduct.OriginalText != null ? manufacturedProduct.OriginalText.Text : null;
                    //if original test is not provided then we can use display name as that is also accine name.
                    if(string.IsNullOrEmpty(ptImmunization.Vaccine))
                    {
                        ptImmunization.Vaccine = manufacturedProduct.DisplayName!=null? manufacturedProduct.DisplayName:null;
                    }
                    if (entrySubstanceAdministration.Consumable.ManufacturedProduct.ManufacturerOrganization != null)
                    {
                       ION oN= entrySubstanceAdministration.Consumable.ManufacturedProduct.ManufacturerOrganization.Name.FirstOrDefault();
                       
                        ptImmunization.Manufacturer = oN?.Text.ToString();
                    }
                    else
                    {
                        ptImmunization.Manufacturer = null;
                    }
                    immunization.Add(ptImmunization);
                }
            }

            return immunization;
        }
        /// <summary>
        /// Patient Plan Of Care Information
        /// </summary>
        /// <param name="dataArr"></param>
        /// <returns></returns>
        public List<PlanOfCare> GetPlanOfCare(Dictionary<string, ArrayList> dataArr)
        {
            List<PlanOfCare> planOfCare = new List<PlanOfCare>();
            if (dataArr.Count > 0)
            {
                for (int i = 0; i < dataArr.Count; i++)
                {
                    PlanOfCare ptPlanOfCare = new PlanOfCare();
                    ArrayList itemplanOfCare = dataArr[i.ToString()];
                    string goal = string.Empty;
                    string instruction = string.Empty;
                    if (itemplanOfCare[1] != null)
                    {
                        var goalInsturction = itemplanOfCare[1].ToString().Split(",");

                        if (goalInsturction.Length >= 2)
                        {
                            var goalval = goalInsturction[0].Split(":");
                            if (goalval.Length >= 2)
                            {
                                goal = goalval[1].ToString();
                            }
                            var instructionval = goalInsturction[1].Split(":");
                            if (instructionval.Length >= 2)
                            {
                                instruction = instructionval[1].ToString();
                            }
                        }
                        ptPlanOfCare.Goal = goal;
                        ptPlanOfCare.PlannedDate = itemplanOfCare[0].ToString() == "null" ? null : new DateTime?(Convert.ToDateTime(itemplanOfCare[0].ToString()));
                        ptPlanOfCare.Instructions = instruction;//Convert.ToDateTime(item[2].ToString());
                        planOfCare.Add(ptPlanOfCare);
                    }
                }

            }
            return planOfCare;
        }
        public List<PlanOfCare> FillPlanOfCare(IEntryCollection entryCollection)
        {
            List<PlanOfCare> planOfCare = new List<PlanOfCare>();
            foreach (IEntry singleRecord in entryCollection)
            {
                IObservation observation = singleRecord.AsObservation;
                IAct entryAct = singleRecord.AsAct;
                string goal = null;
                string instructions = null;
                if (observation!=null)
                {
                    meterialCode = observation.Code;
                    goal = meterialCode.DisplayName;
                    datetime = observation.EffectiveTime;
                }
                if(entryAct!=null)
                {
                    instructions = entryAct.Text.Text;
                }
                PlanOfCare ptPlanOfCare = new PlanOfCare();
                ptPlanOfCare.Goal = goal;
                ptPlanOfCare.PlannedDate = datetime != null ? datetime.Center != null ? new DateTime?(datetime.Center.AsDateTime) : null : null;
                ptPlanOfCare.Instructions = instructions;
                planOfCare.Add(ptPlanOfCare);
            }


            return planOfCare;
        }
        /// <summary>
        /// Patient Reason For Transfer Information
        /// </summary>
        /// <param name="dataArr"></param>
        /// <returns></returns>
        public string GetReasonForTransfer(Dictionary<string, ArrayList> dataArr)
        {
            string reasonforTransfer = string.Empty;
            for (int i = 0; i < dataArr.Count; i++)
            {
                ArrayList itemreason = dataArr[i.ToString()];
                reasonforTransfer = itemreason[0].ToString();
            }
            return reasonforTransfer;
        }
        public string FillReasonForTransfer(ISection section)
        {
            string reasonforTransfer = string.Empty;
            IStrucDocText text = section.Text;
            foreach (var item in text.Items)
            {
                var test = item.GetType();
                reasonforTransfer = item.ToString();
            }

            return reasonforTransfer;
        }
        /// <summary>
        /// Patient Procedure Information
        /// </summary>
        /// <param name="dataArr"></param>
        /// <returns></returns>
        public List<ProcedureList> GetProcedure(Dictionary<string, ArrayList> dataArr)
        {
            List<ProcedureList> procedureList = new List<ProcedureList>();
            if (dataArr.Count > 0)
            {
                for (int i = 0; i < dataArr.Count; i++)
                {
                    ProcedureList ptprocedureList = new ProcedureList();
                    ArrayList itemproc = dataArr[i.ToString()];
                    if (itemproc[0] != null && itemproc[0].ToString() != "null" && itemproc[0].ToString() != string.Empty)
                    {
                        ptprocedureList.CPTCodes = itemproc[0] == null ? "" : itemproc[0].ToString();
                        ptprocedureList.Description = itemproc[1].ToString() == null ? null : itemproc[1].ToString();
                        procedureList.Add(ptprocedureList);
                    }
                }
            }
            return procedureList;
        }
        public List<ProcedureList> FillProcedure(IEntryCollection entryCollection)
        {
            List<ProcedureList> procedureList = new List<ProcedureList>();
            foreach (IEntry entryitem in entryCollection)
            {
                IProcedure entryProcedure = entryitem.AsProcedure;
                if (entryProcedure != null)
                {
                    ICD meterialCode;
                    meterialCode = entryProcedure.Code;
                    ProcedureList ptprocedureList = new ProcedureList();
                    IIVL_TS effectiveDate = entryProcedure.EffectiveTime;
                    if (entryProcedure.Code.Code != null)
                    {
                      
                        //ptprocedureList.SnomedCode = FilterCodes(meterialCode, codeSystems.SNOMEDCTCodesystemOID);
                        //ptprocedureList.CPTCodes = FilterCodes(meterialCode, codeSystems.CPTCodesystemOID);
                        //ptprocedureList.ICD9PCS = FilterCodes(meterialCode, codeSystems.ICD9PCSCodesystemOID);
                        //ptprocedureList.ICD9CM= FilterCodes(meterialCode, codeSystems.ICD9CMCodesystemOID);
                        //ptprocedureList.ICD10CM = FilterCodes(meterialCode, codeSystems.ICD10CMCodesystemOID);
                        //ptprocedureList.ICD10PCS = FilterCodes(meterialCode, codeSystems.ICD10PCSCodesystemOID);
                        //ptprocedureList.HCPCS = FilterCodes(meterialCode, codeSystems.HCPCSCodesystemOID);
                        //ptprocedureList.LOINC = FilterCodes(meterialCode, codeSystems.LOINCCodesystemOID);
                        ptprocedureList.Description = meterialCode.DisplayName;
                       
                        if (effectiveDate!=null && effectiveDate.Low!=null && effectiveDate.Low.Value!=null)
                        {
                            ptprocedureList.StartDate = effectiveDate.Low.AsDateTime;
                        }
                        if (effectiveDate != null && effectiveDate.Low!=null && effectiveDate.High.Value != null)
                        {
                            ptprocedureList.EndDate = effectiveDate.High.AsDateTime;
                        }

                        procedureList.Add(ptprocedureList);
                    }
                    else
                    {
                        //to cover if local coding or translation section of CDA is used to represent code system.
                        ICD cD=  entryProcedure.Code.Translation.FirstOrDefault();
                        if(cD!=null)
                        {
                            //ptprocedureList.SnomedCode = FilterCodes(cD, codeSystems.SNOMEDCTCodesystemOID);
                            //ptprocedureList.CPTCodes = FilterCodes(cD, codeSystems.CPTCodesystemOID);
                            //ptprocedureList.ICD9PCS = FilterCodes(cD, codeSystems.ICD9PCSCodesystemOID);
                            //ptprocedureList.ICD9CM = FilterCodes(meterialCode, codeSystems.ICD9CMCodesystemOID);
                            //ptprocedureList.ICD10CM = FilterCodes(cD, codeSystems.ICD10CMCodesystemOID);
                            //ptprocedureList.ICD10PCS = FilterCodes(cD, codeSystems.ICD10PCSCodesystemOID);
                            //ptprocedureList.HCPCS = FilterCodes(cD, codeSystems.HCPCSCodesystemOID);
                            //ptprocedureList.LOINC = FilterCodes(cD, codeSystems.LOINCCodesystemOID);

                            ptprocedureList.Description = cD.DisplayName;
                          
                            if (effectiveDate != null && effectiveDate.Low.Value != null)
                            {
                                ptprocedureList.StartDate = effectiveDate.Low.AsDateTime;
                            }
                            if (effectiveDate != null && effectiveDate.High.Value != null)
                            {
                                ptprocedureList.EndDate = effectiveDate.High.AsDateTime;
                            }
                            procedureList.Add(ptprocedureList);
                        }
                    }
                  
                }
            }

            return procedureList;
        }
        /// <summary>
        /// Patient Functional Status Information
        /// </summary>
        /// <param name="dataArr"></param>
        /// <returns></returns>
        public List<FunctionalStatus> GetFunctionalStatus(Dictionary<string, ArrayList> dataArr)
        {
            List<FunctionalStatus> functionalStatus = new List<FunctionalStatus>();
            if (dataArr.Count > 0)
            {
                for (int i = 0; i < dataArr.Count; i++)
                {
                    FunctionalStatus ptfunctionalStatus = new FunctionalStatus();
                    ArrayList itemfunctional = dataArr[i.ToString()];
                    ptfunctionalStatus.StatusDate = itemfunctional[2].ToString() == "null" ? null : new DateTime?(Convert.ToDateTime(itemfunctional[2].ToString())); ;
                    ptfunctionalStatus.Description = itemfunctional[1].ToString();
                    functionalStatus.Add(ptfunctionalStatus);
                }
            }
            return functionalStatus;
        }

        public List<FunctionalStatus> FillFunctionalStatus(IEntryCollection entryCollection)
        {
            List<FunctionalStatus> functionalStatus = new List<FunctionalStatus>();
            foreach (IEntry entryitem in entryCollection)
            {
                IObservation entryObservation = entryitem.AsObservation;
                IIVL_TS effectiveDate = entryObservation.EffectiveTime;
                IANY functionalValue = entryObservation.Value[0];
                ICD cD = (ICD)functionalValue;
                FunctionalStatus ptfunctionalStatus = new FunctionalStatus();
                ptfunctionalStatus.StatusDate = effectiveDate != null ? effectiveDate.Low != null ? effectiveDate.Low.Value != null ? new DateTime?(effectiveDate.Low.AsDateTime) : null : null : null;
                ptfunctionalStatus.Description = cD.DisplayName;
                ptfunctionalStatus.Code = cD.Code;
                functionalStatus.Add(ptfunctionalStatus);
            }

            return functionalStatus;
        }
        public string FilterCodes(ICD code, string codeSystemOID)
        {
           
            if (codeSystemOID == code.CodeSystem)
                return code.Code;
            else
                return string.Empty;

        }
        public List<PatientPayerDetail> FillPatientPayer(IEntryCollection entryCollection)
        {
            List<PatientPayerDetail> patientPayers = new List<PatientPayerDetail>();
            IEntry entry = entryCollection.FirstOrDefault();
            if (entry != null)
            {
                IAct act = entry.AsAct;
                foreach (IEntryRelationship payerCollection in act.EntryRelationship)
                {
                    PatientPayerDetail patientPayerDetail = new PatientPayerDetail();
                    IAct innerAct = payerCollection.AsAct;
                    if (innerAct != null)
                    {
                        patientPayerDetail.Status = act.StatusCode.Code;
                        ICD cD = innerAct.Code;
                        patientPayerDetail.ServiceType = cD.Code;
                        patientPayerDetail.ServiceName = cD.DisplayName;
                        IPerformer2 performer2 = innerAct.Performer.FirstOrDefault();
                        IAssignedEntity assignedEntity = performer2.AssignedEntity;
                        IOrganization organization = assignedEntity.RepresentedOrganization;
                        ION oN = organization.Name.FirstOrDefault();
                        patientPayerDetail.PayerName = oN != null ? organization.Name.FirstOrDefault().Text : null;
                        ParseAddress parseAddress = new ParseAddress();
                        AddressModel aD = parseAddress.FillAddress(organization.Addr);
                        patientPayerDetail.Street = aD.street;
                        patientPayerDetail.City = aD.city;
                        patientPayerDetail.State = aD.state;
                        patientPayerDetail.PosttalCode = aD.pinCode;
                        patientPayers.Add(patientPayerDetail);
                    }
                }

            }
            return patientPayers;
        }

    }
}
