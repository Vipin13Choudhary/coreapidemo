CREATE TABLE MasterMeasures(
MasterMeasureId int primary key identity(1,1) ,
Name varchar(250) not null,
IsDelete bit default 0,
DeletedBy int,
IsActive bit default 1,
Createdby int not null,
Createddate datetime not null,
UpdatetedBy int,
UpdatedDate datetime
)

ALTER Table MasterMeasures Add Abbreviation varchar(20)


Insert into MasterMeasures (Name,CREATEDBY,CreatedDate,Abbreviation) Values('AdultBMIAssesment',-1,getutcDate(),'ABA')
Insert into MasterMeasures (Name,CREATEDBY,CreatedDate,Abbreviation) Values('Colorectal Cancer',-1,getutcDate(),'CC')
Insert into MasterMeasures (Name,CREATEDBY,CreatedDate,Abbreviation) Values('Breast Cancer',-1,getutcDate(),'BC')
Insert into MasterMeasures (Name,CREATEDBY,CreatedDate,Abbreviation) Values('Cervical Cancer',-1,getutcDate(),'CC')
Insert into MasterMeasures (Name,CREATEDBY,CreatedDate,Abbreviation) Values('Controlling High Blood Pressure',-1,getutcDate(),'CHBP')