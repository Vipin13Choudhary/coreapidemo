USE [HC_Patient_Merging]
GO
/****** Object:  Table [dbo].[MasterAdministrationSite]    Script Date: 12/13/2018 5:53:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MasterAdministrationSite](
	[AdministrationSiteID] [int] IDENTITY(1,1) NOT NULL,
	[DeletedBy] [int] NULL,
	[Description] [nvarchar](max) NULL,
	[HL7] [nvarchar](max) NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[SNOMED] [nvarchar](50) NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime2](7) NULL,
	[DeletedDate] [datetime2](7) NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[OrganizationID] [int] NOT NULL,
 CONSTRAINT [PK_MasterAdministrationSite] PRIMARY KEY CLUSTERED 
(
	[AdministrationSiteID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MasterAllergies]    Script Date: 12/13/2018 5:53:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MasterAllergies](
	[AllergyID] [int] IDENTITY(1,1) NOT NULL,
	[AllergyType] [nvarchar](max) NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime2](7) NULL,
	[DeletedBy] [int] NULL,
	[DeletedDate] [datetime2](7) NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[OrganizationID] [int] NOT NULL,
 CONSTRAINT [PK_MasterAllergies] PRIMARY KEY CLUSTERED 
(
	[AllergyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MasterEthnicity]    Script Date: 12/13/2018 5:53:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MasterEthnicity](
	[EthnicityID] [int] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime2](7) NULL,
	[DeletedBy] [int] NULL,
	[DeletedDate] [datetime2](7) NULL,
	[EthnicityName] [nvarchar](max) NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[OrganizationID] [int] NOT NULL,
 CONSTRAINT [PK_MasterEthnicity] PRIMARY KEY CLUSTERED 
(
	[EthnicityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MasterImmunityStatus]    Script Date: 12/13/2018 5:53:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MasterImmunityStatus](
	[ImmunityStatusID] [int] IDENTITY(1,1) NOT NULL,
	[ConceptCode] [nvarchar](10) NULL,
	[ConceptName] [nvarchar](max) NULL,
	[Defination] [nvarchar](300) NULL,
	[DeletedBy] [int] NULL,
	[HL7Code] [nvarchar](50) NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[NIP004] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime2](7) NULL,
	[DeletedDate] [datetime2](7) NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[OrganizationID] [int] NOT NULL,
 CONSTRAINT [PK_MasterImmunityStatus] PRIMARY KEY CLUSTERED 
(
	[ImmunityStatusID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MasterImmunization]    Script Date: 12/13/2018 5:53:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MasterImmunization](
	[ImmunizationID] [int] IDENTITY(1,1) NOT NULL,
	[CvxCode] [nvarchar](50) NULL,
	[DeletedBy] [int] NULL,
	[InternalID] [int] NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[NonVaccine] [bit] NULL,
	[Note] [nvarchar](max) NULL,
	[ShortDesc] [nvarchar](50) NULL,
	[VaccineName] [nvarchar](max) NULL,
	[VaccineStatus] [nvarchar](50) NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime2](7) NULL,
	[DeletedDate] [datetime2](7) NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[OrganizationID] [int] NOT NULL,
 CONSTRAINT [PK_MasterImmunization] PRIMARY KEY CLUSTERED 
(
	[ImmunizationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MasterLonic]    Script Date: 12/13/2018 5:53:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MasterLonic](
	[LonicID] [int] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime2](7) NULL,
	[DeletedBy] [int] NULL,
	[DeletedDate] [datetime2](7) NULL,
	[Description] [nvarchar](max) NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[LonicCode] [nvarchar](max) NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[OrganizationID] [int] NOT NULL,
 CONSTRAINT [PK_MasterLonic] PRIMARY KEY CLUSTERED 
(
	[LonicID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MasterManufacture]    Script Date: 12/13/2018 5:53:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MasterManufacture](
	[ManufactureID] [int] IDENTITY(1,1) NOT NULL,
	[DeletedBy] [int] NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[MVXCode] [nvarchar](50) NULL,
	[ManufacturerName] [nvarchar](500) NULL,
	[Notes] [int] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime2](7) NULL,
	[DeletedDate] [datetime2](7) NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[OrganizationID] [int] NOT NULL,
 CONSTRAINT [PK_MasterManufacture] PRIMARY KEY CLUSTERED 
(
	[ManufactureID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MasterNoteType]    Script Date: 12/13/2018 5:53:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MasterNoteType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime2](7) NULL,
	[DeletedBy] [int] NULL,
	[DeletedDate] [datetime2](7) NULL,
	[Description] [nvarchar](1000) NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[OrganizationID] [int] NOT NULL,
	[Type] [nvarchar](max) NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[IsDirectService] [bit] NOT NULL,
	[NavigationLink] [nvarchar](100) NULL,
 CONSTRAINT [PK_MasterNoteType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MasterPatientLocation]    Script Date: 12/13/2018 5:53:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MasterPatientLocation](
	[PatientLocationID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [int] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime2](7) NULL,
	[DeletedBy] [int] NULL,
	[DeletedDate] [datetime2](7) NULL,
	[Description] [nvarchar](max) NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[Location] [nvarchar](100) NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[OrganizationID] [int] NOT NULL,
 CONSTRAINT [PK_MasterPatientLocation] PRIMARY KEY CLUSTERED 
(
	[PatientLocationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MasterRace]    Script Date: 12/13/2018 5:53:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MasterRace](
	[RaceID] [int] IDENTITY(1,1) NOT NULL,
	[DeletedBy] [int] NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[RaceName] [nvarchar](max) NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime2](7) NULL,
	[DeletedDate] [datetime2](7) NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[OrganizationID] [int] NOT NULL,
 CONSTRAINT [PK_MasterRace] PRIMARY KEY CLUSTERED 
(
	[RaceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MasterRejectionReason]    Script Date: 12/13/2018 5:53:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MasterRejectionReason](
	[RejectionReasonID] [int] IDENTITY(1,1) NOT NULL,
	[DeletedBy] [int] NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NULL,
	[ReasonCode] [nvarchar](5) NULL,
	[ReasonDesc] [nvarchar](100) NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime2](7) NULL,
	[DeletedDate] [datetime2](7) NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[OrganizationID] [int] NOT NULL,
 CONSTRAINT [PK_MasterRejectionReason] PRIMARY KEY CLUSTERED 
(
	[RejectionReasonID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MasterRelationship]    Script Date: 12/13/2018 5:53:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MasterRelationship](
	[RelationshipID] [int] IDENTITY(1,1) NOT NULL,
	[DeletedBy] [int] NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NULL,
	[RelationshipName] [nvarchar](max) NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime2](7) NULL,
	[DeletedDate] [datetime2](7) NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[OrganizationID] [int] NOT NULL,
	[RelationshipCode] [varchar](5) NULL,
	[DisplayOrder] [int] NULL,
 CONSTRAINT [PK_MasterRelationship] PRIMARY KEY CLUSTERED 
(
	[RelationshipID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MasterRouteOfAdministration]    Script Date: 12/13/2018 5:53:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MasterRouteOfAdministration](
	[RouteOfAdministrationID] [int] IDENTITY(1,1) NOT NULL,
	[Definition] [nvarchar](500) NULL,
	[DeletedBy] [int] NULL,
	[Description] [nvarchar](100) NULL,
	[FDANCI] [nvarchar](10) NULL,
	[HL7] [nvarchar](10) NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime2](7) NULL,
	[DeletedDate] [datetime2](7) NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[OrganizationID] [int] NOT NULL,
 CONSTRAINT [PK_MasterRouteOfAdministration] PRIMARY KEY CLUSTERED 
(
	[RouteOfAdministrationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MasterVFCEligibility]    Script Date: 12/13/2018 5:53:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MasterVFCEligibility](
	[VFCID] [int] IDENTITY(1,1) NOT NULL,
	[ConceptCode] [nvarchar](10) NULL,
	[ConceptName] [nvarchar](max) NULL,
	[DeletedBy] [int] NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime2](7) NULL,
	[DeletedDate] [datetime2](7) NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[OrganizationID] [int] NOT NULL,
 CONSTRAINT [PK_MasterVFCEligibility] PRIMARY KEY CLUSTERED 
(
	[VFCID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[MasterAdministrationSite] ON 

INSERT [dbo].[MasterAdministrationSite] ([AdministrationSiteID], [DeletedBy], [Description], [HL7], [IsActive], [IsDeleted], [SNOMED], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (1, NULL, N'Test', N'456', 1, 0, N'123', 1, CAST(0x0770F90B3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterAdministrationSite] ([AdministrationSiteID], [DeletedBy], [Description], [HL7], [IsActive], [IsDeleted], [SNOMED], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (2, 1, N'Left Thigh', N'LT', 1, 0, NULL, 1, CAST(0x0770F90B3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterAdministrationSite] ([AdministrationSiteID], [DeletedBy], [Description], [HL7], [IsActive], [IsDeleted], [SNOMED], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (3, 1, N'Left Arm', N'LA', 1, 0, NULL, 1, CAST(0x0770F90B3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterAdministrationSite] ([AdministrationSiteID], [DeletedBy], [Description], [HL7], [IsActive], [IsDeleted], [SNOMED], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (4, 1, N'Left Deltoid', N'LD', 1, 0, NULL, 1, CAST(0x0770F90B3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterAdministrationSite] ([AdministrationSiteID], [DeletedBy], [Description], [HL7], [IsActive], [IsDeleted], [SNOMED], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (5, 1, N'Left Gluteous Medius', N'LG', 1, 0, NULL, 1, CAST(0x0770F90B3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterAdministrationSite] ([AdministrationSiteID], [DeletedBy], [Description], [HL7], [IsActive], [IsDeleted], [SNOMED], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (6, 1, N'Left Vastus Lateralis', N'LVL', 1, 0, NULL, 1, CAST(0x0770F90B3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterAdministrationSite] ([AdministrationSiteID], [DeletedBy], [Description], [HL7], [IsActive], [IsDeleted], [SNOMED], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (7, 1, N'Left Lower Forearm', N'LLFA', 1, 0, NULL, 1, CAST(0x0770F90B3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterAdministrationSite] ([AdministrationSiteID], [DeletedBy], [Description], [HL7], [IsActive], [IsDeleted], [SNOMED], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (8, 1, N'Right Arm', N'RA', 1, 0, NULL, 1, CAST(0x0770F90B3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterAdministrationSite] ([AdministrationSiteID], [DeletedBy], [Description], [HL7], [IsActive], [IsDeleted], [SNOMED], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (9, 1, N'Right Thigh', N'RT', 1, 0, NULL, 1, CAST(0x0770F90B3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterAdministrationSite] ([AdministrationSiteID], [DeletedBy], [Description], [HL7], [IsActive], [IsDeleted], [SNOMED], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (10, 1, N'Right Vastus Lateralis', N'RVL', 1, 0, NULL, 1, CAST(0x0770F90B3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterAdministrationSite] ([AdministrationSiteID], [DeletedBy], [Description], [HL7], [IsActive], [IsDeleted], [SNOMED], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (11, 1, N'Right Gluteous Medius', N'RG', 1, 0, NULL, 1, CAST(0x0770F90B3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterAdministrationSite] ([AdministrationSiteID], [DeletedBy], [Description], [HL7], [IsActive], [IsDeleted], [SNOMED], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (12, 1, N'Right Deltoid', N'RD', 1, 0, NULL, 1, CAST(0x0770F90B3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterAdministrationSite] ([AdministrationSiteID], [DeletedBy], [Description], [HL7], [IsActive], [IsDeleted], [SNOMED], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (13, 1, N'Right Lower Forearm', N'RLFA', 1, 0, NULL, 1, CAST(0x0770F90B3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterAdministrationSite] ([AdministrationSiteID], [DeletedBy], [Description], [HL7], [IsActive], [IsDeleted], [SNOMED], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (14, 1, N'UNKNOWN', NULL, 1, 0, NULL, 1, CAST(0x0770F90B3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterAdministrationSite] ([AdministrationSiteID], [DeletedBy], [Description], [HL7], [IsActive], [IsDeleted], [SNOMED], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (15, NULL, N'Test', N'456', 1, 0, N'123', NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterAdministrationSite] ([AdministrationSiteID], [DeletedBy], [Description], [HL7], [IsActive], [IsDeleted], [SNOMED], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (16, NULL, N'Left Thigh', N'LT', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterAdministrationSite] ([AdministrationSiteID], [DeletedBy], [Description], [HL7], [IsActive], [IsDeleted], [SNOMED], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (17, NULL, N'Left Arm', N'LA', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterAdministrationSite] ([AdministrationSiteID], [DeletedBy], [Description], [HL7], [IsActive], [IsDeleted], [SNOMED], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (18, NULL, N'Left Deltoid', N'LD', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterAdministrationSite] ([AdministrationSiteID], [DeletedBy], [Description], [HL7], [IsActive], [IsDeleted], [SNOMED], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (19, NULL, N'Left Gluteous Medius', N'LG', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterAdministrationSite] ([AdministrationSiteID], [DeletedBy], [Description], [HL7], [IsActive], [IsDeleted], [SNOMED], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (20, NULL, N'Left Vastus Lateralis', N'LVL', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterAdministrationSite] ([AdministrationSiteID], [DeletedBy], [Description], [HL7], [IsActive], [IsDeleted], [SNOMED], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (21, NULL, N'Left Lower Forearm', N'LLFA', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterAdministrationSite] ([AdministrationSiteID], [DeletedBy], [Description], [HL7], [IsActive], [IsDeleted], [SNOMED], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (22, NULL, N'Right Arm', N'RA', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterAdministrationSite] ([AdministrationSiteID], [DeletedBy], [Description], [HL7], [IsActive], [IsDeleted], [SNOMED], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (23, NULL, N'Right Thigh', N'RT', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterAdministrationSite] ([AdministrationSiteID], [DeletedBy], [Description], [HL7], [IsActive], [IsDeleted], [SNOMED], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (24, NULL, N'Right Vastus Lateralis', N'RVL', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterAdministrationSite] ([AdministrationSiteID], [DeletedBy], [Description], [HL7], [IsActive], [IsDeleted], [SNOMED], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (25, NULL, N'Right Gluteous Medius', N'RG', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterAdministrationSite] ([AdministrationSiteID], [DeletedBy], [Description], [HL7], [IsActive], [IsDeleted], [SNOMED], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (26, NULL, N'Right Deltoid', N'RD', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterAdministrationSite] ([AdministrationSiteID], [DeletedBy], [Description], [HL7], [IsActive], [IsDeleted], [SNOMED], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (27, NULL, N'Right Lower Forearm', N'RLFA', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterAdministrationSite] ([AdministrationSiteID], [DeletedBy], [Description], [HL7], [IsActive], [IsDeleted], [SNOMED], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (28, NULL, N'UNKNOWN', NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 118)
SET IDENTITY_INSERT [dbo].[MasterAdministrationSite] OFF
SET IDENTITY_INSERT [dbo].[MasterAllergies] ON 

INSERT [dbo].[MasterAllergies] ([AllergyID], [AllergyType], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [IsActive], [IsDeleted], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (1, N'Specific Drug', 1, CAST(0x07C0AA70975A873D0B AS DateTime2), NULL, NULL, 1, 0, NULL, NULL, 1)
INSERT [dbo].[MasterAllergies] ([AllergyID], [AllergyType], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [IsActive], [IsDeleted], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (2, N'Drug Class', 1, CAST(0x07C0AA70975A873D0B AS DateTime2), NULL, NULL, 1, 0, NULL, NULL, 1)
INSERT [dbo].[MasterAllergies] ([AllergyID], [AllergyType], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [IsActive], [IsDeleted], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (3, N'Non dry Allergy', 1, CAST(0x07C0AA70975A873D0B AS DateTime2), NULL, NULL, 1, 0, NULL, NULL, 1)
INSERT [dbo].[MasterAllergies] ([AllergyID], [AllergyType], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [IsActive], [IsDeleted], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (4, N'Specific Drug', NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, 118)
INSERT [dbo].[MasterAllergies] ([AllergyID], [AllergyType], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [IsActive], [IsDeleted], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (5, N'Drug Class', NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, 118)
INSERT [dbo].[MasterAllergies] ([AllergyID], [AllergyType], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [IsActive], [IsDeleted], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (6, N'Non dry Allergy', NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, 118)
SET IDENTITY_INSERT [dbo].[MasterAllergies] OFF
SET IDENTITY_INSERT [dbo].[MasterEthnicity] ON 

INSERT [dbo].[MasterEthnicity] ([EthnicityID], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [EthnicityName], [IsActive], [IsDeleted], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (1, 1, CAST(0x0770F90B3708153D0B AS DateTime2), 1, NULL, N'Indian', 1, 0, NULL, NULL, 1)
INSERT [dbo].[MasterEthnicity] ([EthnicityID], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [EthnicityName], [IsActive], [IsDeleted], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (2, 1, CAST(0x0700000000001B3D0B AS DateTime2), 1, NULL, N'American', 1, 0, NULL, NULL, 1)
INSERT [dbo].[MasterEthnicity] ([EthnicityID], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [EthnicityName], [IsActive], [IsDeleted], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (3, NULL, NULL, NULL, NULL, N'American', 1, 0, NULL, NULL, 118)
SET IDENTITY_INSERT [dbo].[MasterEthnicity] OFF
SET IDENTITY_INSERT [dbo].[MasterImmunityStatus] ON 

INSERT [dbo].[MasterImmunityStatus] ([ImmunityStatusID], [ConceptCode], [ConceptName], [Defination], [DeletedBy], [HL7Code], [IsActive], [IsDeleted], [NIP004], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (1, N'409498004', N'Anthrax (disorder)', N'History of anthrax infection.', NULL, N'SCT', 1, 0, NULL, 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterImmunityStatus] ([ImmunityStatusID], [ConceptCode], [ConceptName], [Defination], [DeletedBy], [HL7Code], [IsActive], [IsDeleted], [NIP004], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (2, N'397428000', N'Diphtheria (disorder)', N'History of diphteria infection.', NULL, N'SCT', 1, 0, 24, 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterImmunityStatus] ([ImmunityStatusID], [ConceptCode], [ConceptName], [Defination], [DeletedBy], [HL7Code], [IsActive], [IsDeleted], [NIP004], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (3, N'76902006', N'Tetanus (disorder)', N'History of tetanus infection.', NULL, N'SCT', 1, 0, 32, 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterImmunityStatus] ([ImmunityStatusID], [ConceptCode], [ConceptName], [Defination], [DeletedBy], [HL7Code], [IsActive], [IsDeleted], [NIP004], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (4, N'27836007', N'Pertussis (disorder)', N'History of pertussis infection.', NULL, N'SCT', 1, 0, 29, 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterImmunityStatus] ([ImmunityStatusID], [ConceptCode], [ConceptName], [Defination], [DeletedBy], [HL7Code], [IsActive], [IsDeleted], [NIP004], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (5, N'40468003', N'Viral hepatitis, type A (disorder)', N'History of Hepatitis A infection.', NULL, N'SCT', 1, 0, NULL, 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterImmunityStatus] ([ImmunityStatusID], [ConceptCode], [ConceptName], [Defination], [DeletedBy], [HL7Code], [IsActive], [IsDeleted], [NIP004], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (6, N'66071002', N'Type B viral hepatitis (disorder)', N'History of Hepatitis B infection.', NULL, N'SCT', 1, 0, 26, 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterImmunityStatus] ([ImmunityStatusID], [ConceptCode], [ConceptName], [Defination], [DeletedBy], [HL7Code], [IsActive], [IsDeleted], [NIP004], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (7, N'91428005', N'Haemophilus influenzae infection (disorder)', N'History of HIB infection.', NULL, N'SCT', 1, 0, 25, 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterImmunityStatus] ([ImmunityStatusID], [ConceptCode], [ConceptName], [Defination], [DeletedBy], [HL7Code], [IsActive], [IsDeleted], [NIP004], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (8, N'240532009', N'Human papilloma virus infection (disorder)', N'History of HPV infection.', NULL, N'SCT', 1, 0, NULL, 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterImmunityStatus] ([ImmunityStatusID], [ConceptCode], [ConceptName], [Defination], [DeletedBy], [HL7Code], [IsActive], [IsDeleted], [NIP004], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (9, N'6142004', N'Influenza (disorder)', N'History of influenza infection.', NULL, N'SCT', 1, 0, NULL, 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterImmunityStatus] ([ImmunityStatusID], [ConceptCode], [ConceptName], [Defination], [DeletedBy], [HL7Code], [IsActive], [IsDeleted], [NIP004], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (10, N'409498004', N'Anthrax (disorder)', N'History of anthrax infection.', NULL, N'SCT', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterImmunityStatus] ([ImmunityStatusID], [ConceptCode], [ConceptName], [Defination], [DeletedBy], [HL7Code], [IsActive], [IsDeleted], [NIP004], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (11, N'397428000', N'Diphtheria (disorder)', N'History of diphteria infection.', NULL, N'SCT', 1, 0, 24, NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterImmunityStatus] ([ImmunityStatusID], [ConceptCode], [ConceptName], [Defination], [DeletedBy], [HL7Code], [IsActive], [IsDeleted], [NIP004], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (12, N'76902006', N'Tetanus (disorder)', N'History of tetanus infection.', NULL, N'SCT', 1, 0, 32, NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterImmunityStatus] ([ImmunityStatusID], [ConceptCode], [ConceptName], [Defination], [DeletedBy], [HL7Code], [IsActive], [IsDeleted], [NIP004], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (13, N'27836007', N'Pertussis (disorder)', N'History of pertussis infection.', NULL, N'SCT', 1, 0, 29, NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterImmunityStatus] ([ImmunityStatusID], [ConceptCode], [ConceptName], [Defination], [DeletedBy], [HL7Code], [IsActive], [IsDeleted], [NIP004], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (14, N'40468003', N'Viral hepatitis, type A (disorder)', N'History of Hepatitis A infection.', NULL, N'SCT', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterImmunityStatus] ([ImmunityStatusID], [ConceptCode], [ConceptName], [Defination], [DeletedBy], [HL7Code], [IsActive], [IsDeleted], [NIP004], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (15, N'66071002', N'Type B viral hepatitis (disorder)', N'History of Hepatitis B infection.', NULL, N'SCT', 1, 0, 26, NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterImmunityStatus] ([ImmunityStatusID], [ConceptCode], [ConceptName], [Defination], [DeletedBy], [HL7Code], [IsActive], [IsDeleted], [NIP004], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (16, N'91428005', N'Haemophilus influenzae infection (disorder)', N'History of HIB infection.', NULL, N'SCT', 1, 0, 25, NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterImmunityStatus] ([ImmunityStatusID], [ConceptCode], [ConceptName], [Defination], [DeletedBy], [HL7Code], [IsActive], [IsDeleted], [NIP004], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (17, N'240532009', N'Human papilloma virus infection (disorder)', N'History of HPV infection.', NULL, N'SCT', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterImmunityStatus] ([ImmunityStatusID], [ConceptCode], [ConceptName], [Defination], [DeletedBy], [HL7Code], [IsActive], [IsDeleted], [NIP004], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (18, N'6142004', N'Influenza (disorder)', N'History of influenza infection.', NULL, N'SCT', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 118)
SET IDENTITY_INSERT [dbo].[MasterImmunityStatus] OFF
SET IDENTITY_INSERT [dbo].[MasterImmunization] ON 

INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (1, N'998', NULL, 137, 1, 0, 1, N'Code 998 was added for use in VXU HL7 messages where the OBX segment is nested with the RXA segment, but the message does not contain information about a vaccine administration. An example of this use is to report the vaccines due next for a patient when no vaccine administration is being reported.', N'no vaccine administered', N'no vaccine administered', N'Inactive', 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (3, N'99', NULL, 139, 1, 0, 1, N'Code 99 will not be used in this table to avoid confusion with code 999.', N'RESERVED - do not use', N'RESERVED - do not use', N'Inactive', 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (4, N'999', NULL, 138, 1, 0, 1, N'This CVX code has little utility and should rarely be used.', N'unknown', N'unknown vaccine or immune globulin', N'Inactive', 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (5, N'143', NULL, 161, 1, 0, 0, N'This vaccine is administered as 2 tablets.', N'Adenovirus types 4 and 7', N'Adenovirus, type 4 and type 7, live, oral', N'Active', 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (6, N'54', NULL, 1, 1, 0, 0, NULL, N'adenovirus, type 4', N'adenovirus vaccine, type 4, live, oral', N'Inactive', 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (7, N'55', NULL, 2, 1, 0, 0, NULL, N'adenovirus, type 7', N'adenovirus vaccine, type 7, live, oral', N'Inactive', 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (8, N'82', NULL, 3, 1, 0, 0, N'This CVX code allows reporting of a vaccination when formulation is unknown (for example, when recording a adenovirus vaccination when noted on a vaccination card)', N'adenovirus, unspecified formulation', N'adenovirus vaccine, unspecified formulation', N'Inactive', 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (9, N'24', NULL, 4, 1, 0, 0, NULL, N'anthrax', N'anthrax vaccine', N'Active', 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (10, N'19', NULL, 5, 1, 0, 0, NULL, N'BCG', N'Bacillus Calmette-Guerin vaccine', N'Active', 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (11, N'27', NULL, 6, 1, 0, 0, NULL, N'botulinum antitoxin', N'botulinum antitoxin', N'Active', 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (12, N'26', NULL, 7, 1, 0, 0, NULL, N'cholera', N'cholera vaccine', N'Inactive', 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (13, N'29', NULL, 8, 1, 0, 0, NULL, N'CMVIG', N'cytomegalovirus immune globulin, intravenous', N'Active', 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (14, N'56', NULL, 9, 1, 0, 0, NULL, N'dengue fever', N'dengue fever vaccine', N'Never Active', 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (15, N'12', NULL, 10, 1, 0, 0, NULL, N'diphtheria antitoxin', N'diphtheria antitoxin', N'Active', 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (16, N'28', NULL, 11, 1, 0, 0, NULL, N'DT (pediatric)', N'diphtheria and tetanus toxoids, adsorbed for pedia', N'Active', 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (17, N'20', NULL, 12, 1, 0, 0, NULL, N'DTaP', N'diphtheria, tetanus toxoids and acellular pertussi', N'Active', 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (18, N'106', NULL, 13, 1, 0, 0, NULL, N'DTaP, 5 pertussis antigens', N'diphtheria, tetanus toxoids and acellular pertussi', N'Active', 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (19, N'107', NULL, 14, 1, 0, 0, N'This CVX code allows reporting of a vaccination when formulation is unknown (for example, when recording a DTaP vaccination when noted on a vaccination card)', N'DTaP, unspecified formulation', N'diphtheria, tetanus toxoids and acellular pertussi', N'Inactive', 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (20, N'146', NULL, 164, 1, 0, 0, N'Note that this vaccine is different from CVX 132.', N'DTaP,IPV,Hib,HepB', N'Diphtheria and Tetanus Toxoids and Acellular Pertu', N'Pending', 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (21, N'110', NULL, 15, 1, 0, 0, NULL, N'DTaP-Hep B-IPV', N'DTaP-hepatitis B and poliovirus vaccine', N'Active', 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (22, N'50', NULL, 16, 1, 0, 0, NULL, N'DTaP-Hib', N'DTaP-Haemophilus influenzae type b conjugate vacci', N'Active', 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (23, N'120', NULL, 17, 1, 0, 0, NULL, N'DTaP-Hib-IPV', N'diphtheria, tetanus toxoids and acellular pertussi', N'Active', 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (24, N'130', NULL, 19, 1, 0, 0, NULL, N'DTaP-IPV', N'Diphtheria, tetanus toxoids and acellular pertussi', N'Active', 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (25, N'998', NULL, 137, 1, 0, 1, N'Code 998 was added for use in VXU HL7 messages where the OBX segment is nested with the RXA segment, but the message does not contain information about a vaccine administration. An example of this use is to report the vaccines due next for a client when no vaccine administration is being reported.', N'no vaccine administered', N'no vaccine administered', N'Inactive', NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (26, N'99', NULL, 139, 1, 0, 1, N'Code 99 will not be used in this table to avoid confusion with code 999.', N'RESERVED - do not use', N'RESERVED - do not use', N'Inactive', NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (27, N'999', NULL, 138, 1, 0, 1, N'This CVX code has little utility and should rarely be used.', N'unknown', N'unknown vaccine or immune globulin', N'Inactive', NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (28, N'143', NULL, 161, 1, 0, 0, N'This vaccine is administered as 2 tablets.', N'Adenovirus types 4 and 7', N'Adenovirus, type 4 and type 7, live, oral', N'Active', NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (29, N'54', NULL, 1, 1, 0, 0, NULL, N'adenovirus, type 4', N'adenovirus vaccine, type 4, live, oral', N'Inactive', NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (30, N'55', NULL, 2, 1, 0, 0, NULL, N'adenovirus, type 7', N'adenovirus vaccine, type 7, live, oral', N'Inactive', NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (31, N'82', NULL, 3, 1, 0, 0, N'This CVX code allows reporting of a vaccination when formulation is unknown (for example, when recording a adenovirus vaccination when noted on a vaccination card)', N'adenovirus, unspecified formulation', N'adenovirus vaccine, unspecified formulation', N'Inactive', NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (32, N'24', NULL, 4, 1, 0, 0, NULL, N'anthrax', N'anthrax vaccine', N'Active', NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (33, N'19', NULL, 5, 1, 0, 0, NULL, N'BCG', N'Bacillus Calmette-Guerin vaccine', N'Active', NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (34, N'27', NULL, 6, 1, 0, 0, NULL, N'botulinum antitoxin', N'botulinum antitoxin', N'Active', NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (35, N'26', NULL, 7, 1, 0, 0, NULL, N'cholera', N'cholera vaccine', N'Inactive', NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (36, N'29', NULL, 8, 1, 0, 0, NULL, N'CMVIG', N'cytomegalovirus immune globulin, intravenous', N'Active', NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (37, N'56', NULL, 9, 1, 0, 0, NULL, N'dengue fever', N'dengue fever vaccine', N'Never Active', NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (38, N'12', NULL, 10, 1, 0, 0, NULL, N'diphtheria antitoxin', N'diphtheria antitoxin', N'Active', NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (39, N'28', NULL, 11, 1, 0, 0, NULL, N'DT (pediatric)', N'diphtheria and tetanus toxoids, adsorbed for pedia', N'Active', NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (40, N'20', NULL, 12, 1, 0, 0, NULL, N'DTaP', N'diphtheria, tetanus toxoids and acellular pertussi', N'Active', NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (41, N'106', NULL, 13, 1, 0, 0, NULL, N'DTaP, 5 pertussis antigens', N'diphtheria, tetanus toxoids and acellular pertussi', N'Active', NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (42, N'107', NULL, 14, 1, 0, 0, N'This CVX code allows reporting of a vaccination when formulation is unknown (for example, when recording a DTaP vaccination when noted on a vaccination card)', N'DTaP, unspecified formulation', N'diphtheria, tetanus toxoids and acellular pertussi', N'Inactive', NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (43, N'146', NULL, 164, 1, 0, 0, N'Note that this vaccine is different from CVX 132.', N'DTaP,IPV,Hib,HepB', N'Diphtheria and Tetanus Toxoids and Acellular Pertu', N'Pending', NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (44, N'110', NULL, 15, 1, 0, 0, NULL, N'DTaP-Hep B-IPV', N'DTaP-hepatitis B and poliovirus vaccine', N'Active', NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (45, N'50', NULL, 16, 1, 0, 0, NULL, N'DTaP-Hib', N'DTaP-Haemophilus influenzae type b conjugate vacci', N'Active', NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (46, N'120', NULL, 17, 1, 0, 0, NULL, N'DTaP-Hib-IPV', N'diphtheria, tetanus toxoids and acellular pertussi', N'Active', NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterImmunization] ([ImmunizationID], [CvxCode], [DeletedBy], [InternalID], [IsActive], [IsDeleted], [NonVaccine], [Note], [ShortDesc], [VaccineName], [VaccineStatus], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (47, N'130', NULL, 19, 1, 0, 0, NULL, N'DTaP-IPV', N'Diphtheria, tetanus toxoids and acellular pertussi', N'Active', NULL, NULL, NULL, NULL, NULL, 118)
SET IDENTITY_INSERT [dbo].[MasterImmunization] OFF
SET IDENTITY_INSERT [dbo].[MasterLonic] ON 

INSERT [dbo].[MasterLonic] ([LonicID], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [LonicCode], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (4, 1, CAST(0x07605792BA4B553D0B AS DateTime2), NULL, NULL, N'10-Hydroxycarbazepine [Mass/?volume] in Serum or Plasma', 1, 0, N'31019-3', NULL, NULL, 1)
INSERT [dbo].[MasterLonic] ([LonicID], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [LonicCode], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (5, 1, CAST(0x070000000000493C0B AS DateTime2), NULL, NULL, N'Cytokeratin 19 [Units/?volume] in Pleural fluid', 1, 0, N'19183-3', NULL, NULL, 1)
INSERT [dbo].[MasterLonic] ([LonicID], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [LonicCode], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (6, 1, CAST(0x070000000000493C0B AS DateTime2), NULL, NULL, N'Cytokeratin 19 [Units/?volume] in Serum or Plasma', 1, 0, N'19182-5', NULL, NULL, 1)
INSERT [dbo].[MasterLonic] ([LonicID], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [LonicCode], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (7, 1, CAST(0x070000000000493C0B AS DateTime2), NULL, NULL, N'Cytokeratin 19 [Units/volume] in Urine', 1, 0, N'56787-5', NULL, NULL, 1)
INSERT [dbo].[MasterLonic] ([LonicID], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [LonicCode], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (8, NULL, NULL, NULL, NULL, N'10-Hydroxycarbazepine [Mass/?volume] in Serum or Plasma', 1, 0, N'31019-3', NULL, NULL, 118)
INSERT [dbo].[MasterLonic] ([LonicID], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [LonicCode], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (9, NULL, NULL, NULL, NULL, N'Cytokeratin 19 [Units/?volume] in Pleural fluid', 1, 0, N'19183-3', NULL, NULL, 118)
INSERT [dbo].[MasterLonic] ([LonicID], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [LonicCode], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (10, NULL, NULL, NULL, NULL, N'Cytokeratin 19 [Units/?volume] in Serum or Plasma', 1, 0, N'19182-5', NULL, NULL, 118)
INSERT [dbo].[MasterLonic] ([LonicID], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [LonicCode], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (11, NULL, NULL, NULL, NULL, N'Cytokeratin 19 [Units/volume] in Urine', 1, 0, N'56787-5', NULL, NULL, 118)
SET IDENTITY_INSERT [dbo].[MasterLonic] OFF
SET IDENTITY_INSERT [dbo].[MasterManufacture] ON 

INSERT [dbo].[MasterManufacture] ([ManufactureID], [DeletedBy], [IsActive], [IsDeleted], [MVXCode], [ManufacturerName], [Notes], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (4, NULL, 1, 0, N'JPN', N'The Research Foundation for Microbial Diseases of Osaka University (BIKEN)', 0, 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterManufacture] ([ManufactureID], [DeletedBy], [IsActive], [IsDeleted], [MVXCode], [ManufacturerName], [Notes], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (5, NULL, 1, 0, N'IDB', N'ID Biomedical', 0, 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterManufacture] ([ManufactureID], [DeletedBy], [IsActive], [IsDeleted], [MVXCode], [ManufacturerName], [Notes], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (7, NULL, 1, 0, N'OTC', N'Organon Teknika Corporation', 0, 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterManufacture] ([ManufactureID], [DeletedBy], [IsActive], [IsDeleted], [MVXCode], [ManufacturerName], [Notes], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (12, NULL, 1, 0, N'JPN', N'The Research Foundation for Microbial Diseases of Osaka University (BIKEN)', 0, NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterManufacture] ([ManufactureID], [DeletedBy], [IsActive], [IsDeleted], [MVXCode], [ManufacturerName], [Notes], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (13, NULL, 1, 0, N'IDB', N'ID Biomedical', 0, NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterManufacture] ([ManufactureID], [DeletedBy], [IsActive], [IsDeleted], [MVXCode], [ManufacturerName], [Notes], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (14, NULL, 1, 0, N'OTC', N'Organon Teknika Corporation', 0, NULL, NULL, NULL, NULL, NULL, 118)
SET IDENTITY_INSERT [dbo].[MasterManufacture] OFF
SET IDENTITY_INSERT [dbo].[MasterNoteType] ON 

INSERT [dbo].[MasterNoteType] ([Id], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [OrganizationID], [Type], [UpdatedBy], [UpdatedDate], [IsDirectService], [NavigationLink]) VALUES (1, 1, CAST(0x07E0B3268D446A3D0B AS DateTime2), NULL, NULL, N'Soap Note', 1, 0, 1, N'Soap Note', NULL, NULL, 1, N'/soap')
INSERT [dbo].[MasterNoteType] ([Id], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [OrganizationID], [Type], [UpdatedBy], [UpdatedDate], [IsDirectService], [NavigationLink]) VALUES (3, NULL, NULL, NULL, NULL, N'SOAP Note', 1, 0, 118, N'SOAP Note', NULL, NULL, 1, N'/soap')
INSERT [dbo].[MasterNoteType] ([Id], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [OrganizationID], [Type], [UpdatedBy], [UpdatedDate], [IsDirectService], [NavigationLink]) VALUES (4, NULL, NULL, NULL, NULL, N'SOAP Note', 1, 0, 1, N'SOAP Note', NULL, NULL, 0, N'/soap')
INSERT [dbo].[MasterNoteType] ([Id], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [OrganizationID], [Type], [UpdatedBy], [UpdatedDate], [IsDirectService], [NavigationLink]) VALUES (5, NULL, NULL, NULL, NULL, N'Non Billable SOAP Note', 1, 0, 1, N'Non Billable SOAP Note', NULL, NULL, 1, N'/non-billable-soap')
INSERT [dbo].[MasterNoteType] ([Id], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [OrganizationID], [Type], [UpdatedBy], [UpdatedDate], [IsDirectService], [NavigationLink]) VALUES (6, NULL, NULL, NULL, NULL, N'Non Billable SOAP Note', 1, 0, 1, N'Non Billable SOAP Note', NULL, NULL, 0, N'/non-billable-soap')
INSERT [dbo].[MasterNoteType] ([Id], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [OrganizationID], [Type], [UpdatedBy], [UpdatedDate], [IsDirectService], [NavigationLink]) VALUES (7, NULL, NULL, NULL, NULL, N'Video SOAP Note', 1, 0, 1, N'Video SOAP Note', NULL, NULL, 1, N'/video-soap')
INSERT [dbo].[MasterNoteType] ([Id], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [OrganizationID], [Type], [UpdatedBy], [UpdatedDate], [IsDirectService], [NavigationLink]) VALUES (8, NULL, NULL, NULL, NULL, N'Video SOAP Note', 1, 0, 1, N'Video SOAP Note', NULL, NULL, 0, N'/video-soap')
INSERT [dbo].[MasterNoteType] ([Id], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [OrganizationID], [Type], [UpdatedBy], [UpdatedDate], [IsDirectService], [NavigationLink]) VALUES (9, NULL, NULL, NULL, NULL, N'Non Billable SOAP Encounter', 1, 0, 1, N'Non Billable SOAP Encounter', NULL, NULL, 1, N'/soap-encounter')
INSERT [dbo].[MasterNoteType] ([Id], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [OrganizationID], [Type], [UpdatedBy], [UpdatedDate], [IsDirectService], [NavigationLink]) VALUES (10, NULL, NULL, NULL, NULL, N'Non Billable SOAP Encounter', 1, 0, 1, N'Non Billable SOAP Encounter', NULL, NULL, 0, N'/non-billable-soap-encounter')
INSERT [dbo].[MasterNoteType] ([Id], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [OrganizationID], [Type], [UpdatedBy], [UpdatedDate], [IsDirectService], [NavigationLink]) VALUES (11, NULL, NULL, NULL, NULL, N'Non Billable SOAP Encounter', 1, 0, 1, N'Non Billable SOAP Encounter', NULL, NULL, 0, N'/soap-encounter')
INSERT [dbo].[MasterNoteType] ([Id], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [OrganizationID], [Type], [UpdatedBy], [UpdatedDate], [IsDirectService], [NavigationLink]) VALUES (12, NULL, NULL, NULL, NULL, N'Non Billable SOAP Encounter', 1, 0, 1, N'Non Billable SOAP Encounter', NULL, NULL, 1, N'/non-billable-soap-encounter')
INSERT [dbo].[MasterNoteType] ([Id], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [OrganizationID], [Type], [UpdatedBy], [UpdatedDate], [IsDirectService], [NavigationLink]) VALUES (13, NULL, NULL, NULL, NULL, N'Non Billable SOAP Encounter', 1, 0, 1, N'Non Billable SOAP Encounter', NULL, NULL, 1, N'/video-soap-encounter')
INSERT [dbo].[MasterNoteType] ([Id], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [OrganizationID], [Type], [UpdatedBy], [UpdatedDate], [IsDirectService], [NavigationLink]) VALUES (14, NULL, NULL, NULL, NULL, N'Non Billable SOAP Encounter', 1, 0, 1, N'Non Billable SOAP Encounter', NULL, NULL, 0, N'/video-soap-encounter')
SET IDENTITY_INSERT [dbo].[MasterNoteType] OFF
SET IDENTITY_INSERT [dbo].[MasterPatientLocation] ON 

INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (1, 1, 1, CAST(0x070000000000493C0B AS DateTime2), NULL, NULL, N'A facility or location where drugs and other medically related items and services are sold, dispensed, or otherwise provided directly to patients. (effective 10/1/05)', 1, 0, N'Clinic', NULL, NULL, 1)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (2, 2, 1, CAST(0x070000000000493C0B AS DateTime2), NULL, NULL, N'The location where health services and health related services are provided or received, through telecommunication technology.', 1, 0, N'Rural Health Clinic', NULL, NULL, 1)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (3, 3, 1, CAST(0x070000000000493C0B AS DateTime2), NULL, NULL, N'A facility whose primary purpose is education.', 1, 0, N'School', NULL, NULL, 1)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (4, 4, 1, CAST(0x070000000000493C0B AS DateTime2), NULL, NULL, N'A facility or location whose primary purpose is to provide temporary housing to homeless individuals (e.g., emergency shelters, individual or family shelters).', 1, 0, N'Homeless Shelter', NULL, NULL, 1)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (5, 5, 1, CAST(0x070000000000493C0B AS DateTime2), NULL, NULL, N'A facility or location, owned and operated by the Indian Health Service, which provides diagnostic, therapeutic (surgical and non-surgical), and rehabilitation services to American Indians and Alaska Natives who do not require hospitalization.  (See 05-08 Special Considerations below.)', 1, 0, N'Indian Health Service Free-standing Facility', NULL, NULL, 1)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (6, 6, 1, CAST(0x070000000000493C0B AS DateTime2), NULL, NULL, N'A facility or location, owned and operated by the Indian Health Service, which provides diagnostic, therapeutic (surgical and non-surgical), and rehabilitation services rendered by, or under the supervision of, physicians to American Indians and Alaska Natives admitted as inpatients or outpatients.  (See 05-08 Special Considerations below.)', 1, 0, N'Indian Health Service Provider-based Facility', NULL, NULL, 1)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (7, 7, 1, CAST(0x070000000000493C0B AS DateTime2), NULL, NULL, N'A facility or location owned and operated by a federally recognized American Indian or Alaska Native tribe or tribal organization under a 638 agreement, which provides diagnostic, therapeutic (surgical and nonsurgical), and rehabilitation services to tribal members who do not require hospitalization.  (See 05-08 Special Considerations below.)', 1, 0, N'Tribal 638 Free-standing Facility', NULL, NULL, 1)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (8, 8, 1, CAST(0x070000000000493C0B AS DateTime2), NULL, NULL, N'A facility or location owned and operated by a federally recognized American Indian or Alaska Native tribe or tribal organization under a 638 agreement, which provides diagnostic, therapeutic (surgical and nonsurgical), and rehabilitation services to tribal members admitted as inpatients or outpatients.  (See 05-08 Special Considerations below.)', 1, 0, N'Tribal 638 Provider-based Facility', NULL, NULL, 1)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (9, 11, 1, CAST(0x070000000000493C0B AS DateTime2), NULL, NULL, N'A prison, jail, reformatory, work farm, detention center, or any other similar facility maintained by either Federal, State or local authorities for the purpose of confinement or rehabilitation of adult or juvenile criminal offenders.', 1, 0, N'Office Visit', NULL, NULL, 1)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (10, 10, 1, CAST(0x070000000000493C0B AS DateTime2), NULL, NULL, N'N/A', 1, 0, N'Unassigned', NULL, NULL, 1)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (11, 11, 1, CAST(0x070000000000493C0B AS DateTime2), NULL, NULL, N'Location, other than a hospital, skilled nursing facility (SNF), military treatment facility, community health center, State or local public health clinic, or intermediate care facility (ICF), where the health professional routinely provides health examinations, diagnosis, and treatment of illness or injury on an ambulatory basis.', 1, 0, N'Hospital', NULL, NULL, 1)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (12, 12, 1, CAST(0x070000000000493C0B AS DateTime2), NULL, NULL, N'Location, other than a hospital or other facility, where the patient receives care in a private residence.', 1, 0, N'Home', NULL, NULL, 1)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (13, 3, NULL, NULL, NULL, NULL, N'School', 1, 0, N'School', NULL, NULL, 118)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (14, 4, NULL, NULL, NULL, NULL, N'Homeless Shelter', 1, 0, N'Homeless Shelter', NULL, NULL, 118)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (15, 5, NULL, NULL, NULL, NULL, N'Indian Health Service Free-Standing Facility', 1, 0, N'Indian Health Service Free-Standing Facility', NULL, NULL, 118)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (16, 6, NULL, NULL, NULL, NULL, N'Indian Health Service Provider-Based Facility', 1, 0, N'Indian Health Service Provider-Based Facility', NULL, NULL, 118)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (17, 7, NULL, NULL, NULL, NULL, N'Tribal 638 Free-Standing Facility', 1, 0, N'Tribal 638 Free-Standing Facility', NULL, NULL, 118)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (18, 8, NULL, NULL, NULL, NULL, N'Tribal 638 Provider Based-Facility', 1, 0, N'Tribal 638 Provider Based-Facility', NULL, NULL, 118)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (19, 11, NULL, NULL, NULL, NULL, N'Office Visit', 1, 0, N'Office Visit', NULL, NULL, 118)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (20, 12, NULL, NULL, NULL, NULL, N'Home', 1, 0, N'Home', NULL, NULL, 118)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (21, 13, NULL, NULL, NULL, NULL, N'Assisted Living', 1, 0, N'Assisted Living', NULL, NULL, 118)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (22, 14, NULL, NULL, NULL, NULL, N'Group Home', 1, 0, N'Group Home', NULL, NULL, 118)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (23, 15, NULL, NULL, NULL, NULL, N'Mobile Unit', 1, 0, N'Mobile Unit', NULL, NULL, 118)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (24, 20, NULL, NULL, NULL, NULL, N'Urgent Care Facility', 1, 0, N'Urgent Care Facility', NULL, NULL, 118)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (25, 21, NULL, NULL, NULL, NULL, N'Inpatient Hospital', 1, 0, N'Inpatient Hospital', NULL, NULL, 118)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (26, 22, NULL, NULL, NULL, NULL, N'Outpatient Hospital', 1, 0, N'Outpatient Hospital', NULL, NULL, 118)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (27, 23, NULL, NULL, NULL, NULL, N'Emergency Room', 1, 0, N'Emergency Room', NULL, NULL, 118)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (28, 24, NULL, NULL, NULL, NULL, N'Ambulatory Surgical Center', 1, 0, N'Ambulatory Surgical Center', NULL, NULL, 118)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (29, 25, NULL, NULL, NULL, NULL, N'Birthing Center', 1, 0, N'Birthing Center', NULL, NULL, 118)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (30, 26, NULL, NULL, NULL, NULL, N'Military Treatment Facility', 1, 0, N'Military Treatment Facility', NULL, NULL, 118)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (31, 31, NULL, NULL, NULL, NULL, N'Skilled Nursing Facility', 1, 0, N'Skilled Nursing Facility', NULL, NULL, 118)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (32, 32, NULL, NULL, NULL, NULL, N'Nursing Facility', 1, 0, N'Nursing Facility', NULL, NULL, 118)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (33, 33, NULL, NULL, NULL, NULL, N'Custodial Care Facility', 1, 0, N'Custodial Care Facility', NULL, NULL, 118)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (34, 34, NULL, NULL, NULL, NULL, N'Hospice', 1, 0, N'Hospice', NULL, NULL, 118)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (35, 41, NULL, NULL, NULL, NULL, N'Ambulance - Land', 1, 0, N'Ambulance - Land', NULL, NULL, 118)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (36, 42, NULL, NULL, NULL, NULL, N'Ambulance - Air or Water', 1, 0, N'Ambulance - Air or Water', NULL, NULL, 118)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (37, 50, NULL, NULL, NULL, NULL, N'Federally Qualified Health Center', 1, 0, N'Federally Qualified Health Center', NULL, NULL, 118)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (38, 51, NULL, NULL, NULL, NULL, N'Inpatient Psychiatric Facility', 1, 0, N'Inpatient Psychiatric Facility', NULL, NULL, 118)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (39, 52, NULL, NULL, NULL, NULL, N'Psychiatric Facility Partial Hospitalization', 1, 0, N'Psychiatric Facility Partial Hospitalization', NULL, NULL, 118)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (40, 53, NULL, NULL, NULL, NULL, N'Community Mental Health Center', 1, 0, N'Intermediate Care Facility', NULL, NULL, 118)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (41, 54, NULL, NULL, NULL, NULL, N'Intermediate Care Facility', 1, 0, N'Skilled Nursing Facility', NULL, NULL, 118)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (42, 55, NULL, NULL, NULL, NULL, N'Residential Substance Abuse Treatment Facility', 1, 0, N'Residential Substance Abuse Treatment Facility', NULL, NULL, 118)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (43, 56, NULL, NULL, NULL, NULL, N'Psychiatric Residential Treatment Center', 1, 0, N'Psychiatric Residential Treatment Center', NULL, NULL, 118)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (44, 60, NULL, NULL, NULL, NULL, N'Mass Immunization Center', 1, 0, N'Mass Immunization Center', NULL, NULL, 118)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (45, 61, NULL, NULL, NULL, NULL, N'Comprehensive Inpatient Rehab Facility', 1, 0, N'Comprehensive Inpatient Rehab Facility', NULL, NULL, 118)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (46, 62, NULL, NULL, NULL, NULL, N'Comprehensive Outpatient Rehab Facility', 1, 0, N'Comprehensive Outpatient Rehab Facility', NULL, NULL, 118)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (47, 65, NULL, NULL, NULL, NULL, N'End Stage Renal Disease Treatment Facility', 1, 0, N'End Stage Renal Disease Treatment Facility', NULL, NULL, 118)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (48, 71, NULL, NULL, NULL, NULL, N'State or Local Public Health Clinic', 1, 0, N'State or Local Public Health Clinic', NULL, NULL, 118)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (49, 2, NULL, NULL, NULL, NULL, N'Rural Health Clinic', 1, 0, N'Rural Health Clinic', NULL, NULL, 118)
INSERT [dbo].[MasterPatientLocation] ([PatientLocationID], [Code], [CreatedBy], [CreatedDate], [DeletedBy], [DeletedDate], [Description], [IsActive], [IsDeleted], [Location], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (50, 81, NULL, NULL, NULL, NULL, N'Independent Laboratory', 1, 0, N'Independent Laboratory', NULL, NULL, 118)
SET IDENTITY_INSERT [dbo].[MasterPatientLocation] OFF
SET IDENTITY_INSERT [dbo].[MasterRace] ON 

INSERT [dbo].[MasterRace] ([RaceID], [DeletedBy], [IsActive], [IsDeleted], [RaceName], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (1, NULL, 1, 0, N'White', 1, CAST(0x0770F90B3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterRace] ([RaceID], [DeletedBy], [IsActive], [IsDeleted], [RaceName], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (2, NULL, 1, 0, N'Black', 1, CAST(0x07009D3DA00C883D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterRace] ([RaceID], [DeletedBy], [IsActive], [IsDeleted], [RaceName], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (3, NULL, 1, 0, N'Asian', 1, CAST(0x07009D3DA00C883D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterRace] ([RaceID], [DeletedBy], [IsActive], [IsDeleted], [RaceName], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (4, NULL, 1, 0, N'Native American', 1, CAST(0x07009D3DA00C883D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterRace] ([RaceID], [DeletedBy], [IsActive], [IsDeleted], [RaceName], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (5, NULL, 1, 0, N'Black and African American', 1, CAST(0x07009D3DA00C883D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterRace] ([RaceID], [DeletedBy], [IsActive], [IsDeleted], [RaceName], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (6, NULL, 1, 0, N'White', NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterRace] ([RaceID], [DeletedBy], [IsActive], [IsDeleted], [RaceName], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (7, NULL, 1, 0, N'Black', NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterRace] ([RaceID], [DeletedBy], [IsActive], [IsDeleted], [RaceName], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (8, NULL, 1, 0, N'Asian', NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterRace] ([RaceID], [DeletedBy], [IsActive], [IsDeleted], [RaceName], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (9, NULL, 1, 0, N'Native American', NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterRace] ([RaceID], [DeletedBy], [IsActive], [IsDeleted], [RaceName], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (10, NULL, 1, 0, N'Black and African American', NULL, NULL, NULL, NULL, NULL, 118)
SET IDENTITY_INSERT [dbo].[MasterRace] OFF
SET IDENTITY_INSERT [dbo].[MasterRejectionReason] ON 

INSERT [dbo].[MasterRejectionReason] ([RejectionReasonID], [DeletedBy], [IsActive], [IsDeleted], [ReasonCode], [ReasonDesc], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (1, NULL, 1, 0, N'00', N'Parental decision', 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterRejectionReason] ([RejectionReasonID], [DeletedBy], [IsActive], [IsDeleted], [ReasonCode], [ReasonDesc], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (2, NULL, 1, 0, N'01', N'Religious exemption', 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterRejectionReason] ([RejectionReasonID], [DeletedBy], [IsActive], [IsDeleted], [ReasonCode], [ReasonDesc], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (3, NULL, 1, 0, N'02', N'Other (must add text component of the CE field with description)', 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterRejectionReason] ([RejectionReasonID], [DeletedBy], [IsActive], [IsDeleted], [ReasonCode], [ReasonDesc], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (4, NULL, 1, 0, N'03', N'Patient decision', 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterRejectionReason] ([RejectionReasonID], [DeletedBy], [IsActive], [IsDeleted], [ReasonCode], [ReasonDesc], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (5, NULL, 1, 0, N'00', N'Parental decision', NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterRejectionReason] ([RejectionReasonID], [DeletedBy], [IsActive], [IsDeleted], [ReasonCode], [ReasonDesc], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (6, NULL, 1, 0, N'01', N'Religious exemption', NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterRejectionReason] ([RejectionReasonID], [DeletedBy], [IsActive], [IsDeleted], [ReasonCode], [ReasonDesc], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (7, NULL, 1, 0, N'02', N'Other (must add text component of the CE field with description)', NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterRejectionReason] ([RejectionReasonID], [DeletedBy], [IsActive], [IsDeleted], [ReasonCode], [ReasonDesc], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (8, NULL, 1, 0, N'03', N'Client decision', NULL, NULL, NULL, NULL, NULL, 118)
SET IDENTITY_INSERT [dbo].[MasterRejectionReason] OFF
SET IDENTITY_INSERT [dbo].[MasterRelationship] ON 

INSERT [dbo].[MasterRelationship] ([RelationshipID], [DeletedBy], [IsActive], [IsDeleted], [RelationshipName], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID], [RelationshipCode], [DisplayOrder]) VALUES (1, NULL, 1, 0, N'Uncle', 1, CAST(0x0770F90B3708153D0B AS DateTime2), NULL, NULL, NULL, 1, NULL, NULL)
INSERT [dbo].[MasterRelationship] ([RelationshipID], [DeletedBy], [IsActive], [IsDeleted], [RelationshipName], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID], [RelationshipCode], [DisplayOrder]) VALUES (2, NULL, 1, 0, N'Aunty', 1, CAST(0x0770F90B3708153D0B AS DateTime2), NULL, NULL, NULL, 1, NULL, NULL)
INSERT [dbo].[MasterRelationship] ([RelationshipID], [DeletedBy], [IsActive], [IsDeleted], [RelationshipName], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID], [RelationshipCode], [DisplayOrder]) VALUES (3, NULL, 1, 0, N'Father', 1, CAST(0x0770F90B3708153D0B AS DateTime2), NULL, NULL, NULL, 1, NULL, NULL)
INSERT [dbo].[MasterRelationship] ([RelationshipID], [DeletedBy], [IsActive], [IsDeleted], [RelationshipName], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID], [RelationshipCode], [DisplayOrder]) VALUES (4, NULL, 1, 0, N'Mother', 1, CAST(0x0770F90B3708153D0B AS DateTime2), NULL, NULL, NULL, 1, NULL, NULL)
INSERT [dbo].[MasterRelationship] ([RelationshipID], [DeletedBy], [IsActive], [IsDeleted], [RelationshipName], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID], [RelationshipCode], [DisplayOrder]) VALUES (5, NULL, 1, 0, N'Brother', 1, CAST(0x0770F90B3708153D0B AS DateTime2), NULL, NULL, NULL, 1, NULL, NULL)
INSERT [dbo].[MasterRelationship] ([RelationshipID], [DeletedBy], [IsActive], [IsDeleted], [RelationshipName], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID], [RelationshipCode], [DisplayOrder]) VALUES (6, NULL, 1, 0, N'Sister', 1, CAST(0x0770F90B3708153D0B AS DateTime2), NULL, NULL, NULL, 1, NULL, NULL)
INSERT [dbo].[MasterRelationship] ([RelationshipID], [DeletedBy], [IsActive], [IsDeleted], [RelationshipName], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID], [RelationshipCode], [DisplayOrder]) VALUES (7, NULL, 1, 0, N'Nephew', 1, CAST(0x0770F90B3708153D0B AS DateTime2), NULL, NULL, NULL, 1, NULL, NULL)
INSERT [dbo].[MasterRelationship] ([RelationshipID], [DeletedBy], [IsActive], [IsDeleted], [RelationshipName], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID], [RelationshipCode], [DisplayOrder]) VALUES (8, NULL, 1, 0, N'Spouse', 1, CAST(0x07B02D88A3C88F3D0B AS DateTime2), NULL, NULL, NULL, 1, N'01', NULL)
INSERT [dbo].[MasterRelationship] ([RelationshipID], [DeletedBy], [IsActive], [IsDeleted], [RelationshipName], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID], [RelationshipCode], [DisplayOrder]) VALUES (9, NULL, 1, 0, N'Child', 1, CAST(0x07B02D88A3C88F3D0B AS DateTime2), NULL, NULL, NULL, 1, N'19', NULL)
INSERT [dbo].[MasterRelationship] ([RelationshipID], [DeletedBy], [IsActive], [IsDeleted], [RelationshipName], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID], [RelationshipCode], [DisplayOrder]) VALUES (10, NULL, 1, 0, N'Other', 1, CAST(0x07708D6C1B079D3D0B AS DateTime2), NULL, NULL, NULL, 1, N'G8', NULL)
INSERT [dbo].[MasterRelationship] ([RelationshipID], [DeletedBy], [IsActive], [IsDeleted], [RelationshipName], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID], [RelationshipCode], [DisplayOrder]) VALUES (12, NULL, 1, 0, N'Employee', 1, CAST(0x079071ACE03EAF3D0B AS DateTime2), NULL, NULL, NULL, 1, N'20', NULL)
INSERT [dbo].[MasterRelationship] ([RelationshipID], [DeletedBy], [IsActive], [IsDeleted], [RelationshipName], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID], [RelationshipCode], [DisplayOrder]) VALUES (13, NULL, 1, 0, N'Unknown', 1, CAST(0x079071ACE03EAF3D0B AS DateTime2), NULL, NULL, NULL, 1, N'21', NULL)
INSERT [dbo].[MasterRelationship] ([RelationshipID], [DeletedBy], [IsActive], [IsDeleted], [RelationshipName], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID], [RelationshipCode], [DisplayOrder]) VALUES (14, NULL, 1, 0, N'Organ Donor', 1, CAST(0x079071ACE03EAF3D0B AS DateTime2), NULL, NULL, NULL, 1, N'39', NULL)
INSERT [dbo].[MasterRelationship] ([RelationshipID], [DeletedBy], [IsActive], [IsDeleted], [RelationshipName], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID], [RelationshipCode], [DisplayOrder]) VALUES (15, NULL, 1, 0, N'Cadaver Donor', 1, CAST(0x079071ACE03EAF3D0B AS DateTime2), NULL, NULL, NULL, 1, N'40', NULL)
INSERT [dbo].[MasterRelationship] ([RelationshipID], [DeletedBy], [IsActive], [IsDeleted], [RelationshipName], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID], [RelationshipCode], [DisplayOrder]) VALUES (16, NULL, 1, 0, N'Life Partner', 1, CAST(0x079071ACE03EAF3D0B AS DateTime2), NULL, NULL, NULL, 1, N'53', NULL)
INSERT [dbo].[MasterRelationship] ([RelationshipID], [DeletedBy], [IsActive], [IsDeleted], [RelationshipName], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID], [RelationshipCode], [DisplayOrder]) VALUES (17, NULL, 1, 0, N'Mother', NULL, NULL, NULL, NULL, NULL, 118, NULL, 1)
INSERT [dbo].[MasterRelationship] ([RelationshipID], [DeletedBy], [IsActive], [IsDeleted], [RelationshipName], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID], [RelationshipCode], [DisplayOrder]) VALUES (18, NULL, 1, 0, N'Father', NULL, NULL, NULL, NULL, NULL, 118, NULL, 2)
INSERT [dbo].[MasterRelationship] ([RelationshipID], [DeletedBy], [IsActive], [IsDeleted], [RelationshipName], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID], [RelationshipCode], [DisplayOrder]) VALUES (19, NULL, 1, 0, N'Brother', NULL, NULL, NULL, NULL, NULL, 118, NULL, 3)
INSERT [dbo].[MasterRelationship] ([RelationshipID], [DeletedBy], [IsActive], [IsDeleted], [RelationshipName], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID], [RelationshipCode], [DisplayOrder]) VALUES (20, NULL, 1, 0, N'Sister', NULL, NULL, NULL, NULL, NULL, 118, NULL, 4)
INSERT [dbo].[MasterRelationship] ([RelationshipID], [DeletedBy], [IsActive], [IsDeleted], [RelationshipName], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID], [RelationshipCode], [DisplayOrder]) VALUES (21, NULL, 1, 0, N'Uncle', NULL, NULL, NULL, NULL, NULL, 118, NULL, 5)
INSERT [dbo].[MasterRelationship] ([RelationshipID], [DeletedBy], [IsActive], [IsDeleted], [RelationshipName], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID], [RelationshipCode], [DisplayOrder]) VALUES (22, NULL, 1, 0, N'Aunt', NULL, NULL, NULL, NULL, NULL, 118, NULL, 6)
INSERT [dbo].[MasterRelationship] ([RelationshipID], [DeletedBy], [IsActive], [IsDeleted], [RelationshipName], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID], [RelationshipCode], [DisplayOrder]) VALUES (23, NULL, 1, 0, N'Cousin', NULL, NULL, NULL, NULL, NULL, 118, NULL, 7)
INSERT [dbo].[MasterRelationship] ([RelationshipID], [DeletedBy], [IsActive], [IsDeleted], [RelationshipName], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID], [RelationshipCode], [DisplayOrder]) VALUES (24, NULL, 1, 0, N'Spouse', NULL, NULL, NULL, NULL, NULL, 118, NULL, 8)
INSERT [dbo].[MasterRelationship] ([RelationshipID], [DeletedBy], [IsActive], [IsDeleted], [RelationshipName], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID], [RelationshipCode], [DisplayOrder]) VALUES (25, NULL, 1, 0, N'Other', NULL, NULL, NULL, NULL, NULL, 118, NULL, 9)
SET IDENTITY_INSERT [dbo].[MasterRelationship] OFF
SET IDENTITY_INSERT [dbo].[MasterRouteOfAdministration] ON 

INSERT [dbo].[MasterRouteOfAdministration] ([RouteOfAdministrationID], [Definition], [DeletedBy], [Description], [FDANCI], [HL7], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (1, N'within or introduced between the layers of the skin', NULL, N'Intradermal', N'C38238', N'ID', 1, 0, 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterRouteOfAdministration] ([RouteOfAdministrationID], [Definition], [DeletedBy], [Description], [FDANCI], [HL7], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (2, N'within or into the substance of a muscle', NULL, N'Intramuscular', N'C28161', N'IM', 1, 0, 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterRouteOfAdministration] ([RouteOfAdministrationID], [Definition], [DeletedBy], [Description], [FDANCI], [HL7], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (3, N'Given by nose', NULL, N'Nasal', N'C38284', N'NS', 1, 0, 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterRouteOfAdministration] ([RouteOfAdministrationID], [Definition], [DeletedBy], [Description], [FDANCI], [HL7], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (4, N'{Do not use this older code}', NULL, N'Intranasal', NULL, N'IN', 1, 0, 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterRouteOfAdministration] ([RouteOfAdministrationID], [Definition], [DeletedBy], [Description], [FDANCI], [HL7], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (5, N'administered into a vein', NULL, N'Intravenous', N'C38276', N'IV', 1, 0, 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterRouteOfAdministration] ([RouteOfAdministrationID], [Definition], [DeletedBy], [Description], [FDANCI], [HL7], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (6, N'administered by mouth', NULL, N'Oral', N'C38288', N'PO', 1, 0, 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterRouteOfAdministration] ([RouteOfAdministrationID], [Definition], [DeletedBy], [Description], [FDANCI], [HL7], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (7, NULL, NULL, N'Other/Miscellaneous', N' ', N'OTH', 1, 0, 1, CAST(0x07A06E0C3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterRouteOfAdministration] ([RouteOfAdministrationID], [Definition], [DeletedBy], [Description], [FDANCI], [HL7], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (8, N'within or introduced between the layers of the skin', NULL, N'Intradermal', N'C38238', N'ID', 1, 0, NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterRouteOfAdministration] ([RouteOfAdministrationID], [Definition], [DeletedBy], [Description], [FDANCI], [HL7], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (9, N'within or into the substance of a muscle', NULL, N'Intramuscular', N'C28161', N'IM', 1, 0, NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterRouteOfAdministration] ([RouteOfAdministrationID], [Definition], [DeletedBy], [Description], [FDANCI], [HL7], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (10, N'Given by nose', NULL, N'Nasal', N'C38284', N'NS', 1, 0, NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterRouteOfAdministration] ([RouteOfAdministrationID], [Definition], [DeletedBy], [Description], [FDANCI], [HL7], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (11, N'{Do not use this older code}', NULL, N'Intranasal', NULL, N'IN', 1, 0, NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterRouteOfAdministration] ([RouteOfAdministrationID], [Definition], [DeletedBy], [Description], [FDANCI], [HL7], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (12, N'administered into a vein', NULL, N'Intravenous', N'C38276', N'IV', 1, 0, NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterRouteOfAdministration] ([RouteOfAdministrationID], [Definition], [DeletedBy], [Description], [FDANCI], [HL7], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (13, N'administered by mouth', NULL, N'Oral', N'C38288', N'PO', 1, 0, NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterRouteOfAdministration] ([RouteOfAdministrationID], [Definition], [DeletedBy], [Description], [FDANCI], [HL7], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (14, NULL, NULL, N'Other/Miscellaneous', N' ', N'OTH', 1, 0, NULL, NULL, NULL, NULL, NULL, 118)
SET IDENTITY_INSERT [dbo].[MasterRouteOfAdministration] OFF
SET IDENTITY_INSERT [dbo].[MasterVFCEligibility] ON 

INSERT [dbo].[MasterVFCEligibility] ([VFCID], [ConceptCode], [ConceptName], [DeletedBy], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (1, N'V01', N'Not VFC eligible', NULL, 1, 0, 1, CAST(0x0710800D3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterVFCEligibility] ([VFCID], [ConceptCode], [ConceptName], [DeletedBy], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (3, N'V02', N'VFC eligible-Medicaid/Medicaid Managed Care', NULL, 1, 0, 1, CAST(0x0710800D3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterVFCEligibility] ([VFCID], [ConceptCode], [ConceptName], [DeletedBy], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (4, N'V03', N'VFC eligible- Uninsured', NULL, 1, 0, 1, CAST(0x0710800D3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterVFCEligibility] ([VFCID], [ConceptCode], [ConceptName], [DeletedBy], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (5, N'V04', N'VFC eligible- American Indian/Alaskan Native', NULL, 1, 0, 1, CAST(0x0710800D3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterVFCEligibility] ([VFCID], [ConceptCode], [ConceptName], [DeletedBy], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (6, N'V05', N'VFC eligible-Federally Qualified Health Center Patient (under-insured)', NULL, 1, 0, 1, CAST(0x0710800D3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterVFCEligibility] ([VFCID], [ConceptCode], [ConceptName], [DeletedBy], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (7, N'V06', N'Deprecated [VFC eligible- State specific eligibility (e.g. S-CHIP plan)]', NULL, 1, 0, 1, CAST(0x0710800D3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterVFCEligibility] ([VFCID], [ConceptCode], [ConceptName], [DeletedBy], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (8, N'V07', N'Local-specific eligibility', NULL, 1, 0, 1, CAST(0x0710800D3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterVFCEligibility] ([VFCID], [ConceptCode], [ConceptName], [DeletedBy], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (9, N'V08', N'Deprecated [Not VFC eligible-underinsured]', NULL, 1, 0, 1, CAST(0x0710800D3708153D0B AS DateTime2), NULL, NULL, NULL, 1)
INSERT [dbo].[MasterVFCEligibility] ([VFCID], [ConceptCode], [ConceptName], [DeletedBy], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (10, N'V01', N'Not VFC eligible', NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterVFCEligibility] ([VFCID], [ConceptCode], [ConceptName], [DeletedBy], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (11, N'V02', N'VFC eligible-Medicaid/Medicaid Managed Care', NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterVFCEligibility] ([VFCID], [ConceptCode], [ConceptName], [DeletedBy], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (12, N'V03', N'VFC eligible- Uninsured', NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterVFCEligibility] ([VFCID], [ConceptCode], [ConceptName], [DeletedBy], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (13, N'V04', N'VFC eligible- American Indian/Alaskan Native', NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterVFCEligibility] ([VFCID], [ConceptCode], [ConceptName], [DeletedBy], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (14, N'V05', N'VFC eligible-Federally Qualified Health Center Client (under-insured)', NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterVFCEligibility] ([VFCID], [ConceptCode], [ConceptName], [DeletedBy], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (15, N'V06', N'Deprecated [VFC eligible- State specific eligibility (e.g. S-CHIP plan)]', NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterVFCEligibility] ([VFCID], [ConceptCode], [ConceptName], [DeletedBy], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (16, N'V07', N'Local-specific eligibility', NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[MasterVFCEligibility] ([VFCID], [ConceptCode], [ConceptName], [DeletedBy], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [DeletedDate], [UpdatedBy], [UpdatedDate], [OrganizationID]) VALUES (17, N'V08', N'Deprecated [Not VFC eligible-underinsured]', NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, 118)
SET IDENTITY_INSERT [dbo].[MasterVFCEligibility] OFF
ALTER TABLE [dbo].[MasterAdministrationSite] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[MasterAdministrationSite] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[MasterAdministrationSite] ADD  DEFAULT (getutcdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[MasterAdministrationSite] ADD  DEFAULT ((0)) FOR [OrganizationID]
GO
ALTER TABLE [dbo].[MasterAllergies] ADD  DEFAULT ((0)) FOR [OrganizationID]
GO
ALTER TABLE [dbo].[MasterEthnicity] ADD  DEFAULT ((0)) FOR [OrganizationID]
GO
ALTER TABLE [dbo].[MasterImmunityStatus] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[MasterImmunityStatus] ADD  DEFAULT (getutcdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[MasterImmunityStatus] ADD  DEFAULT ((0)) FOR [OrganizationID]
GO
ALTER TABLE [dbo].[MasterImmunization] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[MasterImmunization] ADD  DEFAULT (getutcdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[MasterImmunization] ADD  DEFAULT ((0)) FOR [OrganizationID]
GO
ALTER TABLE [dbo].[MasterLonic] ADD  DEFAULT (getutcdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[MasterLonic] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[MasterLonic] ADD  DEFAULT ((0)) FOR [OrganizationID]
GO
ALTER TABLE [dbo].[MasterManufacture] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[MasterManufacture] ADD  DEFAULT (getutcdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[MasterManufacture] ADD  DEFAULT ((0)) FOR [OrganizationID]
GO
ALTER TABLE [dbo].[MasterNoteType] ADD  DEFAULT ((0)) FOR [IsDirectService]
GO
ALTER TABLE [dbo].[MasterPatientLocation] ADD  DEFAULT (getutcdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[MasterPatientLocation] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[MasterPatientLocation] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[MasterPatientLocation] ADD  DEFAULT ((0)) FOR [OrganizationID]
GO
ALTER TABLE [dbo].[MasterRace] ADD  CONSTRAINT [MasterRaceIsActiveTrue]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[MasterRace] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[MasterRace] ADD  DEFAULT (getutcdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[MasterRace] ADD  DEFAULT ((0)) FOR [OrganizationID]
GO
ALTER TABLE [dbo].[MasterRejectionReason] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[MasterRejectionReason] ADD  DEFAULT ((0)) FOR [OrganizationID]
GO
ALTER TABLE [dbo].[MasterRelationship] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[MasterRelationship] ADD  DEFAULT ((0)) FOR [OrganizationID]
GO
ALTER TABLE [dbo].[MasterRouteOfAdministration] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[MasterRouteOfAdministration] ADD  DEFAULT (getutcdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[MasterRouteOfAdministration] ADD  DEFAULT ((0)) FOR [OrganizationID]
GO
ALTER TABLE [dbo].[MasterVFCEligibility] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[MasterVFCEligibility] ADD  DEFAULT (getutcdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[MasterVFCEligibility] ADD  DEFAULT ((0)) FOR [OrganizationID]
GO
ALTER TABLE [dbo].[MasterAdministrationSite]  WITH CHECK ADD  CONSTRAINT [FK_MasterAdministrationSite_Organization_OrganizationID] FOREIGN KEY([OrganizationID])
REFERENCES [dbo].[Organization] ([OrganizationID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MasterAdministrationSite] CHECK CONSTRAINT [FK_MasterAdministrationSite_Organization_OrganizationID]
GO
ALTER TABLE [dbo].[MasterAdministrationSite]  WITH CHECK ADD  CONSTRAINT [FK_MasterAdministrationSite_User_CreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[MasterAdministrationSite] CHECK CONSTRAINT [FK_MasterAdministrationSite_User_CreatedBy]
GO
ALTER TABLE [dbo].[MasterAdministrationSite]  WITH CHECK ADD  CONSTRAINT [FK_MasterAdministrationSite_User_DeletedBy] FOREIGN KEY([DeletedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[MasterAdministrationSite] CHECK CONSTRAINT [FK_MasterAdministrationSite_User_DeletedBy]
GO
ALTER TABLE [dbo].[MasterAdministrationSite]  WITH CHECK ADD  CONSTRAINT [FK_MasterAdministrationSite_User_UpdatedBy] FOREIGN KEY([UpdatedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[MasterAdministrationSite] CHECK CONSTRAINT [FK_MasterAdministrationSite_User_UpdatedBy]
GO
ALTER TABLE [dbo].[MasterAllergies]  WITH CHECK ADD  CONSTRAINT [FK_MasterAllergies_Organization_OrganizationID] FOREIGN KEY([OrganizationID])
REFERENCES [dbo].[Organization] ([OrganizationID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MasterAllergies] CHECK CONSTRAINT [FK_MasterAllergies_Organization_OrganizationID]
GO
ALTER TABLE [dbo].[MasterAllergies]  WITH CHECK ADD  CONSTRAINT [FK_MasterAllergies_User_CreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[MasterAllergies] CHECK CONSTRAINT [FK_MasterAllergies_User_CreatedBy]
GO
ALTER TABLE [dbo].[MasterAllergies]  WITH CHECK ADD  CONSTRAINT [FK_MasterAllergies_User_DeletedBy] FOREIGN KEY([DeletedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[MasterAllergies] CHECK CONSTRAINT [FK_MasterAllergies_User_DeletedBy]
GO
ALTER TABLE [dbo].[MasterAllergies]  WITH CHECK ADD  CONSTRAINT [FK_MasterAllergies_User_UpdatedBy] FOREIGN KEY([UpdatedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[MasterAllergies] CHECK CONSTRAINT [FK_MasterAllergies_User_UpdatedBy]
GO
ALTER TABLE [dbo].[MasterEthnicity]  WITH CHECK ADD  CONSTRAINT [FK_MasterEthnicity_Organization_OrganizationID] FOREIGN KEY([OrganizationID])
REFERENCES [dbo].[Organization] ([OrganizationID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MasterEthnicity] CHECK CONSTRAINT [FK_MasterEthnicity_Organization_OrganizationID]
GO
ALTER TABLE [dbo].[MasterEthnicity]  WITH CHECK ADD  CONSTRAINT [FK_MasterEthnicity_User_CreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[MasterEthnicity] CHECK CONSTRAINT [FK_MasterEthnicity_User_CreatedBy]
GO
ALTER TABLE [dbo].[MasterEthnicity]  WITH CHECK ADD  CONSTRAINT [FK_MasterEthnicity_User_DeletedBy] FOREIGN KEY([DeletedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[MasterEthnicity] CHECK CONSTRAINT [FK_MasterEthnicity_User_DeletedBy]
GO
ALTER TABLE [dbo].[MasterEthnicity]  WITH CHECK ADD  CONSTRAINT [FK_MasterEthnicity_User_UpdatedBy] FOREIGN KEY([UpdatedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[MasterEthnicity] CHECK CONSTRAINT [FK_MasterEthnicity_User_UpdatedBy]
GO
ALTER TABLE [dbo].[MasterImmunityStatus]  WITH CHECK ADD  CONSTRAINT [FK_MasterImmunityStatus_Organization_OrganizationID] FOREIGN KEY([OrganizationID])
REFERENCES [dbo].[Organization] ([OrganizationID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MasterImmunityStatus] CHECK CONSTRAINT [FK_MasterImmunityStatus_Organization_OrganizationID]
GO
ALTER TABLE [dbo].[MasterImmunityStatus]  WITH CHECK ADD  CONSTRAINT [FK_MasterImmunityStatus_User_CreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[MasterImmunityStatus] CHECK CONSTRAINT [FK_MasterImmunityStatus_User_CreatedBy]
GO
ALTER TABLE [dbo].[MasterImmunityStatus]  WITH CHECK ADD  CONSTRAINT [FK_MasterImmunityStatus_User_DeletedBy] FOREIGN KEY([DeletedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[MasterImmunityStatus] CHECK CONSTRAINT [FK_MasterImmunityStatus_User_DeletedBy]
GO
ALTER TABLE [dbo].[MasterImmunityStatus]  WITH CHECK ADD  CONSTRAINT [FK_MasterImmunityStatus_User_UpdatedBy] FOREIGN KEY([UpdatedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[MasterImmunityStatus] CHECK CONSTRAINT [FK_MasterImmunityStatus_User_UpdatedBy]
GO
ALTER TABLE [dbo].[MasterImmunization]  WITH CHECK ADD  CONSTRAINT [FK_MasterImmunization_Organization_OrganizationID] FOREIGN KEY([OrganizationID])
REFERENCES [dbo].[Organization] ([OrganizationID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MasterImmunization] CHECK CONSTRAINT [FK_MasterImmunization_Organization_OrganizationID]
GO
ALTER TABLE [dbo].[MasterImmunization]  WITH CHECK ADD  CONSTRAINT [FK_MasterImmunization_User_CreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[MasterImmunization] CHECK CONSTRAINT [FK_MasterImmunization_User_CreatedBy]
GO
ALTER TABLE [dbo].[MasterImmunization]  WITH CHECK ADD  CONSTRAINT [FK_MasterImmunization_User_DeletedBy] FOREIGN KEY([DeletedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[MasterImmunization] CHECK CONSTRAINT [FK_MasterImmunization_User_DeletedBy]
GO
ALTER TABLE [dbo].[MasterImmunization]  WITH CHECK ADD  CONSTRAINT [FK_MasterImmunization_User_UpdatedBy] FOREIGN KEY([UpdatedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[MasterImmunization] CHECK CONSTRAINT [FK_MasterImmunization_User_UpdatedBy]
GO
ALTER TABLE [dbo].[MasterLonic]  WITH CHECK ADD  CONSTRAINT [FK_MasterLonic_Organization_OrganizationID] FOREIGN KEY([OrganizationID])
REFERENCES [dbo].[Organization] ([OrganizationID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MasterLonic] CHECK CONSTRAINT [FK_MasterLonic_Organization_OrganizationID]
GO
ALTER TABLE [dbo].[MasterLonic]  WITH CHECK ADD  CONSTRAINT [FK_MasterLonic_User_CreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[MasterLonic] CHECK CONSTRAINT [FK_MasterLonic_User_CreatedBy]
GO
ALTER TABLE [dbo].[MasterLonic]  WITH CHECK ADD  CONSTRAINT [FK_MasterLonic_User_DeletedBy] FOREIGN KEY([DeletedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[MasterLonic] CHECK CONSTRAINT [FK_MasterLonic_User_DeletedBy]
GO
ALTER TABLE [dbo].[MasterLonic]  WITH CHECK ADD  CONSTRAINT [FK_MasterLonic_User_UpdatedBy] FOREIGN KEY([UpdatedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[MasterLonic] CHECK CONSTRAINT [FK_MasterLonic_User_UpdatedBy]
GO
ALTER TABLE [dbo].[MasterManufacture]  WITH CHECK ADD  CONSTRAINT [FK_MasterManufacture_Organization_OrganizationID] FOREIGN KEY([OrganizationID])
REFERENCES [dbo].[Organization] ([OrganizationID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MasterManufacture] CHECK CONSTRAINT [FK_MasterManufacture_Organization_OrganizationID]
GO
ALTER TABLE [dbo].[MasterManufacture]  WITH CHECK ADD  CONSTRAINT [FK_MasterManufacture_User_CreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[MasterManufacture] CHECK CONSTRAINT [FK_MasterManufacture_User_CreatedBy]
GO
ALTER TABLE [dbo].[MasterManufacture]  WITH CHECK ADD  CONSTRAINT [FK_MasterManufacture_User_DeletedBy] FOREIGN KEY([DeletedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[MasterManufacture] CHECK CONSTRAINT [FK_MasterManufacture_User_DeletedBy]
GO
ALTER TABLE [dbo].[MasterManufacture]  WITH CHECK ADD  CONSTRAINT [FK_MasterManufacture_User_UpdatedBy] FOREIGN KEY([UpdatedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[MasterManufacture] CHECK CONSTRAINT [FK_MasterManufacture_User_UpdatedBy]
GO
ALTER TABLE [dbo].[MasterNoteType]  WITH CHECK ADD  CONSTRAINT [FK_MasterNoteType_Organization_OrganizationID] FOREIGN KEY([OrganizationID])
REFERENCES [dbo].[Organization] ([OrganizationID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MasterNoteType] CHECK CONSTRAINT [FK_MasterNoteType_Organization_OrganizationID]
GO
ALTER TABLE [dbo].[MasterNoteType]  WITH CHECK ADD  CONSTRAINT [FK_MasterNoteType_User_CreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[MasterNoteType] CHECK CONSTRAINT [FK_MasterNoteType_User_CreatedBy]
GO
ALTER TABLE [dbo].[MasterNoteType]  WITH CHECK ADD  CONSTRAINT [FK_MasterNoteType_User_DeletedBy] FOREIGN KEY([DeletedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[MasterNoteType] CHECK CONSTRAINT [FK_MasterNoteType_User_DeletedBy]
GO
ALTER TABLE [dbo].[MasterNoteType]  WITH CHECK ADD  CONSTRAINT [FK_MasterNoteType_User_UpdatedBy] FOREIGN KEY([UpdatedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[MasterNoteType] CHECK CONSTRAINT [FK_MasterNoteType_User_UpdatedBy]
GO
ALTER TABLE [dbo].[MasterPatientLocation]  WITH CHECK ADD  CONSTRAINT [FK_MasterPatientLocation_Organization_OrganizationID] FOREIGN KEY([OrganizationID])
REFERENCES [dbo].[Organization] ([OrganizationID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MasterPatientLocation] CHECK CONSTRAINT [FK_MasterPatientLocation_Organization_OrganizationID]
GO
ALTER TABLE [dbo].[MasterPatientLocation]  WITH CHECK ADD  CONSTRAINT [FK_MasterPatientLocation_User_CreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[MasterPatientLocation] CHECK CONSTRAINT [FK_MasterPatientLocation_User_CreatedBy]
GO
ALTER TABLE [dbo].[MasterPatientLocation]  WITH CHECK ADD  CONSTRAINT [FK_MasterPatientLocation_User_DeletedBy] FOREIGN KEY([DeletedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[MasterPatientLocation] CHECK CONSTRAINT [FK_MasterPatientLocation_User_DeletedBy]
GO
ALTER TABLE [dbo].[MasterPatientLocation]  WITH CHECK ADD  CONSTRAINT [FK_MasterPatientLocation_User_UpdatedBy] FOREIGN KEY([UpdatedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[MasterPatientLocation] CHECK CONSTRAINT [FK_MasterPatientLocation_User_UpdatedBy]
GO
ALTER TABLE [dbo].[MasterRace]  WITH CHECK ADD  CONSTRAINT [FK_MasterRace_Organization_OrganizationID] FOREIGN KEY([OrganizationID])
REFERENCES [dbo].[Organization] ([OrganizationID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MasterRace] CHECK CONSTRAINT [FK_MasterRace_Organization_OrganizationID]
GO
ALTER TABLE [dbo].[MasterRace]  WITH CHECK ADD  CONSTRAINT [FK_MasterRace_User_CreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[MasterRace] CHECK CONSTRAINT [FK_MasterRace_User_CreatedBy]
GO
ALTER TABLE [dbo].[MasterRace]  WITH CHECK ADD  CONSTRAINT [FK_MasterRace_User_DeletedBy] FOREIGN KEY([DeletedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[MasterRace] CHECK CONSTRAINT [FK_MasterRace_User_DeletedBy]
GO
ALTER TABLE [dbo].[MasterRace]  WITH CHECK ADD  CONSTRAINT [FK_MasterRace_User_UpdatedBy] FOREIGN KEY([UpdatedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[MasterRace] CHECK CONSTRAINT [FK_MasterRace_User_UpdatedBy]
GO
ALTER TABLE [dbo].[MasterRejectionReason]  WITH CHECK ADD  CONSTRAINT [FK_MasterRejectionReason_Organization_OrganizationID] FOREIGN KEY([OrganizationID])
REFERENCES [dbo].[Organization] ([OrganizationID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MasterRejectionReason] CHECK CONSTRAINT [FK_MasterRejectionReason_Organization_OrganizationID]
GO
ALTER TABLE [dbo].[MasterRejectionReason]  WITH CHECK ADD  CONSTRAINT [FK_MasterRejectionReason_User_CreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[MasterRejectionReason] CHECK CONSTRAINT [FK_MasterRejectionReason_User_CreatedBy]
GO
ALTER TABLE [dbo].[MasterRejectionReason]  WITH CHECK ADD  CONSTRAINT [FK_MasterRejectionReason_User_DeletedBy] FOREIGN KEY([DeletedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[MasterRejectionReason] CHECK CONSTRAINT [FK_MasterRejectionReason_User_DeletedBy]
GO
ALTER TABLE [dbo].[MasterRejectionReason]  WITH CHECK ADD  CONSTRAINT [FK_MasterRejectionReason_User_UpdatedBy] FOREIGN KEY([UpdatedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[MasterRejectionReason] CHECK CONSTRAINT [FK_MasterRejectionReason_User_UpdatedBy]
GO
ALTER TABLE [dbo].[MasterRelationship]  WITH CHECK ADD  CONSTRAINT [FK_MasterRelationship_MasterRelationship] FOREIGN KEY([RelationshipID])
REFERENCES [dbo].[MasterRelationship] ([RelationshipID])
GO
ALTER TABLE [dbo].[MasterRelationship] CHECK CONSTRAINT [FK_MasterRelationship_MasterRelationship]
GO
ALTER TABLE [dbo].[MasterRelationship]  WITH CHECK ADD  CONSTRAINT [FK_MasterRelationship_Organization_OrganizationID] FOREIGN KEY([OrganizationID])
REFERENCES [dbo].[Organization] ([OrganizationID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MasterRelationship] CHECK CONSTRAINT [FK_MasterRelationship_Organization_OrganizationID]
GO
ALTER TABLE [dbo].[MasterRelationship]  WITH CHECK ADD  CONSTRAINT [FK_MasterRelationship_User_CreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[MasterRelationship] CHECK CONSTRAINT [FK_MasterRelationship_User_CreatedBy]
GO
ALTER TABLE [dbo].[MasterRelationship]  WITH CHECK ADD  CONSTRAINT [FK_MasterRelationship_User_DeletedBy] FOREIGN KEY([DeletedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[MasterRelationship] CHECK CONSTRAINT [FK_MasterRelationship_User_DeletedBy]
GO
ALTER TABLE [dbo].[MasterRelationship]  WITH CHECK ADD  CONSTRAINT [FK_MasterRelationShip_User_UpdatedBy] FOREIGN KEY([UpdatedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[MasterRelationship] CHECK CONSTRAINT [FK_MasterRelationShip_User_UpdatedBy]
GO
ALTER TABLE [dbo].[MasterRouteOfAdministration]  WITH CHECK ADD  CONSTRAINT [FK_MasterRouteOfAdministration_Organization_OrganizationID] FOREIGN KEY([OrganizationID])
REFERENCES [dbo].[Organization] ([OrganizationID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MasterRouteOfAdministration] CHECK CONSTRAINT [FK_MasterRouteOfAdministration_Organization_OrganizationID]
GO
ALTER TABLE [dbo].[MasterRouteOfAdministration]  WITH CHECK ADD  CONSTRAINT [FK_MasterRouteOfAdministration_User_CreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[MasterRouteOfAdministration] CHECK CONSTRAINT [FK_MasterRouteOfAdministration_User_CreatedBy]
GO
ALTER TABLE [dbo].[MasterRouteOfAdministration]  WITH CHECK ADD  CONSTRAINT [FK_MasterRouteOfAdministration_User_DeletedBy] FOREIGN KEY([DeletedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[MasterRouteOfAdministration] CHECK CONSTRAINT [FK_MasterRouteOfAdministration_User_DeletedBy]
GO
ALTER TABLE [dbo].[MasterRouteOfAdministration]  WITH CHECK ADD  CONSTRAINT [FK_MasterRouteOfAdministration_User_UpdatedBy] FOREIGN KEY([UpdatedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[MasterRouteOfAdministration] CHECK CONSTRAINT [FK_MasterRouteOfAdministration_User_UpdatedBy]
GO
ALTER TABLE [dbo].[MasterVFCEligibility]  WITH CHECK ADD  CONSTRAINT [FK_MasterVFCEligibility_Organization_OrganizationID] FOREIGN KEY([OrganizationID])
REFERENCES [dbo].[Organization] ([OrganizationID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MasterVFCEligibility] CHECK CONSTRAINT [FK_MasterVFCEligibility_Organization_OrganizationID]
GO
ALTER TABLE [dbo].[MasterVFCEligibility]  WITH CHECK ADD  CONSTRAINT [FK_MasterVFCEligibility_User_CreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[MasterVFCEligibility] CHECK CONSTRAINT [FK_MasterVFCEligibility_User_CreatedBy]
GO
ALTER TABLE [dbo].[MasterVFCEligibility]  WITH CHECK ADD  CONSTRAINT [FK_MasterVFCEligibility_User_DeletedBy] FOREIGN KEY([DeletedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[MasterVFCEligibility] CHECK CONSTRAINT [FK_MasterVFCEligibility_User_DeletedBy]
GO
ALTER TABLE [dbo].[MasterVFCEligibility]  WITH CHECK ADD  CONSTRAINT [FK_MasterVFCEligibility_User_UpdatedBy] FOREIGN KEY([UpdatedBy])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[MasterVFCEligibility] CHECK CONSTRAINT [FK_MasterVFCEligibility_User_UpdatedBy]
GO
